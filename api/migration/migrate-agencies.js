var mongoose = require('mongoose');
var _ = require('lodash');
var fs = require('fs');

var Agency = require('../api/models/Agency');

module.exports = function(cb){

    var agencies = [];
    fs.readFile('./migration/data/agency.json',function(err,result){
        if(err){
            throw err;
        }
        else{
            agencies = JSON.parse(result).agencies;

            // update database
            _.forEach(agencies,function(p){
                var agency = new Agency(p);
                Agency.findOneAndUpdate({name : p.name},agency, {upsert:true}, function(err, data){
                    if(err){
                        console.error(err);
                    }
                    else{
                        console.log('agency saved successfully !');
                        if(cb){
                            cb();
                        }
                    }
                });
            });
        }
    });
}
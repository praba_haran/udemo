process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var mongodb = require('../api/lib/mongodb');
mongodb.connect(function(){

    require('./migrate-agencies')(function () {
        require('./migrate-users')();
    });

});

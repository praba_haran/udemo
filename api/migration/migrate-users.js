var mongoose = require('mongoose');
var _ = require('lodash');
var fs = require('fs');

var User = require('../api/models/User');
var Agency = require('../api/models/Agency');

module.exports = function () {

    var users = [];
    fs.readFile('./migration/data/users.json', function (err, result) {
        if (err) {
            throw err;
        }
        else {
            users = JSON.parse(result).users;
            // update database
            _.forEach(users, function (p) {
                Agency.find({name: p.agency}, function (err, result) {
                    if (err) {
                        console.log(p.agency + ' not found!');
                    }
                    else {
                        if (result.length > 0) {
                            p.agency = result[0]._id;
                            var user = new User(p);

                            User.findOneAndUpdate({email : p.email},user, {upsert:true}, function(err, data){
                                if(err){
                                    console.log('Error while saving ' + p.forename);
                                }
                                else{
                                    console.log(p.forename + ' saved successfully!');
                                }
                            });
                        }
                    }
                });
            });
        }
    });
}
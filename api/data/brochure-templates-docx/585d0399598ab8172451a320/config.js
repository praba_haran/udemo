var replaceall = require("replaceall");

module.exports = {
    'goran-gates-0001':{
        images :[
            'image3.jpg',
            'image1.jpg',
            'image2.jpg'
        ]
    },
    'goran-gates-0004':{
        loopTags:[
            {
                name:'loop_through_features',
                value:'[[loop_through_features]]'
            }
        ],
        images :[
            'image4.jpg',
            'image2.jpg',
            'image3.jpg'
        ]
    }
};

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.TZ = 'Asia/Kolkata';
var moment = require('moment');
var momentTimeZone = require('moment-timezone');
momentTimeZone.tz.setDefault(process.env.TZ);

var express = require('express');
var cors = require('cors');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var customResponses = require('./api/responses');
var mongodb = require('./api/lib/mongodb');
// redis server connection init
//var redisClient = require('./api/redisClient');
//redisClient.connect();
mongodb.connect();
var config = require('./config');
var routes = require('./api/routes');

var app = express();

//app.use(logger('dev'));
app.set('superSecret', config.secret);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());

// applying cors
app.use(cors());
app.options('*', cors());

// initializing custom responses
customResponses.init();
app.use('/api',routes);
app.use('/email-templates',function(req,res){

    Handlebars.registerHelper('capitalize', function capitalize (context) {
        return context.toUpperCase()
    });

    var templateDir = path.join(__dirname,'email-templates', 'templates', 'password-reset');
    var newsletter = new EmailTemplate(templateDir);
    var locals = {
        email: 'aruljothi@56060.in',
        user: {
            firstName: 'Aruljothi'
        }
    };
    newsletter.render(locals,function(err,result){
        if(err){
            console.log('ERROR',err);
        }
        else{
            var transport = nodeMailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'pk.aruljothi@gmail.com',
                    pass: 'jOOthi1988'
                }
            });

            transport.sendMail({
                from: 'Ultima plus <pk.aruljothi@gmail.com>',
                to: locals.email,
                subject: 'Ultima plus test email',
                html: result.html,
                text: result.text
            }, function (err, responseStatus) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.send('Email sent!');
                }
            });
        }
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: err
    });
});


module.exports = app;

process.on('uncaughtException', function (exception) {
    console.log(exception); // to see your exception details in the console
    // if you are on production, maybe you can send the exception details to your
    // email as well ?
});


//Reference Links
// 1. http://stackoverflow.com/questions/10226301/building-contextify-under-windows-7-x64-for-nodejs-jquery

// need to delete jsdom and contextify later
//nodemon --watch api --watch app ./bin/www


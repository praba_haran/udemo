var gulp = require('gulp');
var brochures = require('./gulp/brochures');
var emailTemplates = require('./gulp/email-templates');

// Look for the --production flag
gulp.task('email-templates',emailTemplates.defaultScript);
gulp.task('build-email-templates',emailTemplates.buildScript);

gulp.task('brochures',brochures.defaultScript);
gulp.task('build-brochures',brochures.buildScript);

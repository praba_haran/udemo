import gulp     from 'gulp';
import rename   from 'gulp-rename';
import plugins  from 'gulp-load-plugins';
import browser  from 'browser-sync';
import rimraf   from 'rimraf';
import panini   from 'panini';
import yargs    from 'yargs';
import lazypipe from 'lazypipe';
import inky     from 'inky';
import fs       from 'fs';
import siphon   from 'siphon-media-query';

const $ = plugins();

const PRODUCTION = !!(yargs.argv.production);

function clean(done) {
    rimraf('email-templates/dist', function () {
        rimraf('email-templates/templates', done);
    });
}

function pages() {
    return gulp.src('email-templates/src/pages/**/*.html')
        .pipe(panini({
            root: 'email-templates/src/pages',
            layouts: 'email-templates/src/layouts',
            partials: 'email-templates/src/partials',
            helpers: 'email-templates/src/helpers'
        }))
        .pipe(inky())
        .pipe(gulp.dest('email-templates/dist'));
}

function sass() {
    return gulp.src('email-templates/src/assets/scss/app.scss')
        .pipe($.if(!PRODUCTION, $.sourcemaps.init()))
        .pipe($.sass({
            includePaths: ['node_modules/foundation-emails/scss']
        }).on('error', $.sass.logError))
        .pipe($.if(PRODUCTION, $.uncss(
            {
                html: ['email-templates/dist/**/*.html']
            })))
        .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
        .pipe(gulp.dest('email-templates/dist/css'));
}

function images() {
    return gulp.src('email-templates/src/assets/img/**/*')
        .pipe($.imagemin())
        .pipe(gulp.dest('./email-templates/dist/assets/img'));
}

function inLiner(css) {
    css = fs.readFileSync(css).toString();
    var mqCss = siphon(css);

    var pipe = lazypipe()
        .pipe($.inlineCss, {
            applyStyleTags: false,
            removeStyleTags: true,
            preserveMediaQueries: true,
            removeLinkTags: false
        })
        .pipe($.replace, '<!-- <style> -->', `<style>${mqCss}</style>`)
        .pipe($.replace, '<link rel="stylesheet" type="text/css" href="css/app.css">', '')
        .pipe($.htmlmin, {
            collapseWhitespace: true,
            minifyCSS: true
        });

    return pipe();
}

function inline() {
    return gulp.src('email-templates/dist/**/*.html')
        .pipe($.if(PRODUCTION, inLiner('email-templates/dist/css/app.css')))
        .pipe(gulp.dest('email-templates/dist'));
}

function templates() {
    return gulp.src('email-templates/dist/*.html')
        .pipe(rename(function (path) {
            path.dirname += ("/" + path.basename);
            path.basename = "html";
            path.extname = ".hbs"
        }))
        .pipe(gulp.dest('email-templates/templates'));
}

function resetPages(done) {
    panini.refresh();
    done();
}

function server(done) {
    browser.init({
        server: 'email-templates/dist'
    });
    done();
}

function watch() {
    gulp.watch('email-templates/src/pages/**/*.html').on('all', gulp.series(pages, inline, templates,browser.reload));
    gulp.watch(['email-templates/src/layouts/**/*', 'email-templates/src/partials/**/*']).on('all', gulp.series(resetPages, pages, inline, browser.reload));
    gulp.watch(['../src/scss/**/*.scss', 'email-templates/src/assets/scss/**/*.scss']).on('all', gulp.series(resetPages, sass, pages, inline, browser.reload));
    gulp.watch('email-templates/src/assets/img/**/*').on('all', gulp.series(images, browser.reload));
}

module.exports.buildScript = gulp.series(clean, pages, sass, images, inline, templates);
module.exports.defaultScript = gulp.series(server, watch);
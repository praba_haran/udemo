/**
 * Middleware Order
 *
 * List down the middleware execution order here
 *
 */

module.exports = [
    'authorize'
];
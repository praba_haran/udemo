var _ = require('lodash');
var async = require('async');
var Property = require('../models/Property');
var TermsSigned = require('../models/TermsSigned');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function addTermsSigned(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.isTermsSigned = true;

            var request = new TermsSigned(model);
            request.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function sendMail(terms,cb){
            // need to write code

            cb(null,terms);
        },
        function update(terms,cb){
            // UPDATING CONTRACT IN PROPERTY
            var termsSigned = {
                notes : terms.notes,
                isTermsSigned : terms.isTermsSigned,
                termsSignedId : terms._id
            };
            Property.findOneAndUpdate({_id: model.property }, { termsSigned : termsSigned }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,terms);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Terms signed successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = TermsSigned.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(params,user,successCallback,failureCallback){
    if(!params){
        throw new Error('The argument params is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }

    var query = TermsSigned.find(options);
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
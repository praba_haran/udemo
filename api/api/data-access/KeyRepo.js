var _ = require('lodash');
var async = require('async');
var CabinetMaster = require('../models/CabinetMaster');
var Key = require('../models/Key');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function checkForDuplicate(cb){
            var query = Key.find({
                'agency': user.agency,
                'property' : model.property,
                'cabinetMaster':model.cabinetMaster,
                'keyNo':model.keyNo,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Key number already exist.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function save(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var key = new Key(model);
            key.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Key created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function checkForDuplicate(cb){
            var query = Key.find({
                'agency': user.agency,
                'property' : model.property,
                'cabinetMaster':model.cabinetMaster,
                'keyNo':model.keyNo,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Key number already exist.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function save(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Key.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Key updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    Key.update({
        _id : id,
        agency: user.agency,
        recordStatus : 'Active'
    },{
        $set :{
            recordStatus:'Deleted'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Key.find({
        agency: user.agency,
        _id : id,
        recordStatus : 'Active'
    });
    query.populate('property','status address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    },function(err){
        failureCallback(err);
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Key.find({
        agency: user.agency,
        recordStatus : 'Active'
    });
    query.populate('property','status address.fullAddress');
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');

    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    },function(err){
        failureCallback(err);
    });
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency,
        recordStatus :'Active'
    };

    var query = Key.find(options);
    query.populate('property','status address.fullAddress');
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');

    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    },function(err){
        failureCallback(err);
    });
};
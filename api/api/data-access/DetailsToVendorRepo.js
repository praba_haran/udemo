var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var helper = require('../lib/helper');
var mailer = require('../lib/mailer');
var Property = require('../models/Property');
var DetailsToVendor = require('../models/DetailsToVendor');
var BranchActivityRepo = require('./BranchActivityRepo');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function saveDetails(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();

            var request = new DetailsToVendor(model);
            request.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function addToBranchActivity(detailsToVendor,cb){
            // here we are adding this activity for 2 times
            // one is for Details Emailed and another one is for Details Posted
            var activity = {
                modelType :'DetailsToVendor',
                modelId : detailsToVendor._id,
                activityType : 'Details Emailed'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null,detailsToVendor);
            },function(err){
                cb(err,null);
            });
        },
        function addToBranchActivity(detailsToVendor,cb){
            var activity = {
                modelType :'DetailsToVendor',
                modelId : detailsToVendor._id,
                activityType : 'Details Posted'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null,detailsToVendor);
            },function(err){
                cb(err,null);
            });
        },
        function getDetails(details,cb){
            var query = DetailsToVendor.findOne({
                agency: user.agency,
                _id : details._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate('recipients.contact','firstPerson address.fullAddress');
            query.populate('property','status recordStatus price proposedPrice address.fullAddress');
            query.sort({createdAt: -1});
            query.lean().exec().then(function (data) {
                cb(null,data);
            }, function(err){
                cb(err,null);
            });
        },
        function sendMail(details,cb){
            function getSendMailHandler(contact){
                return function(innerCb){
                    var locals = {
                        contact : contact,
                        details : details,
                        createdAt : helper.formatDateTime(details.createdAt)
                    };
                    var options = {
                        template : 'details-to-vendor',
                        subject : 'Particulars for Approval',
                        from : '"Goran Gates" <'+details.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : details.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }
            var handlers = [];
            _.each(details.recipients, function (applicant) {
                handlers.push(getSendMailHandler(applicant.contact));
            });
            async.parallel(handlers, function (err, data) {
                if (err) {
                    cb(err, null);
                }
                else {
                    cb(null, details);
                }
            });
        },
        function update(details,cb){
            // UPDATING CONTRACT IN PROPERTY
            var detailsToVendor = {
                notes : details.notes,
                isApproved : details.isApproved,
                approvedDate : details.approvedDate,
                detailsToVendorId : details._id
            };
            Property.findOneAndUpdate({_id: model.property }, { detailsToVendor : detailsToVendor }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,details);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Details sent to vendors successfully.');
        }
    });
};

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function saveDetails(cb){
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            DetailsToVendor.findOneAndUpdate({_id: model._id }, model , {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function update(details,cb){
            // UPDATING CONTRACT IN PROPERTY
            var detailsToVendor = {
                notes : model.notes,
                isApproved : model.isApproved,
                approvedDate : model.approvedDate,
                detailsToVendorId : model._id
            };
            Property.findOneAndUpdate({_id: model.property }, { detailsToVendor : detailsToVendor }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,details);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Details updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = DetailsToVendor.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('recipients.contact','firstPerson address.fullAddress');
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(params,user,successCallback,failureCallback){
    if(!params){
        throw new Error('The argument params is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }

    var query = DetailsToVendor.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('recipients.contact','firstPerson address.fullAddress');
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
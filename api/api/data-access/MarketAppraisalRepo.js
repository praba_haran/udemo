var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var MarketAppraisal = require('../models/MarketAppraisal');
var mailer = require('../lib/mailer');
var helper = require('../lib/helper');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function saveMarketAppraisal(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var appraisal = new MarketAppraisal(model);
            appraisal.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function getMarketAppraisal(appraisal,cb){
            var query = MarketAppraisal.find({
                agency: user.agency,
                _id : appraisal._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'firstPerson address.fullAddress',
                    model:'Contact'
                }
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
                else{
                    cb('Appraisal not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function sendConfirmationMail(appraisal,cb){

            function getSendMailHandler(contact){
                return function(innerCb){
                    appraisal.date = helper.formatDate(appraisal.date);
                    appraisal.startTime = helper.toTime(appraisal.startTime);

                    var locals = {
                        contact : contact,
                        appraisal : appraisal,
                        property : appraisal.property
                    };
                    var options = {
                        template : 'appraisal-confirmation',
                        subject : 'Market Appraisal Confirmation',
                        from : '"Goran Gates" <'+appraisal.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : appraisal.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }

            var handlers = [];
            _.each(appraisal.property.owners,function(owner){
                handlers.push(getSendMailHandler(owner.contact));
            });
            async.parallel(handlers,function(err,data){
                if(err){
                    cb(err,null);
                }
                else{
                    cb(null,appraisal);
                }
            });
        }
    ],function(err,data){
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Market Appraisal saved successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function getOldMarketAppraisal(cb){
            // old appraisal details is necessary to check if appraisal is changed or not
            // in order to send a mail to owners
            var query = MarketAppraisal.find({
                agency: user.agency,
                _id : id
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
                else{
                    cb('Appraisal not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function updateMarketAppraisal(oldAppraisal,cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            MarketAppraisal.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,oldAppraisal,data);
                }
            });
        },
        function getMarketAppraisal(oldAppraisal,appraisal,cb){
            var query = MarketAppraisal.find({
                agency: user.agency,
                _id : appraisal._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'firstPerson address.fullAddress',
                    model:'Contact'
                }
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,oldAppraisal,data[0]);
                }
                else{
                    cb('Appraisal not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function sendConfirmationMail(oldAppraisal,appraisal,cb){
            function getSendMailHandler(contact){
                return function(innerCb){
                    appraisal.date = helper.formatDate(appraisal.date);
                    appraisal.startTime = helper.toTime(appraisal.startTime);

                    var locals = {
                        contact : contact,
                        appraisal : appraisal,
                        property : appraisal.property
                    };
                    var options = {
                        template : 'appraisal-change-confirmation',
                        subject : 'Market Appraisal Appointment Changed',
                        from : '"Goran Gates" <'+appraisal.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : appraisal.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }
            // check if start date is different then send a mail
            if(oldAppraisal && moment(oldAppraisal.date).isSame(appraisal.date)){
                cb(null,appraisal);
            }
            else {
                var handlers = [];
                _.each(appraisal.property.owners, function (owner) {
                    handlers.push(getSendMailHandler(owner.contact));
                });
                async.parallel(handlers, function (err, data) {
                    if (err) {
                        cb(err, null);
                    }
                    else {
                        cb(null, appraisal);
                    }
                });
            }
        }
    ],function(err,data){
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Market Appraisal updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    MarketAppraisal.remove({
        _id : id,
        agency: user.agency
    }, function(err, data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.result.n);
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = MarketAppraisal.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = MarketAppraisal.find({
        agency: user.agency
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency
    };
    if(params.propertyId){
        options.property = params.propertyId;
    }
    if(params.type){
        options.type = params.type;
    }

    var query = MarketAppraisal.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
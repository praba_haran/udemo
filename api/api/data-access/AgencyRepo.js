var _ = require('lodash');
var async = require('async');
var Agency = require('../models/Agency');
var Contact = require('../models/Contact');
var User = require('../models/User');

module.exports = {

    getItems: function (successCallback, failureCallback) {

        var query = Agency.find({});
        query.lean().exec().then(function (data) {
            if (successCallback) {
                successCallback(data);
            }
        }, function (err) {
            if (failureCallback) {
                failureCallback(err);
            }
        });
    },

    getItemById: function (id, successCallback, failureCallback) {
        if (!id) {
            throw new Error('The argument id is missing.');
        }
        async.parallel({
            agencyUsers: function (cb) {
                var query = User.find({
                    agency: id
                });
                query.select('-password');
                query.exec().then(function (result) {
                    cb(null, result);
                }, function (err) {
                    cb(err, null);
                });
            },
            branchDetails: function (cb) {
                var query = Agency.find({
                    _id: id
                });
                query.exec().then(function (result) {
                    cb(null, result);
                }, function (err) {
                    cb(err, null);
                });
            },
            solicitors: function(cb){
                var query = Contact.find({
                    type: 'Solicitor',
                    agency : id
                });
                query.select('firstPerson');
                query.exec().then(function (data) {
                    cb(null, data);
                }, function (err) {
                    cb(err, null);
                });
            }
        }, function (err, data) {
            if (err) {
                failureCallback(err);
            }
            else {
                successCallback(data);
            }
        });
    },

    saveItem: function (model, successCallback, failureCallback) {
        if (!model) {
            throw new Error('The argument model is missing.');
        }

        var agency = new Agency(model);
        agency.save(function (err, data) {
            if (err) {
                failureCallback(err);
            }
            else {
                successCallback(data);
            }
        });
    }
};
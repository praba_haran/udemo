var _ = require('lodash');
var async = require('async');
var Agency = require('../models/Agency');
var Contact = require('../models/Contact');
var UserLogin = require('../models/UserLogin');

module.exports = {

    logUserLogin: function (model, successCallback, failureCallback) {
        if (!model) {
            throw new Error('The argument model is missing.');
        }

        var userLogin = new UserLogin(model);
        userLogin.save(function (err, data) {
            if (err) {
                failureCallback(err);
            }
            else {
                successCallback(data);
            }
        });
    },

    getLatestUserLoginByEmail : function(email,successCallback,failureCallback){
        if (!email) {
            throw new Error('The argument email is missing.');
        }
        var query = UserLogin.find({
            'profile.email' : email
        });
        query.sort({createdAt: -1}).limit(1)
        query.select('profile');
        query.exec().then(successCallback, failureCallback);
    }
};
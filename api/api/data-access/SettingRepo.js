var _ = require('lodash');
var async = require('async');
var uuid = require('uuid');
var User = require('../models/User');
var redisClient = require('../redisClient');
var validator = require('../validator');
var mailer  = require('../lib/mailer');
var ForgotPasswordRequest = require('../models/ForgotPasswordRequest');

function getInviteUserHandler(loggedInUser,model){

    return function(callback) {
        async.waterfall([
            function saveNewPasswordRequest(cb){
                console.log('generate token');
                var passwordReq = {};
                passwordReq.createdAt = new Date();
                passwordReq.token = uuid.v4();
                passwordReq.expireAt = new Date();
                passwordReq.isUsed = false;
                passwordReq.expireAt.setMinutes(passwordReq.expireAt.getMinutes()+30);
                passwordReq.email = model.email;

                var request = new ForgotPasswordRequest(passwordReq);
                request.save(function (err, data) {
                    if (err) {
                        cb(err,null);
                    }
                    else {
                        cb(null,data);
                    }
                });
            },
            function getUserByEmail(passwordReq,cb) {
                var query = User.find({
                    agency: loggedInUser.agency,
                    email: model.email
                });
                query.lean().exec().then(function (users) {
                    cb(null,passwordReq, users);
                }, function (err) {
                    cb(err, null);
                });
            },
            function invite(passwordReq,users, cb) {
                var result = {
                    email: model.email,
                    forename : model.forename,
                    surname : model.surname,
                    inviteSent :false
                };
                if (users.length>0) {
                    if (users[0].status === 'Invited') {
                        result.message = 'This person is already invited.';
                        cb(null,passwordReq,result);
                    }
                    else if (users[0].status === 'Active') {
                        result.message = 'This person is already joined in your branch.';
                        cb(null,passwordReq,result);
                    }
                }
                else {
                    model.createdAt = new Date();
                    model.createdBy = loggedInUser._id;
                    model.updatedAt = new Date();
                    model.updatedBy = loggedInUser._id;
                    model.agency = loggedInUser.agency;
                    model.status = 'Invited';
                    model.userType ='branch_user';

                    var newUser = new User(model);
                    newUser.save(function (err, data) {
                        if (err) {
                            result.message = 'Error occurred while inviting user.';
                            cb(null,passwordReq,result);
                        }
                        else {
                            result.message = 'Invite sent.';
                            result.inviteSent = true;
                            cb(null,passwordReq,result);
                        }
                    });
                }
            },
            function sendInviteMail(passwordReq,user,cb){
                if(user.inviteSent){
                    var locals = {
                        user : user,
                        invitedBy : loggedInUser,
                        request : passwordReq
                    };
                    var options = {
                        template : 'inviting-user',
                        subject : "You're invited",
                        email : user.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            cb(err,null);
                        }
                        else{
                            cb(null,user);
                        }
                    });
                }
                else{
                    cb(null,user);
                }
            }
        ],callback);
    }
}

module.exports = {

    inviteBranchUsers: function (loggedInUser,model, successCallback, failureCallback) {
        if (!loggedInUser) {
            throw new Error('The argument agencyId is missing.');
        }
        if (!model) {
            throw new Error('The argument users is missing.');
        }
        if (!successCallback) {
            throw new Error('The argument successCallback is missing.');
        }
        if (!failureCallback) {
            throw new Error('The argument failureCallback is missing.');
        }

        var state = validator.validate('INVITE_USERS',model);
        if(state.errors.length===0){
            var handlers = [];
            _.each(model.users,function(user){
               handlers.push(getInviteUserHandler(loggedInUser,user));
            });
            async.parallel(handlers,function(err,data){
                if(err){
                    failureCallback(err);
                }
                else{
                    successCallback(data);
                }
            });
        }
        else{
            failureCallback(state);
        }
    }
};
var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var GeneralAppointment = require('../models/GeneralAppointment');
var Activity = require('../models/Activity');
var mailer = require('../lib/mailer');
var helper = require('../lib/helper');
var BranchActivityRepo = require('./BranchActivityRepo');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(!p._id){
                p.createdAt = new Date();
                p.createdBy = user._id;
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    // if there is new property, update createdAt and createdBy fields
    if(model.properties.length>0){
        _.each(model.properties,function (p) {
            if(!p._id){
                p.createdAt = new Date();
                p.createdBy = user._id;
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    async.waterfall([
        function saveAppointment(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var appointment = new GeneralAppointment(model);
            appointment.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function addToBranchActivity(appointment,cb){
            var activity = {
                modelType :'GeneralAppointment',
                modelId : appointment._id,
                activityType : 'Booked'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null,appointment);
            },function(err){
                cb(err,null);
            });
        },
        function addActivity(appointment,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.appointment = appointment._id;
            activityModel.appointment_ref = {};
            activityModel.appointment_ref.type = appointment.type;
            activityModel.appointment_ref.appraiser = appointment.appraiser;
            activityModel.appointment_ref.accompaniedBy = appointment.accompaniedBy;
            activityModel.appointment_ref.date = appointment.date;
            activityModel.appointment_ref.startTime = appointment.startTime;
            activityModel.appointment_ref.endTime = appointment.endTime;
            activityModel.appointment_ref.notes = appointment.notes;
            activityModel.appointment_ref.additionalNotes = appointment.additionalNotes;
            activityModel.appointment_ref.reminder = appointment.reminder;
            activityModel.appointment_ref.reminderChannel = appointment.reminderChannel;
            activityModel.appointment_ref.status = appointment.status;
            activityModel.appointment_ref.properties = _.map(appointment.properties,function(p){
                return {
                    property : p.property
                };
            });
            activityModel.appointment_ref.applicants = _.map(appointment.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,appointment);
                }
            });
        },
        function getAppointment(appointment,cb){
            var query = GeneralAppointment.find({
                agency: user.agency,
                _id : appointment._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails');
            query.populate({
                path:'properties.property',
                select:'status recordStatus owners contract address.fullAddress',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
                else{
                    cb('Appointment not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function sendConfirmationMail(appointment,cb){

            function getSendMailHandler(contact){
                return function(innerCb){
                    appointment.startDate = helper.formatDateTime(helper.combineDateAndTime(appointment.date,appointment.startTime));
                    var locals = {
                        contact : contact,
                        appointment : appointment
                    };
                    var options = {
                        template : 'appointment-confirmation',
                        subject : 'Appointment Confirmation',
                        from : '"Goran Gates" <'+appointment.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : appointment.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }
            var handlers = [];
            _.each(appointment.applicants,function(applicant){
               handlers.push(getSendMailHandler(applicant.contact));
            });
            // adding owners
            _.each(appointment.properties,(p)=>{
                _.each(p.property.owners, (owner) => {
                    handlers.push(getSendMailHandler(owner.contact));
                })
            });
            async.parallel(handlers,function(err,data){
               if(err){
                   cb(err,null);
               }
               else{
                   cb(null,appointment);
               }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Appointment created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
            else{
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }
    // if there is new property, update createdAt and createdBy fields
    if(model.properties.length>0){
        _.each(model.properties,function (p) {
            if(p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
            else{
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }

    async.waterfall([
        function getOldAppointment(cb){
            // old appointment details is necessary to check if appointment is changed or not
            // in order to send a mail to applicants
            var query = GeneralAppointment.find({
                agency: user.agency,
                _id : model._id
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function updateAppointment(oldAppointment,cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            GeneralAppointment.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,oldAppointment,data);
                }
            });
        },
        function getAppointment(oldAppointment,appointment,cb){
            var query = GeneralAppointment.find({
                agency: user.agency,
                _id : appointment._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails');
            query.populate({
                path:'properties.property',
                select:'status recordStatus owners contract address.fullAddress',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,oldAppointment,data[0]);
                }
                else{
                    cb('Appointment not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function addActivity(oldAppointment,appointment,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.appointment = appointment._id;
            activityModel.appointment_ref = {};
            activityModel.appointment_ref.type = appointment.type;
            activityModel.appointment_ref.appraiser = appointment.appraiser;
            activityModel.appointment_ref.accompaniedBy = appointment.accompaniedBy;
            activityModel.appointment_ref.date = appointment.date;
            activityModel.appointment_ref.startTime = appointment.startTime;
            activityModel.appointment_ref.endTime = appointment.endTime;
            activityModel.appointment_ref.notes = appointment.notes;
            activityModel.appointment_ref.additionalNotes = appointment.additionalNotes;
            activityModel.appointment_ref.reminder = appointment.reminder;
            activityModel.appointment_ref.reminderChannel = appointment.reminderChannel;
            activityModel.appointment_ref.status = 'Changed';// appointment.status;
            activityModel.appointment_ref.properties = _.map(appointment.properties,function(p){
                return {
                    property : p.property
                };
            });
            activityModel.appointment_ref.applicants = _.map(appointment.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,oldAppointment,appointment);
                }
            });
        },
        function sendConfirmationMail(oldAppointment,appointment,cb){

            function getSendMailHandler(contact){
                return function(innerCb){
                    appointment.startDate = helper.formatDateTime(helper.combineDateAndTime(appointment.date,appointment.startTime));
                    var locals = {
                        contact : contact,
                        appointment : appointment
                    };
                    var options = {
                        template : 'appointment-change-confirmation',
                        subject : 'Appointment Changed',
                        from : '"Goran Gates" <'+appointment.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : appointment.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }
            // check if start date is different then send a mail
            if(oldAppointment && moment(oldAppointment.startDate).isSame(appointment.startDate)){
                cb(null,appointment);
            }
            else {
                var handlers = [];
                _.each(appointment.applicants, function (applicant) {
                    handlers.push(getSendMailHandler(applicant.contact));
                });
                // adding owners
                _.each(appointment.properties,(p)=>{
                    _.each(p.property.owners, (owner) => {
                        handlers.push(getSendMailHandler(owner.contact));
                    })
                });
                async.parallel(handlers, function (err, data) {
                    if (err) {
                        cb(err, null);
                    }
                    else {
                        cb(null, appointment);
                    }
                });
            }
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Appointment updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function cancelAppointment(cb){

            GeneralAppointment.findOneAndUpdate({
                _id: id,
                agency: user.agency,
                status : 'Booked'
            }, {
                status :'Cancelled'
            }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null);
                }
            });
        },
        function addToBranchActivity(cb){
            var activity = {
                modelType :'GeneralAppointment',
                modelId : id,
                activityType : 'Cancelled'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null);
            },function(err){
                cb(err);
            });
        },
        function getAppointment(cb){
            // appointment details is necessary to check if appointment start date is future date or not
            // if it is future date, we have to inform this cancellation to applicants
            var query = GeneralAppointment.find({
                agency: user.agency,
                _id : id
            });
            query.populate('createdBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails');
            query.populate({
                path:'properties.property',
                select:'status recordStatus owners contract address.fullAddress',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function addActivity(appointment,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.appointment = appointment._id;
            activityModel.appointment_ref = {};
            activityModel.appointment_ref.type = appointment.type;
            activityModel.appointment_ref.appraiser = appointment.appraiser;
            activityModel.appointment_ref.accompaniedBy = appointment.accompaniedBy;
            activityModel.appointment_ref.date = appointment.date;
            activityModel.appointment_ref.startTime = appointment.startTime;
            activityModel.appointment_ref.endTime = appointment.endTime;
            activityModel.appointment_ref.notes = appointment.notes;
            activityModel.appointment_ref.additionalNotes = appointment.additionalNotes;
            activityModel.appointment_ref.reminder = appointment.reminder;
            activityModel.appointment_ref.reminderChannel = appointment.reminderChannel;
            activityModel.appointment_ref.status = appointment.status;
            activityModel.appointment_ref.properties = _.map(appointment.properties,function(p){
                return {
                    property : p.property
                };
            });
            activityModel.appointment_ref.applicants = _.map(appointment.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,appointment);
                }
            });
        },
        function sendConfirmationMail(appointment,cb){

            function getSendMailHandler(contact){
                return function(innerCb){
                    appointment.startDate = helper.formatDateTime(helper.combineDateAndTime(appointment.date,appointment.startTime));
                    var locals = {
                        contact : contact,
                        appointment : appointment
                    };
                    var options = {
                        template : 'appointment-cancellation-confirmation',
                        subject : 'Appointment Cancelled',
                        from : '"Goran Gates" <'+appointment.createdBy.email+'>',
                        email : contact.firstPerson.emails[0].email,
                        replyTo : appointment.createdBy.email
                    };
                    mailer.sendEmail(options,locals,function(err,result){
                        if(err){
                            innerCb(err,null);
                        }
                        else{
                            innerCb(null,result);
                        }
                    });
                }
            }
            // check if start date is before today then send a mail
            if(moment(appointment.startDate).isBefore(new Date())){
                cb(null,appointment);
            }
            else {
                var handlers = [];
                _.each(appointment.applicants, function (applicant) {
                    handlers.push(getSendMailHandler(applicant.contact));
                });
                // adding owners
                _.each(appointment.properties,(p)=>{
                    _.each(p.property.owners, (owner) => {
                        handlers.push(getSendMailHandler(owner.contact));
                    })
                });
                async.parallel(handlers, function (err, data) {
                    if (err) {
                        cb(err, null);
                    }
                    else {
                        cb(null, appointment);
                    }
                });
            }
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Appointment deleted successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = GeneralAppointment.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.populate('properties.property','status recordStatus price proposedPrice address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = GeneralAppointment.find({
        agency: user.agency
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.populate('properties.property','status recordStatus price proposedPrice address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency
    };
    if(params.status && params.status.length>0){
        options.status = {
            $in : params.status
        };
    }

    var query = GeneralAppointment.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.populate('properties.property','status recordStatus price proposedPrice address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        var result = [];
        _.each(data,function(p){
            if(params.property) {
                var properties = _.filter(p.properties, function (item) {
                    return item.property._id.toString() === params.property;
                });
                if (properties.length > 0) {
                    result.push(p);
                }
            }
            if(params.contact) {
                var applicants = _.filter(p.applicants, function (item) {
                    return item.contact._id.toString() === params.contact;
                });
                if (applicants.length > 0) {
                    result.push(p);
                }
            }
        });
        successCallback(result);
    }, failureCallback);
};
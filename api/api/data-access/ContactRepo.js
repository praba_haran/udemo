var _ = require('lodash');
var async = require('async');
var Contact = require('../models/Contact');
var Property = require('../models/Property');
var Requirement = require('../models/Requirement');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // updating address
    if (model.address) {
        var keys = ['nameOrNumber', 'street', 'locality', 'town', 'county', 'postcode'];
        var fullAddress = '';
        _.each(keys, function (key) {
            if (model.address[key]) {
                fullAddress += model.address[key];
                fullAddress += ', ';
            }
        });
        fullAddress = fullAddress.substr(0, fullAddress.length - 2);
        model.address.fullAddress = fullAddress;
    }

    // Update createdAt and createdBy fields of interestedProperties
    if(model.interestedProperties.length>0){
        _.each(model.interestedProperties,function (p) {
            if(!p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    async.waterfall([
        function checkDuplicateAddress(cb){
            var query = Contact.find({
                'agency': user.agency,
                'address.fullAddress':model.address.fullAddress,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Contact already exist for this address.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function save(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var contact = new Contact(model);
            contact.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Contact created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // updating address
    if (model.address) {
        var keys = ['nameOrNumber', 'street', 'locality', 'town', 'county', 'postcode'];
        var fullAddress = '';
        _.each(keys, function (key) {
            if (model.address[key]) {
                fullAddress += model.address[key];
                fullAddress += ', ';
            }
        });
        fullAddress = fullAddress.substr(0, fullAddress.length - 2);
        model.address.fullAddress = fullAddress;
    }

    // Update createdAt and createdBy fields of interestedProperties
    if(model.interestedProperties.length>0){
        _.each(model.interestedProperties,function (p) {
            if(!p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    async.waterfall([
        function checkDuplicateAddress(cb){
            var query = Contact.find({
                'agency': user.agency,
                'address.fullAddress':model.address.fullAddress,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Contact already exist for this address.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function save(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Contact.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Contact updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    Contact.update({
        _id : id,
        agency: user.agency,
        recordStatus : 'Active'
    },{
        $set :{
            recordStatus:'Deleted'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.parallel({
        contacts : function(cb){
            var query = Contact.find({
                agency: user.agency,
                _id : id,
                recordStatus : 'Active'
            });
            query.populate({
                path:'interestedProperties.property',
                select:'bedrooms market price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos negotiator',
                model:'Property',
                populate:{
                    path:'negotiator',
                    select:'forename surname',
                    model:'User'
                }
            });
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                cb(null,data);
            },function(err){
                cb(err,null);
            });
        },
        properties : function (cb) {
            var query = Property.find({
                agency: user.agency,
                owners : {
                    $elemMatch:{
                        contact : id
                    }
                }
            });
            query.populate('negotiator', 'email forename surname');
            query.populate('updatedBy', 'email forename surname');
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                cb(null,data);
            }, function (err) {
                cb(err,null);
            });
        }
    },function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            if(data.contacts.length>0){
                var contact = data.contacts[0];
                contact.properties = data.properties;
                successCallback(contact);
            }
            else{
                successCallback(null);
            }
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.parallel({
        contacts: function (cb) {
            var query = Contact.find({
                agency: user.agency,
                recordStatus :'Active'
            });
            query.populate('negotiator','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.sort({ updatedAt : -1});
            query.lean().exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        },
        requirements: function (cb) {
            var query = Requirement.find({
                agency: user.agency
            });
            query.select('name contact');
            query.exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        }
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            _.each(data.contacts,function (p) {
                p.requirements = _.filter(data.requirements,function(r){
                        return r.contact.toString() === p._id.toString();
                    }) || [];
            });
            successCallback(data.contacts);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.paginateList = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    params.pageIndex = parseInt(params.pageIndex);
    params.pageSize = parseInt(params.pageSize);

    var condition = {
        agency: user.agency,
        recordStatus :'Active'
    };
    if(params.searchText){
        var regEx = {
            '$regex' : params.searchText,
            '$options' : 'i'
        };
        condition.$or = [
            { 'status': regEx },
            { 'firstPerson.forename': regEx },
            { 'firstPerson.surname': regEx },
            { 'firstPerson.salutation': regEx },
            { 'address.fullAddress': regEx }
        ];

        if(!params.type){
            condition.$or.push({
                'type': regEx
            });
        }
    }
    if(params.type){
        if(params.type ==='Others'){
            condition.type = {
                $nin : ['Client']
            };
        }
        else {
            condition.type = params.type;
        }
    }

    async.parallel({
        count: function (cb) {

            var query = Contact.find(condition);
            query.count().then(function (count) {
                cb(null, count);
            }, function (err) {
                cb(err, null);
            });
        },
        items: function (cb) {

            var query = Contact.find(condition);
            query.populate('negotiator','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.skip(params.pageSize * (params.pageIndex-1));
            query.limit(params.pageSize);
            query.sort({ updatedAt : -1});
            query.lean().exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        },
        requirements: function (cb) {
            var query = Requirement.find({
                agency: user.agency
            });
            query.select('name contact');
            query.exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        }
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            _.each(data.items,function (p) {
                p.requirements = _.filter(data.requirements,function(r){
                        return r.contact.toString() === p._id.toString();
                    }) || [];
            });
            successCallback({
                count : data.count,
                items : data.items
            });
        }
    });
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency,
        recordStatus :'Active'
    };
    if(params.type){
        options.type = params.type;
    }
    if(params.status){
        options.status = params.status;
    }

    var query = Contact.find(options);
    query.populate('negotiator', 'email forename surname');
    query.populate('updatedBy', 'email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        if(params.searchText){
            var searchText = params.searchText.toLowerCase();
            var records = _.filter(data, function (p) {
                return (
                    p.firstPerson.forename.toLowerCase().indexOf(searchText) > -1
                    || p.firstPerson.surname.toLowerCase().indexOf(searchText) > -1
                    || p.firstPerson.salutation.toLowerCase().indexOf(searchText) > -1
                    || p.address.fullAddress.toLowerCase().indexOf(searchText.toLowerCase())>-1
                );
            });
            successCallback(records);
        }
        else {
            successCallback(data);
        }
    }, failureCallback);
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.deleteItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Contact.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        recordStatus : 'Active'
    },{
        $set :{
            recordStatus:'Deleted'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.archiveItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Contact.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        recordStatus : 'Active'
    },{
        $set :{
            status:'Archived'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.activateItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Contact.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        recordStatus : 'Active'
    },{
        $set :{
            status:'Active'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};
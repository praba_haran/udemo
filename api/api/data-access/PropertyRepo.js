var _ = require('lodash');
var async = require('async');
var Property = require('../models/Property');
var Contact = require('../models/Contact');
var PropertyStatusChange = require('../models/PropertyStatusChange');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // updating address
    if (model.address) {
        var keys = ['nameOrNumber', 'street', 'locality', 'town', 'county', 'postcode'];
        var fullAddress = '';
        _.each(keys, function (key) {
            if (model.address[key]) {
                fullAddress += model.address[key];
                fullAddress += ', ';
            }
        });
        fullAddress = fullAddress.substr(0, fullAddress.length - 2);
        model.address.fullAddress = fullAddress;
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.owners.length>0){
        _.each(model.owners,function (p) {
            if(!p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    async.waterfall([
        function checkDuplicateAddress(cb){
            var query = Property.find({
                'agency': user.agency,
                'address.fullAddress':model.address.fullAddress,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Property already exist for this address.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function save(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var property = new Property(model);
            property.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function addStatus(property,cb){
            // ADDING DEFAULT STATUS WHEN NEW PROPERTY IS CREATED
            var status = {
                property : property._id,
                oldStatus : '',
                status : 'Pre Appraisal',
                notes :''
            };
            status.agency = user.agency;
            status.createdBy = user._id;
            status.createdAt = new Date();

            var st = new PropertyStatusChange(status);
            st.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    property.status = status.status;
                    cb(null,property);
                }
            });
        },
        function update(property,cb){
            // UPDATING STATUS
            property.updatedBy = user._id;
            property.updatedAt = new Date();

            Property.findOneAndUpdate({_id: property._id}, property, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Property created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // updating address
    if (model.address) {
        var keys = ['nameOrNumber', 'street', 'locality', 'town', 'county', 'postcode'];
        var fullAddress = '';
        _.each(keys, function (key) {
            if (model.address[key]) {
                fullAddress += model.address[key];
                fullAddress += ', ';
            }
        });
        fullAddress = fullAddress.substr(0, fullAddress.length - 2);
        model.address.fullAddress = fullAddress;
    }

    // if there is new owners, update createdAt and createdBy fields
    if(model.owners.length>0){
        _.each(model.owners,function (p) {
            if(!p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    // if there is new weblink, update createdAt and createdBy fields
    if(model.webLinks.length>0){
        _.each(model.webLinks,function (p) {
            if(!p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    async.waterfall([
        function checkDuplicateAddress(cb){
            var query = Property.find({
                'agency': user.agency,
                'address.fullAddress':model.address.fullAddress,
                '_id':{
                    "$nin":[model._id]
                }
            });
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb('Property already exist for this address.');
                }
                else{
                    cb(null);
                }
            }, function(err){
                cb(err);
            });
        },
        function addStatus(cb){
            var status = {
                property: model._id,
                oldStatus: '',
                notes: ''
            };
            // ADDING DEFAULT STATUS WHEN NEW PROPERTY IS CREATED AND ITS STATUS IS 'Pre Appraisal'
            if(model.status==='Pre Appraisal' && model.proposedPrice>0) {
                status.oldStatus = model.status;
                status.status = 'Appraised';
                cb(null,status);
            }
            else{
                cb(null,null);
            }
        },
        function updateStatus(status,cb){
            if(status) {

                status.agency = user.agency;
                status.createdBy = user._id;
                status.createdAt = new Date();

                var st = new PropertyStatusChange(status);
                st.save(function (err, data) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        model.status = status.status;
                        cb(null);
                    }
                });
            }
            else{
                cb(null);
            }
        },
        function save(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Property.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function getUpdatedProperty(property,cb){
            var query = Property.find({
                agency: user.agency,
                _id : property._id
            });
            query.populate('owners.contact','type status firstPerson.forename firstPerson.surname address.fullAddress');
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
                else{
                    cb('Property not found.');
                }
            },function(err){
                cb(err,null);
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Property updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    Property.remove({
        _id : id,
        agency: user.agency
    }, function(err, data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.result.n);
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Property.find({
        agency: user.agency,
        _id : id
    });
    query.populate('owners.contact','type status firstPerson address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.paginateList = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    params.pageIndex = parseInt(params.pageIndex);
    params.pageSize = parseInt(params.pageSize);
    var condition = {
        agency: user.agency,
        recordStatus : {
            $nin:['Deleted']
        }
    };

    if(params.searchText){
        var regEx = {
            '$regex' : params.searchText,
            '$options' : 'i'
        };
        condition.$or = [
            { 'address.fullAddress': regEx },
            { 'category': regEx }
        ];

        if(!params.marketFor){
            condition.$or.push({
                'market': regEx
            });
        }
    }
    if(params.marketFor){
        condition.market = params.marketFor;
    }
    if(params.filter){
        switch (params.filter){
            case 'Favourites':
                break;
            case 'Appraised':
            case 'Instructed':
            case 'Available':
            case 'Externally Sold':
            case 'Withdrawn':
            case 'Suspended':
                condition.status = params.filter;
                break;
            case 'Archived':
                condition.recordStatus = params.filter;
                break;
        }
    }

    async.parallel({
        count: function (cb) {

            var query = Property.find(condition);
            query.count().then(function (count) {
                cb(null, count);
            }, function (err) {
                cb(err, null);
            });
        },
        items: function (cb) {

            var query = Property.find(condition);
            query.populate('negotiator','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.skip(params.pageSize * (params.pageIndex-1));
            query.limit(params.pageSize);
            query.sort({ updatedAt : -1});
            query.lean().exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        }
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback({
                count : data.count,
                items : data.items
            });
        }
    });
};


/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency
    };
    if(params.type){
        options.type = params.type;
    }
    if(params.status && params.status.length>0){
        options.status = {
            $in : params.status
        };
    }
    var query = Property.find(options);
    query.populate('negotiator', 'email forename surname');
    query.populate('updatedBy', 'email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        if(params.searchText){
            var records = _.filter(data,function(p){
                return p.address.fullAddress.toLowerCase().indexOf(params.searchText.toLowerCase())>-1;
            });
            successCallback(records);
        }
        else {
            successCallback(data);
        }
    }, failureCallback);
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.deleteItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Property.update({
        _id : {
            $in:ids
        },
        agency: user.agency
    },{
        $set :{
            recordStatus:'Deleted'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.archiveItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Property.update({
        _id : {
            $in:ids
        },
        agency: user.agency
    },{
        $set :{
            recordStatus:'Archived'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.activateItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    Property.update({
        _id : {
            $in:ids
        },
        agency: user.agency
    },{
        $set :{
            recordStatus:'Active'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};
var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var Activity = require('../models/Activity');

/**
 * @param params
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(params,user,successCallback,failureCallback){
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };

    var query = Activity.find(options);
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'appointment_ref.applicants.contact',
        select:'firstPerson.forename firstPerson.surname address.fullAddress',
        model:'Contact'
    });
    query.populate({
        path:'appointment_ref.appraiser',
        select:'email forename surname',
        model:'User'
    });

    query.populate({
        path:'viewing_ref.applicants.contact',
        select:'firstPerson.forename firstPerson.surname address.fullAddress',
        model:'Contact'
    });
    query.populate({
        path:'viewing_ref.appraiser',
        select:'email forename surname',
        model:'User'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        var records = [];
        _.each(data,function(p){
            if(params.property){
                if(p.viewing){
                    records.push(p);
                }
                else if(p.appointment){
                    var index = _.findIndex(p.appointment_ref.properties,function(r){
                       return r.property.toString() === params.property;
                    });
                    if(index>-1){
                        records.push(p);
                    }
                }
            }
            else if(params.contact){
                if(p.viewing){
                    records.push(p);
                }
                else if(p.appointment){
                    var index = _.findIndex(p.appointment_ref.applicants,function(r){
                        return r.contact._id.toString() === params.contact;
                    });
                    if(index>-1){
                        records.push(p);
                    }
                }
            }
            else{
                records.push(p);
            }
        });
        successCallback(records);
    }, failureCallback);
};
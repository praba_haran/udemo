var _ = require('lodash');
var uuid = require('uuid');
var async = require('async');
var moment = require('moment');
var ForgotPasswordRequest = require('../models/ForgotPasswordRequest');
var User = require('../models/User');
var mailer = require('../lib/mailer');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.sendInstructions = function(model,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function save(cb){
            console.log('generate token');
            model.createdAt = new Date();
            model.token = uuid.v4();
            model.expireAt = new Date();
            model.isUsed = false;
            model.expireAt.setMinutes(model.expireAt.getMinutes()+30);

            var request = new ForgotPasswordRequest(model);
            request.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function findUser(request,cb){
            console.log('find user');
            var query = User.findOne({
                email : request.email
            });
            query.lean().exec().then(function (user) {
                cb(null,user,request);
            }, function(err){
                cb(err,null);
            });
        },
        function sendEmail(user,request,cb){
            console.log('send an email');
            var locals = {
                user : user,
                request : request
            };
            var options = {
                template : 'forgot-password',
                subject : 'Forgot Password',
                email : user.email
            };
            mailer.sendEmail(options,locals,function(err,result){
                if(err){
                    cb(err,null);
                }
                else{
                    cb(null,result);
                }
            });
        }
    ],function(err,data){
        console.log('instructions sent');
        if(err){
            failureCallback(err);
        }
        else{
            successCallback('Password reset instruction has been sent to your email.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.validateToken = function(token,successCallback,failureCallback){
    if (!token) {
        throw new Error('The argument token is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = ForgotPasswordRequest.find({
        token: token,
        isUsed : false
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        if(data.length>0){
            successCallback({
                isValidToken : true,
                isExpired : moment(new Date()).diff(data[0].expireAt) > 0
            });
        }
        else{
            successCallback({
                isValidToken : false,
                isExpired : false
            });
        }
    }, failureCallback);
};

/**
 * @param model
 * @param successCallback
 * @param failureCallback
 */
module.exports.changePassword = function(model,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    console.log('change password:\n.................');
    async.waterfall([
        function validateToken(cb){
            console.log('validating token');
            var query = ForgotPasswordRequest.find({
                token: model.token,
                isUsed : false
            });
            query.sort({createdAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    if(moment(new Date()).diff(data[0].expireAt) > 0){
                        cb('Link expired.',null);
                    }
                    else{
                        cb(null,data[0]);
                    }
                }
                else{
                    cb('Link expired.',null);
                }
            },function(err){
                cb(err,null);
            });
        },
        function updateRequest(request,cb){
            console.log('updating password request');
            ForgotPasswordRequest.findOneAndUpdate({_id: request._id},{
                isUsed : true,
                updatedAt : new Date()
            }, { upsert: true }, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function findUser(request,cb){
            console.log('find user');
            var query = User.findOne({
                email : request.email
            });
            query.lean().exec().then(function (user) {
                cb(null,user);
            }, function(err){
                cb(err,null);
            });
        },
        function changePassword(user,cb){
            console.log('change password');
            var obj = {
                password : model.password,
                updatedAt : new Date(),
                status : 'Active'
            };

            User.findOneAndUpdate({_id: user._id}, obj, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function sendEmail(user,cb){
            console.log('send an email');
            var locals = {
                user : user
            };
            var options = {
                template : 'change-password',
                subject : 'Your password have been changed',
                email : user.email
            };
            mailer.sendEmail(options,locals,function(err,result){
                if(err){
                    cb(err,null);
                }
                else{
                    cb(null,result);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback('Password changed successfully.');
        }
    });
};
var _ = require('lodash');
var mongoose = require('mongoose');
var async = require('async');
var Property = require('../models/Property');
var Contact = require('../models/Contact');
var BranchActivity = require('../models/BranchActivity');
var Viewing = require('../models/Viewing');
var GeneralAppointment = require('../models/GeneralAppointment');

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.getPropertySummary = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var aggregatorOpts = [
        {
            $group: {
                _id: "$status",
                count: { $sum: 1 }
            }
        }
    ];

    Property.aggregate(aggregatorOpts,function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.getContactSummary = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var aggregatorOpts = [
        {
            $group: {
                _id: "$type",
                count: { $sum: 1 }
            }
        }
    ];

    Contact.aggregate(aggregatorOpts,function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.getBranchPerformance = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.parallel({
        properties : function (cb) {
            Property.aggregate([
                {
                    $group: {
                        _id: {
                            month: {
                                $month: "$createdAt"
                            },
                            year: {
                                $year: "$createdAt"
                            }
                        },
                        count: { $sum: 1 }
                    }
                }
            ],function(err,data){
                if(err){
                    cb(err,null);
                }
                else{
                    cb(null,data);
                }
            });
        },
        contacts : function(cb){
            Contact.aggregate([
                {
                    $group: {
                        _id: {
                            month: {
                                $month: "$createdAt"
                            },
                            year: {
                                $year: "$createdAt"
                            }
                        },
                        count: { $sum: 1 }
                    }
                }
            ],function(err,data){
                if(err){
                    cb(err,null);
                }
                else{
                    cb(null,data);
                }
            });
        }
    },function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.getBranchActivity = function(user,type,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!type) {
        throw new Error('The argument type is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var modelTypes = [];
    if(type==='property') {
        modelTypes = ['Property','InstructProperty','DetailsToVendor'];
    }
    else if(type==='contact'){
        modelTypes = ['Contact'];
    }
    else if(type==='viewing'){
        modelTypes = ['Viewing'];
    }
    else if(type==='appointment'){
        modelTypes = ['GeneralAppointment'];
    }

    BranchActivity.aggregate([
        {
            $match:{
                modelType:{
                    $in: modelTypes
                }
            }
        },
        {
            $group: {
                _id: {
                    activityType:'$activityType'
                },
                count: {$sum: 1}
            }
        }
    ], function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.getUserActivity = function(user,type,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!type) {
        throw new Error('The argument type is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var modelTypes = [];
    if(type==='property') {
        modelTypes = ['Property','InstructProperty','DetailsToVendor'];
    }
    else if(type==='contact'){
        modelTypes = ['Contact'];
    }
    else if(type==='viewing'){
        modelTypes = ['Viewing'];
    }
    else if(type==='appointment'){
        modelTypes = ['GeneralAppointment'];
    }

    BranchActivity.aggregate([
        {
            $match:{
                modelType:{
                    $in: modelTypes
                },
                createdBy : mongoose.Types.ObjectId(user._id)
            }
        },
        {
            $group: {
                _id: {
                    activityType:'$activityType'
                },
                count: {$sum: 1}
            }
        }
    ], function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data);
        }
    });
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.getViewings = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };

    var query = Viewing.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
            model:'Contact'
        }
    });
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.sort({updatedAt: -1});
    query.lean().exec().then(successCallback, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.getAppointments = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {
        agency : user.agency
    };

    var query = GeneralAppointment.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.populate('properties.property','status recordStatus price proposedPrice address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(successCallback, failureCallback);
};
var _ = require('lodash');
var async = require('async');
var Offer = require('../models/Offer');
var Contact = require('../models/Contact');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(!p._id){
                p.createdAt = new Date();
                p.createdBy = user._id;
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }

    model.agency = user.agency;
    model.createdBy = user._id;
    model.createdAt = new Date();
    model.updatedBy = user._id;
    model.updatedAt = new Date();

    var ma = new Offer(model);
    ma.save(function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Offer created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
            else{
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }

    model.agency = user.agency;
    model.updatedBy = user._id;
    model.updatedAt = new Date();

    Offer.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Offer updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    Offer.remove({
        _id : id,
        agency: user.agency
    }, function(err, data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.result.n);
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Offer.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Offer.find({
        agency: user.agency
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var options = {};
    options.agency = user.agency;
    if(params.property){
        options.property = params.property;
    }

    var query = Offer.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract proposedPrice',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'firstPerson.forename firstPerson.surname address.fullAddress',
            model:'Contact'
        }
    });
    query.populate('applicants.contact','firstPerson.forename firstPerson.surname address.fullAddress');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {

        if(params.contact) {
            var result = [];
            _.each(data, function (p) {
                var items = _.filter(p.applicants, function (item) {
                    return item.contact._id.toString() === params.contact;
                });
                if (items.length > 0) {
                    result.push(p);
                }
            });
            successCallback(result);
        }
        else{
            successCallback(data);
        }
    }, failureCallback);
};
var _ = require('lodash');
var async = require('async');
//var User = require('../models/User');
var jsReport = require('jsreport');
var fs = require('fs');

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    fs.readFile('./brochure-templates/dist/bi-fold-0001.html','utf-8',function(err,content){
        if(err){
            failureCallback(err);
        }
        else {
            jsReport.render({
                template: {
                    content: content,
                    //helpers: "function sayLoudly(str) { return str.toUpperCase(); }",
                    engine: "handlebars",
                    recipe: "phantom-pdf",
                    phantom: {
                        orientation: "landscape",
                        width: "14in",
                        height:"8.5in",
                        margin:"0",
                        timeout: 1800000
                    }
                },
                data: {
                    name: "jsreport"
                },
                "logger": {
                    "console": {"transport": "console", "level": "debug"},
                    "file": {"transport": "file", "level": "info", "filename": "logs/log.txt"},
                    "error": {"transport": "file", "level": "error", "filename": "logs/error.txt"}
                }
            }).then(successCallback).catch(failureCallback);
        }
    });
};

var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var FileNotes = require('../models/FileNotes');
var Activity = require('../models/Activity');
var mailer = require('../lib/mailer');
var helper = require('../lib/helper');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(!p._id){
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }

    async.waterfall([
        function saveFileNotes(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var notes = new FileNotes(model);
            notes.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function getFileNotes(notes,cb){
            var query = FileNotes.find({
                agency: user.agency,
                _id : notes._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property'
            });
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('File notes not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'File notes created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
            else{
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }
    // If vendorConfirmed true and applicantConfirmed true then take the last schedule history
    // and compare it.
    var oldHistory = model.history[model.history.length-1];
    // NOW ONLY USER IS CONFIRMING, THEN ADD THIS TO HISTORY
    if(model.vendorConfirmed && model.applicantsConfirmed && model.confirmationEmailSent) {
        // COMPARE SCHEDULE ARE NOT EQUAL
        // THEN SET isRescheduled to TRUE

        if(!moment(model.date).isSame(oldHistory.date)
            || model.startTime !== oldHistory.startTime
            || model.endTime !== oldHistory.endTime){

            model.status = 'Rescheduled';

            model.history.push({
                appraiser: model.appraiser,
                accompaniedBy: model.accompaniedBy,
                date: model.date,
                startTime: model.startTime,
                endTime: model.endTime,
                vendorConfirmed: model.vendorConfirmed,
                applicantsConfirmed: model.applicantsConfirmed,
                createdBy: user._id
            });
        }
    }

    // UPDATE isConfirmed
    model.isConfirmed = model.vendorConfirmed && model.applicantsConfirmed;

    async.waterfall([
        function updateViewing(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Viewing.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function getViewing(viewing,cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : viewing._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        }
        // function addActivity(viewing,cb){
        //     // adding activity
        //     var activityModel = {};
        //     activityModel.agency = user.agency;
        //     activityModel.createdBy = user._id;
        //     activityModel.createdAt = new Date();
        //     activityModel.viewing = viewing._id;
        //     activityModel.viewing_ref = {};
        //     activityModel.viewing_ref.property = viewing.property;
        //     activityModel.viewing_ref.appraiser = viewing.appraiser;
        //     activityModel.viewing_ref.accompaniedBy = viewing.accompaniedBy;
        //     activityModel.viewing_ref.date = viewing.date;
        //     activityModel.viewing_ref.startTime = viewing.startTime;
        //     activityModel.viewing_ref.endTime = viewing.endTime;
        //     activityModel.viewing_ref.notes = viewing.notes;
        //     activityModel.viewing_ref.additionalNotes = viewing.additionalNotes;
        //     activityModel.viewing_ref.reminder = viewing.reminder;
        //     activityModel.viewing_ref.reminderChannel = viewing.reminderChannel;
        //     activityModel.viewing_ref.status = 'Changed';//viewing.status;
        //     activityModel.viewing_ref.vendorConfirmed = viewing.vendorConfirmed;
        //     activityModel.viewing_ref.applicantsConfirmed = viewing.applicantsConfirmed;
        //     activityModel.viewing_ref.applicants = _.map(viewing.applicants,function(p){
        //         return {
        //             contact : p.contact
        //         };
        //     });
        //
        //     var activity = new Activity(activityModel);
        //     activity.save(function (err, data) {
        //         if (err) {
        //             cb(err,null);
        //         }
        //         else {
        //             cb(null,viewing);
        //         }
        //     });
        // }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Viewing updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.cancel = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    async.waterfall([
        function cancelViewing(cb){

            Viewing.update({
                _id : id,
                agency: user.agency
            },{
                $set :{
                    status:'Cancelled'
                }
            },{
                multi:true
            },function (err,data) {
                if(err){
                    cb(err);
                }
                else{
                    cb(null);
                }
            });
        },
        function addToBranchActivity(cb){
            var activity = {
                modelType :'Viewing',
                modelId : id,
                activityType : 'Cancelled'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null);
            },function(err){
                cb(err);
            });
        },
        function getViewing(cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function addActivity(viewing,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.viewing = viewing._id;
            activityModel.viewing_ref = {};
            activityModel.viewing_ref.property = viewing.property;
            activityModel.viewing_ref.appraiser = viewing.appraiser;
            activityModel.viewing_ref.accompaniedBy = viewing.accompaniedBy;
            activityModel.viewing_ref.date = viewing.date;
            activityModel.viewing_ref.startTime = viewing.startTime;
            activityModel.viewing_ref.endTime = viewing.endTime;
            activityModel.viewing_ref.notes = viewing.notes;
            activityModel.viewing_ref.additionalNotes = viewing.additionalNotes;
            activityModel.viewing_ref.reminder = viewing.reminder;
            activityModel.viewing_ref.reminderChannel = viewing.reminderChannel;
            activityModel.viewing_ref.status = viewing.status;
            activityModel.viewing_ref.vendorConfirmed = viewing.vendorConfirmed;
            activityModel.viewing_ref.applicantsConfirmed = viewing.applicantsConfirmed;
            activityModel.viewing_ref.applicants = _.map(viewing.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,viewing);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Viewing cancelled successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Viewing.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
            model:'Contact'
        }
    });
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }
    if(params.status && params.status.length>0){
        options.status = {
            $in : params.status
        };
    }

    var query = Viewing.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
            model:'Contact'
        }
    });
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        if(params.contact) {
            var result = [];
            _.each(data, function (p) {
                var items = _.filter(p.applicants, function (item) {
                    return item.contact._id.toString() === params.contact;
                });
                if (items.length > 0) {
                    result.push(p);
                }
            });
            successCallback(result);
        }
        else{
            successCallback(data);
        }
    }, failureCallback);
};
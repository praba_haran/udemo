var _ = require('lodash');
var async = require('async');
var User = require('../models/User');
var redisClient = require('../redisClient');

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.getItemByEmail = function(email,successCallback,failureCallback){
    if (!email) {
        throw new Error('The argument email is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = User.findOne({
        email : email
    });
    //query.populate('agency');
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = User.findOne({
        agency: user.agency,
        _id : id
    });
    query.populate('agency');
    query.lean().exec().then(function (data) {
        delete data.password;
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user, successCallback, failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = User.find({
        agency: user.agency
    });
    query.select('agency userType forename surname email notes status createdAt updatedAt createdBy updatedBy');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.paginateList = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    params.pageIndex = parseInt(params.pageIndex);
    params.pageSize = parseInt(params.pageSize);
    var condition = {
        agency: user.agency,
        status : {
            $nin:['Deleted']
        }
    };

    if(params.searchText){
        var regEx = {
            '$regex' : params.searchText,
            '$options' : 'i'
        };
        condition.$or = [
            { 'email': regEx },
            { 'forename': regEx },
            { 'surname': regEx }
        ];
    }

    async.parallel({
        count: function (cb) {
            var query = User.find(condition);
            query.count().then(function (count) {
                cb(null, count);
            }, function (err) {
                cb(err, null);
            });
        },
        items: function (cb) {

            var query = User.find(condition);
            query.select('agency userType forename surname email notes status createdAt updatedAt createdBy updatedBy');
            query.skip(params.pageSize * (params.pageIndex-1));
            query.limit(params.pageSize);
            query.sort({ updatedAt : -1});
            query.lean().exec().then(function (result) {
                cb(null, result);
            }, function (err) {
                cb(err, null);
            });
        }
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback({
                count : data.count,
                items : data.items
            });
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.deleteItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    User.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        status : 'Active'
    },{
        $set :{
            status:'Deleted'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.disableItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    User.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        status : 'Active'
    },{
        $set :{
            status:'Disabled'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};

/**
 * @param ids
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.activateItems = function(ids,user,successCallback,failureCallback){
    if (!ids) {
        throw new Error('The argument ids is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    User.update({
        _id : {
            $in:ids
        },
        agency: user.agency,
        status : {
            $nin :['Active']
        }
    },{
        $set :{
            status:'Active'
        }
    },{
        multi:true
    },function (err,data) {
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.nModified);
        }
    });
};
var _ = require('lodash');
var async = require('async');
var Property = require('../models/Property');
var Contact = require('../models/Contact');
var Matching = require('../models/Matching');
var Requirement = require('../models/Requirement');

module.exports = {

    getList: function (agencyId,propertyId, successCallback, failureCallback) {
        if (!agencyId) {
            throw new Error('The argument agencyId is missing.');
        }
        if (!propertyId) {
            throw new Error('The argument contactId is missing.');
        }
        if (!successCallback) {
            throw new Error('The argument successCallback is missing.');
        }
        if (!failureCallback) {
            throw new Error('The argument failureCallback is missing.');
        }
        var query = Matching.find({
            agency: agencyId,
            property : propertyId
        });
        query.populate('createdBy','email forename surname');
        query.populate('updatedBy','email forename surname');
        query.exec().then(function (data) {
            successCallback(data);
        }, failureCallback);
    },

    save: function (propertyId,model, user, successCallback, failureCallback) {
        if (!model) {
            throw new Error('The argument model is missing.');
        }
        if (!user) {
            throw new Error('The argument user is missing.');
        }
        if (!propertyId) {
            throw new Error('The argument propertyId is missing.');
        }
        if (!successCallback) {
            throw new Error('The argument successCallback is missing.');
        }
        if (!failureCallback) {
            throw new Error('The argument failureCallback is missing.');
        }

        async.waterfall([
            function save(cb){
                model.updatedBy = user._id;
                model.updatedAt = new Date();
                
                if (model._id) {
                    Matching.findOneAndUpdate({_id: model._id}, model, {upsert: true}, function (err, data) {
                        if (err) {
                            cb(err,null);
                        }
                        else {
                            cb(null,data);
                        }
                    });
                }
                else {
                    model.property = propertyId;
                    model.agency = user.agency;
                    model.createdBy = user._id;

                    var matching = new Matching(model);
                    matching.save(function (err, data) {
                        if (err) {
                            cb(err,null);
                        }
                        else {
                            cb(null,data);
                        }
                    });
                }
            },
            function get(matching,cb){
                var query = Matching.find({
                    agency: user.agency,
                    _id : matching._id
                });
                query.populate('createdBy','email forename surname');
                query.populate('updatedBy','email forename surname');
                query.exec().then(function (data) {
                    if(data.length>0){
                        cb(null,data[0]);
                    }
                    else{
                        cb(new Error('No record found.'),null);
                    }
                }, function(err){
                    cb(err,null);
                });
            }
        ],function(err,result){
            if(err){
                failureCallback(err);
            }
            else{
                var message = model._id ?'List updated successfully.':'List saved successfully.';
                successCallback(result, message);
            }
        });        
    },

    removeById: function(id, successCallback, failureCallback){
        if (!id) {
            throw new Error('The argument id is missing.');
        }
        if (!successCallback) {
            throw new Error('The argument successCallback is missing.');
        }
        if (!failureCallback) {
            throw new Error('The argument failureCallback is missing.');
        }

        Matching.findOneAndRemove({_id : id}, function(err, data){
            if(err){
                failureCallback(err);
            }
            else{
                successCallback(data,'List deleted successfully.');
            }
        });
    },

    getMatches: function (id,options,user, successCallback, failureCallback) {
        if (!id) {
            throw new Error('The argument id is missing.');
        }
        if (!options) {
            throw new Error('The argument options is missing.');
        }
        if (!successCallback) {
            throw new Error('The argument successCallback is missing.');
        }
        if (!failureCallback) {
            throw new Error('The argument failureCallback is missing.');
        }

        var hasMatchedFeatures = function(propertyFeatures,requiredFeatures){
            var isMatching = true;
            var matchedArray = [];
            if(propertyFeatures.length>0 && requiredFeatures.length>0 && propertyFeatures.length===requiredFeatures.length){
                for(var i=0;i<propertyFeatures.length;i++){
                    var array1 = propertyFeatures[i];
                    var array2 = requiredFeatures[i];
                    
                    if(array1.values.length === array2.values.length){
                        for(var j=0;j<array1.values.length;j++){
                            var left = array1.values[j];
                            var right = array2.values[j];
                            if(left.selected === right.selected){
                                matchedArray.push(true);
                            }
                        }
                    }                    
                }
                // need to check minimun true values here
                isMatching = matchedArray.indexOf(true)>-1;
            }
            return isMatching;
        }

        var isMatchedContact = function(property,requirement){
            var isMatching = false;
            //console.log('prop',property);
            if(
                (property.market ==='For Sale' && requirement.intension==='Buy')
                && (requirement.newBuild==='No' || (property.age ==='New build' && requirement.newBuild==='Yes'))
                && ( options.matchBedrooms==='No' || (options.matchBedrooms==='Yes' && requirement.bedrooms === property.bedrooms))
                && ( options.matchBathrooms==='No' || (options.matchBathrooms==='Yes' && requirement.bathrooms === property.bathrooms))
                && ( options.matchReceptions==='No' || (options.matchReceptions==='Yes' && requirement.receptions === property.receptions))
                && ( options.matchParking==='No' || (options.matchParking==='Yes' && requirement.parking === property.parking))
                && ( options.matchFeatures==='No' || (options.matchFeatures==='Yes' && hasMatchedFeatures(property.features.list,requirement.features)))
            ){
                isMatching = true;
            }
            //console.log('req',requirement);
            return isMatching;
        }

        var isMatchedContacts = function(property,requirements){
            var isMatching = false;
            var conditions = [];
            if(requirements.length>0){
                for(var i=0;i<requirements.length;i++){
                    conditions.push(isMatchedContact(property,requirements[i]));
                }
                isMatching = conditions.indexOf(true)>-1;
            }            
            return isMatching;
        }

        async.waterfall([
            function getProperty(cb){
                var query = Property.find({
                    agency: user.agency,
                    _id : id,
                    recordStatus : 'Active'
                });
                query.sort({updatedAt: -1});
                query.lean().exec().then(function (result) {
                    var property = result[0];
                    cb(null,property);
                }, function(err){
                    cb(err,null);
                });
            },
            function getContacts(property,cb){
                var params = {
                    agency: user.agency,
                    type : 'Client'
                };

                var query = Contact.find(params);
                query.populate('negotiator', 'email forename surname');
                query.populate('updatedBy', 'email forename surname');
                query.sort({updatedAt: -1});
                query.lean().exec().then(function (result) {
                    cb(null,property,result);
                }, function(err){
                    cb(err,null);
                });
            },
            function getRequirements(property,contacts,cb){
                var contactIds = _.map(contacts,function(p){
                    return p._id;
                });
                var query = Requirement.find({
                    agency: user.agency,
                    contact : {
                        $in :contactIds
                    }
                });
                query.populate('createdBy','email forename surname');
                query.populate('updatedBy','email forename surname');
                query.exec().then(function (requirements) {
                    // if contact has requirements add to collection
                    var filteredContacts = [];
                    _.each(contacts,function(contact){
                        var filteredReqs = _.filter(requirements,function(r){
                            return r.contact.toString() === contact._id.toString();
                        });
                        if(isMatchedContacts(property,filteredReqs)){
                            filteredContacts.push(contact);
                        }
                    });
                    cb(null,filteredContacts);
                }, function(err){
                    cb(err,null);
                });
            }
        ],function(err,data){
            if(err){
                failureCallback(err);
            }
            else{
                successCallback(data);
            }
        });
    }
};
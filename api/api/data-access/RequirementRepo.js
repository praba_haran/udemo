var _ = require('lodash');
var async = require('async');
var Property = require('../models/Property');
var Contact = require('../models/Contact');
var Requirement = require('../models/Requirement');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    model.agency = user.agency;
    model.createdBy = user._id;
    model.createdAt = new Date();
    model.updatedBy = user._id;
    model.updatedAt = new Date();

    var requirement = new Requirement(model);
    requirement.save(function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Profile created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    model.agency = user.agency;
    model.updatedBy = user._id;
    model.updatedAt = new Date();

    Requirement.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data, 'Profile updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.delete = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    Requirement.remove({
        _id : id,
        agency: user.agency
    }, function(err, data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data.result.n);
        }
    });
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(contactId,user,successCallback,failureCallback){
    if (!contactId) {
        throw new Error('The argument contactId is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Requirement.find({
        agency: user.agency,
        contact : contactId
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.matches = function(requirement,user,successCallback,failureCallback){
    if (!requirement) {
        throw new Error('The argument requirement is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    var areas = [];
    var propertyTypes = [];
    _.each(requirement.areas,function(area){
        if(area.selected){
            areas.push(area.name);
        }
    });
    _.each(requirement.propertyTypes,function(type){
        if(type.values.length>0){
            _.each(type.values,function(value){
                if(value.selected){
                    propertyTypes.push(value.name);
                }
            })
        }
    });

    var params = {
        agency: user.agency,
        market : (requirement.intension==='Buy')?'For Sale':'To Let',
        status : 'Available',
        recordStatus : 'Active'
    };
    if(requirement.minPrice>0 && requirement.maxPrice>0){
        params.contract = {
            price : {
                $gte : requirement.minPrice,
                $lte : requirement.maxPrice
            }
        };
    }
    if(requirement.newBuild==='Yes'){
        params.age ={
            $in : ['New build']
        };
    }
    if(requirement.bedrooms>0){
        params.bedrooms = requirement.bedrooms;
    }
    if(requirement.bathrooms>0){
        params.bathrooms = requirement.bathrooms;
    }
    if(requirement.receptions>0){
        params.receptions = requirement.receptions;
    }
    if(requirement.parking>0){
        params.parking = requirement.parking;
    }
    // if(requirement.furnishing){
    //     if(requirement.furnishing === 'Furnished'){
    //         params.furnishing = {
    //             $in : ['Furnished','Part furnished']
    //         };
    //     }
    //     else{
    //         params.furnishing = requirement.furnishing;
    //     }
    // }
    // filter by area
    if(areas.length>0){
        params['address.town'] = {
            $in : areas
        };
    }
    // filter by property types
    if(propertyTypes.length>0){
        params.type = {
            $in : propertyTypes
        };
    }

    var features = [];
    _.each(requirement.features,function(f){
        if(f.values){
            _.each(f.values,function(value){
                if(value.selected){
                    features.push(value.name);
                }
            });
        }
    });

    var query = Property.find(params);
    query.populate('negotiator', 'email forename surname');
    query.populate('updatedBy', 'email forename surname');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        var properties = [];
        // FILTER BY FEATURES
        _.each(data,function(p){
            if(p.features.list.length>0 && features.length>0){
                var pFeatures = [];
                _.each(p.features.list,function(f){
                    if(f.values){
                        _.each(f.values,function(value){
                            if(value.selected){
                                pFeatures.push(value.name);
                            }
                        });
                    }
                });
                if(pFeatures.length>0){
                    if(_.intersection(pFeatures,features).length>0){
                        properties.push(p);
                    }
                }
                else{
                    properties.push(p);
                }
            }
            else{
                properties.push(p);
            }
        });
        successCallback(properties);
    }, failureCallback);
};
var _ = require('lodash');
var async = require('async');
var BranchActivity = require('../models/BranchActivity');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    model.agency = user.agency;
    model.createdBy = user._id;
    model.createdAt = new Date();

    var activity = new BranchActivity(model);
    activity.save(function (err, data) {
        if (err) {
            failureCallback(err);
        }
        else {
            successCallback(data);
        }
    });
};
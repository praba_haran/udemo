var _ = require('lodash');
var async = require('async');
var Property = require('../models/Property');
var BoardChangeRequest = require('../models/BoardChangeRequest');
var PropertyStatusChange = require('../models/PropertyStatusChange');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function addStatus(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();

            var status = new PropertyStatusChange(model);
            status.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function update(status,cb){
            // UPDATING STATUS
            Property.findOneAndUpdate({_id: model.property }, { status : status.status }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,status);
                }
            });
        },
        function changeBoard(status,cb){
            if(model.actions.requireBoardChange){
                var board = model.boardChangeRequest;
                board.property = model.property;
                board.agency = user.agency;
                board.createdBy = user._id;
                board.createdAt = new Date();
                var boardChangeReq = new BoardChangeRequest(board);
                boardChangeReq.save(function (err, data) {
                    if (err) {
                        cb(err,null);
                    }
                    else {
                        cb(null,status);
                    }
                });
            }
            else{
                cb(null,status);
            }
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Status changed successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = PropertyStatusChange.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(params,user,successCallback,failureCallback){
    if(!params){
        throw new Error('The argument params is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }

    var query = PropertyStatusChange.find(options);
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
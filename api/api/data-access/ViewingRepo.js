var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var Viewing = require('../models/Viewing');
var Activity = require('../models/Activity');
var mailer = require('../lib/mailer');
var helper = require('../lib/helper');
var BranchActivityRepo = require('./BranchActivityRepo');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(!p._id){
                p.createdAt = new Date();
                p.createdBy = user._id;
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
        });
    }
    // ADD DATE , START TIME, END TIME, APPRAISER AND ACCOMPANIED BY TO HISTORY FOR TRACKING
    model.history.push({
        appraiser : model.appraiser,
        accompaniedBy : model.accompaniedBy,
        date : model.date,
        startTime : model.startTime,
        endTime : model.endTime,
        vendorConfirmed : model.vendorConfirmed,
        applicantsConfirmed : model.applicantsConfirmed,
        createdBy : user._id
    });

    // UPDATE isConfirmed
    model.isConfirmed = model.vendorConfirmed && model.applicantsConfirmed;

    async.waterfall([
        function saveViewing(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            var viewing = new Viewing(model);
            viewing.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function addToBranchActivity(viewing,cb){
            var activity = {
                modelType :'Viewing',
                modelId : viewing._id,
                activityType : 'Booked'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null,viewing);
            },function(err){
                cb(err,null);
            });
        },
        function addActivity(viewing,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.viewing = viewing._id;
            activityModel.viewing_ref = {};
            activityModel.viewing_ref.property = viewing.property;
            activityModel.viewing_ref.appraiser = viewing.appraiser;
            activityModel.viewing_ref.accompaniedBy = viewing.accompaniedBy;
            activityModel.viewing_ref.date = viewing.date;
            activityModel.viewing_ref.startTime = viewing.startTime;
            activityModel.viewing_ref.endTime = viewing.endTime;
            activityModel.viewing_ref.notes = viewing.notes;
            activityModel.viewing_ref.additionalNotes = viewing.additionalNotes;
            activityModel.viewing_ref.reminder = viewing.reminder;
            activityModel.viewing_ref.reminderChannel = viewing.reminderChannel;
            activityModel.viewing_ref.status = viewing.status;
            activityModel.viewing_ref.vendorConfirmed = viewing.vendorConfirmed;
            activityModel.viewing_ref.applicantsConfirmed = viewing.applicantsConfirmed;
            activityModel.viewing_ref.applicants = _.map(viewing.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,viewing);
                }
            });
        },
        function getViewing(viewing,cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : viewing._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Viewing created successfully.');
        }
    });
};

/**
 * @param id
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.update = function(id,model,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    // if there is new applicant, update createdAt and createdBy fields
    if(model.applicants.length>0){
        _.each(model.applicants,function (p) {
            if(p._id){
                p.updatedAt = new Date();
                p.updatedBy = user._id;
            }
            else{
                p.createdAt = new Date();
                p.createdBy = user._id;
            }
        });
    }
    // If vendorConfirmed true and applicantConfirmed true then take the last schedule history
    // and compare it.
    var oldHistory = model.history[model.history.length-1];
    // NOW ONLY USER IS CONFIRMING, THEN ADD THIS TO HISTORY
    if(model.vendorConfirmed && model.applicantsConfirmed && model.confirmationEmailSent) {
        // COMPARE SCHEDULE ARE NOT EQUAL
        // THEN SET isRescheduled to TRUE

        if(!moment(model.date).isSame(oldHistory.date)
            || model.startTime !== oldHistory.startTime
            || model.endTime !== oldHistory.endTime){

            model.status = 'Rescheduled';

            model.history.push({
                appraiser: model.appraiser,
                accompaniedBy: model.accompaniedBy,
                date: model.date,
                startTime: model.startTime,
                endTime: model.endTime,
                vendorConfirmed: model.vendorConfirmed,
                applicantsConfirmed: model.applicantsConfirmed,
                createdBy: user._id
            });
        }
    }

    // UPDATE isConfirmed
    model.isConfirmed = model.vendorConfirmed && model.applicantsConfirmed;

    async.waterfall([
        function updateViewing(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Viewing.findOneAndUpdate({_id: id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function getViewing(viewing,cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : viewing._id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        }
        // function addActivity(viewing,cb){
        //     // adding activity
        //     var activityModel = {};
        //     activityModel.agency = user.agency;
        //     activityModel.createdBy = user._id;
        //     activityModel.createdAt = new Date();
        //     activityModel.viewing = viewing._id;
        //     activityModel.viewing_ref = {};
        //     activityModel.viewing_ref.property = viewing.property;
        //     activityModel.viewing_ref.appraiser = viewing.appraiser;
        //     activityModel.viewing_ref.accompaniedBy = viewing.accompaniedBy;
        //     activityModel.viewing_ref.date = viewing.date;
        //     activityModel.viewing_ref.startTime = viewing.startTime;
        //     activityModel.viewing_ref.endTime = viewing.endTime;
        //     activityModel.viewing_ref.notes = viewing.notes;
        //     activityModel.viewing_ref.additionalNotes = viewing.additionalNotes;
        //     activityModel.viewing_ref.reminder = viewing.reminder;
        //     activityModel.viewing_ref.reminderChannel = viewing.reminderChannel;
        //     activityModel.viewing_ref.status = 'Changed';//viewing.status;
        //     activityModel.viewing_ref.vendorConfirmed = viewing.vendorConfirmed;
        //     activityModel.viewing_ref.applicantsConfirmed = viewing.applicantsConfirmed;
        //     activityModel.viewing_ref.applicants = _.map(viewing.applicants,function(p){
        //         return {
        //             contact : p.contact
        //         };
        //     });
        //
        //     var activity = new Activity(activityModel);
        //     activity.save(function (err, data) {
        //         if (err) {
        //             cb(err,null);
        //         }
        //         else {
        //             cb(null,viewing);
        //         }
        //     });
        // }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Viewing updated successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.cancel = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }
    async.waterfall([
        function cancelViewing(cb){

            Viewing.update({
                _id : id,
                agency: user.agency
            },{
                $set :{
                    status:'Cancelled'
                }
            },{
                multi:true
            },function (err,data) {
                if(err){
                    cb(err);
                }
                else{
                    cb(null);
                }
            });
        },
        function addToBranchActivity(cb){
            var activity = {
                modelType :'Viewing',
                modelId : id,
                activityType : 'Cancelled'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null);
            },function(err){
                cb(err);
            });
        },
        function getViewing(cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : id
            });
            query.populate('createdBy','email forename surname');
            query.populate('updatedBy','email forename surname');
            query.populate({
                path:'property',
                select:'status recordStatus owners address.fullAddress photos contract',
                model:'Property',
                populate:{
                    path:'owners.contact',
                    select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
                    model:'Contact'
                }
            });
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if (data.length>0) {
                    cb(null,data[0]);
                }
                else {
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function addActivity(viewing,cb){
            // adding activity
            var activityModel = {};
            activityModel.agency = user.agency;
            activityModel.createdBy = user._id;
            activityModel.createdAt = new Date();
            activityModel.viewing = viewing._id;
            activityModel.viewing_ref = {};
            activityModel.viewing_ref.property = viewing.property;
            activityModel.viewing_ref.appraiser = viewing.appraiser;
            activityModel.viewing_ref.accompaniedBy = viewing.accompaniedBy;
            activityModel.viewing_ref.date = viewing.date;
            activityModel.viewing_ref.startTime = viewing.startTime;
            activityModel.viewing_ref.endTime = viewing.endTime;
            activityModel.viewing_ref.notes = viewing.notes;
            activityModel.viewing_ref.additionalNotes = viewing.additionalNotes;
            activityModel.viewing_ref.reminder = viewing.reminder;
            activityModel.viewing_ref.reminderChannel = viewing.reminderChannel;
            activityModel.viewing_ref.status = viewing.status;
            activityModel.viewing_ref.vendorConfirmed = viewing.vendorConfirmed;
            activityModel.viewing_ref.applicantsConfirmed = viewing.applicantsConfirmed;
            activityModel.viewing_ref.applicants = _.map(viewing.applicants,function(p){
                return {
                    contact : p.contact
                };
            });

            var activity = new Activity(activityModel);
            activity.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,viewing);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Viewing cancelled successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Viewing.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
            model:'Contact'
        }
    });
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');

    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(user,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = Viewing.find({
        agency: user.agency
    });
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.search = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }
    if(params.status && params.status.length>0){
        options.status = {
            $in : params.status
        };
    }

    var query = Viewing.find(options);
    query.populate('createdBy','email forename surname');
    query.populate('updatedBy','email forename surname');
    query.populate({
        path:'property',
        select:'status recordStatus owners address.fullAddress photos contract',
        model:'Property',
        populate:{
            path:'owners.contact',
            select:'firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones',
            model:'Contact'
        }
    });
    query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        if(params.contact) {
            var result = [];
            _.each(data, function (p) {
                var items = _.filter(p.applicants, function (item) {
                    return item.contact._id.toString() === params.contact;
                });
                if (items.length > 0) {
                    result.push(p);
                }
            });
            successCallback(result);
        }
        else{
            successCallback(data);
        }
    }, failureCallback);
};

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.sendConfirmationEmail = function(model,user,successCallback,failureCallback){

    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function updateViewing(cb){
            model.agency = user.agency;
            model.updatedBy = user._id;
            model.updatedAt = new Date();

            Viewing.findOneAndUpdate({_id: model._id}, model, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null);
                }
            });
        },
        function getViewing(cb){
            var query = Viewing.find({
                agency: user.agency,
                _id : model._id
            });
            query.populate('updatedBy','email forename surname');
            query.populate('appraiser','email forename surname');
            query.populate('property','status recordStatus contract address.fullAddress');
            query.populate('applicants.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.populate('owners.contact','type firstPerson.forename firstPerson.surname address.fullAddress firstPerson.emails firstPerson.telephones');
            query.sort({updatedAt: -1});
            query.lean().exec().then(function (data) {
                if(data.length>0){
                    cb(null,data[0]);
                }
                else{
                    cb('Viewing not found.',null);
                }
            }, function(err){
                cb(err,null);
            });
        },
        function sendConfirmationMail(viewing,cb){

            function getNotificationInfo(applicant){
                var notification = {};
                if(viewing.status === 'Booked'){
                    if(applicant.status==='Active'){
                        notification.type = 'onConfirmation';
                        notification.canSendEmail = !applicant.onConfirmation.isEmailDelivered;
                    }
                    else if(applicant.status==='Cancelled'){
                        notification.type = 'onCancelled';
                        notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                            && !applicant.onCancelled.isEmailDelivered;
                    }
                }
                if(viewing.status === 'Rescheduled'){
                    if(applicant.status==='Active'){
                        if(!applicant.onConfirmation.isEmailDelivered){
                            notification.type = 'onConfirmation';
                            notification.canSendEmail = true;
                        }
                        else {
                            notification.type = 'onRescheduled';
                            notification.canSendEmail = !applicant.onRescheduled.isEmailDelivered;
                        }
                    }
                    else if(applicant.status==='Cancelled'){
                        notification.type = 'onCancelled';
                        notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                            && !applicant.onCancelled.isEmailDelivered;
                    }
                }

                else if(viewing.status==='Cancelled'){
                    notification.type = 'onCancelled';
                    notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                        && !applicant.onCancelled.isEmailDelivered;
                }
                return notification;
            }

            function getSendMailHandler(obj){
                var notificationInfo = getNotificationInfo(obj);
                var onConfirmation = obj[notificationInfo.type];

                return function(innerCb){
                    if(notificationInfo.canSendEmail){
                        var locals = {
                            contact: obj.contact,
                            viewingDate: helper.formatDateTime(helper.combineDateAndTime(viewing.date,viewing.startTime)),
                            viewing: viewing,
                            property: viewing.property,
                            emailMessage: onConfirmation.emailMessage
                        };
                        var options = {
                            from: '"Goran Gates" <' + viewing.updatedBy.email + '>',
                            email: obj.contact.firstPerson.emails[0].email,
                            replyTo: viewing.updatedBy.email
                        };
                        if (notificationInfo.type === 'onConfirmation') {
                            options.template = 'viewing-confirmation';
                            options.subject = 'Viewing Confirmation';
                        }
                        else if (notificationInfo.type === 'onRescheduled') {
                            options.template = 'viewing-reschedule';
                            options.subject = 'Viewing Rescheduled';
                        }
                        else {
                            options.template = 'viewing-cancellation';
                            options.subject = 'Viewing Cancellation';
                        }

                        mailer.sendEmail(options, locals, function (err, result) {
                            if (err) {
                                innerCb(err, null);
                            }
                            else {
                                // after success set createdBy to logged in user
                                onConfirmation.createdBy = user._id;
                                onConfirmation.isEmailDelivered = result.accepted.indexOf(obj.contact.firstPerson.emails[0].email) > -1;
                                if(onConfirmation.isEmailDelivered) {
                                    onConfirmation.deliveredAt = new Date();
                                }
                                innerCb(null, result);
                            }
                        });
                    }
                    else{
                        innerCb(null, {});
                    }
                }
            }
            var handlers = [];

            _.each(viewing.applicants, function (applicant) {
                handlers.push(getSendMailHandler(applicant));
            });
            _.each(viewing.owners, function (owner) {
                handlers.push(getSendMailHandler(owner));
            });
            async.parallel(handlers, function (err, data) {
                if (err) {
                    cb('Error occurred while sending an email.',null);
                }
                else {
                    // once all mails are sent
                    // set confirmationEmailSent to true

                    var confirmationMailCount = 0;
                    var rescheduleMailCount = 0;
                    var cancellationMailCount = 0;
                    _.each(viewing.applicants,function(p){
                        if(p.onConfirmation.isEmailDelivered){
                            confirmationMailCount++;
                        }
                        if(p.onRescheduled.isEmailDelivered){
                            rescheduleMailCount++;
                        }
                        if(p.onCancelled.isEmailDelivered){
                            cancellationMailCount++;
                        }
                    });

                    if(!viewing.confirmationMailSentAt && confirmationMailCount>0) {
                        viewing.confirmationEmailSent = true;
                        viewing.confirmationMailSentAt = new Date();
                    }
                    if(!viewing.reScheduledEmailSentAt && rescheduleMailCount>0) {
                        viewing.reScheduledEmailSent = true;
                        viewing.reScheduledEmailSentAt = new Date();
                    }
                    if(!viewing.cancellationEmailSentAt && cancellationMailCount>0) {
                        viewing.cancellationEmailSent = true;
                        viewing.cancellationEmailSentAt = new Date();
                    }
                    cb(null,viewing);
                }
            });
        },
        function updateViewing(viewing,cb){
            Viewing.findOneAndUpdate({_id: viewing._id}, viewing , {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,viewing);
                }
            });
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data, 'Email sent successfully to all parties.');
        }
    });
};

/**
 * @param user
 * @param params
 * @param successCallback
 * @param failureCallback
 */
module.exports.getBusyApplicants = function(user,params,successCallback,failureCallback){
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!params) {
        throw new Error('The argument params is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    params.date = moment(params.date).utc(0).set({
        hour:0,
        minute:0,
        second:0,
        millisecond:0
    }).toDate();
    var nextDay = moment(params.date).add(1,'days').toDate();

    var options = {
        agency : user.agency,
        status : 'Booked',
        applicants : {
            $elemMatch: {
                status : 'Active'
            }
        },
        date : {
            $gt : params.date, 
            $lt : nextDay
        },
        $and:[
            {
                startTime :{
                    $lte : params.endTime
                }
            },
            {
                endTime : {
                    $gte : params.startTime
                }
            }
        ]
    };

    var query = Viewing.find(options);
    query.select('date status startTime endTime applicants.contact applicants.status');
    query.sort({updatedAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
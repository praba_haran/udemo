var _ = require('lodash');
var async = require('async');
var BranchActivityRepo = require('./BranchActivityRepo');
var Property = require('../models/Property');
var BoardChangeRequest = require('../models/BoardChangeRequest');
var InstructProperty = require('../models/InstructProperty');
var PropertyStatusChange = require('../models/PropertyStatusChange');

/**
 * @param model
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.save = function(model,user,successCallback,failureCallback){
    if (!model) {
        throw new Error('The argument model is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    async.waterfall([
        function instruct(cb){
            model.agency = user.agency;
            model.createdBy = user._id;
            model.createdAt = new Date();

            var instruct = new InstructProperty(model);
            instruct.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,data);
                }
            });
        },
        function addToBranchActivity(instructProperty,cb){
            var activity = {
                modelType :'InstructProperty',
                modelId : instructProperty._id,
                activityType : 'Instructed'
            };
            BranchActivityRepo.save(activity,user,function(data){
                cb(null,instructProperty);
            },function(err){
                cb(err,null);
            });
        },
        function update(instruct,cb){
            // UPDATING CONTRACT IN PROPERTY
            var contract = {
                contractType : instruct.contractType,
                price : instruct.price,
                saleTerms : instruct.saleTerms,
                rentalFrequency : instruct.rentalFrequency,
                priceQualifier : instruct.priceQualifier,
                contractLength : instruct.contractLength,
                contractEndsOn : instruct.contractEndsOn,
                instructId : instruct._id,
                management : instruct.management,
                commissionAndFees : instruct.commissionAndFees,
                discount : instruct.discount
            };
            Property.findOneAndUpdate({_id: model.property }, { contract : contract, status : 'Instructed' }, {upsert: true}, function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,instruct);
                }
            });
        },
        function addStatus(instruct,cb){
            var status = {
                property: model.property,
                oldStatus: 'Appraised',
                status : 'Instructed',
                notes: ''
            };
            status.agency = user.agency;
            status.createdBy = user._id;
            status.createdAt = new Date();

            var st = new PropertyStatusChange(status);
            st.save(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                else {
                    cb(null,instruct);
                }
            });
        },
        function changeBoard(instruct,cb){
            if(model.actions.requireBoardChange){
                var board = model.boardChangeRequest;
                board.property = model.property;
                board.agency = user.agency;
                board.createdBy = user._id;
                board.createdAt = new Date();
                var boardChangeReq = new BoardChangeRequest(board);
                boardChangeReq.save(function (err, data) {
                    if (err) {
                        cb(err,null);
                    }
                    else {
                        cb(null,instruct);
                    }
                });
            }
            else{
                cb(null,instruct);
            }
        }
    ],function(err,data){
        if(err){
            failureCallback(err);
        }
        else{
            successCallback(data,'Property instructed successfully.');
        }
    });
};

/**
 * @param id
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.get = function(id,user,successCallback,failureCallback){
    if (!id) {
        throw new Error('The argument id is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var query = InstructProperty.find({
        agency: user.agency,
        _id : id
    });
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};

/**
 * @param user
 * @param successCallback
 * @param failureCallback
 */
module.exports.list = function(params,user,successCallback,failureCallback){
    if(!params){
        throw new Error('The argument params is missing.');
    }
    if (!user) {
        throw new Error('The argument user is missing.');
    }
    if (!successCallback) {
        throw new Error('The argument successCallback is missing.');
    }
    if (!failureCallback) {
        throw new Error('The argument failureCallback is missing.');
    }

    var options = {
        agency : user.agency
    };
    if(params.property){
        options.property = params.property;
    }

    var query = InstructProperty.find(options);
    query.populate('createdBy','email forename surname');
    query.populate({
        path:'property',
        select:'price proposedPrice priceQualifier status recordStatus owners address.fullAddress photos',
        model:'Property'
    });
    query.sort({createdAt: -1});
    query.lean().exec().then(function (data) {
        successCallback(data);
    }, failureCallback);
};
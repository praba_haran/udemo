var Repo = require('../data-access/ViewingRepo');

module.exports.save = function(req,res){
    var model = req.body;

    Repo.save(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.update = function(req,res){
    var model = req.body;

    Repo.update(req.params.id, model, req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.cancel = function(req,res){

    Repo.cancel(req.params.id, req.user, function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.get = function (req,res) {

    Repo.get(req.params.id,req.user,function(data){
        if(data.length===0){
            res.notFound();
        }
        else{
            res.success(data[0]);
        }
    },function(err){
        res.error(err);
    });
};

module.exports.list = function (req,res) {

    Repo.list(req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.search = function (req,res) {

    Repo.search(req.user, req.body, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.sendConfirmationEmail = function(req,res){
    var model = req.body;

    Repo.sendConfirmationEmail(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.getBusyApplicants = function (req,res) {

    Repo.getBusyApplicants(req.user, req.body, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};
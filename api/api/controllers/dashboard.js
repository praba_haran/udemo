var Repo = require('../data-access/DashboardRepo');

module.exports.getPropertySummary = function (req,res) {

    Repo.getPropertySummary(req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getContactSummary = function (req,res) {

    Repo.getContactSummary(req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getBranchPerformance = function (req,res) {

    Repo.getBranchPerformance(req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getBranchActivity = function (req,res) {

    var type = req.params.type;

    Repo.getBranchActivity(req.user,type,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getUserActivity = function (req,res) {

    var type = req.params.type;

    Repo.getUserActivity(req.user,type,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getViewings = function (req,res) {

    Repo.getViewings(req.user, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getAppointments = function (req,res) {

    Repo.getAppointments(req.user, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};
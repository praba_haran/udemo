var Repo = require('../data-access/ActivityRepo');

module.exports.list = function (req,res) {
    var model = req.body;
    Repo.list(model,req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};
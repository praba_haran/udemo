var Repo = require('../data-access/BrochureRepo');

module.exports.get = function (req, res) {

    Repo.get(req.user,function(data){
        console.log('success');
        data.stream.pipe(res);
    },function(err){
        console.log('err',err);
        res.error(err);
    });
};
var AgencyRepo = require('../data-access/AgencyRepo');

module.exports.getItems = function (req, res) {

    AgencyRepo.getItems(function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.save = function (req, res) {
    var model = req.body;

    AgencyRepo.saveItem(model,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getItemById = function (req, res) {
    var agencyId = req.user.agency;

    AgencyRepo.getItemById(agencyId,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};
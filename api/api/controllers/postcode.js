var mongoose = require('mongoose');
var axios = require('axios');
var _  = require('lodash');
var Postcode = require('../models/Postcode');

var apiKey = 's0S3JWSfjEauN6k5SamqPQ6242';
var baseUrl = 'https://api.getaddress.io/v2/uk/';

module.exports.getList = function (req, res) {

    var query = Postcode.find({
        postcode: {
            "$regex": req.params.postcode,
            "$options": "i"
        }
    });
    query.limit(100);
    query.exec(function(err, result) {
        if (err) {
            res.error(err);
        }
        else {
            if(result.length>0) {
                var addresses = [];
                _.each(result,function(item){
                    addresses = _.concat(addresses,item.addresses);
                });
                res.success(addresses);
            }
            else{
                axios.get('https://api.getaddress.io/v2/uk/' + req.params.postcode, {
                    params: {
                        'api-key': apiKey
                    }
                }).then(function (response) {
                    var addresses = [];
                    if(response.data) {
                        var model = {};
                        if(response.data.Addresses) {
                            _.each(response.data.Addresses, function (p) {
                                var parts = p.split(',');
                                if(parts.length===7){

                                    var address = null;
                                    // check it not starts with number. If so there is dwelling coming in
                                    if(isNaN(parts[0][0])){
                                        address = {
                                            dwelling: parts[0].trim(),
                                            nameOrNumber: '',
                                            street: parts[1].trim(),
                                            locality: '',
                                            town: parts[5].trim(),
                                            county: parts[6].trim(),
                                            postcode: req.params.postcode,
                                            country: 'United Kingdom',
                                            fullAddress: ''
                                        };
                                    }
                                    else {
                                        address = {
                                            dwelling: '',
                                            nameOrNumber: '',
                                            street: parts[0].trim(),
                                            locality: parts[1].trim(),
                                            town: parts[5].trim(),
                                            county: parts[6].trim(),
                                            postcode: req.params.postcode,
                                            country: 'United Kingdom',
                                            fullAddress: ''
                                        };
                                    }
                                    var keys = ['dwelling','nameOrNumber','street','locality','town','county'];
                                    var fullAddress = '';
                                    _.each(keys,function(key){
                                        if(address[key] && address[key]!==''){
                                            fullAddress +=address[key];
                                            fullAddress += ', ';
                                        }
                                    });
                                    fullAddress = fullAddress.substr(0,fullAddress.length-2);
                                    address.fullAddress = fullAddress;
                                    addresses.push(address);
                                }
                            });
                        }
                        model.postcode = req.params.postcode;
                        model.latitude = response.data.Latitude;
                        model.longitude = response.data.Longitude;
                        model.addresses = addresses;
                        if(model.addresses.length>0) {
                            var postcode = new Postcode(model);
                            postcode.save(function (err, data) {
                                if (err) {
                                    res.error(err);
                                }
                                else {
                                    res.success(data);
                                }
                            });
                        }
                        else{
                            res.error('No data found.');
                        }
                    }
                    else{
                        res.error('No data found.');
                    }
                }).catch(function (error) {
                    res.error(error);
                });
            }
        }
    });
}
var Repo = require('../data-access/PropertyRepo');

module.exports.save = function(req,res){
    var model = req.body;

    Repo.save(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.update = function(req,res){
    var model = req.body;

    Repo.update(req.params.id, model, req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.delete = function(req,res){

    Repo.delete(req.params.id, req.user, function(count){
        if(count>0) {
            res.success({}, 'Property deleted successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.get = function (req,res) {

    Repo.get(req.params.id,req.user,function(data){
        if(data.length===0){
            res.notFound();
        }
        else{
            res.success(data[0]);
        }
    },function(err){
        res.error(err);
    });
};

module.exports.paginateList = function (req,res) {

    Repo.paginateList(req.user, req.query, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.search = function (req,res) {

    Repo.search(req.user, req.body, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.deleteItems = function(req,res){
    var model = req.body;

    Repo.deleteItems(model, req.user, function(count){
        if(count>0) {
            res.success({}, count===1?'Property deleted successfully.':'Properties deleted successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.archiveItems = function(req,res){
    var model = req.body;

    Repo.archiveItems(model, req.user, function(count){
        if(count>0) {
            res.success({},count===1?'Property archived successfully.': 'Properties archived successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.activateItems = function(req,res){
    var model = req.body;

    Repo.activateItems(model, req.user, function(count){
        if(count>0) {
            res.success({},count===1?'Property activated successfully.': 'Properties activated successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};
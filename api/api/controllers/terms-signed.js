var Repo = require('../data-access/TermsSignedRepo');

module.exports.save = function(req,res){
    var model = req.body;

    Repo.save(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.get = function (req,res) {

    Repo.get(req.params.id,req.user,function(data){
        if(data.length===0){
            res.notFound();
        }
        else{
            res.success(data[0]);
        }
    },function(err){
        res.error(err);
    });
};

module.exports.list = function (req,res) {
    var params = req.body;

    Repo.list(params,req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};


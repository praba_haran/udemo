var fs = require('fs');
var path = require('path');
var brochure = require('../lib/brochure');

module.exports.getBrochure = function (req,res) {
    console.log('get brochure');
    brochure.loadBrochure({
        userId: req.user._id,
        agencyId : req.user.agency,
        propertyId : req.params.id,
        templateId : req.params.templateId,
        cache : req.query.cache
    },function(pdfPath){
        var file = fs.createReadStream(pdfPath);
        var stat = fs.statSync(pdfPath);
        res.setHeader('Content-Length', stat.size);
        res.setHeader('Content-Type', 'application/pdf');
        //res.setHeader('Content-Disposition', 'attachment; filename='+templateId+'.pdf');
        file.pipe(res);
    });
};

module.exports.getBrochurePreview = function(req,res){
    console.log('getBrochurePreview');
    var templateId = req.params.templateId;
    var agencyId = req.user.agency;

    var filePath = path.resolve(__dirname,'../../data/brochure-templates-docx/',agencyId,'previews',templateId+'.png');

    var file = fs.createReadStream(filePath);
    var stat = fs.statSync(filePath);
    res.setHeader('Content-Length', stat.size);
    res.setHeader('Content-Type', 'image/png');
    file.pipe(res);
}
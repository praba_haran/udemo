var ContactRepo = require('../data-access/ContactRepo');

module.exports.getAllContacts = function (req,res) {
    ContactRepo.getAllContacts(req.user.agency,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getSolicitors = function (req,res) {

    ContactRepo.getSolicitors(req.user.agency,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.getContactById = function (req,res) {

    ContactRepo.getContactById(req.params.id,function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.saveContact = function(req,res){
    var model = req.body;

    ContactRepo.saveContact(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.removeContactByIds = function(req,res){
    var model = req.body;

    ContactRepo.removeContactByIds(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.archiveContactByIds = function(req,res){
    var model = req.body;

    ContactRepo.archiveContactByIds(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.searchOwners = function (req,res) {
    var searchText = req.params.searchText;

    ContactRepo.searchOwners(req.user.agency,searchText,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};
var Repo = require('../data-access/ForgotPasswordRepo');

module.exports.sendInstructions = function(req,res){
    var model = req.body;

    Repo.sendInstructions(model,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.validateToken = function (req,res) {

    Repo.validateToken(req.params.token,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.changePassword = function(req,res){
    var model = req.body;

    Repo.changePassword(model,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};
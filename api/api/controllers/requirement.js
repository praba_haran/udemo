var Repo = require('../data-access/RequirementRepo');

module.exports.save = function(req,res){
    var model = req.body;

    Repo.save(model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.update = function(req,res){
    var model = req.body;

    Repo.update(req.params.id, model, req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.delete = function(req,res){

    Repo.delete(req.params.id, req.user, function(deletedCount){
        if(deletedCount>0) {
            res.success({}, 'Profile deleted successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.list = function (req,res) {

    Repo.list(req.params.contactId,req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.matches = function (req,res) {

    Repo.matches(req.body, req.user, function (data) {
        res.success(data);
    },function(err){
        res.error(err);
    });
};
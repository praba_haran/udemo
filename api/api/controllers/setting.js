var SettingRepo = require('../data-access/SettingRepo');

module.exports.inviteBranchUsers = function (req, res) {
    var model =  req.body;

    SettingRepo.inviteBranchUsers(req.user,model,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};
var mongoose = require('mongoose');
var _ = require('lodash');
var MatchingRepo = require('../data-access/MatchingRepo');

module.exports.getList = function (req,res) {
    var propertyId = req.params.id;

    MatchingRepo.getList(req.user.agency,propertyId,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.save = function(req,res){
    var propertyId = req.params.id;
    var model = req.body;

    MatchingRepo.save(propertyId,model,req.user,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.removeById = function(req,res){
    var id = req.params.id;

    MatchingRepo.removeById(id,function(data,message){
        res.success(data,message);
    },function(err){
        res.error(err);
    });
};

module.exports.getMatches = function (req,res) {
    var model = req.body;
    var id = req.params.id;

    MatchingRepo.getMatches(id,model,req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};


var Repo = require('../data-access/UserRepo');

module.exports.list = function (req, res) {

    Repo.list(req.user,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};

module.exports.paginateList = function (req, res) {
    var params = req.query;

    Repo.paginateList(req.user,params,function(data){
        res.success(data);
    },function(err){
        res.error(err);
    });
};


module.exports.deleteItems = function(req,res){
    var model = req.body;

    Repo.deleteItems(model, req.user, function(deletedCount){
        if(deletedCount>0) {
            res.success({}, deletedCount===1?'User deleted successfully.' : 'Users deleted successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.disableItems = function(req,res){
    var model = req.body;

    Repo.disableItems(model, req.user, function(count){
        if(count>0) {
            res.success({}, count===1?'User disabled successfully.':'Users disabled successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};

module.exports.activateItems = function(req,res){
    var model = req.body;

    Repo.activateItems(model, req.user, function(count){
        if(count>0) {
            res.success({},count===1?'User activated successfully.': 'Users activated successfully.');
        }
        else{
            res.notFound();
        }
    },function(err){
        res.error(err);
    });
};
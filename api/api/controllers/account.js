var jwt = require('jsonwebtoken');
var UserRepo = require('../data-access/UserRepo');
var AccountRepo = require('../data-access/AccountRepo');

var timeToLive = 2 * 12 * (1 * 60 * 60);// 2 x 12 hours(in seconds) = 1 day

module.exports.authenticate = function(req, res) {
    var model = req.body;
    console.log(model);
    if(!model.email){
        res.error('Please provide email.');
    }
    else {
        UserRepo.getItemByEmail(model.email, function (user) {
            if(user) {
                if (user.password != model.password) {
                    res.error('Invalid credentials.');
                } else {
                    // if user is found and password is right then create a token
                    var token = jwt.sign(user, req.app.get('superSecret'), {
                        expiresIn: timeToLive
                    });
                    res.success({
                        token: token
                    });
                }
            }
            else{
                res.error('Invalid user.');
            }
        }, function (err) {
            res.error('Invalid user.');
        });
    }
};

module.exports.getUserInfo = function(req, res) {
    if(req.user){
        UserRepo.get(req.user._id,req.user,function (data) {
            var result = {
                userInfo : data
            };
            AccountRepo.getLatestUserLoginByEmail(data.email,function(logs){
                if(logs.length>0){
                    result.profile = logs[0].profile;
                }
                res.success(result);
            },function(err){
                res.success(result);
            });
        },function(err){
            res.error('User not found.');
        });
    }
    else{
        res.error('User not found.');
    }
};

module.exports.logUserLogin = function(req,res){
    var model = req.body;
    AccountRepo.logUserLogin(model,function(data){
        var email = '';
        if(data.login_via==='google'){
            email = data.profile.email;
        }
        UserRepo.getItemByEmail(email, function (user) {
            if(user) {
                // if user is found and password is right then create a token
                var token = jwt.sign(user, req.app.get('superSecret'), {
                    expiresIn: timeToLive
                });
                res.success({
                    token: token
                });
            }
            else{
                res.error('Invalid user.');
            }
        }, function (err) {
            res.error('Invalid user.');
        });
    },function(err){
        res.error(err);
    });
};
var jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
	
    var token = req.body.token 
    || req.query.token 
    || req.headers['x-access-token'] 
    || req.headers['X-ACCESS-TOKEN'];

    if (token) {
        jwt.verify(token, req.app.get('superSecret'), function(err, decoded) {
            if (err) {
                return res.notAuthorized('Failed to authenticate token.');
            } else {
                // if everything is good, save to request for use in other routes
                delete decoded.password;
                req.user = decoded;
                next();
            }
        });
    } else {
        return res.notAuthorized('No token provided.');
    }
}
var _ = require('lodash');
var ruleConfig = require('./rules');
var validators = require('./validators');

function validateRule(rules,model,errors){
    _.each(rules, function (r,index) {
        var value = _.get(model, r.name);
        var validator = validators[r.type] || r.getValidator();

        if(validator){
            var isValid = validator(model,r,value);
            if(isValid){
                if(r.getChildRules){
                    var subRules = r.getChildRules(model);
                    validateRule(subRules,model,errors);
                }
                if(r.next&& r.next.length>0){
                    validateRule(r.next,model,errors);
                }
            }
            else{
                errors.push({
                    name : r.name,
                    message : r.message,
                    type : r.type
                });
            }
        }
    });
}

module.exports.validate = function (key, model) {
    var errors = [];
    var rules = ruleConfig[key];
    if(rules){
        validateRule(rules,model,errors);
    }
    return {
        isValid : (errors.length===0),
        errors : errors
    };
};
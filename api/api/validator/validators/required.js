module.exports = function (model,rule,value){
    if(rule.dataType==='string') {
        if (value) {
            value = value.trim();
        }
        return (value !== null && value !== undefined && value !== '');
    }
    else if(rule.dataType==='array'){
        return value.length>0;
    }
    return false;
};
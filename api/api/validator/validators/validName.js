module.exports = function (model,rule,value){
    if(rule.dataType==='string') {
        if (!value) return true;
        value = value.trim();

        var regEx = /^[a-zA-Z ,.'-]+$/i;
        return regEx.test(value);
    }
    return false;
};
var required = require('./required');
var email = require('./email');
var phone = require('./phone');
var validName = require('./validName');

module.exports = {
    required : required,
    email : email,
    phone : phone,
    validName : validName
};
var express = require('express');
var router = express.Router();

var accountCtrl = require('./controllers/account');
var forgotPasswordCtrl = require('./controllers/forgot-password');
router.post('/authenticate',accountCtrl.authenticate);
router.post('/account/google/authenticate',accountCtrl.logUserLogin);

router.post('/forgot-password/send-instructions',forgotPasswordCtrl.sendInstructions);
router.get('/forgot-password/validate-token/:token',forgotPasswordCtrl.validateToken);
router.post('/forgot-password/change-password',forgotPasswordCtrl.changePassword);

var middlewares = require('../config/middlewares');
for(var index=0;index<middlewares.length;index++){
    var middleware = require('./middlewares/'+middlewares[index]);
    router.use(middleware);
}


var agencyCtrl = require('./controllers/agency');
var contactCtrl = require('./controllers/contact');
var requirementCtrl = require('./controllers/requirement');
var matchingCtrl = require('./controllers/matching');
var postcodeCtrl = require('./controllers/postcode');
var propertyCtrl = require('./controllers/property');
var documentCtrl = require('./controllers/document');
var settingCtrl = require('./controllers/setting');

router.get('/agencies',agencyCtrl.getItems);
router.post('/agency',agencyCtrl.save);
router.get('/agency/details', agencyCtrl.getItemById);

router.get('/address/search/:postcode',postcodeCtrl.getList);
router.get('/property/:id/matching',matchingCtrl.getList);
router.post('/property/:id/matching/save',matchingCtrl.save);
router.delete('/property/:id/matching/remove/:id',matchingCtrl.removeById);
router.post('/property/:id/matching/matches',matchingCtrl.getMatches);

//SETTINGS
router.post('/setting/branch/invite-users',settingCtrl.inviteBranchUsers);

router.get('/doc/brochure/:id/:templateId',documentCtrl.getBrochure);
router.get('/doc/preview/:templateId',documentCtrl.getBrochurePreview);

require('./routes/account').addRoutes(router);
require('./routes/dashboard').addRoutes(router);
require('./routes/property').addRoutes(router);
require('./routes/property-status-change').addRoutes(router);
require('./routes/board-change-request').addRoutes(router);
require('./routes/instruct-property').addRoutes(router);
require('./routes/terms-sent').addRoutes(router);
require('./routes/terms-signed').addRoutes(router);
require('./routes/details-to-vendor').addRoutes(router);
require('./routes/contact').addRoutes(router);
require('./routes/market-appraisal').addRoutes(router);
require('./routes/viewing').addRoutes(router);
require('./routes/file-notes').addRoutes(router);
require('./routes/general-appointment').addRoutes(router);
require('./routes/offer').addRoutes(router);
require('./routes/requirements').addRoutes(router);
require('./routes/activity').addRoutes(router);
require('./routes/cabinet-master').addRoutes(router);
require('./routes/key').addRoutes(router);
require('./routes/user').addRoutes(router);
require('./routes/brochure').addRoutes(router);

module.exports = router;

module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/board-change-request');

    router.post('/board-change-request/save',ctrl.save);
    router.get('/board-change-request/get/:id',ctrl.get);
    router.post('/board-change-request/list',ctrl.list);
};

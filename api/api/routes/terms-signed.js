
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/terms-signed');

    router.post('/property/terms-signed/save',ctrl.save);
    router.get('/property/terms-signed/get/:id',ctrl.get);
    router.post('/property/terms-signed/list',ctrl.list);
};

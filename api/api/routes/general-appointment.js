
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/general-appointment');

    router.post('/general-appointment/save',ctrl.save);
    router.put('/general-appointment/update/:id',ctrl.update);
    router.delete('/general-appointment/delete/:id',ctrl.delete);
    router.get('/general-appointment/get/:id',ctrl.get);
    router.get('/general-appointment/list',ctrl.list);
    router.post('/general-appointment/search',ctrl.search);
};

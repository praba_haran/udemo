
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/property');

    router.post('/property/save',ctrl.save);
    router.put('/property/update/:id',ctrl.update);
    router.delete('/property/delete/:id',ctrl.delete);
    router.get('/property/get/:id',ctrl.get);
    router.get('/property/list/paginate',ctrl.paginateList);
    router.post('/property/search',ctrl.search);
    router.post('/property/delete/items',ctrl.deleteItems);
    router.post('/property/archive/items',ctrl.archiveItems);
    router.post('/property/activate/items',ctrl.activateItems);
};


module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/viewing');

    router.post('/viewing/save',ctrl.save);
    router.put('/viewing/update/:id',ctrl.update);
    router.delete('/viewing/cancel/:id',ctrl.cancel);
    router.get('/viewing/get/:id',ctrl.get);
    router.get('/viewing/list',ctrl.list);
    router.post('/viewing/search',ctrl.search);
    router.post('/viewing/send-confirmation-email',ctrl.sendConfirmationEmail);
    router.post('/viewing/get/busy-applicants',ctrl.getBusyApplicants);
};

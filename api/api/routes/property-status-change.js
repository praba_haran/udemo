
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/property-status-change');

    router.post('/property/status/save',ctrl.save);
    router.get('/property/status/get/:id',ctrl.get);
    router.post('/property/status/list',ctrl.list);
};


module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/details-to-vendor');

    router.post('/property/details-to-vendor/save',ctrl.save);
    router.put('/property/details-to-vendor/update/:id',ctrl.update)
    router.get('/property/details-to-vendor/get/:id',ctrl.get);
    router.post('/property/details-to-vendor/list',ctrl.list);
};

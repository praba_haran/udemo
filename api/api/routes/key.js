
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/key');

    router.post('/key/save',ctrl.save);
    router.put('/key/update/:id',ctrl.update);
    router.delete('/key/delete/:id',ctrl.delete);
    router.get('/key/get/:id',ctrl.get);
    router.get('/key/list',ctrl.list);
    router.post('/key/search',ctrl.search);
};

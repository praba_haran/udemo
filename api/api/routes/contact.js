
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/contact');

    router.post('/contact/save',ctrl.save);
    router.put('/contact/update/:id',ctrl.update);
    router.delete('/contact/delete/:id',ctrl.delete);
    router.post('/contact/delete/items',ctrl.deleteItems);
    router.post('/contact/archive/items',ctrl.archiveItems);
    router.post('/contact/activate/items',ctrl.activateItems);
    router.get('/contact/get/:id',ctrl.get);
    router.get('/contact/list',ctrl.list);
    router.get('/contact/list/paginate',ctrl.paginateList);
    router.post('/contact/search',ctrl.search);
};

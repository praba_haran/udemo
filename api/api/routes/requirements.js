
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/requirement');

    router.post('/requirement/save',ctrl.save);
    router.put('/requirement/update/:id',ctrl.update);
    router.delete('/requirement/delete/:id',ctrl.delete);
    router.get('/requirement/list/:contactId',ctrl.list);
    router.post('/requirement/matches',ctrl.matches);
};

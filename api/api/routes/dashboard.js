
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/dashboard');

    router.get('/dashboard/property-summary',ctrl.getPropertySummary);
    router.get('/dashboard/contact-summary',ctrl.getContactSummary);
    router.get('/dashboard/branch-performance',ctrl.getBranchPerformance);
    router.get('/dashboard/branch-activity/:type',ctrl.getBranchActivity);
    router.get('/dashboard/user-activity/:type',ctrl.getUserActivity);
    router.get('/dashboard/viewing/search',ctrl.getViewings);
    router.get('/dashboard/appointments/search',ctrl.getAppointments);
};

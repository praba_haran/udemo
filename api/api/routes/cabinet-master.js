
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/cabinet-master');

    router.post('/cabinet-master/save',ctrl.save);
    router.put('/cabinet-master/update/:id',ctrl.update);
    router.delete('/cabinet-master/delete/:id',ctrl.delete);
    router.get('/cabinet-master/get/:id',ctrl.get);
    router.get('/cabinet-master/list',ctrl.list);
    router.post('/cabinet-master/search',ctrl.search);
};

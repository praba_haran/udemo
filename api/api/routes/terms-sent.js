
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/terms-sent');

    router.post('/property/terms/save',ctrl.save);
    router.get('/property/terms/get/:id',ctrl.get);
    router.post('/property/terms/list',ctrl.list);
};

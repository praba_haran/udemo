
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/offer');

    router.post('/offer/save',ctrl.save);
    router.put('/offer/update/:id',ctrl.update);
    router.delete('/offer/delete/:id',ctrl.delete);
    router.get('/offer/get/:id',ctrl.get);
    router.get('/offer/list',ctrl.list);
    router.post('/offer/search',ctrl.search);
};

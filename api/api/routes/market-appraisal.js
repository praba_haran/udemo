
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/market-appraisal');

    router.post('/market-appraisal/save',ctrl.save);
    router.put('/market-appraisal/update/:id',ctrl.update);
    router.delete('/market-appraisal/delete/:id',ctrl.delete);
    router.get('/market-appraisal/get/:id',ctrl.get);
    router.get('/market-appraisal/list',ctrl.list);
    router.post('/market-appraisal/search',ctrl.search);
};


module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/instruct-property');

    router.post('/property/instruct/save',ctrl.save);
    router.get('/property/instruct/get/:id',ctrl.get);
    router.post('/property/instruct/list',ctrl.list);
};

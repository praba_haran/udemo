
module.exports.addRoutes = function(router){
    var accountCtrl = require('../controllers/account');

    router.get('/account/user/me',accountCtrl.getUserInfo);
};

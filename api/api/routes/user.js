
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/user');

    router.get('/user/list',ctrl.list);
    router.get('/user/list/paginate',ctrl.paginateList);
    router.post('/user/delete/items',ctrl.deleteItems);
    router.post('/user/disable/items',ctrl.disableItems);
    router.post('/user/activate/items',ctrl.activateItems);
};

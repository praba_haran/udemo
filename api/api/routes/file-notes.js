
module.exports.addRoutes = function(router){
    var ctrl = require('../controllers/file-notes');

    router.post('/file-notes/save',ctrl.save);
    router.put('/file-notes/update/:id',ctrl.update);
    router.delete('/file-notes/cancel/:id',ctrl.cancel);
    router.get('/file-notes/get/:id',ctrl.get);
    router.post('/file-notes/search',ctrl.search);
};

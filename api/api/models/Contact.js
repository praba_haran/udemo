var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    referenceNo : {
        type:String,
        required:true,
        default:function(){
            return Math.random().toString(36).substr(2, 5);
        }
    },
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency',
        index : true
    },
    type:{
        type:String,
        index : true
    },
    companyName :{
        type : String
    },
    companyWebsite :{
        type :String
    },
    intension:{
        type:String
    },
    leadSource:{
        type:String
    },
    notes:{
        type:String
    },
    position:{
        type:String
    },
    disposal:{
        type:String
    },
    movingReason:{
        type:String
    },
    rating:{
        type:String
    },
    timeScale:{
        type:Date
    },
    action:{
        type:String
    },
    referredBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    referredToInt:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Contacts'
    },
    referredToExt:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Contacts'
    },
    referralNotes:{
        type: String
    },
    moneyLaunderingCheck:{
        isIdCheckCompleted:{
            type:Boolean
        },
        notes:{
            type:String
        }
    },
    address:{
        dwelling:{
            type:String
        },
        nameOrNumber:{
            type:String
        },
        street:{
            type:String
        },
        locality:{
            type:String
        },
        town:{
            type:String
        },
        county:{
            type:String
        },
        postcode:{
            type:String
        },
        country:{
            type:String
        },
        fullAddress:{
            type:String
        },
        latitude:{
            type:Number
        },
        longitude:{
            type:Number
        }
    },
    firstPerson: {
        title: {
            type: String
        },
        forename: {
            type: String
        },
        surname: {
            type: String
        },
        salutation: {
            type: String
        },
        emails:[
            {
               email:{
                   type:String
               }
            }
        ],
        telephones: [
            {
                type: {
                    type: String
                },
                number: {
                    type: String
                },
                label: {
                    type: String
                }
            }
        ]
    },
    secondPerson: {
        title: {
            type: String
        },
        forename: {
            type: String
        },
        surname: {
            type: String
        },
        salutation: {
            type: String
        },
        emails:[
            {
                email:{
                    type:String
                }
            }
        ],
        telephones: [
            {
                type: {
                    type: String
                },
                number: {
                    type: String
                },
                label: {
                    type: String
                }
            }
        ]
    },
    solicitor:{
        solicitor:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'Contacts'
        },
        notes:{
            type:String
        },
        referralInt:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'Contacts'
        },
        referralExt:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'Contacts'
        }
    },
    interestedProperties:[{
        property : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Property'
        },
        updatedAt :{
            type: Date,
            default: function () {
                return new Date();
            }
        },
        updatedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    }],
    docs:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    status:{
        type:String,
        required:true,
        default:'Active',//Rejected,Completed,Vacated,Archived
        index : true
    },
    reviewDate:{
        type:Date
    },
    reviewNotes:{
        type:String
    },
    lastContactedDate:{
        type:Date
    },
    negotiator:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    updatedBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    },
    recordStatus:{
        type : String, // Active,Deleted
        default:'Active', // No hard delete is allowed
        index : true
    },
    extra:{
        selected : {
            type : Boolean
        }
    }
});

module.exports = mongoose.model('Contact', Schema);
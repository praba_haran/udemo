var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    referenceNo : {
        type:String,
        required:true,
        default:function(){
            return Math.random().toString(36).substr(2, 5);
        }
    },
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency',
        index : true
    },
    type:{
        type:String,
        index : true
    },
    category:{
        type:String
    },
    market:{
        type:String
    },
    age:{
        type:String
    },
    marketing:{
        type:String
    },
    timeOnMarket:{
        type:String
    },
    bedrooms:{
        type:Number
    },
    bathrooms:{
        type:Number
    },
    receptions:{
        type:Number
    },
    parking:{
        type:Number
    },
    floorArea:{
        type:Number
    },
    landArea:{
        type:Number
    },
    landAreaUnit:{
        type : String
    },
    summary:{
        type:String
    },
    advertSummary:{
        type:String
    },
    furnishing:{
        type:String
    },
    appraiserNotes:{
        type:String
    },
    price:{
        type:Number
    },
    lowPrice:{
        type:Number
    },
    highPrice:{
        type:Number
    },
    ownerPrice:{
        type:Number
    },
    proposedPrice:{
        type:Number
    },
    saExpireOn:{
        type:Date
    },
    address:{
        dwelling:{
            type:String
        },
        nameOrNumber:{
            type:String
        },
        street:{
            type:String
        },
        locality:{
            type:String
        },
        town:{
            type:String
        },
        county:{
            type:String
        },
        postcode:{
            type:String
        },
        country:{
            type:String
        },
        fullAddress:{
            type:String
        },
        latitude:{
            type:Number
        },
        longitude:{
            type:Number
        }
    },
    owners:[{
        contact : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Contact'
        },
        updatedAt :{
            type: Date,
            default: function () {
                return new Date();
            }
        },
        updatedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    }],
    photos:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    floorPlans:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    virtualTours:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    brochures:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    externalBrochures:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    otherFiles:[
        {
            id :{
                type:String
            },
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            fileName:{
                type:String
            },
            size:{
                type:Number
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    webLinks:[
        {
            url:{
                type:String
            },
            title:{
                type:String
            },
            description:{
                type:String
            },
            createdAt:{
                type:Date,
                default:function(){
                    return new Date();
                }
            },
            createdBy:{
                type:mongoose.Schema.Types.ObjectId,
                ref :'User'
            }
        }
    ],
    features: {
        selectedIndex : {
            type:Number
        },
        list:[
            {
                key:{
                    type:String
                },
                values:[
                    {
                        name :{
                            type:String
                        },
                        selected:{
                            type:Boolean
                        }
                    }
                ],
                selected:{
                    type:Boolean
                }
            }
        ]
    },
    rooms : [
        {
            name : {
                type:String
            },
            sizeInMeter :{
                type:Number
            },
            sizeInFeet :{
                type:Number
            },
            description:{
                type:String
            },
            photos:[
                {
                    mediaId:{
                        type:String
                    },
                    url:{
                        type:String
                    }
                }
            ]
        }
    ],
    epc : {
        EnergyEfficiencyRating:{
            current : {
                type:Number
            },
            potential : {
                type:Number
            }
        },
        EnvironmentalImpactRating:{
            current : {
                type:Number
            },
            potential : {
                type:Number
            }
        }
    },
    contract:{
        contractType : {
            type :String // For Sale, To Let
        },
        price :{
            type:Number
        },
        saleTerms:{
            type:String
        },
        rentalFrequency :{
            type: String
        },
        priceQualifier :{
            type: String
        },
        contractLength:{
            type:String
        },
        contractEndsOn:{
            type:Date
        },
        instructId :{
            type : String // Reference
        },
        commissionAndFees:{
            commissionMin : {
                type : Number
            },
            commissionMax : {
                type : Number
            },
            commissionAdditional :{
                type :Number
            },
            fee : {
                type : Number
            },
            minFee :{
                type :Number
            },
            withdrawalFee :{
                type :Number
            },
            isVatInclusive :{
                type :Boolean
            }
        },
        discount :{
            percent : {
                type :Number
            },
            fee : {
                type : Number
            },
            appliesFrom : {
                type : Date
            }
        },
        management : {
            deposit : {
                type :Number,
            },
            type : {
                type :String
            },
            percentage :{
                upfrontFee :{
                    type : Number
                },
                recurringFee : {
                    type : Number
                },
                recurringPeriod :{
                    type : String
                }
            },
            fixed : {
                upfrontFee :{
                    type : Number
                },
                recurringFee : {
                    type : Number
                },
                recurringPeriod :{
                    type : String
                }
            },
            maintenanceType :{
                type:String
            },
            overseasLandlordType :{
                type:String
            },
            availableOn : {
                type:Date
            },
            notes :{
                type:String
            }
        }
    },
    termsSent :{
        notes:{
            type:String
        },
        isTermsSent:{
            type: Boolean
        },
        termsSentId : {
            type:String // Reference No
        }
    },
    termsSigned :{
        notes:{
            type:String
        },
        isTermsSigned:{
            type: Boolean
        },
        termsSignedId : {
            type:String // Reference No
        }
    },
    detailsToVendor :{
        notes : {
            type : String
        },
        isApproved : {
            type : Boolean
        },
        approvedDate : {
            type : Date
        },
        detailsToVendorId :{
            type : String // Reference No
        }
    },
    status:{
        type:String,
        required:true,
        default:'Pre Appraisal',
        index : true // latest status will be updated here
    },
    negotiator:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    reviewDate:{
        type:Date
    },
    reviewNotes:{
        type:String
    },
    lastContactedDate:{
        type:Date
    },
    createdBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    updatedBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    },
    recordStatus:{
        type : String, // Active,Deleted,Archived
        default:'Active', // No hard delete is allowed
        index : true
    }
});

module.exports = mongoose.model('Property', Schema);
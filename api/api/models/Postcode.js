var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    postcode:{
        type: String,
        index : true
    },
    latitude:{
        type:Number
    },
    longitude:{
        type:Number
    },
    addresses:[{
        dwelling:{
            type:String
        },
        nameOrNumber:{
            type:String
        },
        street:{
            type:String
        },
        locality:{
            type:String
        },
        town:{
            type:String
        },
        county:{
            type:String
        },
        postcode:{
            type:String
        },
        country:{
            type:String
        },
        fullAddress:{
            type:String
        }
    }],
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    }
});

module.exports = mongoose.model('Postcode', Schema);
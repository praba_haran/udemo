var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    referenceNo : {
        type:String,
        required:true,
        default:function(){
            return Math.random().toString(36).substr(2, 5);
        }
    },
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency',
        index : true
    },
    cabinetLetter:{
        type:String
    },
    capacity :{
        type : Number
    },
    notes:{
        type:String
    },
    status:{
        type:String,
        required:true,
        default:'Active',//Active,Deleted
        index : true
    },
    createdBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    updatedBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    },
    recordStatus:{
        type : String, // Active,Deleted
        default:'Active', // No hard delete is allowed
        index : true
    }
});

module.exports = mongoose.model('cabinet_master', Schema);
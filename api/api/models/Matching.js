var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    _clientId: {
        type: String
    },
    index: {
        type: Number
    },
    name: {
        type: String
    },
    matchPropertyType: {
        type: String
    },
    matchBedrooms: {
        type: String
    },
    matchBathrooms: {
        type: String
    },
    matchReceptions: {
        type: String
    },
    matchParking: {
        type: String
    },
    matchFeatures: {
        type: String
    },
    minPrice: {
        type: Number
    },
    maxPrice: {
        type: Number
    },
    filters: [
        {
            name: {
                type: String
            },
            selected: {
                type: Boolean
            }
        }
    ],
    essentials: [
        {
            key: {
                type: String
            },
            values: [
                {
                    name: {
                        type: String
                    },
                    selected: {
                        type: Boolean
                    }
                }
            ]
        }
    ],
    status: {
        type: String,
        required: true,
        default: 'Active', // Pending,Active,Deleted
        index : true
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: String,
        ref: 'User'
    },
    updatedBy: {
        type: String,
        ref: 'User'
    }
});

module.exports = mongoose.model('Matching', Schema);
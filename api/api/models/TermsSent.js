var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    notes:{
        type:String
    },
    isTermsSent:{
        type: Boolean
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('TermsSent', Schema);
var mongoose = require('mongoose');

var RequirementSchema = mongoose.Schema({
    contact:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Contact',
        index : true
    },
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency',
        index : true
    },
    _clientId:{
        type: String
    },
    name:{
        type: String
    },
    intension:{
        type: String
    },
    newBuild:{
        type: String
    },
    minPrice:{
        type: Number
    },
    maxPrice:{
        type: Number
    },
    rentalFrequency:{
        type: String
    },
    furnishing:{
        type: String
    },
    bedrooms:{
        type: Number
    },
    bathrooms:{
        type: Number
    },
    receptions:{
        type: Number
    },
    parking:{
        type: Number
    },
    floorArea:{
        type: Number
    },
    floorAreaUnit:{
        type : String
    },
    landArea:{
        type: Number
    },
    landAreaUnit :{
        type: String
    },
    features:[
        {
            key:{
                type:String
            },
            values:[
                {
                    name :{
                        type:String
                    },
                    selected:{
                        type:Boolean
                    }
                }
            ],
            selected:{
                type:Boolean
            }
        }
    ],
    propertyTypes:[
        {
            key:{
                type:String
            },
            values:[
                {
                    name :{
                        type:String
                    },
                    selected:{
                        type:Boolean
                    }
                }
            ]
        }
    ],
    areas:[],
    allAreasSelected : false,
    status: {
        type: String,
        required: true,
        default: 'Active', // Pending,Active,Deleted
        index : true
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    },
    createdBy:{
        type:String,
        ref:'User'
    },
    updatedBy:{
        type:String,
        ref:'User'
    }
});

module.exports = mongoose.model('Requirement', RequirementSchema);
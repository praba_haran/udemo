var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency',
        index : true
    },
	userType:{
		type:String,
		required:true, // admin_user,agency_user
        index : true
	},
    forename: {
        type: String
    },
    surname: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String
    },
    notes: {
        type: String
    },
    status: {
        type: String,
        required: true,
        default: 'Invited', // Invited,Active,Deleted
        index : true
    },
    google_profile:{
        email :{
            type:String
        },
        family_name:{
            type:String
        },
        gender:{
            type:String
        },
        given_name:{
            type:String
        },
        hd:{
            type:String
        },
        id:{
            type:String
        },
        link:{
            type:String
        },
        locale:{
            type:String
        },
        name:{
            type:String
        },
        picture:{
            type:String
        },
        verified_email:{
            type:Boolean
        }
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    },
    createdBy:{
        type:String,
        ref:'User'
    },
    updatedBy:{
        type:String,
        ref:'User'
    }
});

module.exports = mongoose.model('User', UserSchema);
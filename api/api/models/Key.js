var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Agency'
    },
    property:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'Property'
    },
    cabinetMaster:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:'CabinetMaster'
    },
    keyNo:{
        type:String
    },
    numberOfKeys:{
        type:Number
    },
    alarmCode:{
        type:String
    },
    notes:{
        type : String
    },
    status:{
        type:String,
        required:true,
        default:'Active'//Deleted,Archived
    },
    createdBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    updatedBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref :'User'
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    recordStatus:{
        type : String, // Active,Deleted
        default:'Active' // No hard delete is allowed
    }
});

module.exports = mongoose.model('Key', Schema);
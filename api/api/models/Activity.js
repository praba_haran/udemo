var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    appointment:{
        type: mongoose.Schema.ObjectId,
        ref: 'GeneralAppointment'
    },
    appointment_ref :{
        type : {
            type : String
        },
        appraiser: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        accompaniedBy: {
            type: String
        },
        date:{
            type: Date
        },
        startTime:{
            type: Number
        },
        endTime:{
            type: Number
        },
        notes: {
            type: String
        },
        additionalNotes:{
            type:String
        },
        reminder: {
            type: String
        },
        reminderChannel: {
            type: String
        },
        status: {
            type: String//Booked,Cancelled
        },
        properties:[
            {
                property : {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Property'
                }
            }
        ],
        applicants:[
            {
                contact : {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Contact'
                }
            }
        ]
    },
    viewing :{
        type : mongoose.Schema.ObjectId,
        ref : 'Viewing'
    },
    viewing_ref:{
        property: {
            type: mongoose.Schema.ObjectId,
            ref: 'Property'
        },
        appraiser: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        accompaniedBy: {
            type: String
        },
        date: {
            type: Date
        },
        startTime: {
            type: Number
        },
        endTime: {
            type: Number
        },
        notes: {
            type: String
        },
        additionalNotes: {
            type: String
        },
        reminder: {
            type: String
        },
        reminderChannel: {
            type: String
        },
        status: {
            type: String//Active,Cancelled
        },
        applicants: [
            {
                contact: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'Contact'
                }
            }
        ],
        vendorConfirmed: {
            type: Boolean
        },
        applicantsConfirmed: {
            type: Boolean
        }
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Activity', Schema);
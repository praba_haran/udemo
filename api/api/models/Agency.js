var mongoose = require('mongoose');

var AgencySchema = mongoose.Schema({
    name:{
        type:String,
        required:true,
        index : true
    },
    referenceNo : {
        type:String,
        required:true,
        default:function(){
            return Math.random().toString(36).substr(2, 5);
        }
    },
    companyNo:{
        type:String,
        required:true
    },
    vatRegNo:{
        type:String,
        required:true
    },
    checquePayee:{
        type:String
    },
    invoiceDueDays:{
        type:Number
    },
    address:{
        dwelling:{
            type:String
        },
        nameOrNumber:{
            type:String
        },
        street:{
            type:String
        },
        locality:{
            type:String
        },
        town:{
            type:String
        },
        county:{
            type:String
        },
        postcode:{
            type:String
        }
    },
    phoneNo:{
        type:String
    },
    email: {
        type: String
    },
    website:{
        type:String
    },
    settings:{
        brochures:[
            {
                template :{
                    type:String
                },
                isDefault:{
                    type : Boolean
                }
            }
        ],
        areas:[]
    },
    status:{
        type:String,
        required:true,
        default:'Pending',//pending,active,inactive,deleted
        index : true
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    }
});

module.exports = mongoose.model('Agency', AgencySchema);
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    modelType:{
        type : String
    },
    modelId:{
        type : mongoose.Schema.ObjectId
    },
    activityType : {
        type : String
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('branch_activity', Schema);
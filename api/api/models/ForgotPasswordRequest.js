var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    expireAt: {
        type: Date,
        required: true,
        index : true
    },
    isUsed : {
        type : Boolean
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        }
    },
    updatedAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    }
});

module.exports = mongoose.model('forgot_password_request', UserSchema);
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    recipients:[
        {
            contact :{
                type: mongoose.Schema.ObjectId,
                required: true,
                ref: 'Contact'
            },
            email : {
                subject : {
                    type :String
                },
                to :{
                    type:String
                },
                message :{
                    type :String
                },
                isSent :{
                    type:Boolean
                }
            },
            letter :{
                message :{
                    type : String
                }
            }
        }
    ],
    notes :{
        type :String
    },
    isApproved :{
        type : Boolean
    },
    approvedDate : {
        type : Date
    },
    selectedIndex:{
        type: Number
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('details_to_vendor', Schema);
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    type : {
        type : String,
        index : true
    },
    appraiser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        index : true
    },
    accompaniedBy: {
        type: String
    },
    date:{
        type: Date
    },
    startTime:{
        type: Number
    },
    endTime:{
        type: Number
    },
    notes: {
        type: String
    },
    additionalNotes:{
        type:String
    },
    reminder: {
        type: String
    },
    reminderChannel: {
        type: String
    },
    status: {
        type: String,//Active,Cancelled
        index : true
    },
    properties:[
        {
            property : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Property'
            },
            createdAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updatedAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            updatedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    applicants:[
        {
            contact : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Contact'
            },
            feedback:{
                type:String
            },
            createdAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updatedAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            updatedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('GeneralAppointment', Schema);
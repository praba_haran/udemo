var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    notes: {
        type: String
    },
    applicants: [
        {
            contact: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Contact'
            },
            createdAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('file_notes', Schema);
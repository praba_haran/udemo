var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    price:{
        type:Number,
        required: true,
        index : true
    },
    priceInPercent:{
        type:Number,
        required: true
    },
    aboutAnOffer: {
        type: String
    },
    chainNotes: {
        type: String
    },
    fixtureAndFittingsNotes: {
        type: String
    },
    mortgageAndFinancialNotes: {
        type: String
    },
    estimatedDates:{
        type:String
    },
    accepted:{
        type:Boolean
    },
    rejected:{
        type:Boolean
    },
    applicants:[
        {
            contact : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Contact'
            },
            feedback:{
                type:String
            },
            createdAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updatedAt :{
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            updatedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    updateApplicantLastContactedDate:{
        type: Boolean
    },
    updateVendorLastContactedDate:{
        type:Boolean
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: String,//Received,Accepted,Rejected
        default: function () {
            return 'Received'
        },
        index : true
    }
});

module.exports = mongoose.model('Offer', Schema);
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    oldStatus:{
        type: String,
        index : true
    },
    status: {
        type: String,
        index : true
    },
    notes: {
        type: String
    },
    actions:{
        updateLastContactedDate : {
            type : Boolean
        },
        requireBoardChange : {
            type:Boolean
        }
    },
    boardChangeRequest:{
        currentBoard:{
            type:String
        },
        newBoard:{
            type:String
        },
        boardContractor :{
            type:String
        },
        message:{
            type:String
        },
        includeOwnerInMail:{
            type:Boolean
        }
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('PropertyStatusChange', Schema);
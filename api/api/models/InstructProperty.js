var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    contractType :{
        type : String
    },
    price:{
        type: Number
    },
    saleTerms: {
        type: String
    },
    rentalFrequency :{
        type: String
    },
    priceQualifier :{
        type: String
    },
    contractLength: {
        type: String
    },
    contractEndsOn:{
        type:Date
    },
    notes :{
        type:String
    },
    actions:{
        requireBoardChange : {
            type:Boolean
        }
    },
    boardChangeRequest:{
        currentBoard:{
            type:String
        },
        newBoard:{
            type:String
        },
        boardContractor :{
            type:String
        },
        message:{
            type:String
        },
        includeOwnerInMail:{
            type:Boolean
        }
    },
    commissionAndFees:{
        commissionMin : {
            type : Number
        },
        commissionMax : {
            type : Number
        },
        commissionAdditional :{
            type :Number
        },
        fee : {
            type : Number
        },
        minFee :{
            type :Number
        },
        withdrawalFee :{
            type :Number
        },
        isVatInclusive :{
            type :Boolean
        }
    },
    discount :{
        percent : {
            type :Number
        },
        fee : {
            type : Number
        },
        appliesFrom : {
            type : Date
        }
    },
    management : {
        deposit : {
            type :Number,
        },
        type : {
            type :String
        },
        percentage :{
            upfrontFee :{
                type : Number
            },
            recurringFee : {
                type : Number
            },
            recurringPeriod :{
                type : String
            }
        },
        fixed : {
            upfrontFee :{
                type : Number
            },
            recurringFee : {
                type : Number
            },
            recurringPeriod :{
                type : String
            }
        },
        maintenanceType :{
            type:String
        },
        overseasLandlordType :{
            type:String
        },
        availableOn : {
            type:Date
        },
        notes :{
            type:String
        }
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('InstructProperty', Schema);
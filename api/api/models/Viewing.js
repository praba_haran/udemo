var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    appraiser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    accompaniedBy: {
        type: String
    },
    date: {
        type: Date
    },
    startTime: {
        type: Number
    },
    endTime: {
        type: Number
    },
    isConfirmed :{
        type : Boolean // (vendorConfirmed && applicantsConfirmed)
    },
    vendorConfirmed: {
        type: Boolean
    },
    applicantsConfirmed: {
        type: Boolean
    },
    // FOR TRACKING
    history:[
        {
            appraiser: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            accompaniedBy: {
                type: String
            },
            date :{
                type : Date
            },
            startTime: {
                type: Number
            },
            endTime: {
                type: Number
            },
            vendorConfirmed: {
                type: Boolean
            },
            applicantsConfirmed: {
                type: Boolean
            },
            createdAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    notes: {
        type: String
    },
    additionalNotes: {
        type: String
    },
    reminder: {
        type: String
    },
    reminderChannel: {
        type: String
    },
    status: {
        type: String,//Booked,Cancelled
        index : true
    },
    applicants: [
        {
            contact: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Contact'
            },
            feedback: {
                type: String
            },
            rating : {
                type : String //ThumbUp,ThumbDown
            },
            onConfirmation : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                smsMessage : {
                    type: String
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            onRescheduled : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                smsMessage : {
                    type: String
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            onCancelled : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                smsMessage : {
                    type: String
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            createdAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updatedAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            updatedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            status :{
                type : String, // Active,Removed
                default: 'Active'
            },
            cancelledReason :{
                type : String
            }
        }
    ],
    owners: [
        {
            contact: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Contact'
            },
            onConfirmation : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                smsMessage : {
                    type: String
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            onRescheduled : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                smsMessage : {
                    type: String
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            onCancelled : {
                emailMessage: {
                    line1: {
                        type: String
                    },
                    line2: {
                        type: String
                    },
                    line3: {
                        type: String
                    }
                },
                isEmailDelivered : {
                    type : Boolean
                },
                deliveredAt : {
                    type: Date
                },
                smsMessage : {
                    type: String
                },
                createdAt: {
                    type: Date,
                    default: function () {
                        return new Date();
                    }
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            },
            createdAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updatedAt: {
                type: Date,
                default: function () {
                    return new Date();
                }
            },
            updatedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            status :{
                type : String, // Active, There is no other status.
                default: 'Active'
            }
        }
    ],

    confirmationEmailSent :{
        type :Boolean
    },
    confirmationMailSentAt :{
        type : Date
    },
    reScheduledEmailSent : {
        type :Boolean
    },
    reScheduledEmailSentAt :{
        type : Date
    },
    cancellationEmailSent :{
        type : Boolean
    },
    cancellationEmailSentAt:{
        type : Date
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Viewing', Schema);
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
	login_via:{
		type:String,
		required:true // app,google,outlook
	},
    profile:{
        email :{
            type:String
        },
        family_name:{
            type:String
        },
        gender:{
            type:String
        },
        given_name:{
            type:String
        },
        hd:{
            type:String
        },
        id:{
            type:String
        },
        link:{
            type:String
        },
        locale:{
            type:String
        },
        name:{
            type:String
        },
        picture:{
            type:String
        },
        verified_email:{
            type:Boolean
        },
        token:{
            access_token:{
                type:String
            },
            client_id :{
                type:String
            },
            cookie_policy : {
                type:String
            },
            g_user_cookie_policy:{
                type:String
            },
            expires_at:{
                type:String
            },
            expires_in:{
                type:String
            },
            issued_at:{
                type:String
            },
            response_type:{
                type:String
            },
            scope:{
                type:String
            },
            state:{
                type:String
            },
            token_type:{
                type:String
            },
            status:{
                google_logged_in:{
                    type:String
                },
                method:{
                    type:String
                },
                signed_in:{
                    type:String
                }
            }
        }
    },
    createdAt:{
        type:Date,
        default:function(){
            return new Date();
        },
        index : true
    }
});

module.exports = mongoose.model('UserLogin', Schema);
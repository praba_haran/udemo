var mongoose = require('mongoose');

var Schema = mongoose.Schema({
    agency: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Agency',
        index : true
    },
    property: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'Property',
        index : true
    },
    type: {
        type: String
    },
    leadSource: {
        type: String
    },
    appraiser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    accompaniedBy: {
        type: String
    },
    date:{
        type: Date
    },
    startTime:{
        type: Number
    },
    endTime:{
        type: Number
    },
    notes: {
        type: String
    },
    reminder: {
        type: String
    },
    reminderChannel: {
        type: String
    },
    status: {
        type: String,//Active,Cancelled
        index : true
    },
    updateLastContactedDate:{
        type: Boolean
    },
    createdAt: {
        type: Date,
        default: function () {
            return new Date();
        }
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: function () {
            return new Date();
        },
        index : true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('MarketAppraisal', Schema);
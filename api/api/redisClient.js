var redisClient = require('redis').createClient;
var redis = null;

function connect (){
    redis = redisClient({
        host:'redis-18809.c10.us-east-1-4.ec2.cloud.redislabs.com',
        port:18809,
        password:'ultima-plus'
    });
    redis.on('connect',function(){
        console.log('redis server connected.');
    });
    redis.on('error',function(){
        console.log('Could not establish connection to redis server.');
    });
    redis.on('end',function(){
       console.log('Redis connection ended.');
    });
}

module.exports = {
    connect : connect,
    init : function(cb){
        if(!redis){
            throw new Error('Could not establish connection to redis server.');
        }
        else{
            if(cb) cb(redis);
        }
    }
};
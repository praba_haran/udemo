var _ = require('lodash');
var fs = require('fs');
var fse = require('fs-extra');
var path = require('path');
var async = require('async');
var extract = require('extract-zip');
var replaceall = require("replaceall");
var https = require('https');
var Stream = require('stream').Transform;
var archiver = require('archiver');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var wordToPDF = require('./wordToPdf')();

var Property = require('../models/Property');

module.exports.loadBrochure = function(opt,callback){
    var t0 = new Date().getTime();
    console.log('load brochure');
    var propertyId = opt.propertyId;
    var templateId = opt.templateId;
    var agencyId = opt.agencyId;
    var userId = opt.userId;
    var property = null;
    var useCache = opt.cache==='true';

    var config = require('../../data/brochure-templates-docx/'+agencyId+'/config')[templateId];
    var templateDir = path.resolve(__dirname,'../../data/brochure-templates-docx',agencyId,templateId);
    var templateDirDoc = templateDir+'.docx';
    var templateDirZip = templateDir+'.zip';
    var templateDirDocumentXml = templateDir + '/word/document.xml';

    var outputDir = path.resolve(__dirname, '../../data/brochure-templates',agencyId,userId, templateId);
    var outputDirDocumentXml = outputDir+'/word/document.xml';
    var outputDirDoc = outputDir+'-'+propertyId+'-'+userId+'.docx';
    var outputDirPdf = outputDir+'-'+propertyId+'-'+userId+'.pdf';

    if(useCache && fs.existsSync(outputDirPdf)){
        callback(outputDirPdf);
    }
    else {
        async.parallel([getPropertyById, extractTemplateDocFile], function (err, data) {
            if (!err) {
                property = data[0];
                async.waterfall([bindImages, preparingDocumentXml, preparingDocFile, docToPdf], function (err, data) {
                    if (!err) {
                        callback(outputDirPdf);
                    }
                    else{
                        console.log('err',err);
                    }
                });
            }
        });
    }

    // parallel execution
    function getPropertyById(cb){
        console.log('get property');
        var query = Property.findById(propertyId);
        query.populate('agency','email phoneNo website');

        query.lean().exec().then(function (result) {
            cb(null, result);
        }, function (err) {
            cb(err, null);
        });
    }

    // parallel execution
    function extractTemplateDocFile(cb){
        console.log('extract template file');
        if(fs.existsSync(templateDir)) {
            fse.copySync(templateDir,outputDir);
            cb(null);
        }
        else {
            fse.copySync(templateDirDoc,templateDirZip);
            extract(templateDirZip, {dir: templateDir}, function (err) {
                if (!err) {
                    fs.unlinkSync(templateDirZip);
                    processDocumentXML(function () {
                        fse.copySync(templateDir,outputDir);
                        cb(null);
                    });
                }
                else {
                    cb(err);
                }
            });
        }
    }

    // after extracting doc file, do pre process document.xml
    function processDocumentXML(cb){

        var data = fs.readFileSync(templateDirDocumentXml).toString();
        var xmlDoc = new dom().parseFromString(data);

        var select = xpath.useNamespaces({"w": "http://schemas.openxmlformats.org/wordprocessingml/2006/main"});
        var nodes = select('//w:t//text()[contains(., "loop_through")]', xmlDoc);

        _.each(nodes,function(currentNode){
            var elementName = replaceall('[','',currentNode.data);
            elementName = replaceall(']','',elementName);

            var foundParagraph = false;
            currentNode = currentNode.parentNode;
            while(!foundParagraph){
                if(currentNode.tagName==='w:p'){
                    foundParagraph = true;
                }
                else{
                    currentNode = currentNode.parentNode;
                }
            }
            // write this piece of xml into loop_through_[tagName].xml
            fs.writeFileSync(templateDir + '/word/loop_through_features.xml',currentNode.toString());
            var parentNode = currentNode.parentNode;
            currentNode.insertBefore(currentNode.ownerDocument.createElement(elementName),currentNode);
            parentNode.removeChild(currentNode);
        });
        fs.writeFileSync(templateDirDocumentXml,xmlDoc.toString());
        cb();
    }

    //binding property images
    function bindImages(cb){
        console.log('bind images');
        var photos = property.photos;
        if(config && config.images) {
            _.each(config.images, function (image, index) {
                if (photos[index] && photos.length > index) {
                    var imagePath = outputDir + '/word/media/' + image;
                    downloadImage(photos[index].url, imagePath);
                }
            });
        }
        cb(null);
    }

    function downloadImage(url,destPath){
        https.request(url, function(response) {
            var data = new Stream();

            response.on('data', function(chunk) {
                data.push(chunk);
            });

            response.on('end', function() {
                fs.writeFileSync(destPath,data.read());
            });
        }).end();
    }

    // processing xml document
    function preparingDocumentXml(cb){
        console.log('preparing document xml');
        fs.readFile(outputDirDocumentXml, 'utf8', function (err,data) {
            if (err) {
                cb(err);
            }
            else {
                var result = replaceall('[[address.town]]',property.address.town,data);
                result = replaceall('[[address.fullAddress]]',property.address.fullAddress,result);
                result = replaceall('[[address.street]]',property.address.street,result);
                result = replaceall('[[address.county]]',property.address.county,result);
                if(property.agency.phoneNo) {
                    result = replaceall('[[agency.phoneNo]]', property.agency.phoneNo, result);
                }
                if(property.agency.email) {
                    result = replaceall('[[agency.email]]', property.agency.email, result);
                }
                if(property.agency.website) {
                    result = replaceall('[[agency.website]]', property.agency.website, result);
                }

                if(property.price){
                    result = replaceall('[[price]]',property.price.toString(),result);
                }
                else if(property.proposedPrice){
                    result = replaceall('[[price]]',property.proposedPrice.toString(),result);
                }
                if(property.priceQualifier){
                    result = replaceall('[[priceQualifier]]',property.priceQualifier,result);
                }
                if(property.summary){
                    result = replaceall('[[summary]]',property.summary,result);
                }

                if(fs.existsSync(outputDir+'/word/loop_through_features.xml')){
                    var template = fs.readFileSync(outputDir+'/word/loop_through_features.xml').toString();
                    if(template && property.features && property.features.list.length>0){
                        var outTemplate = '';

                        var list = property.features.list;
                        _.each(list,function(item){
                            var values = _.filter(item.values,function(p){
                                return p.selected;
                            });
                            if(values){
                                _.each(values,function(value){
                                    outTemplate += replaceall('[[loop_through_features]]',value.name,template);
                                });
                            }
                        });
                        result = replaceall('<loop_through_features/>',outTemplate,result);
                    }
                }

                fs.writeFile(outputDirDocumentXml, result, 'utf8', function (err) {
                    if (err) {
                        cb(err);
                    }
                    else {
                        cb(null);
                    }
                });
            }
        });
    }

    // preparing doc file
    function preparingDocFile(cb){
        console.log('preparingDocFile');
        var dirNames = ['_rels','customXml','docProps','word'];
        var file = fs.createWriteStream(outputDirDoc);
        var archive = archiver('zip', {});
        file.on('close', function() {
            cb(null);
        });
        archive.on('error', function(err) {
            cb(err);
        });
        archive.pipe(file);
        archive.append(fs.readFileSync(outputDir+'/[Content_Types].xml'), { name : '[Content_Types].xml'});
        dirNames.forEach(function(dirName) {
            if(fs.existsSync(path.join(outputDir,dirName))) {
                archive.directory(path.join(outputDir, dirName), dirName);
            }
        });
        archive.finalize();
    }

    // doc to pdf conversion
    function docToPdf(cb){
        console.log('docToPdf');
        wordToPDF.getInstance()(null,function(err,office){
            if(err){
                cb(err);
            }
            else{
                office.word({
                    input: outputDirDoc,
                    output: outputDirPdf
                }, function (err, pdf) {
                     var t1 = new Date().getTime();
                     console.log("Pdf generated in " + (t1 - t0) + " milliseconds.");
                    if (err) {
                        cb(err,null);
                    } else {
                        fs.unlink(outputDirDoc);
                        fse.remove(outputDir);
                        cb(null);
                    }
                });
            }
        });
    }
};





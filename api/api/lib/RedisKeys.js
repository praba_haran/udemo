
module.exports = {

    agentPropertiesKey : function(agentId){
        return 'RECENT_PROPERTIES_'+agentId;
    },

    agentPropertyKey : function (propertyId) {
        return 'PROPERTY_BY_ID_'+propertyId;
    },
    allContactsKey : function(agentId){
        return 'ALL_CONTACTS'+agentId;
    },
    allSolicitorsKey : function (agentId) {
        return 'ALL_SOLICITORS'+agentId;
    },
    contactKey : function (id) {
        return 'CONTACT_'+id;
    },
    allKeySetsKey : function(agentId){
        return 'ALL_KEY_SETS_'+agentId;
    },
    keySetKey : function(id){
        return 'KEY_SET_'+id;
    },

    agentKey :function(id){
        return 'AGENCY_'+id;
    },

    agentUserKey : function(id){
        return 'AGENT_USER_'+id;
    },

    // ma
    getMarketAppraisalsByAgent : function(agentId){
        return 'MA_AGENT_'+agentId;
    },
    getMarketAppraisalsByProperty :function(id){
        return 'MA_PROPERTY_'+id;
    },
    getMarketAppraisalById : function (id) {
        return 'MA_BY_ID_'+id;
    }
};
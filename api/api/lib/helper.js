var moment = require('moment');
var timeNumber = require('time-number');

function toTime(time){
    var ret = timeNumber.timeFromInt(time, false);

    var found = ret.match(/^(\d+):/);
    var hour  = parseInt(found[1], 10);

    if (hour === 0) {
        return `${ret.replace(/^\d+/, '12')} AM`;
    } else if (hour < 12) {
        return `${ret} AM`;
    } else if (hour === 12) {
        return `${ret} PM`;
    }

    var newHour = hour < 22 ? `0${hour - 12}` : (hour - 12).toString();

    return `${ret.replace(/^\d+/, newHour)} PM`;
};

function getHourMinutesFromTime(time){
    let hour = time/3600;
    let obj = {
        hour : 0,
        minutes : 0
    };

    if(hour % 1 === 0){
        obj.hour = hour;
        obj.minutes = 0;
    }
    else {
        obj.hour = hour-0.5;
        obj.minutes = (hour-(hour-0.5)) * 60;
    }
    return obj;
};
function combineDateAndTime(date,time){

    var hourMinutes = getHourMinutesFromTime(time);

    return moment(date).set({
        hour : hourMinutes.hour,
        minute: hourMinutes.minutes,
        second : 0,
        millisecond : 0
    }).toDate();
};

function formatDateTime(date){
    return moment(date).format('DD/MM/YYYY h:mm a');
};

function formatDate(date){
    return moment(date).format('DD/MM/YYYY');
};

module.exports.toTime  = toTime;
module.exports.getHourMinutesFromTime = getHourMinutesFromTime;
module.exports.combineDateAndTime = combineDateAndTime;
module.exports.formatDateTime = formatDateTime;
module.exports.formatDate = formatDate;
var path = require('path');

// var references = [
//     'C:\\Windows\\assembly\\GAC_MSIL\\Microsoft.Office.Interop.Word\\15.0.0.0__71e9bce111e9429c\\Microsoft.Office.Interop.Word.dll',
//     'C:\\Windows\\assembly\\GAC_MSIL\\Microsoft.Office.Interop.Excel\\15.0.0.0__71e9bce111e9429c\\Microsoft.Office.Interop.Excel.dll',
//     'C:\\Windows\\assembly\\GAC_MSIL\\Microsoft.Office.Interop.PowerPoint\\15.0.0.0__71e9bce111e9429c\\Microsoft.Office.Interop.PowerPoint.dll',
//     'C:\\Windows\\assembly\\GAC_MSIL\\Office\\15.0.0.0__71e9bce111e9429c\\Office.dll',
//     'C:\\Windows\\assembly\\GAC_MSIL\\Microsoft.Vbe.Interop\\15.0.0.0__71e9bce111e9429c\\Microsoft.Vbe.Interop.dll'
// ];

console.log('dll path=>',path.join(__dirname, '/dlls/Microsoft.Office.Interop.Word.dll'));

module.exports = function(){
    var edge = require('edge');

    var _instance = null;    
    
    return {
        getInstance : function(){
            if(_instance===null){
                _instance = edge.func({
                    source: path.join(__dirname, 'office.cs'),
                    references: [
                        path.join(__dirname, '/dlls/Microsoft.Office.Interop.Word.dll'),
                        path.join(__dirname, '/dlls/Microsoft.Office.Interop.Excel.dll'),
                        path.join(__dirname, '/dlls/Microsoft.Office.Interop.PowerPoint.dll'),
                        path.join(__dirname, '/dlls/Office.dll'),
                        path.join(__dirname, '/dlls/Microsoft.Vbe.Interop.dll')
                    ]
                });
            }
            return _instance;
        }
    };
    //return wordToPDF;
};
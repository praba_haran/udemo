var path = require('path');
var EmailTemplate = require('email-templates').EmailTemplate;
var Handlebars = require('handlebars');
var nodeMailer = require('nodemailer');
var wellKnown = require('nodemailer-wellknown');
var config = require('../../config');

Handlebars.registerHelper('capitalize', function capitalize (context) {
    return context.toUpperCase()
});

module.exports.sendEmail = function(options,model,callback){
    var templateDir = path.join(__dirname,'..','..','email-templates', 'templates', options.template);
    var newsletter = new EmailTemplate(templateDir);

    model.META =  {
        APP_NAME : 'Ultima Plus',
        APP_BASE_URL : config.APP_BASE_URL,
        LOGO_URL : config.APP_BASE_URL+'/dist/images/logo.png'
    };

    newsletter.render(model,function(err,result){
        if(err){
            callback(err,null);
        }
        else{
            var transport = nodeMailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'ultimaplus2017@gmail.com',
                    pass: 'jOOthi1988'
                }
            });
            // constructing config to send mail
            var config = {
                from: 'Ultima Plus <ultimaplus2017@gmail.com>',
                to: options.email,
                subject: options.subject,
                html: result.html,
                text: result.text
            };
            if(options.from){
                config.from = options.from;
            }
            if(options.replyTo){
                config.replyTo = options.replyTo;
            }

            transport.sendMail(config, function (err, data) {
                if (err) {
                    callback(err,null);
                }
                else {
                    callback(null,data);
                }
            });
        }
    });
};
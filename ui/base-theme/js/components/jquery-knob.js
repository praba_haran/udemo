/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("knob", {
  mode: "default",
  defaults: {
    min: 25000,
    max: 50000,
    width: 200,
    height: 200,
    thickness: ".1"
  }
});

var webpack = require('webpack');

var config = {
    devtool:'eval',
    entry: [
        'babel-polyfill',
        './src/app/index.js'
    ],

    output: {
        path:'./dist/js/',
        filename: './bundle.js',
    },

    devServer: {
        inline: true,
        port: 656
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                include : /src/,
                loader: 'babel',
                plugins: [
                    "transform-es3-member-expression-literals",
                    "transform-es3-property-literals",
                    "transform-es2015-parameters"
                ],
                query: {
                    presets: ['es2015','stage-0', 'react']
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env':{
                'NODE_ENV': JSON.stringify('development')
            }
        })
    ]
};

module.exports = config;
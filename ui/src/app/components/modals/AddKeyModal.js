import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import Typeahead from 'react-bootstrap-typeahead';
import { ValidateText, ValidateTextArea } from '../shared/Form';

class AddKeyModal extends React.Component{

    render(){
        let { keySet, validator, onChange, canShowModal,cancel,onSubmit, isProcessing } = this.props;
        return (
            <Modal bsSize="lg" show={canShowModal} onHide={cancel} dialogClassName="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>New Key Set</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-lg-6">
                            <form className="form-horizontal">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-3 col-sm-8">CHOOSE PROPERTY</h6>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-6">
                            <form className="form-horizontal">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-3 col-sm-8">GENERAL</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Key Set</label>
                                    <ValidateText className="col-sm-7" model={keySet} name="keySet.keyCode" onChange={onChange} validator={validator} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Total Keys</label>
                                    <ValidateText className="col-sm-7" model={keySet} name="keySet.numberOfKeys" onChange={onChange} validator={validator} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Alarm Code</label>
                                    <ValidateText className="col-sm-7" model={keySet} name="keySet.alarmCode" onChange={onChange} validator={validator} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Description</label>
                                    <ValidateTextArea rows="5" className="col-sm-7" model={keySet} name="keySet.description" onChange={onChange} validator={validator} />
                                </div>
                            </form>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={onSubmit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default AddKeyModal;
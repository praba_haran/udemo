import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import Typeahead from 'react-bootstrap-typeahead';
import { ValidateText, ValidateTextArea } from '../shared/Form';

class AddKeyActivityModal extends React.Component{

    render(){
        let { keySet, validator, onChange, canShowModal,cancelActivity,onSubmit, isProcessing } = this.props;
        let index = 0;
        if(keySet.activities.length>1){
            _.each(keySet.activities,function(p,i){
               if(!p._id){
                   index = i;
               }
            });
        }
        return (
            <Modal bsSize="lg" show={canShowModal} onHide={cancelActivity} dialogClassName="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>New Activity</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-lg-6">
                            <form className="form-horizontal">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-3 col-sm-8">KEY HOLDER</h6>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-6">
                            <form className="form-horizontal">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-3 col-sm-8">RECEIVER</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Key Holder</label>
                                    <ValidateText className="col-sm-7" model={keySet} name={'keySet.activities['+index+'].keyHolder'} onChange={onChange} validator={validator} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-3 control-label">Recipient</label>
                                    <ValidateText className="col-sm-7" model={keySet} name={'keySet.activities['+index+'].recipient'} onChange={onChange} validator={validator} />
                                </div>
                            </form>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancelActivity}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={onSubmit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default AddKeyActivityModal;
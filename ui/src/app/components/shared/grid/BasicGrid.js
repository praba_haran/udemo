import React from 'react';
import _ from 'lodash';
import { ajax } from '../../../api/axios';

export default class BasicGrid extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            pageIndex : 1,
            pageSize : 10,
            count : 0,
            items : [],
            searchText : '',
            params : [],

            isLoading: false,
            loadingText: 'Please wait...'
        };
    }

    componentDidUpdate(nextProps, nextState){
        let obj1 = {
            pageIndex : this.state.pageIndex,
            pageSize : this.state.pageSize,
            searchText : this.state.searchText
        };
        let obj2 = {
            pageIndex : nextState.pageIndex,
            pageSize : nextState.pageSize,
            searchText : nextState.searchText
        };
        if(!_.isEqual(obj1,obj2)){
            this.load();
        }
    }

    setUrl(url){
        this.url = url;
        return this;
    }

    refresh(){
        this.setState({
            pageIndex : 1
        });
    }

    load(){
        let data = {
            pageIndex : this.state.pageIndex,
            pageSize : this.state.pageSize,
            searchText : this.state.searchText
        };
        if (this.state.params.length > 0) {
            _.forEach(this.state.params, function (x) {
                data[x.key] = x.value;
            });
        }
        this.startLoader();
        ajax({
            method : 'get',
            url: this.url,
            data: data
        }).done((result)=>{
            if(result.success){
                let data = result.data;
                this.setState({
                    items : data.items,
                    count : data.count
                });
            }
            this.stopLoader();
        });
    }

    getStartIndex(){
        return ((this.state.pageIndex - 1) * this.state.pageSize) + 1;
    }

    getEndIndex(){
        let index = (this.state.pageIndex * this.state.pageSize);
        if (index < this.state.count) {
            return index;
        }
        return this.state.count;
    }

    paginate(){
        this.setState({
            pageIndex : 1
        });
        this.load();
    }

    next(){
        this.setState({
            pageIndex : this.state.pageIndex + 1
        });
    }

    prev(){
        this.setState({
            pageIndex : this.state.pageIndex - 1
        });
    }

    onPageSizeChange(e){
        let { count } = this.state;
        let pageSize = parseInt(e.target.value);
        let newStartIndex = ((this.state.pageIndex - 1) * pageSize) + 1;
        if(newStartIndex > count){
            this.setState({
                pageSize: pageSize,
                pageIndex : 1
            });
        }
        else {
            this.setState({
                pageSize: pageSize
            });
        }
    }

    startLoader(){
        let oldLoadingText = this.state.loadingText;
        this.setState({
            isLoading : true
        });

        setTimeout(()=> {
            this.setState({
                loadingText : this.state.isLoading?'Request is taking bit longer than expected ...':oldLoadingText
            });
        }, 1000);
    }
    stopLoader(){
        this.setState({
            isLoading : false,
        });
    }

    onSearchTextChange(e){
        let value = e.target.value;
        this.setState({
            searchText : value
        });
    }
    
    setParam(key,value){
        let { params } = this.state;
        let param = _.find(params, function (x) {
            return x.key === key;
        });
        if (param) {
            param.value = value;
        }
        else {
            params.push({ key: key, value: value });
        }
        this.setState({
            params : params
        });
        return this;
    }

    removeParam(key){
        let { params } = this.state;
        let index = _.findIndex(params, function (x) {
            return x.key === key;
        });
        if (index>-1) {
            params.splice(index,1);
        }
        this.setState({
            params : params
        });
        return this;
    }

    clearParams(){
        this.setState({
            params : []
        });
        return this;
    }

    setPageIndex(index){
        this.setState({
            pageIndex : index
        });
        return this;
    }

    /* render helpers */

    getPageSummary(){
        let { count } = this.state;
        if(count>0) {
            return (
                <span>
                Showing {this.getStartIndex()} - {this.getEndIndex()} of {count} items
            </span>
            );
        }
        return null;
    }

    renderNoRecord(options){
        let { isLoading,loadingText, items } = this.state;
        if(items.length === 0 && !isLoading){
            return (
                <tr className="no-record-found">
                    <td className="text-center" colSpan={options.colSpan}>{options.noRecordText}</td>
                </tr>
            );
        }
        return null;
    }

    renderFooter(){
        let { count, items, pageIndex, pageSize } = this.state;
        if(count===0){
            return null;
        }
        return (
            <div className="row footer">
                <div className="col-sm-1">
                    <select className="input-sm form-control" value={pageSize} onChange={this.onPageSizeChange.bind(this)}>
                        <option value="2">2</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
                <div className="col-sm-9 page-summary">
                    {this.getPageSummary()}
                </div>
                <div className="col-sm-2 text-right">
                    <div className="btn-group margin-right-5">
                        <button type="button" onClick={this.refresh.bind(this)} className="btn btn-sm">
                            <i className="icon fa-refresh"></i>
                        </button>
                    </div>
                    <div className="btn-group">
                        <button type="button" onClick={this.prev.bind(this)} disabled={(items.length=== count || pageIndex===1)} className="btn btn-sm">
                            <i className="icon fa-angle-left"></i>
                        </button>
                        <button type="button" onClick={this.next.bind(this)} disabled={this.getEndIndex()=== count} className="btn btn-sm">
                            <i className="icon fa-angle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    /* render helpers */
}
import React from 'react';
import scriptLoader from 'react-async-script-loader';

export default function(Component){
    return scriptLoader(
        [
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyD-yv-f-OdQmTT-OFOQXKAjdV-_qNZ5ndY"
        ]
    )(Component);
}

const masks = {

    postcode:{
        mask : [/[a-zA-Z]/,/[a-zA-Z]/,/[0-9]/,' ',/[0-9]/,/[a-zA-Z]/,/[a-zA-Z]/],
        pipe : function (conformedValue,config) {
            return conformedValue.toUpperCase();
        }
    },
    percentage:{
        mask : [/[0-9]/,/[0-9]/,/[0-9]/],
        //mask : [/^(\d\d?(\.\d\d?)?|100(\.00?)?)$/],
        pipe : function (conformedValue,config) {

            if(conformedValue==='0'){
                return '';
            }
            else if(conformedValue!==''){
                return conformedValue+' %';
            }
            return conformedValue;
        }
    }
};

export default masks;
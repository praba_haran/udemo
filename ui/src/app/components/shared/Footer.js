import React from 'react';

export default class Footer extends React.Component{

    render(){
        let { userInfo } = this.props;
        return (
            <footer className="site-footer">
                <div className="site-footer-legal">&copy; 2016 <a href="#">Ultima</a></div>
                {userInfo?(
                    <div className="site-footer-right">
                        Licenced to <a href="#">{userInfo.agency.name}</a>
                    </div>
                ):null}
            </footer>
        );
    }
}
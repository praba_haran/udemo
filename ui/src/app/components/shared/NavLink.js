import React from 'react';
import {Link} from "react-router";

export default React.createClass({
    contextTypes: {
        router: React.PropTypes.object
    },

    render: function () {
        let isActive = this.context.router.isActive(this.props.to, true),
            className = isActive ? "active" : "";

        if(!isActive && !this.props.disableContainsCheck){
            let hashArray = window.location.hash.split('?');
            if(window.location.hash.indexOf(this.props.to)>-1){
                className = 'active';
            }
        }
        return (
            <li className={className}>
                <Link to={this.props.to}>
                    {this.props.children}
                </Link>
            </li>
        );
    }
});
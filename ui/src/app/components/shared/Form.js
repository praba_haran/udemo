import React from 'react';
import _ from 'lodash';
import uuid from 'uuid';
import moment from 'moment';
import accounting from 'accounting';
import dropDownValues from './dropdown-values';
import RcDatePicker from 'rc-calendar/lib/Picker';
import Calendar from 'rc-calendar';
import masks from './text-masks';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { SketchPicker,BlockPicker } from 'react-color';
import BootstrapTimePicker from 'react-bootstrap-time-picker';

function getValue(model,name){
    // name = name.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    // name = name.replace(/^\./, '');           // strip a leading dot
    // let a = name.split('.');
    // for (let i = 0, n = a.length; i < n; ++i) {
    //     let k = a[i];
    //     if (k in model) {
    //         model = model[k];
    //     } else {
    //         return;
    //     }
    // }
    //
    // return model;
    return _.get(model,name);
}

const TextBoxTypeValidators = {
    //TODO: NEED TO REMOVE PRICE TEXT BOX LATER ONCE WE CHANGED IT TO PRICE TEXT
    price :{
        isValidOnChange : function(e){
            return (/^\d*(\.(\d{0,2})?)?$/.test(e.target.value));
        },
        isValidOnBlur : function(e){
            return true;
        }
    },
    positiveNumber:{
        isValidOnChange : function(e){
            return (/^\d*(\.(\d{0,2})?)?$/.test(e.target.value));
        },
        isValidOnBlur : function(e){
            return true;
        }
    },
    onlyAlphabet :{
        isValidOnChange : function(e){
            return /^[a-zA-Z\s]*$/.test(e.target.value);
        },
        isValidOnBlur : function(e){
            return true;
        }
    },
    percent:{
        isValidOnChange : function(e){
            return /^(100(\.0{0,2})?|(\d|[1-9]\d)(\.\d{0,2})?)$/.test(e.target.value);
        },
        isValidOnBlur : function(e){
            return true;
        }
    }
};

export class TextBox extends React.Component{

    constructor(){
        super();
        this.onPipe = this.onPipe.bind(this);
    }

    handleChange(e){
        let { type,name,onChange,OnChangeFinished } = this.props;
        let validator = TextBoxTypeValidators[type];
        if(validator){
            if(validator.isValidOnChange(e)){
                onChange(e);
                if(OnChangeFinished){
                    OnChangeFinished(e.target.value);
                }
            }
        }
        else {
            onChange(e);
            if(OnChangeFinished){
                OnChangeFinished(e.target.value);
            }
        }
    }

    handleBlur(e){
        let { type,name,onChange,onChangeCallback } = this.props;
        if(onChangeCallback && e.type==='blur'){
            onChangeCallback(e);
        }
        else{
            let validator = TextBoxTypeValidators[type];
            if(validator){
                if(validator.isValidOnBlur(e)){
                    onChange(e);
                }
            }
            else {
                onChange(e);
            }
        }
    }

    onPipe(conformedValue,config,handler){
        let { name ,onChange } = this.props;
        onChange({
            target:{
                name : name,
                value : conformedValue.toUpperCase()
            },
            type:'change'
        });
        return handler(conformedValue,config);
    }

    render(){
        let { model, name ,onChange,mask,placeholder,inputClass } = this.props;
        let value = getValue(model,name);
        if(value===undefined || value===null) {
            console.log(name+'===>'+value);
        }
        inputClass = inputClass || "form-control input-sm";
        if(mask){
            let config = masks[mask];
            return (
                <MaskedInput
                    className="form-control input-sm"
                    value={value}
                    name={name}
                    guide={false}
                    mask={config.mask}
                    pipe={(v,c)=> this.onPipe(v,c,config.pipe)}
                />
            );
        }
        return (
            <input
                placeholder={placeholder}
                type="text"
                className={inputClass}
                name={name}
                value={value}
                onChange={this.handleChange.bind(this)}
                onBlur={this.handleBlur.bind(this)}
                disabled={this.props.disabled || false}
            />
        );
    }
}

export class Select extends React.Component{

    handleChange(e){
        let { name, onChange } = this.props;
        // if value of dropdown is empty, then save null value in db
        if(e.target.value===''){
            onChange({
                target:{
                    name : name,
                    value : null
                },
                type: e.type
            });
        }
        else{
            onChange(e);
        }
    }

    render(){
        let showLabel = true;
        let { model, name ,onChange, dropKey,inputClass } = this.props;
        let value = getValue(model,name);
        if(value===null){
            value = '';
        }
        if(typeof(this.props.showLabel)==='string'){
            showLabel = this.props.showLabel.toLowerCase()==="true";
        }
        inputClass = inputClass || 'form-control input-sm';
        if(dropKey){
            let list = dropDownValues[dropKey];
            return (
                <select
                    className={inputClass}
                    name={name}
                    value={value}
                    onChange={this.handleChange.bind(this)}
                    onBlur={this.handleChange.bind(this)}
                    disabled={this.props.disabled || false}
                >
                    {showLabel?(<option value={''}>- Select -</option>):null}
                    {list.map((p,index)=>{
                        return (<option key={uuid.v1()} value={p}>{p}</option>);
                    })}
                </select>
            );
        }
        else {
            return (
                <select
                    className={inputClass}
                    name={name}
                    value={value}
                    onChange={this.handleChange.bind(this)}
                    onBlur={this.handleChange.bind(this)}
                    disabled={this.props.disabled || false}
                >
                    {showLabel?(<option value={''}>- Select -</option>):null}
                    {this.props.children}
                </select>
            );
        }
    }
}

export class CheckBox extends React.Component{

    render(){
        let { model, name ,onChange,label } = this.props;
        let value = getValue(model,name);
        return (
            <span className="checkbox-custom checkbox-default">
                <input
                    className="selectable-item"
                    type="checkbox"
                    id={name}
                    name={name}
                    checked={value}
                    onChange={onChange}
                    onBlur={onChange}
                    disabled={this.props.disabled || false}
                />
                <label htmlFor={name}>{label}</label>
            </span>
        );
    }
}

export class Radio extends React.Component{

    render(){
        let { model, name ,onChange,label, option } = this.props;
        let value = getValue(model,name);
        return (
            <div className="radio-custom radio-primary">
                <input type="radio" name={name} value={option} checked={value===option} onChange={onChange} />
                <label>{label}</label>
            </div>
        );
    }
}

export class TextArea extends React.Component{

    handleChange(e){
        let { name,onChange,OnChangeFinished } = this.props;
        onChange({
            target:{
                name : name,
                value : e.target.value
            },
            type:'change'
        });
        if(OnChangeFinished){
            OnChangeFinished(value);
        }
    }

    render(){
        let { model, name , rows ,onChange, placeholder,inputClass } = this.props;
        let value = getValue(model,name);
        inputClass = inputClass || "form-control input-sm";
        return (
            <textarea type="text"
                      className={inputClass}
                      placeholder={placeholder}
                      rows={rows}
                      name={name}
                      value={value}
                      onChange={this.handleChange.bind(this)}
                      onBlur={onChange}
                      disabled={this.props.disabled || false}
            >{value}</textarea>
        );
    }
}

export class DatePicker extends React.Component{

    onSelect(value){
        let { name ,onChange,getManipulatedDate } = this.props;
        if(getManipulatedDate){
            value = getManipulatedDate(value).toISOString();
        }
        else {
            value = value.toISOString();
        }
        onChange({
            target: {
                value: value,
                name: name
            },
            type: 'change'
        });
        onChange({
            target: {
                value: value,
                name: name
            },
            type: 'blur'
        });
    }

    getCalendarContainer() {
        let { name } = this.props;
        return this.refs.name || document.getElementById(name);
    }

    disabledDate(current) {
        let { options } = this.props;
        if(options && options.disablePreviousDate){
            if (!current) {
                return false;
            }
            const date = moment();
            date.hour(0);
            date.minute(0);
            date.second(0);
            return date.isAfter(current,'day');
        }
        return false;
    }

    render(){
        let { model, name ,onChange } = this.props;
        let value = getValue(model,name) || '';
        const format = 'DD/MM/YYYY';
        let dateValue = (value)?moment(value).format(format):'';

        return (
            <Validate {...this.props}>
                <div>
                    <div id={name} ref={name}/>
                    <div>
                        <RcDatePicker
                            disabled={this.props.disabled || false}
                            getCalendarContainer={this.getCalendarContainer.bind(this)}
                            calendar={
                                <Calendar
                                    showDateInput={false}
                                    showToday={true}
                                    selectedValue ={moment(value)}
                                    onSelect={this.onSelect.bind(this)}
                                    disabledDate={this.disabledDate.bind(this)}
                                />
                            }
                        >
                            {
                                ({ p }) => {
                                    return (
                                        <div className="input-group">
                                            <input
                                                type="text"
                                                placeholder={format}
                                                className="form-control input-sm"
                                                value={dateValue}
                                                onChange={()=>{}}
                                                disabled={this.props.disabled || false}
                                            />
                                            <span className="input-group-addon">
                                                <i className="icon fa-calendar"></i>
                                            </span>
                                        </div>
                                    );
                                }
                            }
                        </RcDatePicker>
                    </div>
                </div>
            </Validate>
        );
    }
}

export class TimePicker extends React.Component{

    handleCallback(value){
        let { model, name ,onChange,OnChangeFinished } = this.props;
        onChange({
            target: {
                value: value,
                name: name
            },
            type: 'change'
        });
        onChange({
            target: {
                value: value,
                name: name
            },
            type: 'blur'
        });
        if(OnChangeFinished){
            OnChangeFinished(value);
        }
    }

    pad(n) {
        return (n < 10) ? ("0" + n) : n.toString();
    }

    render(){
        let { model, name,start,end } = this.props;
        let value = getValue(model,name);

        if(start){
            start = this.pad(start);
        }
        if(end){
            end = this.pad(end);
        }
        return (
            <Validate {...this.props}>
                <BootstrapTimePicker
                    value={value}
                    start={start||'06:00'}
                    end={end || '23:30'}
                    step={30}
                    className="form-control input-sm"
                    onChange={this.handleCallback.bind(this)}
                    disabled={this.props.disabled || false}
                />
            </Validate>
        );
    }
}

export class TilePicker extends React.Component{

    constructor(props){
        super(props);

        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.choose = this.choose.bind(this);
    }

    increment(e,value){
        let { name ,onChange, type } = this.props;
        if(type==='numeric'){
            ++value;
            let event = {
                target:{
                    value : value,
                    name : name
                },
                type:'change'
            };
            onChange(event);
        }
    }
    decrement(e,value){
        let { name ,onChange, type } = this.props;
        if(type==='numeric'){
            --value;
            if(value>=0) {
                let event = {
                    target: {
                        value: value,
                        name: name
                    },
                    type: 'change'
                };
                onChange(event);
            }
        }
    }
    choose(e,value){
        let { name ,onChange, type } = this.props;
        if(type!=='numeric'){
            let event = {
                target:{
                    value : value,
                    name : name,
                },
                type:'change'
            };
            onChange(event);
        }
    }

    onChangeHandler(e){
        let { name ,onChange, type } = this.props;
        if(type==='numeric'){
            if(/^\d*(\.(\d{0,2})?)?$/.test(e.target.value)){
                e.target.value = Number(e.target.value);
                onChange(e);
            }
        }
        else{
            onChange(e);
        }
    }

    render(){
        let { model, name, tileHeading,tileOptions ,type } = this.props;
        let options = [];
        let value = getValue(model,name);
        if(tileOptions){
            options = tileOptions.split(',');
        }
        return (
            <div className="label label-outline label-default active">
                <div className="heading">{tileHeading}</div>
                <div className="value-container">
                    {(type==='toggle')?(
                            <input name={name} value={value} onChange={()=>{}} type="text" className="form-control input-sm" />
                        ):(
                            <input name={name} value={value} onChange={this.onChangeHandler.bind(this)} type="text" className="form-control input-sm" />
                        )}
                </div>
                {(type==='numeric')?
                    (
                        <div className="label-group">
                            <span onClick={(e)=> this.decrement(e,value) } className="label label-outline label-default"><i className="icon md-minus"></i></span>
                            <span onClick={(e)=> this.increment(e,value) } className="label label-outline label-default"><i className="icon md-plus"></i></span>
                        </div>
                    ):
                    (
                        <div className="label-group">
                            {options.map((p)=> {
                                return(
                                    <span key={uuid.v1()} onClick={(e)=> this.choose(e,p) } className="label label-outline label-default">{p}</span>
                                );
                            })}
                        </div>
                    )
                }
            </div>
        );
    }
}

export class PricePicker extends React.Component{

    getVal(){
        let { model, name} = this.props;
        let value = getValue(model,name);
        if(value!==undefined && value!==null) {
            value = accounting.formatMoney(value, {
                symbol: "£",
                format: "%s %v",
                precision : 0,
                thousand: ","
            });
            return value;
        }
        else{
            console.log(name+'===>'+value);
        }
    }

    handleChange(e){
        let { name,onChange,OnChangeFinished } = this.props;
        let value = accounting.unformat(e.target.value);
        onChange({
            target:{
                name : name,
                value : value
            },
            type:'change'
        });
        if(OnChangeFinished){
            OnChangeFinished(value);
        }
    }

    increment(e){
        let {model, name ,onChange } = this.props;
        let value = getValue(model,name);
        ++value;
        let event = {
            target:{
                value : value,
                name : name
            },
            type:'change'
        };
        onChange(event);
    }
    decrement(e){
        let {model, name ,onChange } = this.props;
        let value = getValue(model,name);
        --value;
        if(value>=0) {
            let event = {
                target: {
                    value: value,
                    name: name
                },
                type: 'change'
            };
            onChange(event);
        }
    }

    onChangeHandler(e){
        let { name ,onChange } = this.props;
        let value = accounting.unformat(e.target.value);
        onChange({
            target: {
                value: value,
                name: name
            },
            type: 'change'
        });
    }

    render(){
        let { tileHeading,name } = this.props;
        let value = this.getVal();
        return (
            <div className="label label-outline label-default active">
                <div className="heading">{tileHeading}</div>
                <div className="value-container">
                    <input name={name} value={value} onChange={this.onChangeHandler.bind(this)} type="text" className="form-control input-sm" />
                </div>
                <div className="label-group">
                    <span onClick={this.decrement.bind(this)} className="label label-outline label-default"><i className="icon md-minus"></i></span>
                    <span onClick={this.increment.bind(this)} className="label label-outline label-default"><i className="icon md-plus"></i></span>
                </div>
            </div>
        );
    }
}

export class ValueUnitPicker extends React.Component{

    constructor(props){
        super(props);
        this.choose = this.choose.bind(this);
    }

    choose(e,value){
        let { unitName ,onChange, type } = this.props;
        let event = {
            target:{
                value : value,
                name : unitName,
            },
            type:'change'
        };
        onChange(event);
    }

    onChangeHandler(e){
        let { name ,onChange, type } = this.props;
        if(/^\d*(\.(\d{0,2})?)?$/.test(e.target.value)){
            onChange(e);
        }
    }

    render(){
        let { model, name, unitName,units, tileHeading } = this.props;
        let value = getValue(model,name);
        let unitValue = getValue(model,unitName);
        return (
            <div className="label label-outline label-default active">
                <div className="heading">{tileHeading}
                    <small>{' ( '+unitValue+' )'}</small>
                </div>
                <div className="value-container">
                    <input name={name} value={value} onChange={this.onChangeHandler.bind(this)} type="text" className="form-control input-sm" />
                </div>
                <div className="label-group">
                    {units.map((unit,index)=>{
                        return (
                            <span key={name+'_'+index} onClick={(e)=> this.choose(e,unit)} className="label label-outline label-default">{unit}</span>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export class PriceInput extends React.Component{

    getVal(){
        let { model, name} = this.props;
        let value = getValue(model,name);
        if(value!==undefined && value!==null) {
            value = accounting.formatMoney(value, {
                symbol: "£ ",
                format: "%s %v",
                precision : 0,
                thousand: ","
            });
            return value;
        }
        else{
            console.log(name+'===>'+value);
        }
    }

    handleChange(e){
        let { name,onChange,OnChangeFinished } = this.props;
        let value = accounting.unformat(e.target.value);
        onChange({
            target:{
                name : name,
                value : value
            },
            type:'change'
        });
        if(OnChangeFinished){
            OnChangeFinished(value);
        }
    }

    handleBlur(e){
        let { name,onChange } = this.props;
        let value = accounting.unformat(e.target.value);
        onChange({
            target:{
                name : name,
                value : value
            },
            type:'blur'
        });
    }

    render(){
        let { name  } = this.props;
        let value = this.getVal();
        return (
            <input
                placeholder={this.props.placeholder}
                type="text"
                className={this.props.inputClass||'form-control input-sm'}
                name={name}
                value={value}
                disabled={this.props.disabled || false}
                onChange={this.handleChange.bind(this)}
                onBlur={this.handleBlur.bind(this)}
            />
        );
    }
}

export class PercentageInput extends React.Component{

    handleChange(e){
        let { name,onChange } = this.props;
        let validator = TextBoxTypeValidators['percent'];
        let value = e.target.value.replace(' %','');
        if(value===''){
            value = 0;
            onChange({
                target:{
                    name : name,
                    value : value
                },
                type:'change'
            });
        }
        else if(value!==''){
            value = Number(value);
            let event = {
                target:{
                    name : name,
                    value : value
                },
                type:'change'
            };
            if(validator.isValidOnChange(event)){
                onChange(event);
            }
        }
    }

    render(){
        let { model, name ,onChange,placeholder,className,inputClass } = this.props;
        let value = getValue(model,name);
        if(value===undefined || value===null) {
            console.log(name+'===>'+value);
        }
        else if(value>0){
            value +=' %';
        }
        else if(value===0){
            value = '';
        }

        inputClass = inputClass || "form-control input-sm";
        return (
            <div className={className}>
                <input
                    placeholder={placeholder}
                    type="text"
                    className={inputClass}
                    name={name}
                    value={value}
                    onChange={this.handleChange.bind(this)}
                    onBlur={onChange}
                    disabled={this.props.disabled || false}
                />
            </div>
        );
    }
}

export class Validate extends React.Component{

    render(){
        let { model,className, name  } = this.props;
        let error = _.find(model.validator.errors, function (r) {
            return r.name === name;
        });
        if(error){
            className +=' has-error';
        }
        return (
            <div className={className}>
                {this.props.children}
                {error?(<label className="control-label">{error.message}</label>):null}
            </div>
        );
    }
}

export class ValidateText extends React.Component{
    render(){
        return (
            <Validate {...this.props}>
                <TextBox {...this.props}/>
            </Validate>
        );
    }
}

export class ValidatePriceInput extends React.Component{
    render(){
        return (
            <Validate {...this.props}>
                <PriceInput {...this.props}/>
            </Validate>
        );
    }
}

export class ValidateSelect extends React.Component{
    render(){
        return (
            <Validate {...this.props}>
                <Select {...this.props}/>
            </Validate>
        );
    }
}

export class ValidateTextArea extends React.Component{
    render(){
        return (
            <Validate {...this.props}>
                <TextArea {...this.props}/>
            </Validate>
        );
    }
}

export class ColorBlockPicker extends React.Component{

    constructor(){
        super();
        this.state = {
            showPicker : false
        };
        this.onCloseHandler = this.onCloseHandler.bind(this);
    }

    componentDidMount () {
        document.body.addEventListener('click', this.onCloseHandler);
    }
    componentWillUnmount () {
        document.body.removeEventListener('click', this.onCloseHandler);
    }

    onFocusHandler(e){
        this.setState({
            showPicker : true
        });
    }

    handleChangeComplete(color,event){
        let { onChange,name } = this.props;
        onChange({
            target:{
                name : name,
                value : color.hex
            },
            type:'change'
        });
        this.setState({
            hex : color.hex
        });
    }

    findAncestor (el, cls) {
        while (el&&!el.classList.contains(cls)){
            el = el.parentElement;
        }
        return el;
    }

    onCloseHandler(e){
        let picker = this.findAncestor(e.target,'colorInputUi-wrap');
        if(!picker){
            this.setState({
                showPicker : false
            });
        }
    }

    render(){
        let { model, name , rows, placeholder,inputClass } = this.props;
        let value = getValue(model,name);
        inputClass = inputClass || "form-control input-sm";
        let className = 'asColorpicker '+inputClass+' colorInputUi-input';
        value = value || '#697689';
        return (
            <div className="colorInputUi-wrap" id={name}>
                <input
                    type="text"
                    className={className}
                    value={value}
                    onFocus={this.onFocusHandler.bind(this)}
                    onChange={()=>{}}
                    name={name}
                />
                <a href="#" className="colorInputUi-clear" role="button"></a>
                <div className="colorInputUi-trigger">
                    <span style={{background: value}}></span>
                </div>
                {this.state.showPicker?(
                        <BlockPicker
                            color={value}
                            onChange={this.handleChangeComplete.bind(this)}
                        />
                    ):null}
            </div>
        );
    }
}


import React from 'react';
import {Link} from "react-router";
import SiteMenuLink from './SiteMenuLink';
import SiteMenuProperties from './SiteMenuProperties';
import SiteMenuGeneral from '../../layouts/SiteMenuGeneral';
import store from '../../store';

export default class SiteMenu extends React.Component{

    componentDidMount() {
        Breakpoints();

    }

    render(){
        let showListing = false;
        let { propertyState } = this.props;
        if(propertyState){
            let {actions} = propertyState;
            showListing = actions.showListing;
        }

        return(
            <div className="site-menubar">
                <div className="site-menubar-body">
                    {showListing?(
                        <SiteMenuProperties {...this.props} />
                    ):(
                        <SiteMenuGeneral {...this.props}/>
                    )}
                </div>
                <div className="site-menubar-footer">
                    <a href="#" className="fold-show" data-placement="top" data-toggle="tooltip"
                       data-original-title="Settings">
                        <span className="icon md-settings" aria-hidden="true"></span>
                    </a>
                    <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                        <span className="icon md-eye-off" aria-hidden="true"></span>
                    </a>
                    <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                        <span className="icon md-power" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        );
    }
}
import accounting from 'accounting';
import moment from 'moment';
import _ from 'lodash';
import { timeToInt, timeFromInt } from 'time-number';

function getHour(){
    let hour = moment().get('hour');
    // IF HOUR BETWEEN 12PM TO 6 AM THEN SET TIME T0 10 AM
    if(hour>0 && hour <6){
       hour = 10;
    }
    else {
        let minutes = moment().get('minute');
        if (minutes > 0 && minutes < 30) {
            hour = hour + 0.5;
        }
        else if (minutes > 0 && minutes > 30) {
            hour++;
        }
    }
    return hour;
}

function getDate(){
    let hour = moment().get('hour');
    // IF HOUR BETWEEN 12PM TO 6 AM THEN SET TIME T0 10 AM
    if(hour>0 && hour <6){
        let date = moment().get('date');
        return moment().set({
            date : date
        });
    }
    return moment();
}
let instance = null;

class Helper{

    constructor(){
        if(!instance){
            this.property = new PropertyHelper();
            instance = this;
        }
        return instance;
    }

    bytesToSize (bytes) {
        let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }

    toCurrency(value){
        return accounting.formatMoney(value, {
            symbol: "£ ",
            format: "%s %v",
            precision : 0,
            thousand: ","
        });
    }

    toTime(time){
        const ret = timeFromInt(time, false);

        const found = ret.match(/^(\d+):/);
        const hour  = parseInt(found[1], 10);

        if (hour === 0) {
            return `${ret.replace(/^\d+/, '12')} AM`;
        } else if (hour < 12) {
            return `${ret} AM`;
        } else if (hour === 12) {
            return `${ret} PM`;
        }

        const newHour = hour < 22 ? `0${hour - 12}` : (hour - 12).toString();
        return `${ret.replace(/^\d+/, newHour)} PM`;
    }

    getCurrentDate(){
        let hour = getHour();
        let obj = {
            hour : 0,
            minutes : 0
        };

        if(hour % 1 === 0){
            obj.hour = hour;
            obj.minutes = 0;
        }
        else {
            obj.hour = hour-0.5;
            obj.minutes = (hour-(hour-0.5)) * 60;
        }
        // IF HOUR BETWEEN 12PM TO 6 AM THEN SET DATE TO NEXT DATE
        // AND SET HOUR AS 10 AM AND MINUTES AS 30
        let currentDate = getDate().set({
            hour : 0, //obj.hour,
            minute: 0, //obj.minutes,
            second : 0,
            millisecond : 0
        });
        return currentDate;
    }

    getStartTime() {
        let hour = getHour();
        return hour * 3600;
    }

    getEndTime(){
        let hour = getHour();
        // end time should be one hour longer than start time
        hour++;
        return hour * 3600;
    }

    getHourMinutesFromTime(time){
        let hour = time/3600;
        let obj = {
            hour : 0,
            minutes : 0
        };

        if(hour % 1 === 0){
            obj.hour = hour;
            obj.minutes = 0;
        }
        else {
            obj.hour = hour-0.5;
            obj.minutes = (hour-(hour-0.5)) * 60;
        }
        return obj;
    }

    formatDateTime(date){
        return moment(date).format('DD/MM/YYYY h:mm a');
    }

    formatDate(date){
        return moment(date).format('DD/MM/YYYY');
    }

    isPropertyDetailPage(props){
        props = props || this.props;
        return props.location.pathname.indexOf('/properties/'+props.params.id)>-1;
    }

    isContactDetailPage(props){
        props = props || this.props;
        return props.location.pathname.indexOf('/contacts/'+props.params.id)>-1;
    }
}

class PropertyHelper{
    getStatus (property){
        let { status, recordStatus } = property;
        if(recordStatus!=='Active'){
            status = recordStatus;
        }
        return status;
    }

    disableAddOwnersToProperty(property){
        // if owners are already saved, then we won't allow users to add owners
        let savedOwnersCount = 0;
        _.each(property.owners,(item)=>{
            if(item._id){
                savedOwnersCount++;
            }
        });
        let status = this.getStatus(property);
        return (savedOwnersCount>0 && ['Pre Appraisal','Appraised'].indexOf(status) === -1);
    }

    getPrice(property){
        let status = this.getStatus(property);
        if(['Pre Appraisal','Appraised'].indexOf(status) > -1){
            return property.proposedPrice || property.price;
        }
        return property.contract.price;
    }

    isInstructed(property){
        let { contract } = property;
        return contract.instructId!=='';
    }
};

export default new Helper();
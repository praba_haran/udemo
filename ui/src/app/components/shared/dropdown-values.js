const values = {
    country:[
        "United Kingdom"
    ],
    contactTitle :[
        "Mr",
        "Mrs",
        "Miss",
        "Dr"
    ],
    contactPhoneType:[
        "Home",
        "Mobile",
        "Office",
        "Work"
    ],
    contactType:[
        "Bank",
        "Board Contractor",
        "Builder",
        "Building Society",
        "Client",
        "Developer",
        "Estate Agent",
        "Estate Agent (Local)",
        "External Printer",
        "Financial Advisor",
        "Maintenance",
        "Managing Agent",
        "Miscellaneous",
        "Previous Client",
        "On-Site Sales",
        "Publication",
        "Service",
        "Solicitor",
        "Surveyor",
        "Utility"
    ],
    contactIntension:[
        "To Buy",
        "To Buy or To Rent",
        "To Rent",
        "Not Looking"
    ],
    contactLeadSource:[
        "Publication - newspaper",
        "Existing/historic",
        "Canvassing/marketing",
        "Publication - magazine",
        "Internet - search engine",
        "Internet - portal",
        "Google",
        "Yahoo",
        "Rightmove",
        "Zoopla",
        "Walk-in",
        "Leaflet",
        "Vouchers"
    ],
    contactPosition:[
        "Developer",
        "Assisted move",
        "Buy to let",
        "Cash buyer",
        "Cash buyer - mortgage required",
        "Company bridge",
        "First time buyer",
        "In rented",
        "Property to let",
        "Property to sell",
        "Not specified"
    ],
    contactAction:[
        "Appointment booked",
        "Appointment booked - hot",
        "Appointment declined",
        "Not asked",
        "Pending",
        "Permission declined",
        "Permission granted",
        "Permission granted - Hot"
    ],
    contactDisposal:[
        "On market, with us",
        "Not on market",
        "Nothing to sell",
        "On market",
        "Sold",
        "Under offer",
        "Not applicable"
    ],
    contactMovingReason:[
        "Change of area",
        "Deceased estate",
        "Divorce/separation",
        "Downsizing",
        "Financial",
        "Job relocation",
        "New job",
        "Other",
        "Schools",
        "Upsizing"
    ],
    contactRating:[
        "Cold",
        "Delete",
        "Hot",
        "Normal"
    ],

    propertyType:[
        "House - back to back",
        "House - end mews",
        "House - mid quasi",
        "House - quasi",
        "House - quasi semi",
        "House - retirement",
        "House - terrace inner through",
        "House - end town house",
        "Maisonette - duplex",
        "Maisonette - garden",
        "Maisonette - lower",
        "Maisonette - upper",
        "Studio",
        "Studio - conversion",
        "Studio - purpose built",
        "Business",
        "Factory",
        "House - link semi detached",
        "A2",
        "A3",
        "D1",
        "D2",
        "Room to rent",
        "Investment",
        "Fishing",
        "A1",
        "Apartment",
        "Apartment - above shop",
        "Apartment - purpose built",
        "Block of flats",
        "Bungalow",
        "Bungalow - detached",
        "Bungalow - link detached",
        "Bungalow - semi detached",
        "Coachhouse",
        "Commercial",
        "Cottage - detached",
        "Development property",
        "Flat",
        "Flat - conversion",
        "Flat - penthouse",
        "Flat - purpose built",
        "Holiday Complex",
        "Hotel",
        "House",
        "House - Attached",
        "House - detached",
        "House - end terrace",
        "House - link detached",
        "House - mid terrace",
        "House - semi-detached",
        "House - terraced",
        "House - townhouse",
        "Investment - let",
        "Land",
        "Land - building plot",
        "Land - woodland",
        "Leisure",
        "Office",
        "Open storage yard",
        "Parking space",
        "Retail",
        "Room",
        "Warehouse"
    ],
    propertyCategory:[
        "Residential",
        "Commercial"
    ],
    propertyMarket:[
        "For Sale",
        "To Let"
    ],
    propertyAge:[
        "1900-1909",
        "1910-1919",
        "1920-1929",
        "1930-1939",
        "1930s/1950s",
        "1940-1949",
        "1950-1959",
        "1960-1969",
        "1970-1979",
        "1980-1989",
        "1990-1999",
        "2000 onwards",
        "2000-2009",
        "2010-2019",
        "Edwardian",
        "Georgian",
        "Jacobean",
        "Modern",
        "Nearly new",
        "New",
        "New build",
        "Off plan",
        "Older",
        "Older style",
        "Period",
        "Post-1914",
        "Post-war",
        "Pre-1900",
        "Pre-1914",
        "Pre-war",
        "Queen Anne",
        "Regency",
        "Tudor",
        "Victorian"
    ],
    propertyMarketing:[
        "Not on market",
        "On with us",
        "Other agent",
        "Foxtons, Pinner"
    ],
    propertyTimeOnTheMarket:[
        "One week",
        "Two weeks",
        "Three weeks",
        "Four weeks",
        "Two months",
        "Three months",
        "Four months",
        "Five months",
        "Six months",
        "One year",
        "Over a year"
    ],
    priceQualifier:[
        "Per week",
        "Per month",
        "Per annum"
    ],
    rentalFrequency:[
        "Weekly",
        "Monthly",
        "Quarterly",
        "Yearly"
    ],
    furnishing:[
        "Furnished",
        "Part furnished",
        "Unfurnished"
    ],
    recurringPeriod:[
        "Weekly",
        "Monthly",
        "Quarterly",
        "Yearly"
    ],
    saleTerms:[
        "Auction",
        "Formal tender",
        "Informal tender",
        "Private treaty"
    ],
    contractLengths:[
        "2 Weeks",
        "3 Weeks",
        "4 Weeks",
        "6 Weeks",
        "8 Weeks",
        "10 Weeks",
        "12 Weeks",
        "14 Weeks",
        "16 Weeks",
        "18 Weeks",
        "20 Weeks",
        "22 Weeks",
        "24 Weeks",
        "26 Weeks",
        "28 Weeks",
        "30 Weeks",
        "32 Weeks"
    ],
    boardTypes:[
        "For Sale",
        "SSTC Slip",
        "No Board",
        "To Let",
        "Let",
        "For Sale / To Let",
        "For Auction",
        "Sold",
        "Re-erect Board",
        "Let Agreed",
        "Let Unmanaged",
        "Let and Managed",
        "Under Offer",
        "Take Down Board"
    ],
    managementTypes : [
       "Managed",
        "Part Managed",
        "Not Managed"
    ],
    maintenanceTypes : [
        "Handled by Landlord",
        "Handled by Us"
    ],
    overseasLandlordTypes:[
        "Informed, has Adviser",
        "Tax Deducted by Us"
    ],
    propertyFeatures:[
        {
            key : "Building",
            values : [
                {
                    name:"Cellar",
                    selected:false
                },
                {
                    name:"Dampproof Treatment",
                    selected:false
                },
                {
                    name:"Ex-Local Authority",
                    selected:false
                },
                {
                    name:"Menege",
                    selected:false
                },
                {
                    name:"New Plumbing",
                    selected:false
                },
                {
                    name:"New Wiring",
                    selected:false
                },
                {
                    name:"Offices",
                    selected:false
                },
                {
                    name:"Planning Permission",
                    selected:false
                },
                {
                    name:"Staff Accommodation",
                    selected:false
                },
                {
                    name:"Treated Timber",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Concierge",
            values:[
                {
                    name:"24-Hour Concierge",
                    selected:false
                },
                {
                    name:"Caretaker",
                    selected:false
                },
                {
                    name:"Resident Porter",
                    selected:false
                },
                {
                    name:"Warden Assisted",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Condition",
            values:[
                {
                    name:"Average Condition",
                    selected:false
                },
                {
                    name:"Complete Refurbishment Required",
                    selected:false
                },
                {
                    name:"Cosmetic Improvement Required",
                    selected:false
                },
                {
                    name:"Derelict",
                    selected:false
                },
                {
                    name:"Development Potential",
                    selected:false
                },
                {
                    name:"Excellent Condition",
                    selected:false
                },
                {
                    name:"Fair Condition",
                    selected:false
                },
                {
                    name:"Fully Modernised",
                    selected:false
                },
                {
                    name:"Good Condition",
                    selected:false
                },
                {
                    name:"In Need of Modernisation",
                    selected:false
                },
                {
                    name:"In Need of Renovation",
                    selected:false
                },
                {
                    name:"Luxury Accommodation",
                    selected:false
                },
                {
                    name:"Needs Refurbishment",
                    selected:false
                },
                {
                    name:"Newly Decorated",
                    selected:false
                },
                {
                    name:"Newly Refurbished",
                    selected:false
                },
                {
                    name:"No Work Required",
                    selected:false
                },
                {
                    name:"Only Suitable for Investor",
                    selected:false
                },
                {
                    name:"Poor Condition",
                    selected:false
                },
                {
                    name:"Shell",
                    selected:false
                },
                {
                    name:"Structural Work Required",
                    selected:false
                },
                {
                    name:"Un-mortgageable",
                    selected:false
                },
                {
                    name:"Well Presented",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Eco",
            values:[
                {
                    name:"Cavity Wall Insulation",
                    selected:false
                },
                {
                    name:"Solar Power",
                    selected:false
                },
                {
                    name:"Sustainable Energy",
                    selected:false
                },
                {
                    name:"Wind Turbine",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Equestrian",
            values:[
                {
                    name:"Commercial Stables",
                    selected:false
                },
                {
                    name:"Equestrian",
                    selected:false
                },
                {
                    name:"Paddock",
                    selected:false
                },
                {
                    name:"Private Stables",
                    selected:false
                },
                {
                    name:"Stabling",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Financial",
            values:[
                {
                    name:"Holiday Let",
                    selected:false
                },
                {
                    name:"Income Potential",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Garden",
            values:[
                {
                    name:"East Facing",
                    selected:false
                },
                {
                    name:"Large",
                    selected:false
                },
                {
                    name:"North Facing",
                    selected:false
                },
                {
                    name:"Patio",
                    selected:false
                },
                {
                    name:"Private (Rear)",
                    selected:false
                },
                {
                    name:"Shared",
                    selected:false
                },
                {
                    name:"Small",
                    selected:false
                },
                {
                    name:"South Facing",
                    selected:false
                },
                {
                    name:"West Facing",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Glazing",
            values:[
                {
                    name:"Air Conditioning",
                    selected:false
                },
                {
                    name:"Comfort Cooling",
                    selected:false
                },
                {
                    name:"Communal Heating",
                    selected:false
                },
                {
                    name:"Double Glazing",
                    selected:false
                },
                {
                    name:"Double Glazing (Part)",
                    selected:false
                },
                {
                    name:"Electric Heating",
                    selected:false
                },
                {
                    name:"Gas (GCH)",
                    selected:false
                },
                {
                    name:"Gas LPG",
                    selected:false
                },
                {
                    name:"Geothermic",
                    selected:false
                },
                {
                    name:"Heat Pump",
                    selected:false
                },
                {
                    name:"Multi-Fuel",
                    selected:false
                },
                {
                    name:"No Heating",
                    selected:false
                },
                {
                    name:"Oil",
                    selected:false
                },
                {
                    name:"Secondary Glazing",
                    selected:false
                },
                {
                    name:"Secondary Glazing (Part)",
                    selected:false
                },
                {
                    name:"Solid Fuel",
                    selected:false
                },
                {
                    name:"Warm Air",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Leisure",
            values:[
                {
                    name:"Gymnasium",
                    selected:false
                },
                {
                    name:"Sauna/Steam Room",
                    selected:false
                },
                {
                    name:"Swimming Pool Indoor",
                    selected:false
                },
                {
                    name:"Swimming Pool Outdoor",
                    selected:false
                },
                {
                    name:"Tennis Court",
                    selected:false
                }
            ],
            selected:false
        },
        {
            key:"Location",
            values:[
                {
                    name:"Access to Motorway",
                    selected:false
                },
                {
                    name:"Access to Water",
                    selected:false
                },
                {
                    name:"City Centre",
                    selected:false
                },
                {
                    name:"Close to Station",
                    selected:false
                },
                {
                    name:"Cul-de-Sac",
                    selected:false
                },
                {
                    name:"Fishing Rights",
                    selected:false
                },
                {
                    name:"Rural",
                    selected:false
                },
                {
                    name:"Sea/Estuary Views",
                    selected:false
                },
                {
                    name:"Suburban",
                    selected:false
                },
                {
                    name:"Town Centre",
                    selected:false
                },
                {
                    name:"Urban",
                    selected:false
                },
                {
                    name:"Views",
                    selected:false
                },
                {
                    name:"Village",
                    selected:false
                },
                {
                    name:"Waterside",
                    selected:false
                },
                {
                    name:"Woodland",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Nautical",
            values:[
                {
                    name:"Berths",
                    selected:false
                },
                {
                    name:"Mooring Rights",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Outside",
            values:[
                {
                    name:"Annexe",
                    selected:false
                },
                {
                    name:"Communal Area",
                    selected:false
                },
                {
                    name:"Courtyard",
                    selected:false
                },
                {
                    name:"Farm Buildings",
                    selected:false
                },
                {
                    name:"Garden",
                    selected:false
                },
                {
                    name:"Granny Annexe",
                    selected:false
                },
                {
                    name:"Outbuildings",
                    selected:false
                },
                {
                    name:"Rear Yard",
                    selected:false
                },
                {
                    name:"Roof Terrace",
                    selected:false
                },
                {
                    name:"Terrace",
                    selected:false
                },
                {
                    name:"Workshop",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Ownership",
            values:[
                {
                    name:"Housing Association",
                    selected:false
                },
                {
                    name:"Local Authority",
                    selected:false
                },
                {
                    name:"Shared Ownership",
                    selected:false
                }
            ],
            selected:false
        },
        {
            key:"Roofing",
            values:[
                {
                    name:"New Roof",
                    selected:false
                },
                {
                    name:"Non-Thatched Roof",
                    selected:false
                },
                {
                    name:"Thatched Roof",
                    selected:false
                }
            ],
            selected:false
        },
        {
            key:"Security",
            values:[
                {
                    name:"Automated Entrance Gates",
                    selected:false
                },
                {
                    name:"Burglar Alarm",
                    selected:false
                },
                {
                    name:"CCTV Security",
                    selected:false
                },
                {
                    name:"Entrance Phone",
                    selected:false
                },
                {
                    name:"Fire Escape",
                    selected:false
                },
                {
                    name:"Video Entrance Phone",
                    selected:false
                }
            ],
            selected:false
        },
        {
            key:"Special",
            values:[
                {
                    name:"Architectural Features",
                    selected:false
                },
                {
                    name:"Balcony",
                    selected:false
                },
                {
                    name:"Dumb Waiter",
                    selected:false
                },
                {
                    name:"Home Cinema",
                    selected:false
                },
                {
                    name:"Integrated Audio System",
                    selected:false
                },
                {
                    name:"Modern Buildings",
                    selected:false
                },
                {
                    name:"Passenger Lift",
                    selected:false
                },
                {
                    name:"Service Lift",
                    selected:false
                },
                {
                    name:"Traditional Range",
                    selected:false
                },
                {
                    name:"Under-Floor Heating",
                    selected:false
                },
                {
                    name:"Wine Cellar",
                    selected:false
                }
            ],
            selected:false
        },
        {
            key:"Utilities",
            values:[
                {
                    name:"All Mains Services",
                    selected:false
                },
                {
                    name:"Communal Hot Water",
                    selected:false
                },
                {
                    name:"Mains Drainage",
                    selected:false
                },
                {
                    name:"Mains Water",
                    selected:false
                },
                {
                    name:"Private Drainage",
                    selected:false
                },
                {
                    name:"Private Water Supply",
                    selected:false
                }
            ],
            selected:false
        }
    ],
    propertyTypesByCategory:[
        {
            key : "House",
            values : [
                {
                    name:"House",
                    selected:false
                },
                {
                    name:"House - back to back",
                    selected:false
                },
                {
                    name:"House - end mews",
                    selected:false
                },
                {
                    name:"House - mid quasi",
                    selected:false
                },
                {
                    name:"House - quasi",
                    selected:false
                },
                {
                    name:"House - quasi semi",
                    selected:false
                },
                {
                    name:"House - retirement",
                    selected:false
                },
                {
                    name:"House - terrace inner through",
                    selected:false
                },
                {
                    name:"House - end town house",
                    selected:false
                },
                {
                    name:"House - Attached",
                    selected:false
                },
                {
                    name:"House - detached",
                    selected:false
                },
                {
                    name:"House - end terrace",
                    selected:false
                },
                {
                    name:"House - link detached",
                    selected:false
                },
                {
                    name:"House - mid terrace",
                    selected:false
                },
                {
                    name:"House - semi-detached",
                    selected:false
                },
                {
                    name:"House - terraced",
                    selected:false
                },
                {
                    name:"House - townhouse",
                    selected:false
                },
                {
                    name:"Cottage - detached",
                    selected:false
                },
                {
                    name:"House - link semi detached",
                    selected:false
                }
            ]
        },
        {
            key : "Flat/Apartment",
            values : [
                {
                    name:"Flat",
                    selected:false
                },
                {
                    name:"Flat - conversion",
                    selected:false
                },
                {
                    name:"Flat - penthouse",
                    selected:false
                },
                {
                    name:"Apartment",
                    selected:false
                },
                {
                    name:"Flat - purpose built",
                    selected:false
                },
                {
                    name:"Maisonette - duplex",
                    selected:false
                },
                {
                    name:"Maisonette - garden",
                    selected:false
                },
                {
                    name:"Maisonette - lower",
                    selected:false
                },
                {
                    name:"Maisonette - upper",
                    selected:false
                },
                {
                    name:"Studio",
                    selected:false
                },
                {
                    name:"Studio - conversion",
                    selected:false
                },
                {
                    name:"Studio - purpose built",
                    selected:false
                },
                {
                    name:"Apartment - above shop",
                    selected:false
                },
                {
                    name:"Apartment - purpose built",
                    selected:false
                },
                {
                    name:"Block of flats",
                    selected:false
                }
            ]
        },
        {
            key : "Bungalow",
            values : [
                {
                    name:"Bungalow",
                    selected:false
                },
                {
                    name:"Bungalow - detached",
                    selected:false
                },
                {
                    name:"Bungalow - link detached",
                    selected:false
                },
                {
                    name:"Bungalow - semi detached",
                    selected:false
                }
            ]
        },
        {
            key : "Guest House/Hotel",
            values : [
                {
                    name:"Hotel",
                    selected:false
                }
            ]
        },
        {
            key : "Land",
            values : [
                {
                    name:"Land",
                    selected:false
                },
                {
                    name:"Land - building plot",
                    selected:false
                },
                {
                    name:"Land - woodland",
                    selected:false
                },
                {
                    name:"Fishing",
                    selected:false
                }
            ]
        },
        {
            key : "Character Property",
            values : [
                {
                    name:"Coachhouse",
                    selected:false
                }
            ]
        },
        {
            key : "Garage/Parking",
            values : [
                {
                    name:"Parking space",
                    selected:false
                }
            ]
        },
        {
            key : "Retirement Property",
            values : [
                {
                    name:"House - retirement",
                    selected:false
                }
            ]
        },
        {
            key : "Commercial Property",
            values : [
                {
                    name:"Development property",
                    selected:false
                },
                {
                    name:"Business",
                    selected:false
                },
                {
                    name:"Commercial",
                    selected:false
                },
                {
                    name:"Factory",
                    selected:false
                },
                {
                    name:"Office",
                    selected:false
                },
                {
                    name:"Leisure",
                    selected:false
                },
                {
                    name:"Retail",
                    selected:false
                },
                {
                    name:"Warehouse",
                    selected:false
                },
                {
                    name:"Holiday Complex",
                    selected:false
                },
                {
                    name:"Open storage yard",
                    selected:false
                },
                {
                    name:"Investment - let",
                    selected:false
                },
                {
                    name:"A1",
                    selected:false
                },
                {
                    name:"A2",
                    selected:false
                },
                {
                    name:"A3",
                    selected:false
                },
                {
                    name:"D1",
                    selected:false
                },
                {
                    name:"D2",
                    selected:false
                },
                {
                    name:"Investment",
                    selected:false
                }
            ]
        },
        {
            key : "Flat/House share",
            values : [
                {
                    name:"Room",
                    selected:false
                },
                {
                    name:"Room to rent",
                    selected:false
                }
            ]
        }
    ],
    marketAppraisalTypes:[
        "Free market appraisal",
        "Chargeable valuation",
        "Commercial",
        "Part Exchange",
        "Probate",
        "Re-possession"
    ],
    reminderMinutes:[
        "None",
        "5 mins",
        "10 mins",
        "15 mins",
        "30 mins",
        "45 mins",
        "60 mins",
        "2 hrs",
        "1 day",
        "3 days",
        "7 days"
    ],
    reminderChannel:[
        "Email",
        "Popup",
        "Whatsapp"
    ],
    appointmentType:[
        'Miscellaneous',
        'Electrical safety check',
        'Fire and furnishings safety check',
        'FS appointment',
        'Gas safety check',
        'Holiday',
        'Home condition report',
        'Inventory appointment',
        'Maintenance',
        'Measure up',
        'Meeting',
        'Absent',
        'Out of office',
        'Property inspection',
        'Read meters',
        'Sales progression',
        'Survey',
        'Take on',
        'Unavailable'
    ],
    matchingEssentials:[
        {
            key : "Situation",
            values : [
                {
                    name:"Developer",
                    selected:false
                },
                {
                    name:"Assisted move",
                    selected:false
                },
                {
                    name:"Buy to let",
                    selected:false
                },
                {
                    name:"Cash buyer",
                    selected:false
                },
                {
                    name:"Cash buyer - mortgage required",
                    selected:false
                },
                {
                    name:"Company bridge",
                    selected:false
                },
                {
                    name:"In rented",
                    selected:false
                },
                {
                    name:"Property to let",
                    selected:false
                },
                {
                    name:"Property to sell",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Disposal",
            values:[
                {
                    name:"On market, with us",
                    selected:false
                },
                {
                    name:"Not on market",
                    selected:false
                },
                {
                    name:"Nothing to sell",
                    selected:false
                },
                {
                    name:"On market",
                    selected:false
                },
                {
                    name:"Sold",
                    selected:false
                },
                {
                    name:"Under offer",
                    selected:false
                }
            ],
            selected :false
        },
        {
            key:"Rating",
            values:[
                {
                    name:"Cold",
                    selected:false
                },
                {
                    name:"Delete",
                    selected:false
                },
                {
                    name:"Hot",
                    selected:false
                },
                {
                    name:"Normal",
                    selected:false
                }
            ],
            selected :false
        }
    ]
};

export default values;
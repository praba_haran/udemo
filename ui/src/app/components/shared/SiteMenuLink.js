import React from 'react';
import {Link} from "react-router";

export default React.createClass({
    contextTypes: {
        router: React.PropTypes.object
    },

    onClick : function(e){
        var parent = $(e.target).closest('.site-menu-item');
        if(parent.is('.open')){
            $(parent).removeClass('open');
        }
        else{
            $(parent).addClass('open');
        }
    },

    render: function () {
        let {routeId , subMenus } = this.props;
        let isActive = this.context.router.isActive(this.props.to, false);
        let className = isActive ? "site-menu-item active open" : "site-menu-item";

        if(typeof(routeId)==='string'){
            if(!isActive){
                if(window.location.hash.indexOf(routeId)>-1){
                    className = 'site-menu-item active open';
                }
            }
        }
        if(subMenus){
            className += ' has-sub';
            return (
                <li className={className}>
                    <a href="javascript:void(0)" className="clearfix animsition-link" onClick={this.onClick}>
                        {this.props.children}
                        <span className="site-menu-arrow"></span>
                    </a>
                    {subMenus}
                </li>
            );
        }
        //activeClassName={this.props.activeClassName}
        return (
            <li className={className}>
                <Link className="clearfix animsition-link" to={this.props.to} onClick={this.onClick}>
                    {this.props.children}
                </Link>
            </li>
        );
    }
});
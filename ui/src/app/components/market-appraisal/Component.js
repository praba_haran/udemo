import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import { Select,DatePicker, ValidateSelect,TimePicker, ValidateTextArea,CheckBox } from '../shared/Form';

class Component extends React.Component{

    render(){
        let {
            MARKET_APPRAISAL,
            onChange,
            PROPERTY_DETAIL,
            cancel,
            submit,
            canShowModal,
            isProcessing
        } = this.props;

        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal modal-md">
                <Modal.Header closeButton>
                    <Modal.Title>Book a Market Appraisal</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Type</label>
                                    <ValidateSelect className="col-sm-6" model={MARKET_APPRAISAL} name="type" onChange={onChange} dropKey="marketAppraisalTypes" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Lead Source</label>
                                    <ValidateSelect className="col-sm-6" model={MARKET_APPRAISAL} name="leadSource" onChange={onChange} dropKey="contactLeadSource" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Appraiser</label>
                                    {this._renderAppraiser()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Accompanied By</label>
                                    {this._renderAccompainedBy()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Date</label>
                                    <div className="col-sm-6">
                                        <DatePicker
                                            model={MARKET_APPRAISAL}
                                            name="date"
                                            onChange={onChange}
                                            options={{
                                                disablePreviousDate: true
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Time</label>
                                    <div className="col-sm-3">
                                        <TimePicker
                                            model={MARKET_APPRAISAL}
                                            name="startTime"
                                            onChange={onChange}
                                        />
                                    </div>
                                    <div className="col-sm-3">
                                        <TimePicker
                                            model={MARKET_APPRAISAL}
                                            name="endTime"
                                            onChange={onChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Reminder</label>
                                    <ValidateSelect className="col-sm-3" model={MARKET_APPRAISAL} name="reminder" onChange={onChange} dropKey="reminderMinutes" showLabel="false" />
                                    <ValidateSelect className="col-sm-3" model={MARKET_APPRAISAL} name="reminderChannel" onChange={onChange} dropKey="reminderChannel" showLabel="true" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Notes</label>
                                    <ValidateTextArea rows="6" className="col-sm-6" model={MARKET_APPRAISAL} name="notes" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-4 col-sm-6">
                                        <CheckBox model={MARKET_APPRAISAL} name="updateLastContactedDate" onChange={onChange} label="Update Last Contacted Date" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    canEdit(){
        let { PROPERTY_DETAIL } = this.props;
        return PROPERTY_DETAIL.recordStatus==='Active'
            &&  (PROPERTY_DETAIL.status==='Pre Appraisal' ||  PROPERTY_DETAIL.status==='Appraised');
    }

    _renderAppraiser(){
        let { MARKET_APPRAISAL, onChange, APP} = this.props;
        let { agencyDetails } = APP;
        if(agencyDetails && agencyDetails.agencyUsers) {
            return (
                <ValidateSelect className="col-sm-6" model={MARKET_APPRAISAL} name="appraiser"
                                onChange={onChange}>
                    {agencyDetails.agencyUsers.map(function (p) {
                        return (
                            <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                        );
                    })}
                </ValidateSelect>
            );
        }
    }

    _renderAccompainedBy(){
        let { MARKET_APPRAISAL, onChange, APP} = this.props;
        let { agencyDetails } = APP;
        if(agencyDetails && agencyDetails.agencyUsers) {
            return (
                <div className="col-sm-6">
                    <Select model={MARKET_APPRAISAL} name="accompaniedBy" onChange={onChange}>
                        {(MARKET_APPRAISAL && MARKET_APPRAISAL.appraiser) ? agencyDetails.agencyUsers.map(function (p) {
                            if (p._id !== MARKET_APPRAISAL.appraiser) {
                                return (
                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                );
                            }
                        }) : null}
                    </Select>
                </div>
            );
        }
    }
}

export default Component;
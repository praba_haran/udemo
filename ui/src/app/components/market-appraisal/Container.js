import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import MarketAppraisal from '../../models/MarketAppraisal';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false
        };
    }
    componentWillReceiveProps(props){
        let { PROPERTY_DETAIL,APP, COMMANDS,MARKET_APPRAISAL }= props;
        if(COMMANDS[constants.COMMANDS.BOOK_MARKET_APPRAISAL]!==this.state.canShowModal){
            let owners = _.filter(PROPERTY_DETAIL.owners,(p)=>{
                return p._id;
            });
            if(owners.length===0) {
                Alert.clearLogs().error('Please link owner(s) to this property.');
                Commands.actions.change(constants.COMMANDS.BOOK_MARKET_APPRAISAL,false);
            }
            else {
                this.setState({
                    canShowModal: COMMANDS[constants.COMMANDS.BOOK_MARKET_APPRAISAL]
                });
                // BIND NEGOTIATOR AND BRANCH AS DEFAULT IF IT IS NOT SAVED
                if(!MARKET_APPRAISAL._id) {
                    if (!this.state.canShowModal) {
                        let {userInfo} = APP;
                        if (userInfo && userInfo._id) {
                            MarketAppraisal.actions.change('appraiser', userInfo._id);
                        }
                    }
                }
            }
        }
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.BOOK_MARKET_APPRAISAL,false);
    }
    submitHandler(result){
        if (result.success) {
            this.cancel();
            Alert.clearLogs().success(result.message);
            Commands.actions.change(constants.COMMANDS.MARKET_APPRAISAL_SAVED, true);
        }
        else {
            console.log(result);
        }
        this.setState({
            isProcessing: false
        });
    }

    submit(){
        let { PROPERTY_DETAIL } = this.props;
        MarketAppraisal.actions.validateModel();
        let model = store.getState().MARKET_APPRAISAL.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            model.property = PROPERTY_DETAIL._id;
            if(model._id) {
                MarketAppraisal.api.update(model._id,model, this.submitHandler.bind(this));
            }
            else{
                MarketAppraisal.api.save(model, this.submitHandler.bind(this));
            }
        }
    }

    render () {
        return(
            <Component
                onChange={MarketAppraisal.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        MARKET_APPRAISAL : store.MARKET_APPRAISAL.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
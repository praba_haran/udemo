import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Users from '../../../models/Users';
import Alert from '../../../lib/Alert';
import Component from './Component';
import BasicGrid from '../../shared/grid/BasicGrid';

class Container extends BasicGrid {

    componentDidMount () {
        this.setUrl('user/list/paginate').load();
    }

    remove(id){
        Users.api.deleteItems([id],(result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);
                this.load();
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }
    disable(id){
        Users.api.disableItems([id],(result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);
                this.load();
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }
    activate(id){
        Users.api.activateItems([id],(result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);
                this.load();
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onSearchTextChange={this.onSearchTextChange.bind(this)}
                renderNoRecord={this.renderNoRecord.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                disable={this.disable.bind(this)}
                activate={this.activate.bind(this)}
                remove={this.remove.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        USERS: store.USERS.toJS(),
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

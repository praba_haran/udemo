import React from 'react';

class Component extends React.Component{

    render(){
        let { items, renderNoRecord, renderFooter,searchText,onSearchTextChange,
            remove, disable, activate } = this.props;
        return (
            <div className="clear users">
                <form className="form-horizontal invitation-form">
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-6 header">
                            <h3>Users</h3>
                        </div>
                    </div>
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-12">
                            <div className="basic-grid">
                                <div className="row">
                                    <div className="col-lg-6">
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="input-search input-search-dark pull-right">
                                            <i className="input-search-icon md-search" aria-hidden="true"></i>
                                            <input value={searchText} onChange={onSearchTextChange} type="text" className="form-control" placeholder="Search ..." />
                                            <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                                <table className="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Email Address</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Position</th>
                                        <th>Date Joined</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {items.map((p,index)=>{
                                        return (
                                            <tr key={p._id}>
                                                <td>{index+1}</td>
                                                <td>{p.email}</td>
                                                <td>{p.forename}</td>
                                                <td>{p.surname}</td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <span className="label label-primary label-outline">{p.status}</span>
                                                </td>
                                                <td>
                                                    <div className="btn-group" role="group">
                                                        <button type="button" className="btn btn-pure btn-xs waves-effect waves-light" id="exampleIconDropdown4" data-toggle="dropdown" aria-expanded="false">
                                                            <i className="icon md-more-vert" aria-hidden="true"></i>
                                                        </button>
                                                        <ul className="dropdown-menu" aria-labelledby="exampleIconDropdown4" role="menu">
                                                            <li role="presentation">
                                                                <a onClick={()=>remove(p._id)} href="javascript:void(0)" role="menuitem">
                                                                    <i className="icon md-delete"></i> Delete
                                                                </a>
                                                            </li>
                                                            {p.status!=='Disabled'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>disable(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-archive"></i> Disable
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                            {p.status!=='Active'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>activate(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-check"></i> Activate
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    {renderNoRecord({
                                        colSpan:8,
                                        noRecordText : 'No users found.'
                                    })}
                                    </tbody>
                                </table>
                                {renderFooter()}
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Component;
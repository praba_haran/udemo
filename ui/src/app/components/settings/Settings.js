import React from 'react';
import NavLink from '../shared/NavLink';

class Settings extends React.Component{

    render(){
        return (
            <div className="page page-settings">
                <div className="page-content">
                    <div className="nav-tabs-vertical nav-tabs-inverse">
                        <ul className="nav nav-tabs nav-tabs-solid">
                            <NavLink activeClassName="active" to="/settings/branches">Branches</NavLink>
                            <NavLink activeClassName="active" to="/settings/users">Users</NavLink>
                            <NavLink activeClassName="active" to="/settings/invite-people">Invite People</NavLink>
                            <NavLink activeClassName="active" to="/settings/groups">Groups</NavLink>
                            <NavLink activeClassName="active" to="/settings/diary-google">Diary - Google</NavLink>
                            <NavLink activeClassName="active" to="/settings/preferences">Preferences</NavLink>
                        </ul>
                        <div className="tab-content">
                            <div className="tab-pane active">{this.props.children}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Settings;
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../../store';
import InviteUsers from '../../../models/InviteUsers';
import Component from './CalendarSetting';
import GoogleCalendar from '../../../models/GoogleCalendar';
import GoogleCalendars from '../../../models/GoogleCalendars';

class Container extends React.Component {

    constructor(){
        super();
        this.connectGoogle = this.connectGoogle.bind(this);
    }

    componentDidMount(){
        let { GOOGLE_CALENDARS} = this.props;
        if(this.props.params.id!=='new'){
            if(GOOGLE_CALENDARS.items.length>0) {
                this.bindCalendarForEdit(GOOGLE_CALENDARS.items);
            }
            else{
                // IF THERE IS NO ITEMS IN THE CALENDAR LOAD CALENDARS FROM GOOGLE
                let { APP } = this.props;
                if(APP.meta.isGoogleAuthDone){
                    GoogleCalendars.api.getCalendars();
                }
            }
        }
    }

    componentWillReceiveProps(newProps){
        let { APP,GOOGLE_CALENDARS } = newProps;
        if(APP.meta.isGoogleAuthDone && GOOGLE_CALENDARS.items.length===0){
            GoogleCalendars.api.getCalendars((items)=>{
                this.bindCalendarForEdit(items);
            });
        }
    }

    bindCalendarForEdit(items){
        let calendar = _.find(items, (p) => {
            return p.summary === this.props.params.id;
        });
        if (calendar !== undefined) {
            GoogleCalendar.actions.setObjectInPath(['calendar'], calendar);
            GoogleCalendar.actions.change('error',null);
            GoogleCalendar.api.getPermissions(calendar);
        }
    }

    connectGoogle(){
        GoogleCalendars.api.connectToGoogle((result)=>{
            GoogleCalendars.api.getCalendars();
        });
    }

    addPermission(){
        let { GOOGLE_CALENDAR } = this.props;
        let length = GOOGLE_CALENDAR.permissions.length;
        GoogleCalendar.actions.setObjectInPath(['permissions',length],{
            etag :'',
            id:'',
            role :'reader',
            scope:{
                type:'user',
                value :''
            },
            status:'active'
        });
    }

    removePermission(index) {
        let { GOOGLE_CALENDAR } = this.props;
        if(GOOGLE_CALENDAR.permissions.length>1){
            GoogleCalendar.actions.setObjectInPath(['permissions', index,'status'],'deleted');
        }
    }

    createCalendar(){
        GoogleCalendar.actions.change('error',null);
        GoogleCalendar.actions.validateModel();
        let model = store.getState().GOOGLE_CALENDAR.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            model.permissions = _.filter(model.permissions,function(p){
                return p.scope.value!=='';
            });
            GoogleCalendar.api.createCalendar(model);
        }
    }

    updateCalendar(){
        GoogleCalendar.actions.change('error',null);
        GoogleCalendar.actions.validateModel();
        let model = store.getState().GOOGLE_CALENDAR.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            model.permissions = _.filter(model.permissions,function(p){
                return p.scope.value!=='';
            });
            GoogleCalendar.api.updateCalendar(model);
        }
    }

    render () {
        return(
            <Component {...this.props}
                       onChange={GoogleCalendar.actions.valueChange}
                       addPermission={this.addPermission.bind(this)}
                       removePermission={this.removePermission.bind(this)}
                       createCalendar={this.createCalendar.bind(this)}
                       updateCalendar={this.updateCalendar.bind(this)}
                       connectGoogle={this.connectGoogle}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        GOOGLE_CALENDAR: store.GOOGLE_CALENDAR.toJS(),
        GOOGLE_CALENDARS : store.GOOGLE_CALENDARS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

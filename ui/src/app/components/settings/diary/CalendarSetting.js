import React from 'react';
import _ from 'lodash';
import {Link} from "react-router";
import { ValidateText,ValidateTextArea,Validate,Select,ColorBlockPicker } from '../../shared/Form';

class CalendarSetting extends React.Component{

    render(){
        let { APP,GOOGLE_CALENDAR,GOOGLE_CALENDARS,onChange,addPermission,removePermission,createCalendar,updateCalendar,connectGoogle } = this.props;
        let { calendar } = GOOGLE_CALENDAR;
        let permissions = _.filter(GOOGLE_CALENDAR.permissions,(p)=>{
            return p.status ==='active';
        });
        return (
            <form className="form-horizontal custom-form">
                <div className="form-group margin-bottom-0">
                    <div className="col-sm-offset-2 col-sm-6 header">
                        <h3>{GOOGLE_CALENDAR.calendar.id?'Calendar Settings':'New Calendar'}</h3>
                        {!APP.meta.isGoogleAuthDone?(
                                <button onClick={connectGoogle} type="button" className="btn btn-labeled social-google-plus waves-effect waves-light">
                                        <span className="btn-label">
                                            <i className="icon bd-google-plus" aria-hidden="true"></i>
                                        </span>
                                    Connect Calendar
                                </button>
                            ):null}
                    </div>
                </div>
                <div className="form-group margin-bottom-0">
                    <div className="col-sm-offset-2 col-sm-6">
                        {GOOGLE_CALENDAR.error?(
                                <p className="message text-danger">
                                    <strong>{GOOGLE_CALENDAR.error}</strong>
                                </p>
                            ):null}
                        {GOOGLE_CALENDAR.message?(
                                <p className="message text-success">
                                    <strong>{GOOGLE_CALENDAR.message}</strong>
                                </p>
                            ):null}
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-2 col-sm-6">
                        <label>Calendar Name</label>
                        <ValidateText inputClass="form-control" model={GOOGLE_CALENDAR} name="calendar.summary" onChange={onChange} />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-2 col-sm-6">
                        <label>Kind</label>
                        <ValidateText inputClass="form-control" model={GOOGLE_CALENDAR} name="calendar.kind" onChange={onChange} />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-2 col-sm-3">
                        <label>Background Color</label>
                        <div>
                            <ColorBlockPicker inputClass="form-control" model={GOOGLE_CALENDAR} name="calendar.backgroundColor" onChange={onChange} />
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <label>Foreground Color</label>
                        <div>
                            <ColorBlockPicker inputClass="form-control" model={GOOGLE_CALENDAR} name="calendar.foregroundColor" onChange={onChange} />
                        </div>
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-2 col-sm-6">
                        <label>Description</label>
                        <ValidateTextArea rows="4" inputClass="form-control" model={GOOGLE_CALENDAR} name="calendar.description" onChange={onChange} />
                    </div>
                </div>
                <div className="form-group margin-top-30 margin-bottom-0">
                    <div className="col-sm-offset-2 col-sm-3">
                        <label>Email Address</label>
                    </div>
                    <div className="col-sm-3">
                        <label>Permission</label>
                    </div>
                </div>
                {permissions.map((p,index)=>{
                    return (
                        <div key={'permission_'+index} className="form-group margin-bottom-10">
                            <div className="col-sm-offset-2 col-sm-3">
                                <ValidateText inputClass="form-control" model={GOOGLE_CALENDAR} name={'permissions['+index+'].scope.value'} onChange={onChange} placeholder="name@domain.com" />
                            </div>
                            <div className="col-sm-3">
                                <Validate model={GOOGLE_CALENDAR} name={'permissions['+index+'].role'}>
                                    <Select showLabel="false" inputClass="form-control" model={GOOGLE_CALENDAR} name={'permissions['+index+'].role'} onChange={onChange}>
                                        <option value="owner">Make changes AND manage sharing</option>
                                        <option value="writer">Make changes to events</option>
                                        <option value="reader">See all event details</option>
                                        <option value="freeBusyReader">See only free/busy (hide details)</option>
                                    </Select>
                                </Validate>
                            </div>
                            <div className="col-sm-1">
                                {permissions.length > 1 ? (
                                        <button onClick={() => removePermission(index)} type="button" className="btn btn-pure btn-default icon md-close
                             waves-effect waves-circle waves-classic"></button>
                                    ) : null}
                            </div>
                        </div>
                    );
                })}
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-6">
                        <div className="add-person-block">
                            <a onClick={addPermission} href="javascript:void(0)" role="button">
                                <i className="icon md-plus-circle-o" aria-hidden="true"></i>
                                Add person
                            </a>
                        </div>
                        <div className="clear">
                            {GOOGLE_CALENDAR.calendar.id?(
                                    <button disabled={GOOGLE_CALENDAR.isProcessing} onClick={updateCalendar} type="button" className="btn btn-success waves-effect waves-light pull-right">
                                        {GOOGLE_CALENDAR.isProcessing?'Saving ...':'Save'}
                                    </button>
                                ):(
                                    <button disabled={GOOGLE_CALENDAR.isProcessing} onClick={createCalendar} type="button" className="btn btn-success waves-effect waves-light pull-right">
                                        {GOOGLE_CALENDAR.isProcessing?'Creating ...':'Create Calendar'}
                                    </button>
                                )}
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

export default CalendarSetting;
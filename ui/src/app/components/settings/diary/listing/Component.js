import React from 'react';
import {Link} from "react-router";
import _ from 'lodash';

class Component extends React.Component{

    render(){
        let { GOOGLE_CALENDARS, connectGoogle,APP } = this.props;
        let calendars = _.filter(GOOGLE_CALENDARS.items,(item)=>{
            return item.accessRole ==='owner';
        });
        return (
            <div className="clear calendar-listing">
                <form className="form-horizontal">
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-12 header">
                            <h3>Google Calendars</h3>
                        </div>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <div className="col-sm-6">
                            {!APP.userProfile?(
                                    <button onClick={connectGoogle} type="button" className="btn btn-labeled social-google-plus waves-effect waves-light">
                                        <span className="btn-label">
                                            <i className="icon bd-google-plus" aria-hidden="true"></i>
                                        </span>
                                        Connect Calendar
                                    </button>
                                ):null}
                        </div>
                        <div className="col-sm-6 text-right">
                            {APP.userProfile?(
                                    <Link to="/settings/diary-google/new" className="btn btn-sm btn-outline btn-primary btn-round waves-effect waves-light waves-round">
                                        <i className="icon md-plus" aria-hidden="true"></i>
                                        <span className="text hidden-xs">Create New</span>
                                    </Link>
                                ):null}
                        </div>
                    </div>
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-12">
                            {calendars.map((item)=>{
                                return (
                                    <div key={item.id} className="listing-item">
                                        <Link to={'/settings/diary-google/'+item.summary}>
                                            <i style={{color:item.backgroundColor}} className="icon fa-square"></i>
                                            {item.summary}
                                        </Link>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Component;
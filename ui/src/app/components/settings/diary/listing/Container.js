import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import GoogleCalendars from '../../../../models/GoogleCalendars';
import GoogleClient from '../../../../lib/GoogleClient';

class Container extends React.Component {

    constructor(){
        super();
        this.connectGoogle = this.connectGoogle.bind(this);
    }

    componentDidMount(){
        let { APP } = this.props;
        if(APP.meta.isGoogleApiLoaded && APP.meta.isGoogleAuthDone){
            GoogleCalendars.api.getCalendars();
        }
    }

    componentWillReceiveProps(newProps){
        let { APP,GOOGLE_CALENDARS } = newProps;
        if(APP.meta.isGoogleApiLoaded && APP.meta.isGoogleAuthDone && GOOGLE_CALENDARS.items.length===0){
            GoogleCalendars.api.getCalendars();
        }
    }

    connectGoogle(){
        GoogleCalendars.api.connectToGoogle((result)=>{
            GoogleCalendars.api.getCalendars();
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                connectGoogle={this.connectGoogle}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        GOOGLE_CALENDARS: store.GOOGLE_CALENDARS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
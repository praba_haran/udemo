import React from 'react';

class GoogleCalendars extends React.Component{

    render(){
        return (
            <div className="clear google-calendar">{this.props.children}</div>
        );
    }
}

export default GoogleCalendars;
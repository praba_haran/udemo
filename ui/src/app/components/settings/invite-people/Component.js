import React from 'react';
import _ from 'lodash';
import { ValidateText } from '../../shared/Form';

class Component extends React.Component{

    render(){
        let { INVITE_USERS} = this.props;
        return (
            <div className="clear invite-people">
                {this.renderInviteForm()}
                {this.renderInviteResponse()}
            </div>
        );
    }

    renderInviteForm(){
        let { INVITE_USERS,onChange,addUser,removeUser,inviteUsers } = this.props;
        let { users } = INVITE_USERS;
        if(INVITE_USERS.responses.length===0) {
            return (
                <form className="form-horizontal custom-form invitation-form">
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-offset-1 col-sm-9 header">
                            <h3>Invite Users</h3>
                        </div>
                    </div>
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-offset-2">
                            {INVITE_USERS.error ? (
                                    <p className="message text-danger"><strong>{INVITE_USERS.error}</strong>
                                    </p>) : null}
                        </div>
                    </div>
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-offset-2 col-sm-3">
                            <label><strong>Email Address</strong></label>
                        </div>
                        <div className="col-sm-2">
                            <label><strong>Forename</strong></label>
                        </div>
                        <div className="col-sm-2">
                            <label><strong>Surname</strong></label>
                        </div>
                    </div>
                    {users.map((user, index) => {
                        return (
                            <div key={'invite_users_' + index} className="form-group margin-bottom-10">

                                <div className="col-sm-offset-2 col-sm-3">
                                    <ValidateText inputClass="form-control" model={INVITE_USERS}
                                                  name={'users[' + index + '].email'} onChange={onChange}
                                                  placeholder="name@domain.com"/>
                                </div>
                                <div className="col-sm-2">
                                    <ValidateText inputClass="form-control" model={INVITE_USERS}
                                                  name={'users[' + index + '].forename'} onChange={onChange}
                                                  placeholder="Optional"/>
                                </div>
                                <div className="col-sm-2">
                                    <ValidateText inputClass="form-control" model={INVITE_USERS}
                                                  name={'users[' + index + '].surname'} onChange={onChange}
                                                  placeholder="Optional"/>
                                </div>
                                <div className="col-sm-1">
                                    {users.length > 1 ? (
                                            <button onClick={() => removeUser(index)} type="button" className="btn btn-pure btn-default icon md-close
                             waves-effect waves-circle waves-classic"></button>
                                        ) : null}
                                </div>
                            </div>
                        );
                    })}
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-7">
                            <div className="add-people-block">
                                <a onClick={addUser} href="javascript:void(0)" role="button">
                                    <i className="icon md-plus-circle-o" aria-hidden="true"></i>
                                    Add another invitation
                                </a>
                            </div>
                            <div className="clear">
                                <button disabled={INVITE_USERS.isProcessing} onClick={inviteUsers} type="button"
                                        className="btn btn-success waves-effect waves-light pull-right">
                                    {INVITE_USERS.isProcessing?'Inviting Users ...':'Invite Users'}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }
    }

    getTitle(responses,successResponses){
        if(successResponses.length===responses.length){
            return (<h3>Your invitation have been sent !</h3>);
        }
        else if(successResponses.length>0 && successResponses.length!==responses.length){
            return (<h3>That was only a partial success.</h3>);
        }
        else if(successResponses.length===0){
            return (<h3>That didn't work !</h3>);
        }
        return null;
    }

    getSubTitle(responses,successResponses){
        if(successResponses.length===responses.length){
            if(responses.length===1){
                return (<h5>You've <span className="highlight">invited 1</span> User.</h5>);
            }
            else {
                return (<h5>You've<span className="highlight">{" invited " + responses.length}</span> Users.</h5>);
            }
        }
        else if(successResponses.length===0){
            return (<h5><span>{responses.length+" invitation"}</span> didn't send. Review the results below.</h5>);
        }
        else if(successResponses.length>0 && successResponses.length!==responses.length){
            let subPart;
            let pendingCount = (responses.length-successResponses.length);
            if(pendingCount===1){
                subPart =(<span><span className="highlight">1 invitation didn't send.</span> Review the results below.</span>);
            }
            else{
                subPart =(<span><span className="hightlight">{pendingCount+" invitations didn't send."}</span> Review the results below.</span>);
            }
            if(successResponses.length===1){
                return (<h5>You've <span className="highlight">invited 1</span> User, but {subPart}</h5>);
            }
            else {
                return (<h5>You've <span className="hightlight">{"invited " + successResponses.length}</span> Users, but {subPart}</h5>);
            }
        }
        return null;
    }

    renderInviteResponse(){
        let { INVITE_USERS,inviteMorePeople } = this.props;
        let { responses } = INVITE_USERS;
        let successResponses = _.filter(responses,(r)=>{
            return r.inviteSent;
        });
        if(responses.length>0) {
            return (
                <form className="form-horizontal">
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-offset-1 col-sm-9 header">
                            {this.getTitle(responses,successResponses)}
                            {this.getSubTitle(responses,successResponses)}
                        </div>
                    </div>
                    <div className="form-group margin-bottom-0">
                        <div className="col-sm-offset-1 col-sm-9">
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {responses.map((item, index) => {
                                    return (
                                        <tr key={item.email}>
                                            <td>{item.email}</td>
                                            <td>{item.message}</td>
                                        </tr>
                                    );
                                })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-1 col-sm-3 margin-bottom-5">
                            <button type="button" className="btn btn-block btn-default">View Pending Invitations
                            </button>
                        </div>
                        <div className="col-sm-3 margin-bottom-5">
                            <button onClick={inviteMorePeople} type="button" className="btn btn-block btn-default">Invite More Users</button>
                        </div>
                        <div className="col-sm-3 margin-bottom-5">
                            <button onClick={inviteMorePeople} type="button" className="btn btn-block btn-default">Done</button>
                        </div>
                    </div>
                </form>
            );
        }
    }
}

export default Component;
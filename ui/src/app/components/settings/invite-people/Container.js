import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../../store';
import InviteUsers from '../../../models/InviteUsers';
import Component from './Component';

class Container extends React.Component {

    addUser(){
        let { INVITE_USERS } = this.props;
        let length = INVITE_USERS.users.length;
        InviteUsers.actions.setObjectInPath(['users',length],{
            email : '',
            forename : '',
            surname : ''
        });
    }

    removeUser(index) {
        let { INVITE_USERS } = this.props;
        if(INVITE_USERS.users.length>1){
            InviteUsers.actions.removeObjectInPath(['users', index]);
        }
    }

    inviteUsers(){
        InviteUsers.actions.change('error',null);
        InviteUsers.actions.validateModel();
        let model = store.getState().INVITE_USERS.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            model.users = _.filter(model.users,function(p){
                return p.email!=='';
            });
            if(model.users.length>0) {
                InviteUsers.actions.change('isProcessing',true);
                let _this = this;
                InviteUsers.api.inviteUsers(model,(result)=>{
                    if(result.success){
                        InviteUsers.actions.setObjectInPath(['responses'],result.data);
                    }
                    else{
                        if(!result.data.error.isValid){
                            InviteUsers.actions.setObjectInPath(['validator'],{
                                isSubmitted :true,
                                errors : result.data.error.errors,
                                isValid : result.data.error.isValid
                            });
                        }
                    }
                    InviteUsers.actions.change('isProcessing',false);
                });
            }
            else{
                InviteUsers.actions.change('error','Add at least one email address before sending invitations.');
            }
        }
    }
    inviteMorePeople(){
        InviteUsers.actions.setObjectInPath(['responses'],[]);
        InviteUsers.actions.setObjectInPath(['users'],[]);
        InviteUsers.actions.setObjectInPath(['users',0],{
            email : '',
            forename : '',
            surname : ''
        });
    }

    render () {
        return(
            <Component {...this.props}
                       onChange={InviteUsers.actions.valueChange}
                       addUser={this.addUser.bind(this)}
                       removeUser={this.removeUser.bind(this)}
                       inviteUsers={this.inviteUsers.bind(this)}
                       inviteMorePeople={this.inviteMorePeople.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        INVITE_USERS: store.INVITE_USERS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

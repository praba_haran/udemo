import React from 'react';
import { connect } from 'react-redux';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import PropertyDetail from '../../models/PropertyDetail';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            canShowModal : false
        };
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.SHOW_PROPERTY_SLIDE_SHOW,false);
    }

    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.SHOW_PROPERTY_SLIDE_SHOW]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.SHOW_PROPERTY_SLIDE_SHOW]
            });
        }
    }

    render () {
        return(
            <Component
                cancel={this.cancel.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
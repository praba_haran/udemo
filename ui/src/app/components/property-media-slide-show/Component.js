import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import ImageGallery from 'react-image-gallery';

class Component extends React.Component{

    handleImageLoad(event) {
        console.log('Image loaded ', event.target)
    }

    render(){
        let { PROPERTY_DETAIL,cancel,canShowModal } = this.props;
        let images = [];
        _.each(PROPERTY_DETAIL.photos,(p)=> {
            images.push({
                original : p.url.replace('upload','upload/w_1024,h_768,c_pad,b_black'),
                thumbnail : p.url.replace('upload','upload/w_354,h_255,c_pad,b_black')
            });
        });
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="modal-slide-show">
                <Modal.Header closeButton>
                    <Modal.Title>{PROPERTY_DETAIL.address.fullAddress}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-sm-offset-3 col-sm-6">
                            <ImageGallery
                                items={images}
                                slideInterval={2000}
                                showIndex = {true}
                                showFullscreenButton={false}
                            />
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

export default Component;
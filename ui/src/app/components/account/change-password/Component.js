import React from 'react';

export default class Component extends React.Component{

    render(){
        let { onSubmit, onChange, CHANGE_PASSWORD } = this.props;
        return(
            <div className="page vertical-align text-center">
                <div className="page-content vertical-align-middle">
                    <div className="panel">
                        <div className="panel-body">
                            <div className="brand">
                                <img className="brand-img" src="/dist/images/logo.png" alt="..." />
                                <h2 className="brand-text font-size-18">Change Password</h2>
                                {this.renderMessage()}
                            </div>
                            <form method="post" onSubmit={onSubmit}>
                                <div className="form-group form-material floating">
                                    <input value={CHANGE_PASSWORD.model.password} onChange={onChange} onBlur={onChange} type="password" className="form-control" name="model.password" id="model.password" />
                                    <label className="floating-label" htmlFor="model.password">New Password</label>
                                </div>
                                <div className="form-group form-material floating">
                                    <input value={CHANGE_PASSWORD.model.confirmPassword} onChange={onChange} onBlur={onChange} type="password" className="form-control" name="model.confirmPassword" id="model.confirmPassword" />
                                    <label className="floating-label" htmlFor="model.confirmPassword">Confirm New Password</label>
                                </div>
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-block margin-top-40"
                                    disabled={(CHANGE_PASSWORD.error!==null || CHANGE_PASSWORD.isProcessing)?'disabled':''}
                                >{CHANGE_PASSWORD.isProcessing?'Changing password ...':'Change password'}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderMessage(){
        let { CHANGE_PASSWORD } = this.props;
        if(CHANGE_PASSWORD.success!==null){
            return (<p><span className="text-success">{CHANGE_PASSWORD.success}</span></p>);
        }
        else if(CHANGE_PASSWORD.error!==null){
            return (<p><span className="text-danger">{CHANGE_PASSWORD.error}</span></p>);
        }
        else{
            return (<p>Input your new password</p>);
        }
    }
}
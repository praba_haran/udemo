import React from 'react';
import { hashHistory } from 'react-router';
import { connect } from 'react-redux';
import Component from './Component';
import store from '../../../store';
import ChangePassword from '../../../models/ChangePassword';

class Container extends React.Component{

    componentDidMount(){
        let { params } = this.props;
        if(params.token){
            ChangePassword.api.validateToken(params.token,(result)=>{
                ChangePassword.actions.change('isProcessing',false);
                if(result.success){
                    if(result.data.isValidToken){
                        if(result.data.isExpired){
                            ChangePassword.actions.change('error','Link Expired.');
                        }
                        else{
                            ChangePassword.actions.change('model.token',params.token);
                        }
                    }
                    else{
                        ChangePassword.actions.change('error','Invalid Link.');
                    }
                }
                else{
                    ChangePassword.actions.change('error',result.error);
                }
            });
        }
        else {
            ChangePassword.actions.change('error', 'Link is invalid.');
        }
    }

    onSubmit(e){
        e.preventDefault();
        let changePassword = store.getState().CHANGE_PASSWORD.toJS();
        if (changePassword.model.password==='') {
            ChangePassword.actions.change('error', 'Please enter your password.');
        }
        else if (changePassword.model.confirmPassword!==changePassword.model.password) {
            ChangePassword.actions.change('error', 'Confirm password is not matched.');
        }
        else {
            ChangePassword.actions.change('error',null);
            ChangePassword.actions.change('isProcessing',true);
            ChangePassword.api.changePassword(changePassword.model,(result)=>{
                ChangePassword.actions.change('isProcessing',false);
                if(result.success){
                    ChangePassword.actions.change('success',result.data);
                    // clearing the input
                    ChangePassword.actions.change('model.password','');
                    ChangePassword.actions.change('model.confirmPassword','');
                }
                else{
                    ChangePassword.actions.change('error',result.error);
                }
            });
        }
    }

    render (){
        return (
            <Component
                onSubmit={this.onSubmit.bind(this)}
                onChange={ChangePassword.actions.valueChange}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) =>{
    return {
        CHANGE_PASSWORD: store.CHANGE_PASSWORD.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';

export default class Component extends React.Component{

    render(){
        let { onSubmit, onChange, FORGOT_PASSWORD } = this.props;
        return(
            <div className="page vertical-align text-center">
                <div className="page-content vertical-align-middle">
                    <div className="panel">
                        <div className="panel-body">
                            <div className="brand">
                                <img className="brand-img" src="/dist/images/logo.png" alt="..." />
                                <h2 className="brand-text font-size-18">Forgot Your Password ?</h2>
                                {this.renderMessage()}
                            </div>
                            <form method="post" onSubmit={onSubmit}>
                                <div className="form-group form-material floating">
                                    <input value={FORGOT_PASSWORD.email} onChange={onChange} onBlur={onChange} type="email" className="form-control" name="email" id="email" />
                                    <label className="floating-label" htmlFor="email">Email</label>
                                </div>
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-block margin-top-40"
                                    disabled={(FORGOT_PASSWORD.success!==null || FORGOT_PASSWORD.isProcessing)?'disabled':''}
                                >{FORGOT_PASSWORD.isProcessing?'Sending instructions ...':'Send instructions'}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderMessage(){
        let { FORGOT_PASSWORD } = this.props;
        if(FORGOT_PASSWORD.success!==null){
            return (<p><span className="text-success">{FORGOT_PASSWORD.success}</span></p>);
        }
        else if(FORGOT_PASSWORD.error!==null){
            return (<p><span className="text-danger">{FORGOT_PASSWORD.error}</span></p>);
        }
        else{
            return (<p>Input your registered email to reset your password</p>);
        }
    }
}
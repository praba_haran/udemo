import React from 'react';
import { hashHistory } from 'react-router';
import { connect } from 'react-redux';
import Component from './Component';
import store from '../../../store';
import ForgotPassword from '../../../models/ForgotPassword';
import App from '../../../models/App';
import GoogleClient from '../../../lib/GoogleClient';
import scriptLoader from 'react-async-script-loader';

class Container extends React.Component{

    onSubmit(e){
        e.preventDefault();
        let model = store.getState().FORGOT_PASSWORD.toJS();
        if (model.email==='') {
            ForgotPassword.actions.change('error', 'Input your registered email to reset your password.');
        }
        else {
            ForgotPassword.actions.change('error',null);
            ForgotPassword.actions.change('isProcessing',true);
            ForgotPassword.api.sendInstructions(model,(result)=>{
                ForgotPassword.actions.change('isProcessing',false);
                if(result.success){
                    ForgotPassword.actions.change('success',result.data);
                }
                else{
                    ForgotPassword.actions.change('error',result.error);
                }
            });
        }
    }

    render (){
        return (
            <Component
                onSubmit={this.onSubmit.bind(this)}
                onChange={ForgotPassword.actions.valueChange}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) =>{
    return {
        FORGOT_PASSWORD: store.FORGOT_PASSWORD.toJS()
    };
};

export default connect(mapStateToProps)(Container);
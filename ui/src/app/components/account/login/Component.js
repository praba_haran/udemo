import React from 'react';
import {Link} from "react-router";

export default class Component extends React.Component{

    render(){
        let { onSubmit, onChange, model,signInWithGoogle,showGoogleSignIn } = this.props;
        return(
            <div className="page vertical-align text-center">
                <div className="page-content vertical-align-middle">
                    <div className="panel">
                        <div className="panel-body">
                            <div className="brand">
                                <img className="brand-img" src="/dist/images/logo.png" alt="..." />
                                <div className="text-danger">{model.error}</div>
                            </div>
                            <form method="post" onSubmit={onSubmit}>
                                <div className="form-group form-material floating">
                                    <input value={model.email} onChange={onChange} onBlur={onChange} type="email" className="form-control" name="email" id="email" />
                                    <label className="floating-label" htmlFor="email">Email</label>
                                </div>
                                <div className="form-group form-material floating">
                                    <input value={model.password} onChange={onChange} onBlur={onChange} type="password" className="form-control" name="password" id="password" />
                                    <label className="floating-label" htmlFor="password">Password</label>
                                </div>
                                <div className="form-group clearfix">
                                    <div className="checkbox-custom checkbox-inline checkbox-primary checkbox-lg pull-left">
                                        <input type="checkbox" id="remember" name="remember" />
                                        <label htmlFor="remember">Remember me</label>
                                    </div>
                                    <Link className="pull-right" to="forgot-password">Forgot password?</Link>
                                </div>
                                <button type="submit" className="btn btn-primary btn-block margin-top-40"
                                        disabled={model.isProcessing?'disabled':''}>{model.isProcessing?'Signing in ...':'Sign in'}</button>
                            </form>
                            <p>Still no account? Please go to <a href="register-v3.html">Sign up</a></p>
                            {showGoogleSignIn?(
                                    <div className="clear">
                                        <button onClick={signInWithGoogle} type="button" className="btn btn-block social-google-plus waves-effect waves-light">
                                            <i className="icon bd-google-plus"></i>
                                            Signin with Google
                                        </button>
                                    </div>
                                ):null}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
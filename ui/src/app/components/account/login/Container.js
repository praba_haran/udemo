import React from 'react';
import { hashHistory } from 'react-router';
import { connect } from 'react-redux';
import Component from './Component';
import store from '../../../store';
import Login from '../../../models/Login';
import App from '../../../models/App';
import GoogleClient from '../../../lib/GoogleClient';
import scriptLoader from 'react-async-script-loader';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            showGoogleSignIn : false
        };
    }

    componentDidMount () {
        const { isScriptLoaded, isScriptLoadSucceed } = this.props;
        if (isScriptLoaded && isScriptLoadSucceed) {
            this.setState({
                showGoogleSignIn : true
            });
        }
    }

    componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
        if (isScriptLoaded && !this.props.isScriptLoaded) {
            if (isScriptLoadSucceed) {
                this.setState({
                    showGoogleSignIn : true
                });
            }
            //else this.props.onError()
        }
    }

    isValid(model){
        let isValid = true;
        if(model.email === '' && model.password===''){
            Login.actions.change('error', 'Please provide email and password.');
            isValid = false;
        }
        else {
            if (model.email === '') {
                Login.actions.change('error', 'Please provide email.');
                isValid = false;
            }
            if (model.password === '') {
                Login.actions.change('error', 'Please provide password.');
                isValid = false;
            }
        }
        return isValid;
    }

    onSubmit(e){
        e.preventDefault();
        let model = store.getState().LOGIN.toJS();
        if(this.isValid(model)){
            Login.api.login({
                email : model.email,
                password : model.password
            }, function () {
                Login.actions.clearModel();
                hashHistory.replace('/');
            });
        }
    }

    signInWithGoogle(){
        let Calendar = GoogleClient.getCalendar();
        Calendar.login(function(profile){
            Login.api.logUserLogin({
                login_via:'google',
                profile : profile
            },function(){
                App.actions.change('meta.isGoogleAuthDone',true);
                hashHistory.push('/');
            });
        });
    }

    render (){
        return (
            <Component
                onSubmit={this.onSubmit.bind(this)}
                onChange={Login.actions.valueChange}
                signInWithGoogle={this.signInWithGoogle.bind(this)}
                showGoogleSignIn={this.state.showGoogleSignIn}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = (store) =>{
    return {
        model: store.LOGIN.toJS()
    };
};

export default scriptLoader([
    'https://apis.google.com/js/client:plusone.js'
])( connect(mapStateToProps)(Container));
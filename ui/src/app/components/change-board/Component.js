import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import { Select, ValidateTextArea,ValidateText, ValidateSelect,CheckBox } from '../shared/Form';

class Component extends React.Component{

    render(){
        let { BOARD_CHANGE_REQUEST, onChange, PROPERTY_DETAIL,boardContractors ,cancel,submit,canShowModal,isProcessing } = this.props;
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal modal-md">
                <Modal.Header closeButton>
                    <Modal.Title>CHANGE BOARD</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-6">MANAGE BOARD</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Current Board</label>
                                    <ValidateText className="col-sm-6" model={BOARD_CHANGE_REQUEST} name="currentBoard" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Board Change</label>
                                    <ValidateSelect className="col-sm-6" model={BOARD_CHANGE_REQUEST} name="newBoard" onChange={onChange} dropKey="boardTypes" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Board Contractor</label>
                                    <div className="col-sm-6">
                                        <ValidateSelect model={BOARD_CHANGE_REQUEST} name="boardContractor" onChange={onChange}>
                                            {boardContractors.map((p)=>{
                                                return (<option key={p._id} value={p._id}>{p.firstPerson.forename}</option>);
                                            })}
                                        </ValidateSelect>
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Message</label>
                                    <ValidateTextArea rows="6" className="col-sm-6" model={BOARD_CHANGE_REQUEST} name="message" onChange={onChange} />
                                </div>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
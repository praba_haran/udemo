import React from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import BoardChangeRequest from '../../models/BoardChangeRequest';
import ContactDetail from '../../models/ContactDetail';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false,
            boardContractors : []
        };
    }

    cancel(){
        BoardChangeRequest.actions.clearModel();
        Commands.actions.change(constants.COMMANDS.CHANGE_BOARD,false);
    }

    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.CHANGE_BOARD]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.CHANGE_BOARD]
            });
            // LOADING RECENT BOARD CHANGE REQUEST
            BoardChangeRequest.api.list({
                property : PROPERTY_DETAIL._id
            },(result)=>{
                if(result.success){
                    if(result.data.length>0){
                        let oldBoardChangeReq = result.data[0];
                        let newBoardChangeReq = {
                            property : PROPERTY_DETAIL._id,
                            currentBoard : oldBoardChangeReq.newBoard,
                            newBoard :'',
                            boardContractor : oldBoardChangeReq.boardContractor,
                            message : '',
                            includeOwnerInMail : false
                        };
                        BoardChangeRequest.actions.setModel(newBoardChangeReq);
                    }
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });
            // LOADING BOARD CONTRACTORS
            this.loadBoardContractors();
        }
    }

    loadBoardContractors(){
        ContactDetail.api.search({ type : 'Board Contractor' },(result)=>{
            if(result.success){
                this.setState({
                    boardContractors : result.data
                });
            }
            else{
                Alert.clearLogs().error(result.error);
            }
        });
    }

    submit(){
        BoardChangeRequest.actions.validateModel();
        let model = store.getState().BOARD_CHANGE_REQUEST.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            BoardChangeRequest.api.save(model,(result)=>{
                if (result.success) {
                    this.cancel();
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                    Alert.clearLogs().success(result.message);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing: false
                });
            });
        }
    }

    render () {
        return(
            <Component
                onChange={BoardChangeRequest.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        BOARD_CHANGE_REQUEST : store.BOARD_CHANGE_REQUEST.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
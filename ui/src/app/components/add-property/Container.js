import React from 'react';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Component from './Component';
import Commands from '../../models/Commands';
import Property from '../../models/Property';
import ErrorMessages from '../../models/ErrorMessages';
import Alert from '../../lib/Alert';

class Container extends React.Component {

    constructor() {
        super();
        this.state = {
            canShowModal : false,
            addresses: [],
            isFetchingAddress: false,
            isProcessing : false
        };
    }

    componentWillReceiveProps(props){
        let { COMMANDS, APP }= props;
        if(COMMANDS[constants.COMMANDS.ADD_PROPERTY]!==this.state.ADD_PROPERTY){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.ADD_PROPERTY]
            });
            // BIND NEGOTIATOR AND BRANCH AS DEFAULT
            if(! this.state.canShowModal) {
                let {userInfo} = APP;
                if (userInfo && userInfo._id) {
                    Property.actions.change('negotiator', userInfo._id);
                    Property.actions.change('agency', userInfo.agency._id);
                }
            }
        }
    }

    onSubmit(e) {
        // e.preventDefault();
        console.log('hete');
        Property.actions.validateModel();
        let model = store.getState().PROPERTY.toJS();
        // if (model.validator.isValid) {//problem while save but we need to check because of schmea rules not matching
            delete model.validator;
            this.setState({
                isProcessing : true
            });
            ErrorMessages.actions.change(constants.ERROR_MESSAGES.ADD_PROPERTY_ERROR,null);
            Property.api.save(model,(result)=>{
                if(result.success){
                    Alert.clearLogs().success(result.message);
                    hashHistory.push('/properties/' + result.data._id+'/overview');
                    this.cancel();
                }
                else{
                    if(typeof result.error==='string') {
                        ErrorMessages.actions.change(constants.ERROR_MESSAGES.ADD_PROPERTY_ERROR, result.error);
                    }
                    else{
                        console.log('errors',result.error);
                    }
                }
                this.setState({
                    isProcessing : false
                });
            });
        // }
    }

    cancel() {
        Commands.actions.change(constants.COMMANDS.ADD_PROPERTY, false);
        Property.actions.clearModel();
    }

    onPostcodeChange(text) {
        if (text.length > 2) {
            text = text.toUpperCase();
            this.setState({
                isFetchingAddress: true
            });
            Property.api.searchAddress(text,(result)=>{
                if(result.success) {
                    this.setState({
                        addresses: result.data,
                        isFetchingAddress: false
                    });
                }
            });
        }
    }

    onPostcodeSelected(items) {
        if (items.length > 0) {
            let address = items[0];
            let matchNumbers = address.street.match(/\d+/);
            if(matchNumbers.length>0) {
                address.nameOrNumber = matchNumbers[0];
            }
            address.street = address.street.replace(address.nameOrNumber,'').trim();
            Property.actions.setObjectInPath(['address'],address);

            // once the address is populated validate the text box
            let model = store.getState().PROPERTY.toJS();
            if (model.validator.isSubmitted) {
                Property.actions.validateModel();
            }
            // once address is binded clear the error message
            ErrorMessages.actions.change(constants.ERROR_MESSAGES.ADD_PROPERTY_ERROR,null);
        }
    }

    render() {
        return (
            <Component
                {...this.props}
                {...this.state}
                onChange={Property.actions.valueChange}
                onSubmit={this.onSubmit.bind(this)}
                cancel={this.cancel.bind(this)}
                onPostcodeChange={this.onPostcodeChange.bind(this)}
                onPostcodeSelected={this.onPostcodeSelected.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.PROPERTY.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        error_messages : store.ERROR_MESSAGES.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import Typeahead from 'react-bootstrap-typeahead';
import { TextBox, Select, TilePicker , ValidateText, ValidateSelect } from '../shared/Form';
import * as constants from '../../constants';

class Component extends React.Component{

    render(){
        let {
            model ,
            APP,
            onChange,
            onPostcodeChange,
            onPostcodeSelected ,
            addresses,
            isFetchingAddress,
            error_messages,
            isProcessing,
            canShowModal,
            onSubmit,
            cancel
        } = this.props;
        let { agencyDetails, userInfo } = APP;
        return (
            <Modal backdrop="static" bsSize="lg" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Add Property....</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {error_messages[constants.ERROR_MESSAGES.ADD_PROPERTY_ERROR]?(
                        <p className="text-danger text-center">{error_messages[constants.ERROR_MESSAGES.ADD_PROPERTY_ERROR]}</p>
                    ):null}
                    <div className="row">
                        <div className="col-lg-6">
                            <form className="form-horizontal custom-form">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-8">ADDRESS </h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Find Address</label>
                                    <div className="col-sm-7">
                                        <Typeahead
                                            placeholder="Post Code"
                                            emptyLabel={isFetchingAddress?'Loading ...':'No address found.'}
                                            minLength={2}
                                            filterBy={['fullAddress']}
                                            labelKey="postcode"
                                            options={addresses}
                                            onInputChange={onPostcodeChange}
                                            onChange ={ onPostcodeSelected}
                                            maxResults={200}
                                            paginate={true}
                                            renderMenuItemChildren={(props, option, idx) => {
                                                return (<span><i className="icon fa-map-marker"></i>{option.fullAddress+', '+option.postcode}</span>);
                                            }} />
                                        <span className="help-block">Or Enter Address Manually</span>
                                    </div>
                                </div>
                                <div className="clear">
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Dwelling</label>
                                        <ValidateText className="col-sm-3" model={model} name="address.dwelling" onChange={onChange} />
                                        <label className="col-sm-2 control-label">Name/No</label>
                                        <ValidateText className="col-sm-2" model={model} name="address.nameOrNumber" onChange={onChange} />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Street</label>
                                        <ValidateText className="col-sm-7" model={model} name="address.street" onChange={onChange} />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Locality</label>
                                        <ValidateText className="col-sm-7" model={model} name="address.locality" onChange={onChange} />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Town</label>
                                        <ValidateText className="col-sm-7" model={model} name="address.town" onChange={onChange} type="onlyAlphabet" />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">County</label>
                                        <ValidateText className="col-sm-7" model={model} name="address.county" onChange={onChange} type="onlyAlphabet" />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Postcode</label>
                                        <ValidateText className="col-sm-7" model={model} name="address.postcode" onChange={onChange} mask="postcode" />
                                    </div>
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Country</label>
                                        <ValidateSelect className="col-sm-7" model={model} name="address.country" onChange={onChange} dropKey="country" showLabel="false"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-6">
                            <form className="form-horizontal custom-form">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-8">GENERAL</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Market</label>
                                    <ValidateSelect className="col-sm-7" model={model} name="market" onChange={onChange} dropKey="propertyMarket" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Type</label>
                                    <ValidateSelect className="col-sm-7" model={model} name="type" onChange={onChange} dropKey="propertyType" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Category</label>
                                    <ValidateSelect className="col-sm-7" model={model} name="category" onChange={onChange} dropKey="propertyCategory" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Rooms</label>
                                    <div className="col-sm-7 tile-manager tile-odd-even">
                                        <TilePicker model={model} name="bedrooms" onChange={onChange} tileHeading="Bedrooms" type="numeric" />
                                        <TilePicker model={model} name="bathrooms" onChange={onChange} tileHeading="Bathrooms" type="numeric" />
                                        <TilePicker model={model} name="receptions" onChange={onChange} tileHeading="Receptions" type="numeric" />
                                        <TilePicker model={model} name="parking" onChange={onChange} tileHeading="Parking" type="numeric" />
                                    </div>
                                </div>
                                {agencyDetails!==null?(
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Branch</label>
                                        <ValidateSelect className="col-sm-7" model={model} name="agency" onChange={onChange}>
                                            {agencyDetails.branchDetails.map(function(p){
                                                return (
                                                    <option key={p._id} value={p._id}>{p.address.locality+'-'+p.address.town}</option>
                                                );
                                            })}
                                        </ValidateSelect>
                                    </div>
                                ):null}
                                {agencyDetails!==null?(
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-4 control-label">Negotiator</label>
                                        <ValidateSelect className="col-sm-7" model={model} name="negotiator" onChange={onChange}>
                                            {agencyDetails.agencyUsers.map(function(p){
                                                return (
                                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                                );
                                            })}
                                        </ValidateSelect>
                                    </div>
                                ):null}
                            </form>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={onSubmit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
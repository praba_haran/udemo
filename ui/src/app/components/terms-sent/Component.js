import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import { ValidateTextArea } from '../shared/Form';

class Component extends React.Component{

    render(){
        let { TERMS_SENT, onChange, PROPERTY_DETAIL ,cancel,submit,canShowModal,isProcessing } = this.props;
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal modal-md">
                <Modal.Header closeButton>
                    <Modal.Title>TERMS SENT</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal">
                        <div className="form-group margin-bottom-0">
                            <div className="col-sm-12">
                                <h6>NOTES</h6>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateTextArea className="col-sm-12" rows="10" model={TERMS_SENT} name="notes" onChange={onChange} />
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
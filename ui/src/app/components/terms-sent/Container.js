import React from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import TermsSent from '../../models/TermsSent';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false
        };
    }

    cancel(){
        TermsSent.actions.clearModel();
        Commands.actions.change(constants.COMMANDS.TERMS_SENT,false);
    }

    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.TERMS_SENT]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.TERMS_SENT]
            });
            // SET PROPERTY ID
            TermsSent.actions.change('property',PROPERTY_DETAIL._id);
            // LOADING RECENT TERMS SENT
            TermsSent.api.list({
                property : PROPERTY_DETAIL._id
            },(result)=>{
                if(result.success){
                    if(result.data.length>0){
                        let termsSent = result.data[0];
                        TermsSent.actions.setModel(termsSent);
                    }
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    submit(){
        TermsSent.actions.validateModel();
        let model = store.getState().TERMS_SENT.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            TermsSent.api.save(model,(result)=>{
                if (result.success) {
                    this.cancel();
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                    Alert.clearLogs().success(result.message);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing: false
                });
            });
        }
    }

    render () {
        return(
            <Component
                onChange={TermsSent.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        TERMS_SENT : store.TERMS_SENT.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
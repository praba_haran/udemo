import React from 'react';
import _ from 'lodash';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import Alert from '../../lib/Alert';
import Component from './Component';
import KeyListing from '../../models/KeyListing';

class Container extends React.Component {

    componentDidMount () {
        this.getItems();
    }

    getItems(){
        KeyListing.actions.change('isProcessing',true);
        KeyListing.api.list((result)=>{
            if(result.success){
                console.log(result.data);
                KeyListing.actions.setObjectInPath(['items'],result.data);
            }
            KeyListing.actions.change('isProcessing', false);
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                onChange={KeyListing.actions.valueChange}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        KEY_LISTING: store.KEY_LISTING.toJS(),
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

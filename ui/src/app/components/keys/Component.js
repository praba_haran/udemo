import React from 'react';
import Helper from '../shared/helper';

class Component extends React.Component{

    render () {
        let { KEY_LISTING, onQueryChange } = this.props;
        let { items, isProcessing, query } = KEY_LISTING;
        let noRecordFound;
        if(items.length===0){
            noRecordFound = (
                <tr className="no-record-found ">
                    <td colSpan="11" className="text-center">{isProcessing?'Loading ...':'No items found !'}</td>
                </tr>
            );
        }
        return (
            <div className="page">
                <div className="page-content">
                    <div className="panel panel-transparent contact-listing-panel">
                        <div className="panel-body container-fluid">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="input-search input-search-dark pull-right">
                                        <i className="input-search-icon md-search" aria-hidden="true"></i>
                                        <input value={query.searchText} onChange={(e)=>onQueryChange('query.searchText',e.target.value)} type="text" className="form-control" placeholder="Search ..." />
                                        <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <div className="example">
                                <table className="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Key Number</th>
                                        <th>Property </th>
                                        <th>Total Keys</th>
                                        <th>Alarm Code</th>
                                        <th>Notes</th>
                                        <th>Key Holder</th>
                                        <th>Status</th>
                                        <th></th>
                                        <th>Time Out</th>
                                        <th>Expected In</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {items.map((p,index)=>{
                                        return (
                                            <tr key={p._id}>
                                                <td>{p.keyNo}</td>
                                                <td>{p.property.address.fullAddress}</td>
                                                <td>{p.numberOfKeys}</td>
                                                <td>{p.alarmCode}</td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <span className="label label-primary label-outline">{p.status}</span>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>

                                                </td>
                                            </tr>
                                        );
                                    })}
                                    {noRecordFound}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
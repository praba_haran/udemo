import React from 'react';
import NavLink from '../shared/NavLink';
import * as constants from '../../constants';
import GeneralAppointmentModal from '../general-appointment/Container';
import MakeAnOffer from '../offer/Container';
import ViewingModal from '../viewing/Container';
import ViewingEmailConfirmation from '../viewing-email-confirmation/Container';
import FileNotesModal from '../file-notes/Container';

class Component extends React.Component{

    canShowRequirements(){
        let { model } = this.props;
        if(!model._id){
            return false;
        }
        else {
            return model.type ==='Client' && ( model.intension==='To Buy'
                || model.intension === 'To Rent'
                || model.intension === 'To Buy or To Rent');
        }
    }

    render(){
        let id = this.props.params.id;
        let { COMMANDS } = this.props;
        return (
            <div className="page contact-detail-page">
                <div className="page-content">
                    <div className="panel nav-tabs-horizontal panel-transparent">
                        <ul className="nav nav-tabs nav-tabs-line">
                            <NavLink activeClassName="active" to={'/contacts/'+id+'/overview'}>Overview</NavLink>
                            <NavLink activeClassName="active" to={'/contacts/'+id+'/details'}>Details</NavLink>
                            <NavLink activeClassName="active" to={'/contacts/'+id+'/properties'}>Properties</NavLink>
                            <NavLink activeClassName="active" to={'/contacts/'+id+'/documents'}>Documents</NavLink>
                            {this.canShowRequirements()?(
                                <NavLink activeClassName="active" to={'/contacts/'+id+'/requirements'}>Requirements</NavLink>
                            ):null}
                        </ul>
                        <div className="panel-body">
                            <div className="tab-content">{this.props.children}</div>
                        </div>
                    </div>
                </div>
                <GeneralAppointmentModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_GENERAL_APPOINTMENT]} location={this.props.location} params={this.props.params} />
                <MakeAnOffer canShowModal={COMMANDS[constants.COMMANDS.MAKE_AN_OFFER]} location={this.props.location} params={this.props.params} />
                <ViewingModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING]} location={this.props.location} params={this.props.params} />
                <ViewingEmailConfirmation canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM]} location={this.props.location} params={this.props.params} />
                <FileNotesModal canShowModal={COMMANDS[constants.COMMANDS.ADD_NOTES]} location={this.props.location} params={this.props.params} />
            </div>
        );
    }
}

export default Component
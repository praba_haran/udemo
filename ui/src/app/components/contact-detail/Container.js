import React from 'react';
import {connect} from 'react-redux';
import ContactDetail from '../../models/ContactDetail';
import ContactListing from '../../models/ContactListing';
import Commands from '../../models/Commands';
import Component from './Component';
import * as constants from '../../constants';
import Alert from '../../lib/Alert';

class Container extends React.Component {

    constructor() {
        super();
        this.onPostcodeChange = this.onPostcodeChange.bind(this);
        this.onPostcodeSelected = this.onPostcodeSelected.bind(this);

        this.state = {
            addresses: [],
            isFetchingAddress: false,
            isSearchingProperty: false,
            interestedProperties: []
        };
    }
    componentDidMount() {
        this.getContactById(this.props);
    }
    componentWillReceiveProps(newProps) {
        let { COMMANDS } = newProps;
        if (this.props.params.id !== newProps.params.id) {
            this.getContactById(newProps);
        }
        else if(COMMANDS.ARCHIVE_CONTACT){
            Commands.actions.change(constants.COMMANDS.ARCHIVE_CONTACT, !COMMANDS.ARCHIVE_CONTACT);
            this.archive();
        }
        else if(COMMANDS.ACTIVATE_CONTACT){
            Commands.actions.change(constants.COMMANDS.ACTIVATE_CONTACT, !COMMANDS.ACTIVATE_CONTACT);
            this.activate();
        }
    }

    getContactById(props) {
        ContactDetail.api.get(props.params.id, (result) => {
            if (result.success) {
                ContactDetail.actions.setModel(result.data);
            }
        });
    }
    onPostcodeChange(text) {
        if (text.length === 7) {
            text = text.toUpperCase().trim();
            this.setState({
                isFetchingAddress: true
            });
            let _this = this;
            ContactDetail.api.getAddress(text, function (data) {
                _this.setState({
                    addresses: data.addresses,
                    isFetchingAddress: false
                });
            });
        }
    }
    onPostcodeSelected(items) {
        if (items.length > 0) {
            ContactDetail.actions.setObjectInPath(['address'], items[0]);
        }
    }
    archive(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to archive this contact ?', ()=>{
            let { params } = this.props;
            ContactListing.api.archiveItems([params.id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.getContactById(this.props);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        });
    }

    activate(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to activate this contact ?', ()=>{
            let { params } = this.props;
            ContactListing.api.activateItems([params.id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.getContactById(this.props);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        });
    }

    render() {
        return (
            <Component
                {...this.props}
                onChange={ContactDetail.actions.valueChange}
                onPostcodeChange={this.onPostcodeChange}
                onPostcodeSelected={this.onPostcodeSelected}
                addresses={this.state.addresses}
                isFetchingAddress={this.state.isFetchingAddress}
                isSearchingProperty={this.state.isSearchingProperty}
                interestedProperties={this.state.interestedProperties}

            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS: store.COMMANDS.toJS(),
        CONTACT_REQUIREMENT: store.CONTACT_REQUIREMENT.toJS(),
        PROPERTY_LISTING: store.PROPERTY_LISTING.toJS(),
    };
};

export default connect(mapStateToProps)(Container);

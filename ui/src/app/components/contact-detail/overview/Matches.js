import React from 'react';
import MatchListItem from './MatchListItem';

class Matches extends React.Component{

    renderItem (){
        return (
            <li className="list-group-item">
                <div className="media">
                    <div className="media-left">
                        <a className="avatar" href="javascript:void(0)">
                            <img className="img-responsive" src="/dist/images/photos/overview-1.jpg"/>
                        </a>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">3 bed semi-detached bungalow for sale</h4>
                        <p className="margin-bottom-0">
                            <span className="text-danger">£ 50,00,000</span>
                            <small className="margin-left-5">Guide price</small>
                        </p>
                        <p className="margin-bottom-0">Chancery Fields, Euxton PR7</p>
                        <p className="margin-top-10 margin-bottom-0">
                            <small>
                                <i className="icon fa-envelope margin-right-5"></i>
                                <a className="margin-right-10" href="javascript:void(0)" title="">aruljothiparthiban@hotmail.com</a>
                                <i className="icon fa-mobile margin-right-5"></i>
                                <a href="javascript:void(0)" title="">+44 7123 123456</a>
                            </small>
                        </p>
                        <div className="actions">
                            <span className="label label-outline label-default">Add Appointment</span>
                            <span className="label label-outline label-default">Book a MA</span>
                            <span className="label label-outline label-default">Book a Viewing</span>
                            <span className="label label-outline label-default">Make an Offer</span>
                        </div>
                    </div>
                </div>
            </li>
        );
    }

    render () {
        let { CONTACT_REQUIREMENT } = this.props;
        let { matches } = CONTACT_REQUIREMENT;
        let noMatchFound;
        if(matches.length===0){
            noMatchFound = (
                <li className="list-group-item no-item-found">
                    <p className="img-container"><img src="/dist/images/photos/no-matches-found.png" /></p>
                    <h4 className="text">No matches to see here ...</h4>
                </li>
            );
        }
        return(
            <div className="row">
                <div className="col-lg-12 matching-container">
                    <ul className="list-group list-group-dividered list-group-full">
                        {matches.map((p)=>{
                            return (
                                <MatchListItem key={p._id} property={p} />
                            )
                        })}
                        {noMatchFound}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Matches;
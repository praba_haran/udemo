import React from 'react';
import _ from 'lodash';
import Activities from './Activities';
import Appointments from './Appointments';
import Offers from './Offers';
import Viewings from './Viewings';
import { Scrollbars } from 'react-custom-scrollbars';

class SectionRight extends React.Component{

    constructor(){
        super();
        this.state ={
            activeTab:'activities'
        };
        this.handleTabChange = this.handleTabChange.bind(this);
    }
    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }
    isActiveContact(){
        let { CONTACT_DETAIL } = this.props;
        return (CONTACT_DETAIL.status.toLowerCase()==='active');
    }
    isClient(){
        let { CONTACT_DETAIL } = this.props;
        return CONTACT_DETAIL.type === 'Client';
    }
    isBoardContractor(){
        let { CONTACT_DETAIL } = this.props;
        return CONTACT_DETAIL.type === 'Board Contractor';
    }

    render(){
        let { activeTab } = this.state;
        return (
            <div className="col-lg-6 section-right">
                <div className="nav-tabs-horizontal">
                    <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                        <li role="presentation" className={(activeTab==='activities')?'active':''}>
                            <a onClick={()=>this.handleTabChange('activities')} role="button" href="javascript:void(0)">Activities</a>
                        </li>
                        <li role="presentation" className={(activeTab==='appointments')?'active':''}>
                            <a onClick={()=>this.handleTabChange('appointments')} role="button" href="javascript:void(0)">Appointments</a>
                        </li>
                        {this.isClient()?(
                                <li role="presentation" className={(activeTab==='matches')?'active':''}>
                                    <a onClick={()=>this.handleTabChange('matches')} role="button" href="javascript:void(0)">Matches</a>
                                </li>
                            ):null}
                        {this.isActiveContact()?(
                                <li role="presentation" className={(activeTab==='offers')?'active':''}>
                                    <a onClick={()=>this.handleTabChange('offers')} role="button" href="javascript:void(0)">Offers</a>
                                </li>
                            ):null}
                        {this.isActiveContact()?(
                                <li role="presentation" className={(activeTab==='viewings')?'active':''}>
                                    <a onClick={()=>this.handleTabChange('viewings')} role="button" href="javascript:void(0)">Viewings</a>
                                </li>
                            ):null}
                        {this.isBoardContractor()?(
                                <li role="presentation" className={(activeTab==='board-change-requests')?'active':''}>
                                    <a onClick={()=>this.handleTabChange('board-change-requests')} role="button" href="javascript:void(0)">Requests</a>
                                </li>
                            ):null}
                    </ul>
                    <div className="tab-content">
                        <Scrollbars
                            style={{height:420}}
                        >
                            <div className="scrolling-area">
                                <div className="tab-pane active">
                                    {activeTab==='activities'?(<Activities {...this.props} />):null}
                                    {activeTab==='appointments'?(<Appointments {...this.props} />):null}
                                    {/*{activeTab==='matches'?(<Matches {...this.props} />):null}*/}
                                    {activeTab==='offers'?(<Offers {...this.props} />):null}
                                    {activeTab==='viewings'?(<Viewings {...this.props} />):null}
                                </div>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        );
    }
}

export default SectionRight;
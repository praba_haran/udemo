import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import SectionRight from './partials/SectionRight';

class Component extends React.Component{

    render(){
        let { CONTACT_DETAIL } = this.props;
        let { firstPerson, secondPerson } = CONTACT_DETAIL;
        return(
            <div className="tab-pane active">
                <div className="row contact-overview">
                    <div className="col-lg-6 section-left">
                        <div className="clear">
                            <h4 className="contact-name">{firstPerson.title+' '+firstPerson.forename+' '+firstPerson.surname}</h4>
                            <h5 className="contact-address">{CONTACT_DETAIL.address.fullAddress}</h5>
                            {/*<div className="row">*/}
                                {/*<div className="col-md-12">*/}
                                    {/*<h5 className="margin-bottom-10">*/}
                                        {/*<i className="icon fa-envelope margin-right-5"></i>*/}
                                        {/*<a className="margin-right-10">{firstPerson.emails[0].email}</a>*/}
                                    {/*</h5>*/}
                                {/*</div>*/}
                                {/*{this.renderTelephone(firstPerson,0)}*/}
                                {/*{this.renderTelephone(firstPerson,1)}*/}
                                {/*{this.renderTelephone(firstPerson,2)}*/}
                            {/*</div>*/}
                            <Panel>
                                {this.props.isInvalidAddress?(
                                        <div className="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            ERROR : Unable to locate the address in map.
                                        </div>
                                    ):null}
                                <div id="map" style={{height:'400px'}}></div>
                            </Panel>
                        </div>
                    </div>
                    <SectionRight {...this.props}/>
                </div>
            </div>
        );
    }

    renderTelephone(person,index){
        if(person.telephones[index]){
            return (
                <div className="col-md-4 telephone ">
                    <h5 className="margin-bottom-10">
                        <i className="icon fa-phone margin-right-5"></i>
                        <a href="javascript:void(0)" className="margin-right-5">{person.telephones[index].number}</a>
                        <small className="help-block">{person.telephones[index].type}</small>
                    </h5>
                </div>
            );
        }
    }
}

export default Component;
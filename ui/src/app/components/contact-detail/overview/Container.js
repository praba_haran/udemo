import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import * as constants from '../../../constants';
import ContactDetail from '../../../models/ContactDetail';
import Commands from '../../../models/Commands';
import Viewing from '../../../models/Viewing';
import Offer from '../../../models/Offer';
import Activity from '../../../models/Activity';
import GeneralAppointment from '../../../models/GeneralAppointment';
import GoogleMapWrapper from '../../shared/GoogleMapWrapper';

import Component from './Component';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            VIEWINGS : [],
            isReloading : false,
            viewingFilter : 'Upcoming',

            APPOINTMENTS : [],
            isAppointmentsReloading : false,
            appointmentFilter : 'Upcoming',

            OFFERS : [],
            isOffersReloading :false,
            isInvalidAddress :false,
            ACTIVITIES : [],
            activityFilters : ['Appointments','Viewings','Offers','Others']
        };
    }

    componentDidMount(){
        // load
        this.loadViewings(this.props);
        this.loadAppointments(this.props);
        this.loadOffers(this.props);
        this.loadActivities(this.props);
    }

    componentWillReceiveProps(newProps){
        let { COMMANDS, isScriptLoaded, isScriptLoadSucceed  } = newProps;
        if(newProps.params.id!==this.props.params.id){
            this.loadViewings(newProps);
            this.loadAppointments(newProps);
            this.loadOffers(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.BOOK_VIEWING_CHANGED !==this.props.COMMANDS.BOOK_VIEWING_CHANGED && !this.state.isReloading){
            Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, !COMMANDS.BOOK_VIEWING_CHANGED);
            this.loadViewings(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.GENERAL_APPOINTMENT_CHANGED !==this.props.COMMANDS.GENERAL_APPOINTMENT_CHANGED && !this.state.isAppointmentsReloading){
            Commands.actions.change(constants.COMMANDS.GENERAL_APPOINTMENT_CHANGED, !COMMANDS.GENERAL_APPOINTMENT_CHANGED);
            this.loadAppointments(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.MAKE_AN_OFFER_CHANGED !==this.props.COMMANDS.MAKE_AN_OFFER_CHANGED && !this.state.isOffersReloading){
            Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER_CHANGED, !COMMANDS.MAKE_AN_OFFER_CHANGED);
            this.loadOffers(newProps);
            this.loadActivities(newProps);
        }
        // load map
        // if (isScriptLoaded && !this.props.isScriptLoaded) {
        //     if (isScriptLoadSucceed) {
        //         setTimeout(()=>{
        //             this.getGeocode();
        //         },1000);
        //     }
        // }
    }

    // viewings
    loadViewings(props){
        this.setState({
            isReloading : true,
            VIEWINGS : []
        });
        let params = {
            contact : props.params.id
        };
        Viewing.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    VIEWINGS : result.data,
                    isReloading : false
                });
            }
        });
    }
    editViewing(viewing){
        Viewing.actions.setModel(viewing);
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    addViewing(){
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    changeViewingFilter(filter){
        this.setState({
            viewingFilter : filter
        });
    }

    // appointments
    loadAppointments(props){
        this.setState({
            isAppointmentsReloading : true,
            APPOINTMENTS : []
        });
        let params = {
            contact : props.params.id
        };
        GeneralAppointment.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    APPOINTMENTS : result.data,
                    isAppointmentsReloading : false
                });
            }
        });
    }
    editAppointment(appointment){
        GeneralAppointment.actions.setModel(appointment);
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    addAppointment(){
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    changeAppointmentFilter(filter){
        this.setState({
            appointmentFilter : filter
        });
    }

    // offers
    loadOffers(props){
        this.setState({
            isOffersReloading : true,
            OFFERS : []
        });
        let params = {
            contact : props.params.id
        };
        Offer.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    OFFERS : result.data,
                    isOffersReloading : false
                });
            }
        });
    }
    editOffer(offer){
        Offer.actions.setModel(offer);
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }
    addOffer(){
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }
    // activities
    loadActivities(props){
        this.setState({
            isActivityReloading : true
        });
        let params = {
            contact : props.params.id
        };
        Activity.api.list(params,(result)=>{
            if(result.success){
                this.setState({
                    ACTIVITIES : result.data,
                    isActivityReloading : false
                });
            }
        });
    }
    activityFilterChange(filter){
        let { activityFilters } = this.state;
        let index = _.findIndex(activityFilters,(f)=>{
            return f === filter;
        });
        if(index>-1){
            activityFilters = _.filter(activityFilters,(f)=>{
                return f!==filter;
            });
        }
        else{
            activityFilters.push(filter);
        }
        this.setState({
            activityFilters : activityFilters
        });
    }
    // google map
    getAddressToGeoCode(address){
        let text = '';
        let fields = ['street','town','county','postcode'];
        _.each(fields,(p)=>{
            if(address[p]){
                text +=address[p];
                text +=',';
            }
        });
        return text.substr(0,text.length-1);
    }
    getGeocode(){
        let { CONTACT_DETAIL } = this.props;
        let google = window.google;
        let geocoder = new google.maps.Geocoder();
        let latLng = new google.maps.LatLng(51.509865, -0.118092);
        if(CONTACT_DETAIL.address) {
            let address = this.getAddressToGeoCode(CONTACT_DETAIL.address);
            geocoder.geocode({'address': address}, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    this.loadMap(results[0].geometry.location, true);
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                    this.loadMap(latLng, false);
                }
            });
        }
        else{
            this.loadMap(latLng, false);
        }
    }
    loadMap(location,isCorrectAddress){
        let node = document.getElementById('map');
        let map = new google.maps.Map(node, {
            center: {
                lat : location.lat(),
                lng : location.lng()
            },
            zoom: 14,
            disableDefaultUI:true
        });
        map.setCenter(location);
        if(isCorrectAddress) {
            let marker = new google.maps.Marker( {
                map     : map,
                position: location
            });
        }
        this.setState({
            isInvalidAddress : !isCorrectAddress
        });
    }
    isActiveContact(){
        let { CONTACT_DETAIL } = this.props;
        return (CONTACT_DETAIL.status.toLowerCase()==='active');
    }

    render(){
        return (
            <Component
                onChange={ContactDetail.actions.valueChange}
                editViewing={this.editViewing.bind(this)}
                addViewing={this.addViewing.bind(this)}
                changeViewingFilter={this.changeViewingFilter.bind(this)}
                addAppointment={this.addAppointment.bind(this)}
                editAppointment={this.editAppointment.bind(this)}
                changeAppointmentFilter={this.changeAppointmentFilter.bind(this)}
                addOffer={this.addOffer.bind(this)}
                editOffer={this.editOffer.bind(this)}
                isActiveContact={this.isActiveContact.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) => {
    let app = store.APP.toJS();
    return {
        CONTACT_DETAIL: store.CONTACT_DETAIL.toJS(),
        CONTACT_REQUIREMENT : store.CONTACT_REQUIREMENT.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP: store.APP.toJS(),
    };
};

export default connect(mapStateToProps)(GoogleMapWrapper(Container));

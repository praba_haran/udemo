import React from 'react';

export default class MatchListItem extends React.Component{

    render(){
        let { property }= this.props;
        let { photos } = property;
        return (
            <li className="list-group-item">
                <div className="media">
                    <div className="media-left">
                        <a className="avatar" href="javascript:void(0)">
                            {photos.length>0?(
                                    <img className="img-responsive" src={photos[0].url.replace('upload','upload/w_200')} alt={photos[0].title} />
                                ):(
                                    <img className="img-responsive" src="/dist/images/photos/no-image.png" alt="No Image" />
                                )}
                        </a>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">3 bed semi-detached bungalow for sale</h4>
                        <p className="price">
                            <span className="text-danger">£ {property.price || property.proposedPrice}</span>
                            <small className="margin-left-5">{property.priceQualifier}</small>
                            <small className="margin-left-5">{property.rentalFrequency}</small>
                        </p>
                        <p className="address">{property.address.fullAddress}</p>
                    </div>
                </div>
            </li>
        );
    }
}
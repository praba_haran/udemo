import React from 'react';
import NavLink from '../../shared/NavLink';
//import ActionButtonGroup from '../helper/ActionButtonGroup';

class Component extends React.Component{

    canShowInterestedInProperties(){
        let { model } = this.props;
        if(model) {
            return model.type ==='Client' && ( model.intension==='To Buy'
                || model.intension === 'To Rent'
                || model.intension === 'To Buy or To Rent');
        }
        return false;
    }

    isClient(){
        let { model } = this.props;
        if(model) {
            return model.type ==='Client';
        }
        return false;
    }

    render () {
        let { model,submit } = this.props;
        return (
            <div className="tab-pane active">
                <div className="nav-tabs-horizontal nav-tabs-inverse">
                    <ul className="nav nav-tabs nav-tabs-solid">
                        <NavLink to={'/contacts/'+model._id+'/details'} disableContainsCheck={true}>Contact</NavLink>
                        {this.canShowInterestedInProperties()?(<NavLink to={'/contacts/'+model._id+'/details/interested-properties'}>Interested In</NavLink>):null}
                        {this.isClient()?(<NavLink to={'/contacts/'+model._id+'/details/position'}>Position</NavLink>):null}
                        {this.isClient()?(<NavLink to={'/contacts/'+model._id+'/details/solicitor'}>Solicitor</NavLink>):null}
                        <NavLink to={'/contacts/'+model._id+'/details/agency'}>Agency</NavLink>
                    </ul>
                    <div className="tab-content">{this.props.children}</div>
                </div>

                <div className="site-action">
                    <button onClick={submit} type="button" className="btn btn-floating btn-primary btn-sm waves-effect waves-float waves-light">
                        <i className="icon md-check" aria-hidden="true"></i>
                    </button>
                    <button type="button" className="btn btn-floating btn-default btn-sm waves-effect waves-float waves-light">
                        <i className="icon md-close" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        );
    }
}

export default Component;

import React from 'react';
import Helper from '../../../shared/helper';
import {Link} from "react-router";
import Typeahead from 'react-bootstrap-typeahead';

class Component extends React.Component{

    render () {
        let {
            model,
            isSearchingProperty,
            propertiesToFilter,
            getAllProperties,
            onPropertiesFocus,
            onInterestedPropertyChange,
            onInterestedPropertySelected,
            removeInterestedProperty
        } = this.props;
        let noItemFound;
        if(model.interestedProperties.length===0){
            noItemFound = (
                <tr>
                    <td colSpan="8" className="text-center">No interested in properties.</td>
                </tr>
            );
        }
        return (
            <div className="clear interested-properties">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="input-group">
                            <Typeahead
                                ref="typeahead"
                                placeholder="Search address ..."
                                emptyLabel={isSearchingProperty?'Loading ...':'No property found.'}
                                minLength={0}
                                filterBy={['fullAddress']}
                                labelKey="postcode"
                                options={propertiesToFilter}
                                onInputChange={onInterestedPropertyChange}
                                onChange ={(items)=> { onInterestedPropertySelected(items,this.refs.typeahead.getInstance()); } }
                                onFocus = {onPropertiesFocus}
                                renderMenuItemChildren={(props, option, idx) => {
                                    return (<span><i className="icon fa-map-marker"></i>{option.fullAddress}</span>);
                                }}
                            />
                            <span className="input-group-btn">
                                <button onClick={()=>getAllProperties(this.refs.typeahead.getInstance())} type="button" className="btn btn-default waves-effect waves-light">
                                    <i className="icon md-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <p className="help-block margin-top-5 margin-bottom-5">Add interested properties here.</p>
                        <table className="table table-hover table-striped property-table">
                            <colgroup>
                                <col width="5%" />
                                <col width="10%" />
                                <col width="45%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="5%" />
                                <col width="5%"/>
                                <col width="5%"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th></th>
                                <th>Price</th>
                                <th>Address</th>
                                <th>Beds</th>
                                <th>Market For</th>
                                <th>Available</th>
                                <th className="hidden-xs">Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {model.interestedProperties.map(item=>{
                                let p = item.property;
                                return (
                                    <tr key={p._id}>
                                        <td className="image">
                                            <a className="avatar" href="javascript:void(0)">
                                                {p.photos.length>0?(
                                                        <img className="img-responsive" src={p.photos[0].url.replace('upload','upload/w_40,h_30,c_pad,b_black')} alt={p.photos[0].title} />
                                                    ):(
                                                        <img className="img-responsive" src="/dist/images/photos/no-image.png" alt="No Image" />
                                                    )}
                                            </a>
                                        </td>
                                        <td>£ {p.price || p.proposedPrice}
                                            <small className="margin-left-5">{p.priceQualifier}</small>
                                        </td>
                                        <td>
                                            <Link className="address-link" to={'properties/'+p._id+'/overview'}>{p.address.fullAddress}</Link>
                                        </td>
                                        <td>{p.bedrooms}</td>
                                        <td>{p.market}</td>
                                        <td>{p.negotiator.forename}</td>
                                        <td className="hidden-xs">
                                            <span className="label label-primary label-outline">{Helper.property.getStatus(p)}</span>
                                        </td>
                                        <td className="hidden-xs">
                                                <span onClick={()=>{ removeInterestedProperty(p._id)}} className="label label-default label-outline">
                                                    <i className="icon fa-trash"></i>
                                                </span>
                                        </td>
                                    </tr>
                                );
                            })}
                            {noItemFound}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
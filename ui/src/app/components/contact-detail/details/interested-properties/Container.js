import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Alert from '../../../../lib/Alert';
import ContactDetail from '../../../../models/ContactDetail';
import PropertyDetail from '../../../../models/PropertyDetail';
import Component from './Component';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            isSearchingProperty : false,
            properties : [],
            propertiesToFilter :[]
        };
    }

    onPropertiesFocus(){
        this.onInterestedPropertyChange('');
    }

    getAllProperties(typeAhead){
        typeAhead.focus();
    }

    onInterestedPropertyChange(text){
        let {model} = this.props;
        text = text.toUpperCase().trim();
        this.setState({
            isSearchingProperty:true
        });
        PropertyDetail.api.search({ searchText : text },(result)=>{
            if(result.success) {
                let existingProperties = _.map(model.interestedProperties, (p) => {
                    return p.property._id;
                });
                let records = _.map(result.data, function (p) {
                    let address = p.address;
                    address._id = p._id;
                    return address;
                });
                records = _.filter(records, (p) => {
                    return existingProperties.indexOf(p._id) === -1;
                });
                this.setState({
                    properties : result.data,
                    propertiesToFilter: records,
                    isSearchingProperty: false
                });
            }
        });
    }

    onInterestedPropertySelected(items,typeAhead) {
        if (items.length > 0) {
            let { model } = this.props;
            let index = model.interestedProperties.length;
            typeAhead.clear();
            //typeAhead.focus();

            let property = _.find(model.interestedProperties,function(p){
                return p.property._id === items[0]._id;
            });
            if(property===undefined) {
                let item = _.find(this.state.properties,function(p){
                    return p._id === items[0]._id;
                });
                let obj = {
                    property : item
                };
                ContactDetail.actions.setObjectInPath(['interestedProperties', index],obj);
            }
            else{
                Alert.clearLogs().error('This property is already added!');
            }
        }
    }

    removeInterestedProperty(id){
        let { model } = this.props;
        let index = _.findIndex(model.interestedProperties,function(p){
            return p.id === id;
        });
        ContactDetail.actions.removeObjectInPath(['interestedProperties', index]);
    }

    render() {
        return (
            <Component
                {...this.props}
                {...this.state}
                onChange={ContactDetail.actions.valueChange}
                onPropertiesFocus={this.onPropertiesFocus.bind(this)}
                getAllProperties={this.getAllProperties.bind(this)}
                onInterestedPropertyChange={this.onInterestedPropertyChange.bind(this)}
                onInterestedPropertySelected ={this.onInterestedPropertySelected.bind(this)}
                removeInterestedProperty={this.removeInterestedProperty.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS(),
        PROPERTY_LISTING: store.PROPERTY_LISTING.toJS()
    };
};

export default connect(mapStateToProps)(Container);

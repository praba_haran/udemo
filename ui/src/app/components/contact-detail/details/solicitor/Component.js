import React from 'react';
import { Select,TextArea } from '../../../shared/Form';

class Component extends React.Component{

    render () {
        let { model, onChange, APP } = this.props;
        let { agencyDetails } = APP;
        let solicitors = [];
        if(agencyDetails){
            solicitors = agencyDetails.solicitors;
        }
        return (
            <div className="row">
                <div className="col-lg-6">
                    <form className="form-horizontal custom-form">
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Solicitor</label>
                            <div className="col-sm-6">
                                <Select model={model} name="solicitor.solicitor" onChange={onChange}>
                                    {solicitors.map(function(p){
                                        return (
                                            <option key={p._id} value={p._id}>{p.firstPerson.forename+' '+p.firstPerson.surname}</option>
                                        );
                                    })}
                                </Select>
                            </div>
                            <div className="col-sm-2">
                                <button onClick={this.props.addSolicitor} type="button" className="btn btn-sm btn-default">Add New</button>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Notes</label>
                            <div className="col-sm-6">
                                <TextArea model={model} name="solicitor.notes" onChange={onChange} rows="5"></TextArea>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Referred (Ext)</label>
                            <div className="col-sm-6">
                                <Select model={model} name="solicitor.referralExt" onChange={onChange}>
                                    {(model.solicitor && model.solicitor.solicitor)?solicitors.map(function(p){
                                            if(p._id !==model.solicitor.solicitor) {
                                                return (
                                                    <option key={p._id}
                                                            value={p._id}>{p.firstPerson.forename + ' ' + p.firstPerson.surname}</option>
                                                );
                                            }
                                        }):null}
                                </Select>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Referred (Int)</label>
                            <div className="col-sm-6">
                                <Select model={model} name="solicitor.referralInt" onChange={onChange}>
                                    {(model.solicitor && model.solicitor.solicitor && model.solicitor.referralExt)?solicitors.map(function(p){
                                            if(p._id !==model.solicitor.solicitor && p._id !==model.solicitor.referralExt) {
                                                return (
                                                    <option key={p._id}
                                                            value={p._id}>{p.firstPerson.forename + ' ' + p.firstPerson.surname}</option>
                                                );
                                            }
                                        }):null}
                                </Select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Component;
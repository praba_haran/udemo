import React from 'react';
import { connect } from 'react-redux';
import * as constants from '../../../../constants';
import ContactDetail from '../../../../models/ContactDetail';
import Commands from '../../../../models/Commands';
import Contact from '../../../../models/Contact';
import App from '../../../../models/App';
import Component from './Component';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            isReloadingSolicitors : false
        };
    }

    addSolicitor(){
        Contact.actions.change('type','Solicitor');
        Commands.actions.change(constants.COMMANDS.ADD_CONTACT_DO_NOT_REDIRECT, true);
        Commands.actions.change(constants.COMMANDS.ADD_CONTACT, true);
    }

    componentWillReceiveProps(newProps){
        let { COMMANDS } = newProps;
        if(COMMANDS.ADD_CONTACT_CHANGED !==this.props.COMMANDS.ADD_CONTACT_CHANGED && !this.state.isReloadingSolicitors){
            Commands.actions.change(constants.COMMANDS.ADD_CONTACT_CHANGED, !COMMANDS.ADD_CONTACT_CHANGED);
            Commands.actions.change(constants.COMMANDS.ADD_CONTACT_DO_NOT_REDIRECT, !COMMANDS.ADD_CONTACT_DO_NOT_REDIRECT);
            this.loadingSolicitors();
        }
    }

    loadingSolicitors(){
        this.setState({
            isReloadingSolicitors : true
        });
        App.api.getAgency((result)=>{
            if (result.success) {
                App.actions.setObjectInPath(['agencyDetails'], result.data);
                this.setState({
                    isReloadingSolicitors : false
                });
            }
        });
    }

    render() {
        return (
            <Component
                {...this.props}
                onChange={ContactDetail.actions.valueChange}
                addSolicitor={this.addSolicitor.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

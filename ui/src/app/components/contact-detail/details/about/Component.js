import React from 'react';
import { Select, TextBox , TextArea , Validate, ValidateText, ValidateSelect } from '../../../shared/Form';

class Component extends React.Component{

    renderBasicInformation(){
        let { model } = this.props;
        if(model) {
            let {onChange, onSubmit} = this.props;
            return (
                <form className="form-horizontal custom-form" onSubmit={onSubmit}>
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-offset-4 col-sm-6">BASIC INFORMATION</h6>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Type</label>
                        <ValidateSelect className="col-sm-6" model={model} name="type" onChange={onChange}
                                        dropKey="contactType" showLabel="false"/>
                    </div>
                    {this.isClient()?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Intension</label>
                                <ValidateSelect className="col-sm-6" model={model} name="intension" onChange={onChange}
                                                dropKey="contactIntension" showLabel="false"/>
                            </div>
                        ):null}
                    {this.isClient()?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Lead Source</label>
                                <ValidateSelect className="col-sm-6" model={model} name="leadSource" onChange={onChange}
                                                dropKey="contactLeadSource" showLabel="false"/>
                            </div>
                        ):null}
                    {!this.isClient()?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Company</label>
                                <ValidateText className="col-sm-6" model={model} name="companyName" onChange={onChange}/>
                            </div>
                        ):null}
                    {!this.isClient()?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Website</label>
                                <ValidateText className="col-sm-6" model={model} name="companyWebsite" onChange={onChange}/>
                            </div>
                        ):null}
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Notes</label>
                        <div className="col-sm-6">
                            <TextArea model={model} name="notes" onChange={onChange} rows="4"></TextArea>
                        </div>
                    </div>
                </form>
            );
        }
    }

    renderPerson(title){
        let { model } = this.props;
        if(model) {
            let { onChange ,onSubmit, addRemoveEmail , addRemoveTelephone } = this.props;
            let person;
            let isFirstPerson = false;
            let prefix = 'firstPerson';
            if(title==='FIRST PERSON'){
                person = model.firstPerson;
                isFirstPerson= true;
            }
            else{
                prefix = 'secondPerson';
                person = model.secondPerson;
            }

            return (
                <form className="form-horizontal custom-form">
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-offset-3 col-sm-7">{title}</h6>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-3 control-label">Forename</label>
                        <div className="col-sm-2">
                            <Select model={model} name={prefix+'.title'} onChange={onChange} dropKey="contactTitle" showLabel="false" />
                        </div>
                        <ValidateText className="col-sm-5" model={model} name={prefix+'.forename'} onChange={onChange} />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-3 control-label">Surname</label>
                        <ValidateText className="col-sm-7" model={model} name={prefix+'.surname'} onChange={onChange} />
                    </div>
                    {isFirstPerson?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-3 control-label">Salutation</label>
                            <ValidateText className="col-sm-7" model={model} name={prefix+'.salutation'} onChange={onChange} />
                        </div>
                    ):null}
                    {person.emails.map((p,index)=>{
                        let iconClass = index===0?'icon md-plus':'icon md-minus';
                        return (
                            <div key={prefix+'.email.'+index} className="form-group margin-bottom-10">
                                <label className="col-sm-3 control-label">{index===0?'Email':''}</label>
                                <Validate model={model} className="col-sm-7" name={prefix+'.emails['+index+'].email'}>
                                    <div className="input-group">
                                        <TextBox model={model} name={prefix+'.emails['+index+'].email'} onChange={onChange}></TextBox>
                                        <span
                                            onClick={()=>{ addRemoveEmail(person.emails.length,[prefix,'emails',(index===0?person.emails.length:index)],index===0?'add':'remove') }}
                                            className="input-group-addon">
                                            <i className={iconClass}></i>
                                        </span>
                                    </div>
                                </Validate>
                            </div>
                        );
                    })}
                    {person.telephones.map((p,index)=>{
                        let iconClass = index===0?'icon md-plus':'icon md-minus';
                        return (
                            <div key={prefix+'.telephones.'+index} className="form-group margin-bottom-10">
                                <label className="col-sm-3 control-label">{index===0?'Telephone':''}</label>
                                <div className="col-sm-2">
                                    <Select model={model} name={prefix+'.telephones['+index+'].type'} onChange={onChange} dropKey="contactPhoneType" showLabel="false" />
                                </div>
                                <Validate model={model} className="col-sm-5" name={prefix+'.telephones['+index+'].number'}>
                                    <div className="input-group">
                                        <TextBox model={model} name={prefix+'.telephones['+index+'].number'} onChange={onChange}></TextBox>
                                        <span
                                            onClick={()=>{ addRemoveTelephone(person.telephones.length,[prefix,'telephones',(index===0?person.telephones.length:index)],index===0?'add':'remove') }}
                                            className="input-group-addon">
                                            <i className={iconClass}></i>
                                        </span>
                                    </div>
                                </Validate>
                            </div>
                        );
                    })}

                </form>
            );
        }
    }

    renderAddress(){
        let { model } = this.props;
        if(model) {
            let { onChange ,onSubmit} = this.props;
            return (
                <form className="form-horizontal custom-form">
                    <div className="form-group margin-bottom-0 margin-top-20">
                        <h6 className="col-sm-offset-4 col-sm-6">ADDRESS</h6>
                    </div>
                    {!model.address.fullAddress?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Find Address</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control input-sm" placeholder="Post Code" />
                                <a href="" className="help-block">Or Enter Address Manually</a>
                            </div>
                        </div>
                    ):null}
                    <div className="clear">
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Dwelling</label>
                            <ValidateText className="col-sm-6" name="address.dwelling" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Name/No</label>
                            <ValidateText className="col-sm-6" name="address.nameOrNumber" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Street</label>
                            <ValidateText className="col-sm-6" name="address.street" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Locality</label>
                            <ValidateText className="col-sm-6" name="address.locality" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Town</label>
                            <ValidateText className="col-sm-6" name="address.town" model={model} onChange={onChange} type="onlyAlphabet" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">County</label>
                            <ValidateText className="col-sm-6" name="address.county" model={model} onChange={onChange} type="onlyAlphabet" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Postcode</label>
                            <ValidateText className="col-sm-6" name="address.postcode" model={model} onChange={onChange} mask="postcode" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Country</label>
                            <ValidateSelect className="col-sm-6" model={model} name="address.country" onChange={onChange} dropKey="country" showLabel="false"/>
                        </div>
                    </div>
                </form>
            );
        }
    }

    render () {
        return (
            <div className="row">
                <div className="col-lg-6">
                    {this.renderBasicInformation()}
                    <div className="margin-top-20">
                        {this.renderAddress()}
                    </div>
                </div>
                <div className="col-lg-6">
                    {this.renderPerson('FIRST PERSON')}
                    <div className="margin-top-20">
                        {this.renderPerson('SECOND PERSON')}
                    </div>
                </div>
            </div>
        );
    }

    isClient(){
        let { model } = this.props;
        if(model) {
            return model.type ==='Client';
        }
        return false;
    }
}

export default Component;
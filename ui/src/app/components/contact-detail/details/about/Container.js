import React from 'react';
import { connect } from 'react-redux';
import ContactDetail from '../../../../models/ContactDetail';
import Component from './Component';

class Container extends React.Component {

    addRemoveEmail(length,path,command){
        if(command==='add') {
            if(length<2) {
                ContactDetail.actions.setObjectInPath(path,{
                    email : ''
                });
            }
        }
        else{
            ContactDetail.actions.removeObjectInPath(path);
        }
    }

    addRemoveTelephone(length,path,command){
        if(command==='add') {
            if(length<4) {
                ContactDetail.actions.setObjectInPath(path,{
                    type:'Home',
                    label:'',
                    number:''
                });
            }
        }
        else{
            ContactDetail.actions.removeObjectInPath(path);
        }
    }

    render() {
        return (
            <Component
                {...this.props}
                onChange={ContactDetail.actions.valueChange}
                addRemoveTelephone={this.addRemoveTelephone.bind(this)}
                addRemoveEmail={this.addRemoveEmail.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import ContactDetail from '../../../models/ContactDetail';
import Component from './Component';

class Container extends React.Component {

    submit() {
        ContactDetail.actions.validateModel();
        let model = store.getState().CONTACT_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            ContactDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    render() {
        return (
            <Component
                {...this.props}
                onChange={ContactDetail.actions.valueChange}
                submit = {this.submit.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

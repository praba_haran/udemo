import React from 'react';
import _ from 'lodash';
import { ValidateTextArea,DatePicker,CheckBox, ValidateSelect } from '../../../shared/Form';

class Component extends React.Component{

    renderPosition(){
        let { model,onChange } = this.props;
        return (
            <form className="form-horizontal custom-form">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-5 col-sm-7">POSITION</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Position</label>
                    <ValidateSelect className="col-sm-7" model={model} name="position" onChange={onChange} dropKey="contactPosition" />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Disposal</label>
                    <ValidateSelect className="col-sm-7" model={model} name="disposal" onChange={onChange} dropKey="contactDisposal" />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Why Moving</label>
                    <ValidateSelect className="col-sm-7" model={model} name="movingReason" onChange={onChange} dropKey="contactMovingReason" />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Timescale</label>
                    <div className="col-sm-7">
                        <DatePicker
                            model={model}
                            name="timeScale"
                            onChange={onChange}
                            options={{
                                disablePreviousDate: true
                            }}
                        />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Rating</label>
                    <ValidateSelect className="col-sm-7" model={model} name="rating" onChange={onChange} dropKey="contactRating" />
                </div>
                <div className="form-group margin-top-20 margin-bottom-0">
                    <h6 className="col-sm-offset-5 col-sm-7">MONEY LAUNDERING CHECKS</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label"></label>
                    <div className="col-sm-7">
                        <CheckBox label="ID Check Completed" model={model} name="moneyLaunderingCheck.isIdCheckCompleted" onChange={onChange} />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-5 control-label">Notes</label>
                    <ValidateTextArea className="col-sm-7" rows="5" model={model} name="moneyLaunderingCheck.notes" onChange={onChange} />
                </div>
            </form>
        );
    }

    renderFinancialServices(){
        let { model, APP,onChange } = this.props;
        let { agencyDetails} = APP;
        if(model && agencyDetails!==null) {
            return (
                <form className="form-horizontal custom-form">
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-offset-6 col-sm-7">FINANCIAL SERVICES</h6>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-6 control-label">Action</label>
                        <ValidateSelect className="col-sm-6" model={model} name="action" onChange={onChange} dropKey="contactAction" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-6 control-label">Referred By</label>
                        <ValidateSelect className="col-sm-6" model={model} name="referredBy" onChange={onChange}>
                            {agencyDetails.agencyUsers.map(function(p){
                                return (
                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                );
                            })}
                        </ValidateSelect>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-6 control-label">Referral Notes</label>
                        <ValidateTextArea className="col-sm-6" rows="5" model={model} name="referralNotes" onChange={onChange}/>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-6 control-label">Referred To (External)</label>
                        <ValidateSelect className="col-sm-6" model={model} name="referredToExt" onChange={onChange}>
                            {_.filter(agencyDetails.agencyUsers,(user)=>{
                                return model.referredBy && user._id !== model.referredBy;
                            }).map(function(p){
                                return (
                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                );
                            })}
                        </ValidateSelect>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-6 control-label">Referred To (Internal)</label>
                        <ValidateSelect className="col-sm-6" model={model} name="referredToInt" onChange={onChange}>
                            {_.filter(agencyDetails.agencyUsers,(user)=>{
                                return (model.referredBy && user._id !== model.referredBy && model.referredToExt && user._id !== model.referredToExt);
                            }).map(function(p){
                                return (
                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                );
                            })}
                        </ValidateSelect>
                    </div>
                </form>
            );
        }
    }

    render () {
        return (
            <div className="row">
                <div className="col-lg-5">
                    {this.renderPosition()}
                </div>
                <div className="col-lg-6">
                    {this.renderFinancialServices()}
                </div>
            </div>
        );
    }
}

export default Component;
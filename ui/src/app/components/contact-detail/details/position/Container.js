import React from 'react';
import { connect } from 'react-redux';
import ContactDetail from '../../../../models/ContactDetail';
import Component from './Component';

class Container extends React.Component {

    render() {
        return (
            <Component
                {...this.props}
                onChange={ContactDetail.actions.valueChange}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            market : 'All',
            searchText : ''
        };
    }

    onMarketChange(type){
        this.setState({
            market : type
        });
    }

    onSearchTextChange(e){
        this.setState({
            searchText : e.target.value
        });
    }

    render(){
        return (
            <Component
                onMarketChange={this.onMarketChange.bind(this)}
                onSearchTextChange={this.onSearchTextChange.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) => {
    let app = store.APP.toJS();
    return {
        model: store.CONTACT_DETAIL.toJS(),
        APP: store.APP.toJS(),
    };
};

export default connect(mapStateToProps)(Container);

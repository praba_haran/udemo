import React from 'react';
import {Link} from "react-router";
import Helper from '../../../shared/helper';

export default class ListItem extends React.Component{

    render(){
        let {property,getPrice} = this.props;
        let { photos } = property;
        return(
            <tr>
                <td className="image">
                    <a className="avatar" href="javascript:void(0)">
                        {photos.length>0?(
                            <img className="img-responsive" src={photos[0].url.replace('upload','upload/w_40,h_30,c_pad,b_black')}
                                 alt={photos[0].title} />
                        ):(
                            <img className="img-responsive" src="/dist/images/photos/no-image.png" alt="No Image" />
                        )}
                    </a>
                </td>
                <td>
                    {Helper.toCurrency(getPrice())}
                    <small className="margin-left-5">{property.priceQualifier}</small>
                </td>
                <td>
                    <Link className="address-link" to={'properties/'+property._id+'/details/owners'}>{property.address.fullAddress}</Link>
                </td>
                <td>{property.category}</td>
                <td>{property.market}</td>
                <td>{property.negotiator.forename}</td>
                <td className="hidden-xs">
                    <span className="label label-primary label-outline">{Helper.property.getStatus(property)}</span>
                </td>
            </tr>
        );
    }
}
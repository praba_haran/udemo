import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import ListItem from './partials/ListItem';
import Helper from '../../shared/helper';

class Component extends React.Component{

    render (){
        let{ model,onMarketChange,market,searchText,onSearchTextChange } = this.props;
        let properties = _.filter(model.properties,(p)=>{
            return (
                (market==='All' || p.market === market) &&
                (
                    p.address.fullAddress.toLowerCase().indexOf(searchText) > -1
                    || p.category.toLowerCase().indexOf(searchText) > -1
                    || p.market.toLowerCase().indexOf(searchText) > -1
                    || p.negotiator.forename.toLowerCase().indexOf(searchText) > -1
                    || p.negotiator.surname.toLowerCase().indexOf(searchText) > -1
                    || Helper.property.getStatus(p).toLowerCase().indexOf(searchText) > -1)
                    || this.getPrice(p).toString().indexOf(searchText) > -1
                );
        });
        let noRecordFound;
        if(properties.length===0){
            noRecordFound = (
                <tr className="no-record-found ">
                    <td colSpan="8" className="text-center">No Properties Found !</td>
                </tr>
            );
        }
        return (
            <div className="tab-pane active">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="btn-group margin-right-5">
                            <button onClick={()=> onMarketChange('All')} type="button" className={classNames({
                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                'active':market==='All'
                            })}>
                                All
                            </button>
                            <button onClick={()=> onMarketChange('For Sale')} type="button" className={classNames({
                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                'active':market==='For Sale'
                            })}>
                                Sale
                            </button>
                            <button onClick={()=> onMarketChange('To Let')} type="button" className={classNames({
                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                'active':market==='To Let'
                            })}>
                                Rent
                            </button>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="input-search input-search-dark pull-right">
                            <i className="input-search-icon md-search" aria-hidden="true"></i>
                            <input value={searchText} onChange={onSearchTextChange} type="text" className="form-control" placeholder="Search ..." />
                            <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div className="example">
                    <table className="table table-hover table-striped property-table">
                        <colgroup>
                            <col width="5%" />
                            <col width="10%" />
                            <col width="45%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="5%" />
                            <col width="10%"/>
                            <col width="5%"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th></th>
                            <th>Price</th>
                            <th>Address</th>
                            <th>Category</th>
                            <th>Market For</th>
                            <th>Available</th>
                            <th className="hidden-xs">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {properties.map(p=>{
                            return (
                                <ListItem key={p._id} property={p} getPrice={()=> this.getPrice(p)} />
                            );
                        })}
                        {noRecordFound}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    getPrice (property){
        if(property.status==='Instructed'){
            return property.contract.price;
        }
        return property.proposedPrice || property.price;
    }
}

export default Component;
import React from 'react';

class NoDocumentFound extends React.Component{

    render(){

        return (
            <div className="clear no-item-found">
                <p className="img-container"><img src="/dist/images/photos/no-matches-found.png" /></p>
                <h4 className="text">No documents to see here ...</h4>
            </div>
        );
    }
}

export default NoDocumentFound;
import React from 'react';
import helper from '../../../shared/helper';

class Documents extends React.Component{

    render(){
        let {model,removeFile,isDeleting,currentDocId} = this.props;
        return (
            <div className="clear">
                <table className="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Size</th>
                        <th>Uploaded At</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {model.docs.map((p,index)=> {
                        return (
                            <tr key={p.id}>
                                <td>{index+1}</td>
                                <td>
                                    <a className="tdn" href={p.url} target="_blank">
                                        <i className="icon fa-file-pdf-o margin-right-5"></i>
                                        {p.fileName}
                                    </a>
                                </td>
                                <td>{helper.bytesToSize(p.size)}</td>
                                <td>{moment(p.createdAt).format('DD/MM/YYYY h:mm a')}</td>
                                <td className="hidden-xs text-right">
                                    <a href={p.url} className="btn btn-pure btn-xs waves-effect waves-light" download>
                                        <i className="icon md-download"></i>
                                    </a>
                                    <button onClick={(e)=>removeFile(p.id)} className="btn btn-pure btn-xs waves-effect waves-light">
                                        {(isDeleting && currentDocId === p.id)?('Deleting ...'):(<i className="icon fa-trash"></i>)}
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Documents;
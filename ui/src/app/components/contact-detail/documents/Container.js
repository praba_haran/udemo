import React from 'react';
import _ from 'lodash';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import ContactDetail from '../../../models/ContactDetail';
import Component from './Component';
import Cloudinary from '../../../lib/Cloudinary';

class Container extends React.Component {

    constructor() {
        super();
        this.state = {
            showDropZone: false,
            isUploading :false,
            isDeleting : false,
            currentDocId : null,
            uploadingText : '',
            progressPercentage : 10
        };
    }

    toggleDropZone() {
        this.setState({
            showDropZone: !this.state.showDropZone,
            isUploading : false,
            uploadingText : '',
            progressPercentage :10
        });
    }

    onFilesDrop(files) {
        let _this = this;
        let { model,APP } = this.props;
        let { userInfo } = APP;
        let docIndex = model.docs.length;
        Cloudinary.upload(files,{
            onProgress : function(e){
                _this.setState({
                    progressPercentage: e.percent
                });
            },
            onFinished: function(data){
                _.each(data,(file)=>{
                    let doc = {
                        id : file.public_id,
                        url : file.url,
                        fileName : file.original_filename,
                        size : file.bytes,
                        createdAt: new Date(),
                        createdBy : userInfo._id
                    };
                    ContactDetail.actions.setObjectInPath(['docs',docIndex],doc);
                    docIndex++;
                });
                _this.toggleDropZone();
                _this.submit(data.length>1?'Documents uploaded successfully.':'Document uploaded successfully.');
            },
            onError: function(err){
                Alert.clearLogs().error(err);
            }
        });
        let uploadingText = files.length+' Files are uploading';
        if(files.length===1){
            uploadingText = '1 File is uploading';
        }
        this.setState({
            isUploading : true,
            uploadingText : uploadingText
        });
    }

    removeFile(public_id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this file?', ()=>{
            this.setState({
                isDeleting : true,
                currentDocId : public_id
            });
            let _this = this;
            let { model } = this.props;
            Cloudinary.destroy([public_id],{
                onFinished: function(data){
                    _.each(data,function(res){
                        if(res.result==='ok'){
                            let docIndex = _.findIndex(model.docs,(doc)=>{
                                return doc.id === public_id;
                            });
                            if(docIndex>-1){
                                ContactDetail.actions.removeObjectInPath(['docs',docIndex]);
                                _this.submit('Document removed successfully.');
                            }
                        }
                        else{
                            Alert.clearLogs().error(res.result);
                        }
                    });
                    _this.setState({
                        isDeleting : false,
                        currentDocId : null
                    });
                },
                onError: function(err){
                    console.log('eer',err);
                    Alert.clearLogs().error(err);
                    _this.setState({
                        isDeleting : false,
                        currentDocId : null
                    });
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }

    submit(message) {
        ContactDetail.actions.validateModel();
        let model = store.getState().CONTACT_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            ContactDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    render(){
        return (
            <Component
                onChange={ContactDetail.actions.valueChange}
                onFilesDrop = {this.onFilesDrop.bind(this)}
                removeFile = {this.removeFile.bind(this)}
                toggleDropZone = {this.toggleDropZone.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        PROPERTY_LISTING : store.PROPERTY_LISTING.toJS(),
        APP: store.APP.toJS(),
    };
};

export default connect(mapStateToProps)(Container);

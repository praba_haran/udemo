import React from 'react';
import Dropzone from 'react-dropzone';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';
import Panel from 'react-bootstrap/lib/Panel';
import Documents from './partials/Documents';
import NoDocumentFound from './partials/NoDocumentFound';

class Component extends React.Component{

    render(){
        let {model} = this.props;
        return (
            <div className="clear">
                {!this.props.showDropZone?(
                    <p className="text-right">
                        <button onClick={this.props.toggleDropZone} type="button" className="btn btn-sm btn-outline btn-primary btn-round waves-effect waves-light waves-round">
                            <span className="text hidden-xs">Upload</span>
                            <i className="icon md-upload" aria-hidden="true"></i>
                        </button>
                    </p>
                ):null}
                <div className="drop-zone-container">
                    <Panel collapsible expanded={this.props.showDropZone}>
                        <a onClick={this.props.toggleDropZone} className="close">
                            <i className="icon md-close"></i>
                        </a>
                        <Dropzone
                            disableClick={true}
                            ref={ (node)=> {
                                this.dropzone = node;
                            } }
                            className="drop-zone"
                            activeClassName="active"
                            multiple={true}
                            onDrop={this.props.onFilesDrop}>
                            {this.props.isUploading?(
                                <div>
                                    <p>{this.props.uploadingText}</p>
                                    <p>
                                        <small>Please wait ...</small>
                                    </p>
                                    <ProgressBar bsStyle="success" now={this.props.progressPercentage} />
                                </div>
                            ):(
                                <div>
                                    <p>Drop your files here</p>
                                    <p>
                                        <small>Or, if you prefer ...</small>
                                    </p>
                                    <button onClick={()=> this.dropzone.open() }
                                            className="btn btn-sm btn-primary margin-top-5">Choose files to upload
                                    </button>
                                </div>
                            )}
                        </Dropzone>
                    </Panel>
                </div>
                {model.docs.length>0?(
                    <Documents {...this.props} removeFile={this.props.removeFile} />
                ):(
                    <NoDocumentFound />
                )}
            </div>
        );
    }
}

export default Component;
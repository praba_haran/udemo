import React from 'react';
import _ from 'lodash';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import { TilePicker,PricePicker, TextBox, CheckBox, ValueUnitPicker } from '../../../shared/Form';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Filter extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: 'general',
            featureIndex : 0,
            propertyTypeIndex: 0
        };
        this.handleSelect = this.handleSelect.bind(this);
        this.onFeatureChange = this.onFeatureChange.bind(this);
        this.onPropertyTypeChange = this.onPropertyTypeChange.bind(this);
        this.onItemChange = this.onItemChange.bind(this);
        this.getSelectedCount = this.getSelectedCount.bind(this);
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    onFeatureChange(index){
        this.setState({
            featureIndex : index
        });
    }
    onPropertyTypeChange(index){
        this.setState({
            propertyTypeIndex : index
        });
    }

    onItemChange(name,selected){
        let { onRequirementChange } = this.props;
        onRequirementChange({
            target : {
                name : name,
                value : !selected
            },
            type :'change'
        });
    }
    onPropertyTypeItemChange(name,selected){
        let { onRequirementChange } = this.props;
        onRequirementChange({
            target : {
                name : name,
                value : !selected
            },
            type :'change'
        });
    }

    getSelectedCount(category){
        let selectedItems = _.filter(category.values,function(p){
            return p.selected;
        });
        return selectedItems.length;
    }

    render (){
        let { CONTACT_REQUIREMENT, index, onRequirementChange,onAreaChanged,onAllAreasChanged } = this.props;
        let { requirements } = CONTACT_REQUIREMENT;
        let feature = requirements[index].features[this.state.featureIndex];
        let propertyType = requirements[index].propertyTypes[this.state.propertyTypeIndex];
        return (
            <div className="col-lg-4">
                <div className="matching-filter">
                    <div className="header">
                        <div className="row">
                            <div className="col-md-8 col-xs-8">
                                <span className="help-block">
                                    <TextBox model={CONTACT_REQUIREMENT} name={'requirements['+index+'].name'} onChange={onRequirementChange} />
                                </span>
                            </div>
                            <div className="col-md-4 col-xs-4 text-right">
                                <button onClick={this.props.save} className="btn btn-xs margin-right-5">
                                    <i className="icon fa-check"></i>
                                </button>
                                {(requirements[index] && requirements[index]._id)?(
                                        <button onClick={()=>this.props.remove(requirements[index])} className="btn btn-xs">
                                            <i className="icon fa-trash"></i>
                                        </button>
                                    ):null}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <PanelGroup id="requirement-filter" bsClass="panel-group panel-group-continuous" activeKey={this.state.activeKey} onSelect={this.handleSelect} accordion>
                            <Panel header="General" eventKey="general">
                                <div className="tile-manager">
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].intension'} onChange={onRequirementChange} tileHeading="Intension" type="toggle" tileOptions="Buy,Rent" />
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].newBuild'} onChange={onRequirementChange} tileHeading="New Build" type="toggle" tileOptions="Yes,No" />
                                    <PricePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].minPrice'} onChange={onRequirementChange} tileHeading="Min Price" />
                                    <PricePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].maxPrice'} onChange={onRequirementChange} tileHeading="Max Price" />
                                    {(requirements[index] && requirements[index].intension==='Rent')?(
                                            <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].rentalFrequency'} onChange={onRequirementChange} tileHeading="Rental Frequency" type="toggle" tileOptions="Pcm,Pw" />
                                        ):null}
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].furnishing'} onChange={onRequirementChange} tileHeading="Furnishing" type="toggle" tileOptions="Furnished,Unfurnished" />
                                </div>
                            </Panel>
                            <Panel header="Rooms and Areas" eventKey="rooms-and-areas">
                                <div className="tile-manager">
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].bedrooms'} onChange={onRequirementChange} tileHeading="Bedrooms" type="numeric" />
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].bathrooms'} onChange={onRequirementChange} tileHeading="Bathrooms" type="numeric" />
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].receptions'} onChange={onRequirementChange} tileHeading="Receptions" type="numeric" />
                                    <TilePicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].parking'} onChange={onRequirementChange} tileHeading="Parking" type="numeric" />
                                    <ValueUnitPicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].floorArea'} unitName={'requirements['+index+'].floorAreaUnit'} units={['Acre','Sq.Ft']} onChange={onRequirementChange} tileHeading="Floor Area" />
                                    <ValueUnitPicker model={CONTACT_REQUIREMENT} name={'requirements['+index+'].landArea'} unitName={'requirements['+index+'].landAreaUnit'} units={['Acre','Sq.Ft']} onChange={onRequirementChange} tileHeading="Land Area" />
                                </div>
                            </Panel>
                            <Panel header="Areas" eventKey="areas">
                                    <Scrollbars
                                        style={{height:400}}
                                        autoHide
                                        autoHideTimeout={1000}
                                        autoHideDuration={200}
                                    >
                                        <div className="clear">
                                            <span className="checkbox-custom checkbox-default">
                                                <CheckBox model={CONTACT_REQUIREMENT} name={'requirements['+index+'].allAreasSelected'} onChange={(e)=> onAllAreasChanged(index, e)} label="Select All" />
                                            </span>
                                            {requirements[index].areas.map((p,pIndex)=>{
                                                let name = 'requirements['+index+'].areas['+pIndex+'].selected';
                                                return (
                                                    <span key={name} className="checkbox-custom checkbox-default">
                                                        <CheckBox model={CONTACT_REQUIREMENT} name={name} label={p.name} onChange={(e)=>onAreaChanged(index,e)} />
                                                    </span>
                                                );
                                            })}
                                        </div>
                                    </Scrollbars>
                            </Panel>
                            <Panel header="Property Types" eventKey="property-types">
                                <Scrollbars
                                    style={{height:400}}
                                    autoHide
                                    autoHideTimeout={1000}
                                    autoHideDuration={200}
                                >
                                    <div className="clear">
                                        <div className="col-sm-5 features-list-group">
                                            <div className="list-group">
                                                {requirements[index].propertyTypes.map((p,fIndex)=>{
                                                    let cssClass= 'list-group-item waves-effect waves-block waves-classic';
                                                    if(fIndex===this.state.propertyTypeIndex){
                                                        cssClass+=' selected';
                                                    }
                                                    let count = this.getSelectedCount(requirements[index].propertyTypes[fIndex]);
                                                    return (
                                                        <a onClick={()=>this.onPropertyTypeChange(fIndex)} key={'type_'+fIndex} className={cssClass}>
                                                            {/*<i className="icon fa-angle-right"></i>*/}
                                                            {p.key}
                                                            {count>0?(<span className="badge badge-default">{count}</span>):null}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                        <div className="col-sm-7 features-list-group-details">
                                            <div className="list-group">
                                                {propertyType.values.map((p,itemIndex)=>{
                                                    let name = 'requirements['+index+'].propertyTypes['+this.state.propertyTypeIndex+'].values['+itemIndex+'].selected';
                                                    return (
                                                        <a onClick={()=>this.onItemChange(name,p.selected)} key={name} className="list-group-item waves-effect waves-block waves-classic">
                                                            {p.selected?(<i className="icon md-check-square"></i>):(<i className="icon md-square-o"></i>)}
                                                            {p.name}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </Scrollbars>
                            </Panel>
                            <Panel header="Features" eventKey="features">
                                <Scrollbars
                                    style={{height:400}}
                                    autoHide
                                    autoHideTimeout={1000}
                                    autoHideDuration={200}
                                >
                                    <div className="clear">
                                        <div className="col-sm-5 features-list-group">
                                            <div className="list-group">
                                                {requirements[index].features.map((p,fIndex)=>{
                                                    let cssClass= 'list-group-item waves-effect waves-block waves-classic';
                                                    if(fIndex===this.state.featureIndex){
                                                        cssClass+=' selected';
                                                    }
                                                    let count = this.getSelectedCount(requirements[index].features[fIndex]);
                                                    return (
                                                        <a onClick={()=>this.onFeatureChange(fIndex)} key={'list_'+fIndex} className={cssClass}>
                                                            {/*<i className="icon fa-angle-right"></i>*/}
                                                            {p.key}
                                                            {count>0?(<span className="badge badge-default">{count}</span>):null}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                        <div className="col-sm-7 features-list-group-details">
                                            <div className="list-group">
                                                {feature.values.map((p,itemIndex)=>{
                                                    let name = 'requirements['+index+'].features['+this.state.featureIndex+'].values['+itemIndex+'].selected';
                                                    return (
                                                        <a onClick={()=>this.onItemChange(name,p.selected)} key={name} className="list-group-item waves-effect waves-block waves-classic">
                                                            {p.selected?(<i className="icon md-check-square"></i>):(<i className="icon md-square-o"></i>)}
                                                            {p.name}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </Scrollbars>
                            </Panel>
                        </PanelGroup>
                    </div>
                </div>
            </div>
        );
    }
}
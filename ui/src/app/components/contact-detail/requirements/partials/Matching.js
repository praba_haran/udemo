import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import classNames from 'classnames';
import Helper from '../../../shared/helper';
import { CheckBox } from '../../../shared/Form';
import {Link} from "react-router";

export default class Matching extends React.Component {

    render() {
        let {
            CONTACT_REQUIREMENT,
            addRemoveShortList,
            onChange,
            pageIndex,
            pageSize,
            nextPage,
            prevPage
        } = this.props;
        let { matches } = CONTACT_REQUIREMENT;
        let noMatchFound;
        if(matches.length===0){
            noMatchFound = (
                <li className="list-group-item no-item-found">
                    <p className="img-container"><img src="/dist/images/photos/no-matches-found.png" /></p>
                    <h4 className="text">No matches to see here ...</h4>
                </li>
            );
        }
        let matchList = _(matches).slice((pageIndex-1)*pageSize).take(pageSize).value();
        return (
            <div className="col-lg-8">
                <div className="matching-body">
                    <div className="header">
                        <div className="row">
                            <div className="col-md-6 col-xs-8">
                                <span className="help-block">{matches.length} Matches found</span>
                            </div>
                            <div className="col-md-6 col-xs-4">
                                <div className="label-group pull-right">
                                    <span onClick={prevPage} className={classNames({
                                        'label label-outline label-default':true,
                                        'disabled':pageIndex===1
                                    })}>
                                        <i className="icon fa-angle-left"></i>
                                    </span>
                                    <span onClick={nextPage} className={classNames({
                                        'label label-outline label-default':true,
                                        'disabled':(matches.length/pageSize)<pageIndex
                                    })}>
                                        <i className="icon fa-angle-right"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <ul id="matching-list" className="list-group property-list-group">
                                {matchList.map((property,index)=>{
                                    let { photos } = property;
                                    let isShortListed = this.isShortListedProperty(property);
                                    return (
                                        <li key={property._id} className="list-group-item">
                                            <div className="matching clearfix">
                                                <div className="media">
                                                    <div className="media-left">
                                                        <Link to={'/properties/'+property._id+'/overview'} className="avatar">
                                                            {photos.length>0?(
                                                                    <img className="img-responsive" src={photos[0].url.replace('upload','upload/w_354,h_255,c_pad,b_black')} alt={photos[0].title} />
                                                                ):(
                                                                    <img className="img-responsive" src="/dist/images/photos/no-image-354x255.png" alt="No Image" />
                                                                )}
                                                        </Link>
                                                    </div>
                                                    <div className="media-body">
                                                        <div className="row">
                                                            <div className="col-sm-8">
                                                                <Link to={'/properties/'+property._id+'/overview'} className="title">{property.address.fullAddress}</Link>
                                                                <p className="price">
                                                                    <span>{Helper.toCurrency(property.proposedPrice)} - {Helper.property.getStatus(property)}</span>
                                                                    <small className="margin-left-5">{property.priceQualifier}</small>
                                                                    <small className="margin-left-5">{property.rentalFrequency}</small>
                                                                </p>
                                                                <p className="type">{property.type}</p>
                                                            </div>
                                                            <div className="col-sm-4">
                                                                <ul className="measurement-list clearfix">
                                                                    <li className="list-item">
                                                                        <p className="icon-container">
                                                                            <img src="/dist/images/icons/bed.png" />
                                                                        </p>
                                                                        <p className="value">{property.bedrooms}</p>
                                                                    </li>
                                                                    <li className="list-item">
                                                                        <p className="icon-container">
                                                                            <img src="/dist/images/icons/bath.png" />
                                                                        </p>
                                                                        <p className="value">{property.bathrooms}</p>
                                                                    </li>
                                                                    <li className="list-item">
                                                                        <p className="icon-container">
                                                                            <img src="/dist/images/icons/floor.png" />
                                                                        </p>
                                                                        <p className="value">2</p>
                                                                    </li>
                                                                    <li className="list-item">
                                                                        <p className="icon-container">
                                                                            <img src="/dist/images/icons/garage.png" />
                                                                        </p>
                                                                        <p className="value">{property.parking}</p>
                                                                    </li>
                                                                </ul>
                                                                <p className="available">Available : {this.getAvailability(property)}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="select">
                                                <CheckBox model={CONTACT_REQUIREMENT} name={'Hi'} onChange={(e)=>{}} />
                                            </div>
                                            <div className="actions">
                                                <span className="action" onClick={()=> addRemoveShortList(property,isShortListed)}>
                                                    <i className={classNames({
                                                        'icon md-favorite-outline' : !isShortListed,
                                                        'icon md-favorite' : isShortListed
                                                    })}></i>
                                                </span>
                                            </div>
                                            {(!this.isInterestedProperty(property) && property.age && property.age==='New build')?(
                                                    <div className="ribbon ribbon-clip ribbon-reverse ribbon-primary">
                                                        <span className="ribbon-inner">New Build</span>
                                                    </div>
                                                ):null}
                                        </li>
                                    )
                                })}
                                {noMatchFound}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    isInterestedProperty(property){
        let { model } = this.props;
        return _.findIndex(model.interestedProperties,(p)=>{
                return (p.property && p.property._id === property._id);
            })>-1;
    }

    isShortListedProperty(property){
        let { CONTACT_REQUIREMENT } = this.props;
        return _.findIndex(CONTACT_REQUIREMENT.shortlisted,(p)=>{
                return p.property._id === property._id;
            })>-1;
    }

    getAvailability(property){
        let date = property.contract.management.availableOn;
        let diff = moment(date).diff(moment(), 'days');
        if(diff===0){
            return 'Immediately';
        }
        else if(diff > 0  && diff < 2){
            return 'In '+diff+' Day';
        }
        else if(diff >1  && diff < 5){
            return 'In '+diff+' Days';
        }
        else if(diff > 5 && diff <= 7){
            return 'With in 1 Week';
        }
        return moment(property.contract.management.availableOn).format('DD/MM/YYYY');
    }
}
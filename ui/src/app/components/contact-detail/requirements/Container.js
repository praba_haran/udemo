import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import ContactDetail from '../../../models/ContactDetail';
import Requirement from '../../../models/Requirement';
import Component from './Component';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            pageIndex : 1,
            pageSize : 10
        };
    }

    componentDidMount(){
        this.getRequirements();
    }

    nextPage(){
        let { CONTACT_REQUIREMENT } = this.props;
        let { matches } =  CONTACT_REQUIREMENT;
        let pageIndex = this.state.pageIndex+1;
        if((matches.length/this.state.pageSize)>pageIndex){
            this.setState({
                pageIndex: pageIndex
            });
        }
    }
    prevPage(){
        if(this.state.pageIndex>1) {
            this.setState({
                pageIndex: --this.state.pageIndex
            });
        }
    }
    getCurrentProfile(){
        let CONTACT_REQUIREMENT = store.getState().CONTACT_REQUIREMENT.toJS();
        let { requirements, selectedId } = CONTACT_REQUIREMENT;
        return _.find(requirements,function (p) {
            return p._clientId ===  selectedId;
        });
    }
    add(){
        let profile = this.getCurrentProfile();
        if(profile && profile._id){
            let { CONTACT_REQUIREMENT } = this.props;
            let { requirements } = CONTACT_REQUIREMENT;

            let requirement = Requirement.api.getDefaultRequirement(requirements.length);
            Requirement.actions.setObjectInPath(['requirements', requirements.length], requirement);
            Requirement.actions.change('selectedId',requirement._clientId);
        }
        else{
            Alert.maxLogItems(1).error('Please save this profile and add another.')
        }
    }
    choose(requirement,clientId){
        Requirement.actions.change('selectedId',clientId);
        Requirement.api.matches(requirement, (result) => {
            if (result.success) {
                Requirement.actions.setObjectInPath(['matches'], result.data);
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }
    save(){
        let requirement = this.getCurrentProfile();
        let callbackHandler = (result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);
                this.getRequirements();
            }
            else{
                Alert.clearLogs().error(result.error);
            }
        };
        if(requirement){
            requirement.contact = this.props.params.id;
            if (requirement._id) {
                Requirement.api.update(requirement._id, requirement, callbackHandler);
            }
            else {
                Requirement.api.save(requirement,callbackHandler);
            }
        }
    }
    remove(requirement){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove the profile named "'+requirement.name+'".', ()=>{
            Requirement.api.delete(requirement._id,(result)=>{
                if(result.success){
                    this.getList();
                    Alert.clearLogs().success(result.message);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }
    onRequirementChange(e){
        Requirement.actions.valueChange(e);
        this.getMatches(this.getCurrentProfile());
    }
    onAreaChanged(index,e){
        Requirement.actions.valueChange(e);
        let requirement = this.getCurrentProfile();
        let areas = _.filter(requirement.areas,(area)=>{
            return area.selected;
        });
        Requirement.actions.valueChange({
            target :{
                name : 'requirements['+index+'].allAreasSelected',
                checked : requirement.areas.length === areas.length,
                type :'checkbox'
            },
            type :'change'
        });
        this.getMatches(this.getCurrentProfile());
    }
    onAllAreasChanged (index,e){
        let requirement = this.getCurrentProfile();
        _.each(requirement.areas,(area,aIndex)=>{
            Requirement.actions.valueChange({
                target :{
                    name : 'requirements['+index+'].areas['+aIndex+'].selected',
                    checked : e.target.checked,
                    type :'checkbox'
                },
                type :'change'
            });
        });
        this.getMatches(this.getCurrentProfile());
    }
    getList(){
        Requirement.actions.change('isProcessing', true);
        Requirement.api.list(this.props.params.id, (result) => {
            if (result.success) {
                if(result.data.length > 0){
                    let requirements = result.data;
                    Requirement.actions.change('selectedId', requirements[0]._clientId);
                    Requirement.actions.setObjectInPath(['requirements'], requirements);
                    this.getMatches(requirements[0]);
                }
                else{
                    // AFTER DELETION, IF THERE IS NO REQUIREMENT FOR A CONTACT, LOAD DEFAULT REQUIREMENT
                    let requirement = Requirement.api.getDefaultRequirement(0);
                    Requirement.actions.change('selectedId', requirement._clientId);
                    Requirement.actions.setObjectInPath(['requirements'],[requirement]);
                    this.getMatches(requirement);
                }
            }
            else {
                Alert.clearLogs().error(result.error);
            }
            Requirement.actions.change('isProcessing', false);
        });
    }
    getMatches(requirement){
        Requirement.api.matches(requirement, (result) => {
            if (result.success) {
                Requirement.actions.setObjectInPath(['matches'], result.data);
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }
    getRequirements() {
        // WE ARE SHOWING REQUIREMENTS IN DASHBOARD, SO WE NEED TO CALL HERE
        Requirement.actions.change('isProcessing', true);
        Requirement.api.list(this.props.params.id, (result) => {
            if (result.success) {
                if(result.data.length > 0){
                    let requirements = result.data;
                    Requirement.actions.change('selectedId', requirements[0]._clientId);
                    Requirement.actions.setObjectInPath(['requirements'], requirements);
                    this.getMatches(requirements[0]);
                }
                else{
                    Requirement.actions.setObjectInPath(['requirements'],[this.getCurrentProfile()]);
                    this.getMatches(this.getCurrentProfile());
                }
            }
            else {
                Alert.clearLogs().error(result.error);
            }
            Requirement.actions.change('isProcessing', false);
        });
    }

    // Short List Actions
    addRemoveShortList(property,isShortListed){
        let { CONTACT_REQUIREMENT } = this.props;
        let index = CONTACT_REQUIREMENT.shortlisted.length;
        if(isShortListed){
            index = _.findIndex(CONTACT_REQUIREMENT.shortlisted,(p)=>{
                return p.property._id === property._id;
            });
            Requirement.actions.removeObjectInPath(['shortlisted', index]);
        }
        else {
            Requirement.actions.setObjectInPath(['shortlisted', index], {
                property: property
            });
        }
    }

    render() {
        return (
            <Component
                {...this.props}
                {...this.state}
                onChange={ContactDetail.actions.valueChange}
                add={this.add.bind(this)}
                choose={this.choose.bind(this)}
                save={this.save.bind(this)}
                remove={this.remove.bind(this)}
                nextPage={this.nextPage.bind(this)}
                prevPage={this.prevPage.bind(this)}
                onRequirementChange={this.onRequirementChange.bind(this)}
                onAreaChanged={this.onAreaChanged.bind(this)}
                onAllAreasChanged={this.onAllAreasChanged.bind(this)}

                addRemoveShortList={this.addRemoveShortList.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT_DETAIL.toJS(),
        CONTACT_REQUIREMENT: store.CONTACT_REQUIREMENT.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

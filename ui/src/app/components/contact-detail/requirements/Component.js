import React from 'react';
import _ from 'lodash';
import Filter from './partials/Filter';
import Matching from './partials/Matching';

class Component extends React.Component{

    renderRequirements(){
        let { CONTACT_REQUIREMENT, onChange,choose } = this.props;
        let { requirements, selectedId, isProcessing } = CONTACT_REQUIREMENT;
        if(requirements && requirements.length>0){
            return (
                <div className="label-manager">
                    {requirements.map(function (p,index) {
                        let cssClass = 'label label-outline label-default';
                        if(selectedId===p._clientId){
                            cssClass += ' active';
                        }
                        return (
                            <span key={p._clientId} onClick={()=>choose(p,p._clientId)} className={cssClass}>{p.name?p.name:'Untitled Profile '}</span>
                        );
                    })}
                    {(requirements.length<5)? (
                        <span className="label label-outline label-default" onClick={this.props.add}>
                            <i className="icon md-plus"></i>
                        </span>
                    ):null}
                </div>
            );
        }
    }

    render(){
        let { CONTACT_REQUIREMENT,userInfo, onChange } = this.props;
        let {requirements, selectedId, isProcessing} = CONTACT_REQUIREMENT;
        let index = _.findIndex(requirements, function (p) {
            return p._clientId === selectedId;
        });
        index = (index<0)?0:index;
        let shortListCount = CONTACT_REQUIREMENT.shortlisted.length;
        return (
            <div className="tab-pane active contact-requirement">
                <div className="row">
                    <div className="col-md-4">
                        {this.renderRequirements()}
                    </div>
                    <div className="col-md-8">
                        <div className="label-manager align-right">
                            <span className="label label-outline label-default short-list-btn">
                                <i className="icon md-favorite-outline"></i>
                                Shortlisted
                                {shortListCount>0?(<span className="badge badge-radius badge-default">{shortListCount}</span>):null}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <Filter {...this.props} index={index} userInfo={userInfo} />
                    <Matching {...this.props} />
                </div>
            </div>
        );
    }
}

export default Component;
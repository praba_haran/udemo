import React from 'react';
import _ from 'lodash';
import Helper from '../../shared/helper';

export default class PropertySection extends React.Component{

    render(){
        let { FILE_NOTES } = this.props;
        let { property } = FILE_NOTES;
        let {
            isSearchingProperties,
            propertiesToFilter,
            onPropertiesChange,
            onPropertySelected
        } = this.props;
        return (
            <div className="clear describe-property">
                {property!=null?(
                        <ul className="list-group">
                            <li className="list-group-item">
                                <div className="media">
                                    <div className="media-left">{this._renderThumbnail()}</div>
                                    <div className="media-body">
                                        <p className="media-heading margin-bottom-0">{property.address.fullAddress}</p>
                                        <p className="margin-bottom-0">
                                            <span className="text-danger price">{Helper.toCurrency(property.contract.price)}</span>
                                            <small className="margin-left-5">{property.contract.priceQualifier}</small>
                                        </p>
                                        <p className="margin-bottom-0"><small>{Helper.property.getStatus(property)}</small></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    ):null}
            </div>
        );
    }

    _renderThumbnail(){
        let { FILE_NOTES } = this.props;
        let { property } = FILE_NOTES;
        if(property.photos.length>0) {
            return (
                <a className="avatar" href="javascript:void(0)">
                    <img className="img-responsive" src={property.photos[0].url.replace('upload', 'upload/w_354,h_255,c_pad,b_black')}
                         alt={property.photos[0].title}/>
                </a>
            );
        }
        return (
            <a className="avatar" href="javascript:void(0)">
                <img className="overlay-figure" src="/dist/images/photos/no-image-354x255.png"/>
            </a>
        );
    }
}
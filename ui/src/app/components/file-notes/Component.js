import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import Form from './form/Container';
import Applicants from './applicants/Container';

class Component extends React.Component {

    render() {
        let {
            FILE_NOTES,
            cancel,
            submit,
            remove
        } = this.props;
        return (
            <Modal backdrop="static"
                   show={this.props.canShowModal}
                   onHide={cancel}
                   keyboard={false}
                   dialogClassName="modal-viewing modal-lg">
                <Modal.Header closeButton>
                    <Modal.Title>{FILE_NOTES._id?'NOTES':'ADD NOTES'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props} />
                    <div className="row">
                        <Form {...this.props} />

                        <div className="col-sm-6">
                            <div className="nav-tabs-horizontal">
                                <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                                    <li role="presentation" className="active">
                                        <a role="button" href="javascript:void(0)">Applicants</a>
                                    </li>
                                </ul>
                                <div className="tab-content">
                                    <div className="tab-pane active">
                                        <Applicants {...this.props} />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className="row">
                        <div className="col-sm-12 text-right">
                            <Button disabled={this.props.isSaving || this.props.isDeleting} onClick={cancel}>Cancel</Button>
                            <Button className="btn btn-primary" onClick={submit} disabled={this.props.isSaving || this.props.isDeleting}>{this.props.isSaving ? 'Saving ...' : 'Save'}</Button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
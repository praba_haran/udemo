import React from 'react';
import _ from 'lodash';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import FileNotes from '../../../models/FileNotes';
import ContactDetail from '../../../models/ContactDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSearchingApplicants : false,
            applicants : [],
            applicantsToFilter : [],
            applicantFilter : 'Active'
        };
    }

    onApplicantsFocus(){
        this.onApplicantsChange('');
    }
    getAllApplicants(typeAhead){
        typeAhead.focus();
    }
    onApplicantsChange(text){
        let {FILE_NOTES} = this.props;
        text = text.trim();
        this.setState({
            isSearchingApplicants: true
        });

        // searching contacts
        ContactDetail.api.search({
            searchText: text,
            status : 'Active'
        }, (result) => {
            if (result.success) {
                let existingApplicants = _.map(FILE_NOTES.applicants, (p) => {
                    return p.contact._id;
                });
                let applicants = _.map(result.data, (p) => {

                    return {
                        _id: p._id,
                        forename: p.firstPerson.forename,
                        surname: p.firstPerson.surname,
                        fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                        address: p.address.fullAddress,
                        type : p.type
                    }
                });
                applicants = _.filter(applicants, (p) => {
                    return existingApplicants.indexOf(p._id) === -1;
                });
                this.setState({
                    applicantsToFilter: applicants,
                    applicants: result.data,
                    isSearchingApplicants: false
                });
            }
        });
    }
    onApplicantSelected(items,typeAhead) {

        if (items.length > 0) {
            let { FILE_NOTES } = this.props;
            let index = FILE_NOTES.applicants.length;
            typeAhead.clear();
            if(items[0].isBusy){
                Alert.clearLogs().error('Applicant holds viewing with same date/time.');
            }
            else {
                let applicant = _.find(FILE_NOTES.applicants, function (p) {
                    return p.contact._id === items[0]._id;
                });
                if (applicant === undefined) {
                    let item = _.find(this.state.applicants, function (p) {
                        return p._id === items[0]._id;
                    });

                    FileNotes.actions.setObjectInPath(['applicants', index],{
                        contact : item
                    });
                }
                else {
                    Alert.clearLogs().error('This applicant is already added!');
                }
            }
        }
    }

    removeApplicant(id){
        // IF WE HAVE ONLY ONE APPLICANT IN THE FILE_NOTES AND HE WANTS TO CANCEL THE FILE_NOTES
        // THEN YOU CANNOT REMOVE THE APPLICANT FROM THE LIST
        // RATHER ASK USER TO CANCEL THE FILE_NOTES IF THEY WANT
        let { FILE_NOTES,remove } = this.props;
        if(FILE_NOTES.applicants.length===1 && FILE_NOTES._id){
            Alert.okBtn('Yes').cancelBtn('No').confirm('There is no active applicants. Would you like to cancel this viewing ?', ()=>{
                remove();
            });
        }
        else {
            let activeApplicantsCount = _.filter(FILE_NOTES.applicants,(p)=>{
               return p.status === 'Active';
            }).length;
            let index = _.findIndex(FILE_NOTES.applicants, function (p) {
                return p.contact._id === id;
            });
            if (index > -1) {
                // IF FILE_NOTES IS CONFIRMED, THEN DON'T DELETE THE APPLICANTS PERMANATELY
                if(FILE_NOTES._id && FILE_NOTES.isConfirmed){
                    if(activeApplicantsCount===1){
                        Alert.okBtn('Yes').cancelBtn('No').confirm('There is no active applicants. Would you like to cancel this viewing ?', () => {
                            remove();
                        });
                    }
                    else {
                        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this applicant ?', () => {
                            FileNotes.actions.setObjectInPath(['applicants', index, 'status'], 'Cancelled');
                        });
                    }
                }
                else{
                    FileNotes.actions.removeObjectInPath(['applicants', index]);
                }
            }
        }
    }
    changeApplicantFilter(filter){
        this.setState({
            applicantFilter : filter
        });
    }

    render(){
        return (
            <Component
                onChange={FileNotes.actions.valueChange}

                onApplicantsFocus={this.onApplicantsFocus.bind(this)}
                getAllApplicants={this.getAllApplicants.bind(this)}
                onApplicantsChange={this.onApplicantsChange.bind(this)}
                onApplicantSelected={this.onApplicantSelected.bind(this)}
                removeApplicant = {this.removeApplicant.bind(this)}
                changeApplicantFilter={this.changeApplicantFilter.bind(this)}

                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        FILE_NOTES : store.FILE_NOTES.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import store from '../../store';
import Alert from '../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import * as constants from '../../constants';
import FileNotes from '../../models/FileNotes';
import Commands from '../../models/Commands';
import PropertyDetail from '../../models/PropertyDetail';
import ContactDetail from '../../models/ContactDetail';
import Helper from '../../components/shared/helper';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSaving : false,
            canShowModal : false,
            applicantId : null,
            busyApplicants : []
        };
    }

    componentWillReceiveProps(props){
        let { APP,COMMANDS, FILE_NOTES, CONTACT_DETAIL,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.ADD_NOTES]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.ADD_NOTES]
            });
            // IF THIS IS IN CONTACT DETAIL PAGE, THEN THE CONTACT HAS TO BE ADDED IN APPLICANT SECTION
            // AND IF THIS IS IN PROPERTY DETAIL PAGE, THE PROPERTY HAS TO BE ADDED IN PROPERTIES SECTION
            if(! this.state.canShowModal) {
                if (FILE_NOTES.applicants.length === 0 && Helper.isContactDetailPage(props) && CONTACT_DETAIL._id) {
                    //FileNotes.actions.setObjectInPath(['applicants', 0], this.getApplicantObject(CONTACT_DETAIL));
                }

                // IF WE ARE IN PROPERTY DETAIL PAGE, THEN SET THIS PROPERTY AS DEFAULT
                if (FILE_NOTES.property === null && Helper.isPropertyDetailPage(props) && PROPERTY_DETAIL._id) {
                    FileNotes.actions.setObjectInPath(['property'],PROPERTY_DETAIL);
                    // BIND PROPERTY OWNERS TO FILE_NOTES IN CREATE MODE
                    if(PROPERTY_DETAIL.owners.length>0 && FILE_NOTES.owners.length ===0) {
                        _.each(PROPERTY_DETAIL.owners, (owner, index) => {
                            //FileNotes.actions.setObjectInPath(['owners', index], this.getApplicantObject(owner.contact));
                        });
                    }
                }
                // BIND NEGOTIATOR AND BRANCH AS DEFAULT IF IT IS NOT SAVED
                if(!FILE_NOTES._id) {
                    let {userInfo} = APP;
                    if (userInfo && userInfo._id) {
                        FileNotes.actions.change('appraiser', userInfo._id);
                    }
                }
            }
        }
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.ADD_NOTES,false);
        FileNotes.actions.clearModel();
    }

    isMailSentToAllApplicants(){
        return false;
    }

    submitHandler(result){
        let { FILE_NOTES } = this.props;
        if (result.success) {
            Alert.clearLogs().success(result.message);
            Commands.actions.change(constants.COMMANDS.FILE_NOTES_CHANGED, true);
            //FileNotes.actions.setModel(result.data);
        }
        else{
            Alert.clearLogs().error(result.error);
        }
        this.setState({
            isSaving: false
        });
        this.cancel();
    }

    submit(){
        FileNotes.actions.validateModel();
        let model = store.getState().FILE_NOTES.toJS();
        if (model.validator.isValid && model.applicants.length>0 &&  model.property) {
            delete model.validator;
            model.property = model.property._id;
            this.setState({
                isSaving: true
            });
            if (model._id) {
                FileNotes.api.update(model._id, model, this.submitHandler.bind(this));
            }
            else {
                FileNotes.api.save(model, this.submitHandler.bind(this));
            }
        }
    }

    remove(e){
        let { FILE_NOTES } = this.props;
        this.setState({
            isDeleting: true
        });
        FileNotes.api.cancel(FILE_NOTES._id, (result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);

                Commands.actions.change(constants.COMMANDS.FILE_NOTES_CHANGED, true);
                FileNotes.actions.setModel(result.data);
            }
            else {
                Alert.clearLogs().error(result.error);
            }
            this.setState({
                isDeleting: false
            });
        });
    }

    selectApplicantById(id){
        if(this.state.applicantId===id){
            this.setState({
                applicantId : null
            });
        }
        else{
            this.setState({
                applicantId : id
            });
        }
    }

    render(){
        return (
            <Component
                onChange={FileNotes.actions.valueChange}
                cancel = {this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                remove={this.remove.bind(this)}

                selectApplicantById={this.selectApplicantById.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        FILE_NOTES : store.FILE_NOTES.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
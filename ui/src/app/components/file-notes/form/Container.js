import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import FileNotes from '../../../models/FileNotes';
import PropertyDetail from '../../../models/PropertyDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSearchingProperties : false,
            properties : [],
            propertiesToFilter : []
        };
    }

    onPropertiesFocus(typeAhead){
        typeAhead.focus();
        this.onPropertiesChange('');
    }
    onPropertiesChange(text){
        let {FILE_NOTES} = this.props;
        text = text.trim();
        this.setState({
            isSearchingProperties: true
        });
        PropertyDetail.api.search({
            searchText: text,
            status : ['Available']
        }, (result) => {
            if (result.success) {
                let properties = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        price: p.price.toString(),
                        status: p.status,
                        address: p.address.fullAddress
                    }
                });
                if(FILE_NOTES.property!==null) {
                    properties = _.filter(properties, (p) => {
                        return p._id !== FILE_NOTES.property._id;
                    });
                }
                this.setState({
                    propertiesToFilter: properties,
                    properties: result.data,
                    isSearchingProperties: false
                });
            }
        });
    }
    onPropertySelected(items,typeAhead) {
        let { FILE_NOTES } = this.props;
        if (items.length > 0) {
            typeAhead.clear();

            let item = _.find(this.state.properties,function(p){
                return p._id === items[0]._id;
            });
            FileNotes.actions.setObjectInPath(['property'], item);
        }
    }

    render(){
        return (
            <Component
                onChange={FileNotes.actions.valueChange}
                onPropertiesFocus={this.onPropertiesFocus.bind(this)}
                onPropertiesChange={this.onPropertiesChange.bind(this)}
                onPropertySelected={this.onPropertySelected.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        FILE_NOTES : store.FILE_NOTES.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
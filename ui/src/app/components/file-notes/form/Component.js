import React from 'react';
import TypeAhead from 'react-bootstrap-typeahead';
import classNames from 'classnames';
import {ValidateTextArea } from '../../shared/Form';
import Helper from '../../shared/helper';

class Component extends React.Component {

    renderMenuItem(props,option,idx){
        return (
            <div className="property-item">
                <p className="address">{option.address}</p>
            </div>
        );
    }

    render() {
        let {
            FILE_NOTES,
            onChange,
            isSearchingProperties,
            propertiesToFilter,
            onPropertiesFocus,
            onPropertiesChange,
            onPropertySelected
        } = this.props;

        return (
            <div className="col-md-6">
                <form className="form-horizontal custom-form">
                    {(!FILE_NOTES._id && !Helper.isPropertyDetailPage(this.props))?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Property</label>
                                <div className={classNames({
                                    'col-sm-8':true,
                                    'has-error': (FILE_NOTES.validator.isSubmitted && FILE_NOTES.property===null)
                                })}>
                                    <TypeAhead
                                        ref="typeahead"
                                        placeholder="Search property ..."
                                        emptyLabel={isSearchingProperties?'Loading ...':'No property found.'}
                                        minLength={0}
                                        filterBy={['price','address','_id']}
                                        labelKey="address"
                                        options={propertiesToFilter}
                                        onInputChange={onPropertiesChange}
                                        onChange ={(items)=> { onPropertySelected(items,this.refs.typeahead.getInstance()); } }
                                        onFocus = { ()=> onPropertiesFocus(this.refs.typeahead.getInstance()) }
                                        renderMenuItemChildren={this.renderMenuItem.bind(this)}
                                    />
                                    {(FILE_NOTES.validator.isSubmitted && FILE_NOTES.property===null)?(<label className="control-label">Choose a property</label>):null}
                                </div>
                            </div>
                        ):null}

                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Notes</label>
                        <ValidateTextArea
                            rows="6"
                            className="col-sm-8"
                            model={FILE_NOTES}
                            name="notes"
                            onChange={onChange}
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import Applicants from '../applicants/Container';
import Feedbacks from '../feedbacks/Container';
import { ValidateTextArea } from '../../shared/Form';

class Component extends React.Component {

    render() {
        let {
            VIEWING,
            handleTabChange,
            activeTab
        } = this.props;

        return (
            <div className="col-sm-6">
                <div className="nav-tabs-horizontal">
                    <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                        <li role="presentation" className={(activeTab === 'applicants') ? 'active' : ''}>
                            <a onClick={() => handleTabChange('applicants')} role="button" href="javascript:void(0)">Applicants</a>
                        </li>
                        {VIEWING.applicants.length>0?(
                                <li role="presentation" className={(activeTab === 'feedbacks') ? 'active' : ''}>
                                    <a onClick={() => handleTabChange('feedbacks')} role="button" href="javascript:void(0)">Feedbacks</a>
                                </li>
                            ):null}
                        <li role="presentation" className={(activeTab === 'notes') ? 'active' : ''}>
                            <a onClick={() => handleTabChange('notes')} role="button" href="javascript:void(0)">Notes</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane active">
                            {activeTab === 'applicants' ? (<Applicants {...this.props} />) : null}
                            {activeTab === 'feedbacks' ? (<Feedbacks {...this.props} />) : null}
                            {activeTab === 'notes' ? (this.renderNotes()) : null}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderNotes(){
        let { VIEWING, onChange } = this.props;
        return (
            <div className="clear">
                <div className="scroll-area">
                    <ValidateTextArea
                        placeholder="Additional notes ..." rows="8"
                        model={VIEWING} name="additionalNotes"
                        onChange={onChange}
                    />
                </div>
            </div>
        );
    }
}

export default Component;
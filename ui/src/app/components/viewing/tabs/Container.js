import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Viewing from '../../../models/Viewing';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            activeTab:'applicants'
        };
    }
    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    render(){
        return (
            <Component
                onChange={Viewing.actions.valueChange}
                handleTabChange={this.handleTabChange.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS()
    };
};

export default connect(mapStateToProps)(Container);
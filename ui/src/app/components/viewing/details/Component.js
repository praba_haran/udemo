import React from 'react';
import Applicant from './Applicant';
import General from './General';

class Component extends React.Component {

    render() {
        let {
            VIEWING,
            applicantId
        } = this.props;

        if(applicantId){
            return <Applicant {...this.props} />;
        }
        return <General {...this.props} />;
    }
}

export default Component;
import React from 'react';
import Helper from '../../shared/helper';

class Details extends React.Component{

    render(){
        let { VIEWING } = this.props;
        return (
            <div className="detail">
                <div className="header">
                    <h4>General Information</h4>
                </div>
                <div className="content">
                    <div className="activities-container">
                        {this._renderConfirmationActivity()}
                        {this._renderRescheduledActivity()}
                        {this._renderCancelledActivity()}
                    </div>

                    {this._renderActionsWhenConfirmed()}
                    {this._renderActionsWhenRescheduled()}
                    {this._renderActionsWhenCancelled()}
                    {this._renderActionsWhenStarted()}
                </div>
            </div>
        );
    }

    _renderActionsWhenConfirmed(){
        let { VIEWING,startViewing,finishViewing,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Booked') {
            return (
                <div className="actions">
                    {!VIEWING.confirmationEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block btn-primary" type="button">Send
                                Confirmation Email
                            </button>
                        ):null}
                    {VIEWING.confirmationEmailSent?(
                            <button onClick={startViewing} className="btn btn-sm btn-block btn-primary" type="button">Set as Started</button>
                        ):null}
                    {VIEWING.confirmationEmailSent?(
                            <button onClick={finishViewing} className="btn btn-sm btn-block btn-primary" type="button">Set as Finished</button>
                        ):null}
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenRescheduled(){
        let { VIEWING,startViewing,finishViewing,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Rescheduled'&& VIEWING.confirmationEmailSent) {
            return (
                <div className="actions">
                    {!VIEWING.reScheduledEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block btn-primary" type="button">Send Rescheduled Email</button>
                        ):null}
                    <button onClick={startViewing} className="btn btn-sm btn-block btn-primary" type="button">Set as Started</button>
                    <button onClick={finishViewing} className="btn btn-sm btn-block btn-primary" type="button">Set as Finished</button>
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenCancelled(){
        let { VIEWING,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Cancelled'&& VIEWING.confirmationEmailSent) {
            return (
                <div className="actions">
                    {!VIEWING.cancellationEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block btn-primary" type="button">Send Cancellation Email</button>
                        ):null}
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenStarted(){
        let { VIEWING,finishViewing } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Started') {
            return (
                <div className="actions">
                    <button onClick={finishViewing} className="btn btn-sm btn-block btn-primary" type="button">Set as Finished</button>
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderConfirmationActivity(){
        let { VIEWING } = this.props;
        if(!VIEWING.confirmationEmailSent) {
            return null;
        }
        return (
            <ul className="activities">
                <li className="activity">
                    <span className="status-icon">
                        <i className="icon md-check"></i>
                    </span>
                    <p className="title">CONFIRMATION</p>
                    <div className="info">
                        <div>
                            <span>Mail Sent At</span>
                        </div>
                        <div>
                            <span>{Helper.formatDateTime(VIEWING.confirmationMailSentAt)}</span>
                        </div>
                    </div>
                    <div className="info">
                        <div>
                            <span>Mail Delivered</span>
                        </div>
                        <div>
                            <span>Yes</span>
                        </div>
                    </div>
                </li>
            </ul>
        );
    }

    _renderRescheduledActivity(){
        let { VIEWING } = this.props;
        if(!VIEWING.reScheduledEmailSent) {
            return null;
        }
        return (
            <ul className="activities">
                <li className="activity">
                    <span className="status-icon">
                        <i className="icon md-check"></i>
                    </span>
                    <p className="title">RESCHEDULE CONFIRMATION</p>
                    <div className="info">
                        <div>
                            <span>Mail Sent At</span>
                        </div>
                        <div>
                            <span>{Helper.formatDateTime(VIEWING.reScheduledEmailSentAt)}</span>
                        </div>
                    </div>
                    <div className="info">
                        <div>
                            <span>Mail Delivered</span>
                        </div>
                        <div>
                            <span>Yes</span>
                        </div>
                    </div>
                </li>
            </ul>
        );
    }

    _renderCancelledActivity(){
        let { VIEWING } = this.props;
        if(!VIEWING.cancellationEmailSent) {
            return null;
        }
        return (
            <ul className="activities">
                <li className="activity">
                    <span className="status-icon">
                        <i className="icon md-check"></i>
                    </span>
                    <p className="title">CANCELLATION CONFIRMATION</p>
                    <div className="info">
                        <div>
                            <span>Mail Sent At</span>
                        </div>
                        <div>
                            <span>{Helper.formatDateTime(VIEWING.cancellationEmailSentAt)}</span>
                        </div>
                    </div>
                    <div className="info">
                        <div>
                            <span>Mail Delivered</span>
                        </div>
                        <div>
                            <span>Yes</span>
                        </div>
                    </div>
                </li>
            </ul>
        );
    }
}

export default Details;
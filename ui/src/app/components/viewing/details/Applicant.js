import React from 'react';
import _ from 'lodash';
import Helper from '../../shared/helper';

class Applicant extends React.Component{

    render(){
        let {
            VIEWING,
            applicantId,
            selectApplicantById
        } = this.props;
        let applicant = _.find(VIEWING.applicants,(p)=>{
            return p.contact._id === applicantId;
        });

        return (
            <div className="detail">
                <div className="header">
                    <h4>
                        {applicant.contact.firstPerson.forename+' '+applicant.contact.firstPerson.surname}
                        <span onClick={()=>selectApplicantById(null)} className="close-button">
                            <i className="icon md-close"></i>
                        </span>
                    </h4>
                </div>
                <div className="content">
                    <div className="activities-container">
                        <ul className="activities">
                            {this._renderConfirmationActivity(applicant)}
                            {this._renderRescheduledActivity(applicant)}
                            {this._renderCancelledActivity(applicant)}
                        </ul>
                    </div>

                    {this._renderActionsWhenConfirmed()}
                    {this._renderActionsWhenRescheduled()}
                    {this._renderActionsWhenCancelled()}
                    {this._renderActionsWhenStarted()}
                </div>
            </div>
        );
    }

    _renderActionsWhenConfirmed(){
        let { VIEWING,startViewing,finishViewing,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Booked') {
            return (
                <div className="actions">
                    {!VIEWING.confirmationEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block" type="button">Send
                                Confirmation Email
                            </button>
                        ):null}
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenRescheduled(){
        let { VIEWING,startViewing,finishViewing,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Rescheduled'&& VIEWING.confirmationEmailSent) {
            return (
                <div className="actions">
                    {!VIEWING.reScheduledEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block" type="button">Send Rescheduled Email</button>
                        ):null}
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenCancelled(){
        let { VIEWING,openConfirmationEmail } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Cancelled'&& VIEWING.confirmationEmailSent) {
            return (
                <div className="actions">
                    {!VIEWING.cancellationEmailSent?(
                            <button onClick={openConfirmationEmail} className="btn btn-sm btn-block" type="button">Send Cancellation Email</button>
                        ):null}
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderActionsWhenStarted(){
        let { VIEWING,finishViewing } = this.props;
        if( VIEWING.isConfirmed && VIEWING.status ==='Started') {
            return (
                <div className="actions">
                    <button onClick={finishViewing} className="btn btn-sm btn-block" type="button">Set as Finished</button>
                    <button className="btn btn-sm btn-block" type="button">Print</button>
                </div>
            );
        }
        return null;
    }

    _renderConfirmationActivity(applicant){
        let {
            VIEWING,
            sendConfirmationEmailToApplicant
        } = this.props;
        if(!VIEWING.confirmationEmailSent) {
            return null;
        }
        return (
            <li className="activity">
                <span className="status-icon">
                    <i className="icon md-check"></i>
                </span>
                <p className="title">CONFIRMATION</p>
                <div className="info">
                    <div>
                        <span>Mail Sent At</span>
                    </div>
                    <div>
                        <span>{Helper.formatDateTime(applicant.onConfirmation.createdAt)}</span>
                    </div>
                </div>
                <div className="info">
                    <div>
                        <span>Mail Delivered</span>
                    </div>
                    <div>
                        <span>{applicant.onConfirmation.isEmailDelivered?'Yes':'No'}</span>
                        {!applicant.onConfirmation.isEmailDelivered?(
                                <span
                                    onClick={()=>sendConfirmationEmailToApplicant(applicant._id)}
                                    className="label label-default label-outline">Send Email</span>
                            ):null}
                    </div>
                </div>
            </li>
        );
    }

    _renderRescheduledActivity(applicant){
        let { VIEWING } = this.props;
        if(!VIEWING.reScheduledEmailSent) {
            return null;
        }
        return (
            <li className="activity">
                <span className="status-icon">
                    <i className="icon md-check"></i>
                </span>
                <p className="title">RESCHEDULE CONFIRMATION</p>
                <div className="info">
                    <div>
                        <span>Mail Sent At</span>
                    </div>
                    <div>
                        <span>{Helper.formatDateTime(applicant.onRescheduled.createdAt)}</span>
                    </div>
                </div>
                <div className="info">
                    <div>
                        <span>Mail Delivered</span>
                    </div>
                    <div>
                        <span>{applicant.onRescheduled.isEmailDelivered?'Yes':'No'}</span>
                    </div>
                </div>
            </li>
        );
    }

    _renderCancelledActivity(applicant){
        let { VIEWING } = this.props;
        if(!VIEWING.cancellationEmailSent) {
            return null;
        }
        return (
            <li className="activity">
                <span className="status-icon">
                    <i className="icon md-check"></i>
                </span>
                <p className="title">CANCELLATION CONFIRMATION</p>
                <div className="info">
                    <div>
                        <span>Mail Sent At</span>
                    </div>
                    <div>
                        <span>{Helper.formatDateTime(applicant.onCancelled.createdAt)}</span>
                    </div>
                </div>
                <div className="info">
                    <div>
                        <span>Mail Delivered</span>
                    </div>
                    <div>
                        <span>{applicant.onCancelled.isEmailDelivered?'Yes':'No'}</span>
                        <span className="label label-default">Resend</span>
                    </div>
                </div>
            </li>
        );
    }
}

export default Applicant;
import React from 'react';
import TypeAhead from 'react-bootstrap-typeahead';
import classNames from 'classnames';
import {Select, DatePicker, TimePicker, ValidateSelect, ValidateTextArea, CheckBox} from '../../shared/Form';
import Helper from '../../shared/helper';

class Component extends React.Component {

    renderMenuItem(props,option,idx){
        return (
            <div className="property-item">
                <p className="address">{option.address}</p>
            </div>
        );
    }

    render() {
        let {
            VIEWING,
            onChange,
            isSearchingProperties,
            propertiesToFilter,
            onPropertiesFocus,
            onPropertiesChange,
            onPropertySelected,
            manipulateDate,
            canDisableViewing,
            canDisableDateAndTimePickers,
            canDisableConfirmedCheckBoxes
        } = this.props;
        let disableViewing = canDisableViewing();
        let canEditCheckBoxes = canDisableConfirmedCheckBoxes();
        let disableDateAndTimePickers = canDisableDateAndTimePickers();

        return (
            <div className="col-md-6">
                <form className="form-horizontal custom-form">
                    {(!VIEWING._id && !Helper.isPropertyDetailPage(this.props))?(
                            <div className="form-group margin-bottom-10">
                                <label className="col-sm-4 control-label">Property</label>
                                <div className={classNames({
                                    'col-sm-8':true,
                                    'has-error': (VIEWING.validator.isSubmitted && VIEWING.property===null)
                                })}>
                                    <TypeAhead
                                        ref="typeahead"
                                        placeholder="Search property ..."
                                        emptyLabel={isSearchingProperties?'Loading ...':'No property found.'}
                                        minLength={0}
                                        filterBy={['price','address','_id']}
                                        labelKey="address"
                                        options={propertiesToFilter}
                                        onInputChange={onPropertiesChange}
                                        onChange ={(items)=> { onPropertySelected(items,this.refs.typeahead.getInstance()); } }
                                        onFocus = { ()=> onPropertiesFocus(this.refs.typeahead.getInstance()) }
                                        renderMenuItemChildren={this.renderMenuItem.bind(this)}
                                    />
                                    {(VIEWING.validator.isSubmitted && VIEWING.property===null)?(<label className="control-label">Choose a property</label>):null}
                                </div>
                            </div>
                        ):null}
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Appraiser</label>
                        {this._renderAppraiser()}
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Accompanied By</label>
                        {this._renderAccompainedBy()}
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Date</label>
                        <div className="col-sm-8">
                            <DatePicker
                                model={VIEWING}
                                name="date"
                                onChange={onChange}
                                getManipulatedDate={manipulateDate}
                                options={{
                                    disablePreviousDate: true
                                }}
                                disabled={disableDateAndTimePickers}
                            />
                        </div>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Time</label>
                        <div className="col-sm-4">
                            <TimePicker
                                model={VIEWING}
                                name="startTime"
                                onChange={onChange}
                                disabled={disableDateAndTimePickers}
                            />
                        </div>
                        <div className="col-sm-4">
                            <TimePicker
                                model={VIEWING}
                                name="endTime"
                                onChange={onChange}
                                disabled={disableDateAndTimePickers}
                            />
                        </div>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Reminder</label>
                        <ValidateSelect
                            className="col-sm-4"
                            model={VIEWING}
                            name="reminder"
                            onChange={onChange}
                            dropKey="reminderMinutes"
                            showLabel="false"
                            disabled={disableViewing}
                        />
                        <ValidateSelect
                            className="col-sm-4"
                            model={VIEWING}
                            name="reminderChannel"
                            onChange={onChange}
                            dropKey="reminderChannel"
                            showLabel="true"
                            disabled={disableViewing}
                        />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Notes</label>
                        <ValidateTextArea
                            rows="6"
                            className="col-sm-8"
                            model={VIEWING}
                            name="notes"
                            onChange={onChange}
                            disabled={disableViewing}
                        />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <div className="col-sm-offset-4 col-sm-8">
                            <CheckBox
                                model={VIEWING}
                                name="vendorConfirmed"
                                onChange={onChange}
                                label="Vendor Confirmed"
                                disabled={canEditCheckBoxes}
                            />
                            <CheckBox
                                model={VIEWING}
                                name="applicantsConfirmed"
                                onChange={onChange}
                                label="Applicants Confirmed"
                                disabled={canEditCheckBoxes}
                            />
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    _renderAppraiser() {
        let {VIEWING, canDisableViewing, onChange, APP} = this.props;
        let {agencyDetails} = APP;
        if (agencyDetails && agencyDetails.agencyUsers) {
            return (
                <ValidateSelect
                    className="col-sm-8"
                    model={VIEWING}
                    name="appraiser"
                    onChange={onChange}
                    disabled={canDisableViewing()}
                >
                    {agencyDetails.agencyUsers.map(function (p) {
                        return (
                            <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                        );
                    })}
                </ValidateSelect>
            );
        }
    }

    _renderAccompainedBy() {
        let {VIEWING,canDisableViewing, onChange, APP} = this.props;
        let {agencyDetails} = APP;
        if (agencyDetails && agencyDetails.agencyUsers) {
            return (
                <div className="col-sm-8">
                    <Select
                        model={VIEWING}
                        name="accompaniedBy"
                        onChange={onChange}
                        disabled={canDisableViewing()}
                    >
                        {(VIEWING && VIEWING.appraiser) ? agencyDetails.agencyUsers.map(function (p) {
                                if (p._id !== VIEWING.appraiser) {
                                    return (
                                        <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                    );
                                }
                            }) : null}
                    </Select>
                </div>
            );
        }
    }
}

export default Component;
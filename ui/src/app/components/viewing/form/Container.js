import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import Component from './Component';
import Viewing from '../../../models/Viewing';
import PropertyDetail from '../../../models/PropertyDetail';
import Helper from '../../../components/shared/helper';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSearchingProperties : false,
            properties : [],
            propertiesToFilter : []
        };
    }

    onPropertiesFocus(typeAhead){
        typeAhead.focus();
        this.onPropertiesChange('');
    }
    onPropertiesChange(text){
        let {VIEWING} = this.props;
        text = text.trim();
        this.setState({
            isSearchingProperties: true
        });
        PropertyDetail.api.search({
            searchText: text,
            status : ['Available']
        }, (result) => {
            if (result.success) {
                let properties = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        price: p.price.toString(),
                        status: p.status,
                        address: p.address.fullAddress
                    }
                });
                if(VIEWING.property!==null) {
                    properties = _.filter(properties, (p) => {
                        return p._id !== VIEWING.property._id;
                    });
                }
                this.setState({
                    propertiesToFilter: properties,
                    properties: result.data,
                    isSearchingProperties: false
                });
            }
        });
    }
    onPropertySelected(items,typeAhead) {
        let { VIEWING,getApplicantObject } = this.props;
        if (items.length > 0) {
            typeAhead.clear();

            let item = _.find(this.state.properties,function(p){
                return p._id === items[0]._id;
            });
            Viewing.actions.setObjectInPath(['property'], item);
            // BIND PROPERTY OWNERS TO VIEWING IN CREATE MODE
            if(item.owners.length>0 && VIEWING.owners.length ===0) {
                _.each(item.owners, (owner, index) => {
                    Viewing.actions.setObjectInPath(['owners', index], getApplicantObject(owner.contact));
                });
            }
        }
    }

    // DATE AND TIME MANIPULATION START
    manipulateDate(date){
        let { VIEWING } = this.props;
        //let hourMinutes = Helper.getHourMinutesFromTime(VIEWING.startTime);
        return moment(date).set({
            hour : 0, // hourMinutes.hour,
            minute: 0, // hourMinutes.minutes,
            second : 0,
            millisecond : 0
        });
    }
    // DATE AND TIME MANIPULATION END

    render(){
        return (
            <Component
                onChange={Viewing.actions.valueChange}
                onPropertiesFocus={this.onPropertiesFocus.bind(this)}
                onPropertiesChange={this.onPropertiesChange.bind(this)}
                onPropertySelected={this.onPropertySelected.bind(this)}

                manipulateDate = {this.manipulateDate.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
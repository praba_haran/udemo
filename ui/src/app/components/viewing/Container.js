import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import store from '../../store';
import Alert from '../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import * as constants from '../../constants';
import Viewing from '../../models/Viewing';
import Commands from '../../models/Commands';
import PropertyDetail from '../../models/PropertyDetail';
import ContactDetail from '../../models/ContactDetail';
import Helper from '../../components/shared/helper';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSaving : false,
            canShowModal : false,
            applicantId : null,
            busyApplicants : []
        };
    }

    componentWillReceiveProps(props){
        let { APP,COMMANDS, VIEWING, CONTACT_DETAIL,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.BOOK_VIEWING]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.BOOK_VIEWING]
            });
            // IF THIS IS IN CONTACT DETAIL PAGE, THEN THE CONTACT HAS TO BE ADDED IN APPLICANT SECTION
            // AND IF THIS IS IN PROPERTY DETAIL PAGE, THE PROPERTY HAS TO BE ADDED IN PROPERTIES SECTION
            if(! this.state.canShowModal) {
                if (VIEWING.applicants.length === 0 && Helper.isContactDetailPage(props) && CONTACT_DETAIL._id) {
                    Viewing.actions.setObjectInPath(['applicants', 0], this.getApplicantObject(CONTACT_DETAIL));
                }

                // IF WE ARE IN PROPERTY DETAIL PAGE, THEN SET THIS PROPERTY AS DEFAULT
                if (VIEWING.property === null && Helper.isPropertyDetailPage(props) && PROPERTY_DETAIL._id) {
                    Viewing.actions.setObjectInPath(['property'],PROPERTY_DETAIL);
                    // BIND PROPERTY OWNERS TO VIEWING IN CREATE MODE
                    if(PROPERTY_DETAIL.owners.length>0 && VIEWING.owners.length ===0) {
                        _.each(PROPERTY_DETAIL.owners, (owner, index) => {
                            Viewing.actions.setObjectInPath(['owners', index], this.getApplicantObject(owner.contact));
                        });
                    }
                }
                // BIND NEGOTIATOR AND BRANCH AS DEFAULT IF IT IS NOT SAVED
                if(!VIEWING._id) {
                    let {userInfo} = APP;
                    if (userInfo && userInfo._id) {
                        Viewing.actions.change('appraiser', userInfo._id);
                    }
                }
            }
        }
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,false);
        Viewing.actions.clearModel();
    }

    isMailSentToAllApplicants(){
        return false;
    }

    getNotificationInfo(applicant){
        let { VIEWING } = this.props;
        let notification = {};
        if(VIEWING.status === 'Booked'){
            if(applicant.status==='Active'){
                notification.type = 'onConfirmation';
                notification.canSendEmail = !applicant.onConfirmation.isEmailDelivered;
            }
            else if(applicant.status==='Cancelled'){
                notification.type = 'onCancelled';
                notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                    && !applicant.onCancelled.isEmailDelivered;
            }
        }
        if(VIEWING.status === 'Rescheduled'){
            if(applicant.status==='Active'){
                if(!applicant.onConfirmation.isEmailDelivered){
                    notification.type = 'onConfirmation';
                    notification.canSendEmail = true;
                }
                else{
                    notification.type = 'onRescheduled';
                    notification.canSendEmail = !applicant.onRescheduled.isEmailDelivered;
                }
            }
            else if(applicant.status==='Cancelled'){
                notification.type = 'onCancelled';
                notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                    && !applicant.onCancelled.isEmailDelivered;
            }
        }
        else if(VIEWING.status==='Cancelled'){
            notification.type = 'onCancelled';
            notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                && !applicant.onCancelled.isEmailDelivered;
        }
        return notification;
    }

    openConfirmationEmail(){
        let { VIEWING } = this.props;
        // CLOSING VIEWING MODAL
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,false);
        let canSendEmailCount = 0;
        _.each(VIEWING.applicants,(p)=>{
             let info = this.getNotificationInfo(p);
             if(info.canSendEmail){
                 canSendEmailCount ++;
             }
        });
        if(canSendEmailCount>0 && VIEWING.vendorConfirmed && VIEWING.applicantsConfirmed){
            Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM, true);
        }
        else{
            Viewing.actions.clearModel();
        }
    }

    submitHandler(result){
        let { VIEWING } = this.props;
        if (result.success) {
            Alert.clearLogs().success(result.message);
            Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, true);
            Viewing.actions.setModel(result.data);
            this.openConfirmationEmail();
        }
        else{
            Alert.clearLogs().error(result.error);
        }
        this.setState({
            isSaving: false
        });
    }

    submit(){
        Viewing.actions.validateModel();
        let model = store.getState().VIEWING.toJS();
        if (model.validator.isValid && model.applicants.length>0 &&  model.property) {
            delete model.validator;
            model.property = model.property._id;
            this.checkBusyApplicants(model,(busyApplicants)=>{
                this.setState({
                    busyApplicants : busyApplicants
                });
               if(busyApplicants.length === 0){
                   this.setState({
                       isSaving: true
                   });
                   if (model._id) {
                       Viewing.api.update(model._id, model, this.submitHandler.bind(this));
                   }
                   else {
                       Viewing.api.save(model, this.submitHandler.bind(this));
                   }
               }
            });
        }
    }

    checkBusyApplicants(model,cb){
        let { VIEWING } = this.props;
        let params = {
            date : VIEWING.date,
            startTime : VIEWING.startTime,
            endTime : VIEWING.endTime
        };

        Viewing.api.getBusyApplicants(params,(result)=> {
            if (result.success) {

                let applicants = _.map(model.applicants,(p)=>{
                   return p.contact._id;
                });

                let busyApplicants = [];

                _.each(result.data, (p) => {
                    _.each(p.applicants, (a) => {
                        if (a.status === 'Active') {

                            if(applicants.indexOf(a.contact)>-1 && model._id !==p._id) {
                                // preparing busy applicants
                                busyApplicants.push({
                                    contact: a.contact,
                                    status: a.status,
                                    date: p.date,
                                    startTime: p.startTime,
                                    endTime: p.endTime
                                });
                            }
                        }
                    });
                });
                cb(busyApplicants);
            }
            else{
                Alert.clearLogs().error(result.error);
            }
        });
    }

    remove(e){
        let { VIEWING } = this.props;
        this.setState({
            isDeleting: true
        });
        Viewing.api.cancel(VIEWING._id, (result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);

                Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, true);
                Viewing.actions.setModel(result.data);
                this.openConfirmationEmail();
            }
            else {
                Alert.clearLogs().error(result.error);
            }
            this.setState({
                isDeleting: false
            });
        });
    }

    startViewing(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to start this viewing ?', ()=>{
            Viewing.actions.change('status','Started');
            this.submit();
        });
    }

    finishViewing(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to finish this viewing ?', ()=>{
            Viewing.actions.change('status','Finished');
            this.submit();
        });
    }

    canDisableViewing(){
        let { VIEWING } = this.props;
        return VIEWING.status === 'Cancelled' || VIEWING.status==='Started' || VIEWING.status==='Finished';
    }

    canDisableConfirmedCheckBoxes(){
        let { VIEWING } = this.props;
        let canDisable = this.canDisableViewing();
        if(!canDisable) {
            canDisable = VIEWING.confirmationEmailSent;
        }
        return canDisable;
    }
    canDisableDateAndTimePickers(){
        let { VIEWING } = this.props;
        let canDisable = this.canDisableViewing();
        if(!canDisable) {
            canDisable = (VIEWING.status === 'Rescheduled' && VIEWING.reScheduledEmailSent);
        }
        return canDisable;
    }

    sendConfirmationEmailToApplicant(applicantId){
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM_APPLICANT_ID, applicantId);
        this.openConfirmationEmail();
    }

    selectApplicantById(id){
        if(this.state.applicantId===id){
            this.setState({
                applicantId : null
            });
        }
        else{
            this.setState({
                applicantId : id
            });
        }
    }

    getApplicantObject(applicant){
        let obj = {
            contact: applicant,
            feedback: '',
            rating: null,
            onConfirmation: {
                contact: applicant,
                emailMessage: {
                    line1: 'This is a gentle reminder for Viewing for a property.',
                    line2: 'I would be very fascinated to acquire your opinion of the property and shall have good contact with you following the viewing.',
                    line3: 'If you are unable to keep the appointment, please let me know so that I can re-schedule.'
                },
                smsMessage: '',
                isEmailDelivered : false,
                deliveredAt : null
            },
            onRescheduled: {
                contact: applicant,
                emailMessage: {
                    line1: 'I am pleased to inform you that your viewings for a property schedule have been changed. I regretted for this inconvenience.',
                    line2: null,
                    line3: null
                },
                smsMessage: '',
                isEmailDelivered : false,
                deliveredAt : null
            },
            onCancelled: {
                contact: applicant,
                emailMessage: {
                    line1: 'I am pleased to inform you that your viewing schedule have been cancelled. I regretted for this inconvenience.',
                    line2: null,
                    line3: null
                },
                smsMessage: '',
                isEmailDelivered : false,
                deliveredAt : null
            },
            status: 'Active',
            cancelReason: null
        };
        return obj;
    }

    render(){
        return (
            <Component
                onChange={Viewing.actions.valueChange}
                cancel = {this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                remove={this.remove.bind(this)}
                startViewing={this.startViewing.bind(this)}
                finishViewing ={this.finishViewing.bind(this)}

                canDisableViewing={this.canDisableViewing.bind(this)}
                canDisableDateAndTimePickers={this.canDisableDateAndTimePickers.bind(this)}
                canDisableConfirmedCheckBoxes={this.canDisableConfirmedCheckBoxes.bind(this)}
                openConfirmationEmail={this.openConfirmationEmail.bind(this)}

                selectApplicantById={this.selectApplicantById.bind(this)}
                sendConfirmationEmailToApplicant={this.sendConfirmationEmailToApplicant.bind(this)}
                getApplicantObject={this.getApplicantObject.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
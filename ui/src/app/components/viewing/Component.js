import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import classNames from 'classnames';
import PropertySection from './partials/PropertySection';
import Form from './form/Container';
import Tabs from './tabs/Container';
import Details from './details/Component';

class Component extends React.Component {

    render() {
        let {
            VIEWING,
            cancel,
            submit,
            remove,
            canDisableViewing
        } = this.props;
        let disableViewing = canDisableViewing();
        return (
            <Modal backdrop="static"
                   show={this.props.canShowModal}
                   onHide={cancel}
                   keyboard={false}
                   dialogClassName={classNames({
                       'modal-viewing' : (VIEWING._id && VIEWING.isConfirmed),
                       'modal-viewing modal-lg': !(VIEWING._id && VIEWING.isConfirmed)
                   })}>
                <Modal.Header closeButton>
                    <Modal.Title>{VIEWING._id?'Viewing':'Book A Viewing'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <table className="table-section">
                        <tbody>
                            <tr>
                                <td style={{width:'70%'}}>
                                    <PropertySection {...this.props} />
                                    <div className="row">
                                        <Form {...this.props} />
                                        <Tabs {...this.props} />
                                    </div>
                                </td>
                                {(VIEWING._id && VIEWING.isConfirmed)?(
                                        <td style={{width:'30%'}} className="detail-container">
                                            <Details {...this.props} />
                                        </td>
                                    ):null}
                            </tr>
                        </tbody>
                    </table>
                </Modal.Body>
                <Modal.Footer>
                    <div className="row">
                        <div className="col-sm-6 text-left">
                            {(VIEWING._id && !disableViewing)?(
                                    <Button disabled={this.props.isSaving || this.props.isDeleting} onClick={remove} className="btn btn-danger">{this.props.isDeleting ? 'Cancelling ...' : 'Cancel Viewing'}</Button>
                                ):null}
                        </div>
                        <div className="col-sm-6 text-right">
                            <Button disabled={this.props.isSaving || this.props.isDeleting} onClick={cancel}>Cancel</Button>
                            <Button className="btn btn-primary" onClick={submit} disabled={this.props.isSaving || this.props.isDeleting}>{this.props.isSaving ? 'Saving ...' : 'Save'}</Button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
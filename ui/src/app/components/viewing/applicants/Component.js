import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import TypeAhead from 'react-bootstrap-typeahead';
import { Scrollbars } from 'react-custom-scrollbars';
import classNames from 'classnames';
import Helper from '../../shared/helper';

class Applicants extends React.Component{

    renderMenuItem(props,option,idx){
        return (
            <div className={classNames({
                'contact-item':true,
                'busy-contact':option.isBusy
            })}>
                <p className="name">
                    {option.fullName}
                    <span className="contact-type">{option.type}</span>
                </p>
                <p className="address">{option.address}</p>
                {option.isBusy?(
                        <p className="busy-indicator">
                            Viewing booked at
                            <span className="date"> {moment(option.busyInfo.date).format('DD MMM YYYY')} </span>
                            between <span className="time">{Helper.toTime(option.busyInfo.startTime)} </span>
                            to <span className="time">{Helper.toTime(option.busyInfo.endTime)}</span>
                        </p>
                    ):null}
            </div>
        );
    }
    render(){
        let {
            VIEWING,
            isSearchingApplicants,
            applicantsToFilter,
            applicants,
            applicantId,
            applicantFilter
        } = this.props;

        let {
            onApplicantsFocus,
            getAllApplicants,
            onApplicantsChange,
            onApplicantSelected,
            selectApplicantById,
            removeApplicant,
            canDisableViewing,
        } = this.props;

        let disableViewing = canDisableViewing();

        let filteredApplicants = VIEWING.applicants;
        if(VIEWING._id) {
            filteredApplicants = _.filter(VIEWING.applicants, (p) => {
                return p.status === applicantFilter;
            });
        }

        return (
            <div className="clear">
                <div className="content-area">
                    <div className="input-group">
                        <TypeAhead
                            ref="typeahead"
                            placeholder="Search applicants by name ..."
                            emptyLabel={isSearchingApplicants?'Loading ...':'No applicants found.'}
                            minLength={0}
                            filterBy={['forename','surname']}
                            labelKey="fullName"
                            options={applicantsToFilter}
                            onInputChange={onApplicantsChange}
                            onChange ={(items)=> onApplicantSelected(items,this.refs.typeahead.getInstance()) }
                            onFocus = {onApplicantsFocus}
                            renderMenuItemChildren={this.renderMenuItem.bind(this)}
                            disabled={disableViewing}
                        />
                        <span className="input-group-btn">
                                <button onClick={()=>getAllApplicants(this.refs.typeahead.getInstance())} type="button" className="btn btn-default waves-effect waves-light">
                                    <i className="icon md-search" aria-hidden="true"></i>
                                </button>
                            </span>
                    </div>
                    {(VIEWING.applicants.length===0 && VIEWING.validator.isSubmitted)?(
                            <p className="help-block text-danger">Applicant is required for this viewing.</p>
                        ):null}
                    {(VIEWING.applicants.length===0 && !VIEWING.validator.isSubmitted)?(
                            <p className="help-block">Add an applicant for this viewing.</p>
                        ):null}
                </div>
                <div className="clear">
                    <Scrollbars
                        style={{height:330}}
                    >
                        <div className="scroll-area">
                            {this.renderFilters()}
                            <ul className="list-group list-group-gap applicants-list-group">
                                {filteredApplicants.map((p)=>{
                                    return (
                                        <li key={p.contact._id} className={classNames({
                                            'list-group-item':true,
                                            'active': applicantId === p.contact._id
                                        })}>
                                            <a onClick={()=>selectApplicantById(p.contact._id)} href="javascript:void(0)" role="button">
                                                <p className="title">
                                                    {p.contact.firstPerson.forename+' '+p.contact.firstPerson.surname}
                                                    <span className="contact-type">{p.contact.type}</span>
                                                </p>
                                                <p className="address">{p.contact.address.fullAddress}</p>
                                                {this.getApplicantBusySummary(p.contact._id)}
                                            </a>
                                            {(!disableViewing && applicantFilter==='Active')?(
                                                    <span className="remove" onClick={()=>removeApplicant(p.contact._id)}>
                                                        <i className="icon md-close"></i>
                                                    </span>
                                                ):null}
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </Scrollbars>
                </div>
            </div>
        );
    }

    renderFilters(){
        let {
            VIEWING,
            applicantFilter,
            changeApplicantFilter
        } = this.props;
        if(!VIEWING._id){
            return null;
        }
        return (
            <p className="filters">
                <span
                    onClick={()=>changeApplicantFilter('Active')}
                    className={classNames({
                        'label label-outline label-default' : true,
                        'active': applicantFilter==='Active'
                    })}
                >
                    Active
                </span>
                <span
                    onClick={()=>changeApplicantFilter('Cancelled')}
                    className={classNames({
                        'label label-outline label-default' : true,
                        'active': applicantFilter==='Cancelled'
                    })}
                >
                    Cancelled
                </span>
            </p>
        );
    }

    getApplicantBusySummary(applicantId){
        let { busyApplicants } = this.props;

        let busyApplicant = _.find(busyApplicants,(p)=>{
            return p.contact === applicantId;
        });
        if(busyApplicant){
            return (
                <p className="busy-indicator">
                    Viewing booked at
                    <span className="date"> {moment(busyApplicant.date).format('DD MMM YYYY')} </span>
                    between <span className="time">{Helper.toTime(busyApplicant.startTime)} </span>
                    to <span className="time">{Helper.toTime(busyApplicant.endTime)}</span>
                </p>
            );
        }
        return null;
    }
}

export default Applicants;
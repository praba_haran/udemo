import React from 'react';
import _ from 'lodash';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import Viewing from '../../../models/Viewing';
import ContactDetail from '../../../models/ContactDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSearchingApplicants : false,
            applicants : [],
            applicantsToFilter : [],
            applicantFilter : 'Active'
        };
    }

    onApplicantsFocus(){
        this.onApplicantsChange('');
    }
    getAllApplicants(typeAhead){
        typeAhead.focus();
    }
    onApplicantsChange(text){
        let {VIEWING} = this.props;
        text = text.trim();
        this.setState({
            isSearchingApplicants: true
        });

        let params = {
            date : VIEWING.date,
            startTime : VIEWING.startTime,
            endTime : VIEWING.endTime
        };

        Viewing.api.getBusyApplicants(params,(result)=>{
            if(result.success){

                let busyApplicants = {};
                let busyApplicantIds = [];

                _.each(result.data,(p)=>{
                    _.each(p.applicants,(a)=>{
                       if(a.status==='Active'){
                           busyApplicantIds.push(a.contact);

                           // preparing busy applicants
                           busyApplicants[a.contact] = {
                               contact : a.contact,
                               status : a.status,
                               date : p.date,
                               startTime : p.startTime,
                               endTime : p.endTime
                           };
                       }
                    });
                });

                // searching contacts
                ContactDetail.api.search({
                    searchText: text,
                    status : 'Active'
                }, (result) => {
                    if (result.success) {
                        let existingApplicants = _.map(VIEWING.applicants, (p) => {
                            return p.contact._id;
                        });
                        let propertyOwners = [];
                        if(VIEWING.property) {
                            propertyOwners = _.map(VIEWING.property.owners, (p) => {
                                return p.contact._id;
                            });
                        }
                        let applicants = _.map(result.data, (p) => {

                            return {
                                _id: p._id,
                                forename: p.firstPerson.forename,
                                surname: p.firstPerson.surname,
                                fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                                address: p.address.fullAddress,
                                type : p.type,
                                isBusy : busyApplicantIds.indexOf(p._id)>-1,
                                busyInfo : busyApplicants[p._id]
                            }
                        });
                        applicants = _.filter(applicants, (p) => {
                            return existingApplicants.indexOf(p._id) === -1 && propertyOwners.indexOf(p._id) === -1;
                        });
                        this.setState({
                            applicantsToFilter: applicants,
                            applicants: result.data,
                            isSearchingApplicants: false
                        });
                    }
                });

            }
        });


    }
    onApplicantSelected(items,typeAhead) {

        if (items.length > 0) {
            let { VIEWING, getApplicantObject } = this.props;
            let index = VIEWING.applicants.length;
            typeAhead.clear();
            if(items[0].isBusy){
                Alert.clearLogs().error('Applicant holds viewing with same date/time.');
            }
            else {
                let applicant = _.find(VIEWING.applicants, function (p) {
                    return p.contact._id === items[0]._id;
                });
                if (applicant === undefined) {
                    let item = _.find(this.state.applicants, function (p) {
                        return p._id === items[0]._id;
                    });

                    Viewing.actions.setObjectInPath(['applicants', index], getApplicantObject(item));
                }
                else {
                    Alert.clearLogs().error('This applicant is already added!');
                }
            }
        }
    }

    removeApplicant(id){
        // IF WE HAVE ONLY ONE APPLICANT IN THE VIEWING AND HE WANTS TO CANCEL THE VIEWING
        // THEN YOU CANNOT REMOVE THE APPLICANT FROM THE LIST
        // RATHER ASK USER TO CANCEL THE VIEWING IF THEY WANT
        let { VIEWING,remove } = this.props;
        if(VIEWING.applicants.length===1 && VIEWING._id){
            Alert.okBtn('Yes').cancelBtn('No').confirm('There is no active applicants. Would you like to cancel this viewing ?', ()=>{
                remove();
            });
        }
        else {
            let activeApplicantsCount = _.filter(VIEWING.applicants,(p)=>{
               return p.status === 'Active';
            }).length;
            let index = _.findIndex(VIEWING.applicants, function (p) {
                return p.contact._id === id;
            });
            if (index > -1) {
                // IF VIEWING IS CONFIRMED, THEN DON'T DELETE THE APPLICANTS PERMANATELY
                if(VIEWING._id && VIEWING.isConfirmed){
                    if(activeApplicantsCount===1){
                        Alert.okBtn('Yes').cancelBtn('No').confirm('There is no active applicants. Would you like to cancel this viewing ?', () => {
                            remove();
                        });
                    }
                    else {
                        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this applicant ?', () => {
                            Viewing.actions.setObjectInPath(['applicants', index, 'status'], 'Cancelled');
                        });
                    }
                }
                else{
                    Viewing.actions.removeObjectInPath(['applicants', index]);
                }
            }
        }
    }
    changeApplicantFilter(filter){
        this.setState({
            applicantFilter : filter
        });
    }

    render(){
        return (
            <Component
                onChange={Viewing.actions.valueChange}

                onApplicantsFocus={this.onApplicantsFocus.bind(this)}
                getAllApplicants={this.getAllApplicants.bind(this)}
                onApplicantsChange={this.onApplicantsChange.bind(this)}
                onApplicantSelected={this.onApplicantSelected.bind(this)}
                removeApplicant = {this.removeApplicant.bind(this)}
                changeApplicantFilter={this.changeApplicantFilter.bind(this)}

                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import {ValidateTextArea} from '../../shared/Form';
import classNames from 'classnames';
import {Scrollbars} from 'react-custom-scrollbars';

class Component extends React.Component {

    render() {
        let {
            VIEWING,
            onChange,
            setApplicantRating,
            handleSelect,
            applicantFilter,
            activeKey
        } = this.props;

        let filteredApplicants = _.filter(VIEWING.applicants, (p) => {
            return p.status === applicantFilter;
        });
        return (
            <div className="clear viewing-feedback">
                <Scrollbars style={{height: 330}}>
                    <div className="scroll-area">
                        {this.renderFilters()}
                        <PanelGroup bsClass="panel-group" activeKey={activeKey}
                                    onSelect={handleSelect} accordion>
                            {filteredApplicants.map((p) => {
                                let index = this.getApplicantIndex(p.contact._id);
                                let name = 'applicants[' + index + '].feedback';
                                let placeHolder = p.contact.firstPerson.forename + "'s feedback here...";
                                return (
                                    <Panel key={p.contact._id}
                                           header={p.contact.firstPerson.forename + ' ' + p.contact.firstPerson.surname}
                                           eventKey={p.contact._id}>
                                        <p className="icons">
                                            <a
                                                onClick={() => setApplicantRating('applicants[' + index + '].rating', 'ThumbUp')}
                                                href="javascript:void(0)" className={classNames({
                                                'like': VIEWING.applicants[index].rating === 'ThumbUp'
                                            })}>
                                                <i className="icon md-thumb-up"></i>
                                            </a>
                                            <a
                                                onClick={() => setApplicantRating('applicants[' + index + '].rating', 'ThumbDown')}
                                                href="javascript:void(0)"
                                                className={classNames({
                                                    'dislike': VIEWING.applicants[index].rating === 'ThumbDown'
                                                })}>
                                                <i className="icon md-thumb-down"></i>
                                            </a>
                                        </p>
                                        <ValidateTextArea placeholder={placeHolder} rows="4" model={VIEWING} name={name}
                                                          onChange={onChange}/>
                                    </Panel>
                                );
                            })}
                        </PanelGroup>
                    </div>
                </Scrollbars>
            </div>
        );
    }

    renderFilters() {
        let {
            changeApplicantFilter,
            applicantFilter
        } = this.props;
        return (
            <p className="filters">
                <span
                    onClick={() => changeApplicantFilter('Active')}
                    className={classNames({
                        'label label-outline label-default': true,
                        'active': applicantFilter === 'Active'
                    })}
                >
                    Active
                </span>
                <span
                    onClick={() => changeApplicantFilter('Cancelled')}
                    className={classNames({
                        'label label-outline label-default': true,
                        'active': applicantFilter === 'Cancelled'
                    })}
                >
                    Cancelled
                </span>
            </p>
        );
    }

    getApplicantIndex(applicantId){
        let { VIEWING, applicantFilter } = this.props;
        return _.findIndex(VIEWING.applicants, (p) => {
            return p.status === applicantFilter && p.contact._id === applicantId;
        });
    }
}

export default Component;

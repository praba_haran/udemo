import React from 'react';
import _ from 'lodash';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import Viewing from '../../../models/Viewing';
import ContactDetail from '../../../models/ContactDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            applicantFilter : 'Active',
            activeKey : ''
        };
    }

    componentDidMount() {
        let {VIEWING} = this.props;
        if (VIEWING.applicants.length > 0) {
            this.setState({
                activeKey: VIEWING.applicants[0].contact._id
            })
        }
    }

    handleSelect(activeKey) {
        this.setState({activeKey});
    }

    setApplicantRating(name,rating){
        Viewing.actions.change(name,rating);
    }

    changeApplicantFilter(filter){
        this.setState({
            applicantFilter : filter
        });
        let {VIEWING} = this.props;
        let filteredApplicants = _.filter(VIEWING.applicants, (p) => {
            return p.status === filter;
        });

        if (filteredApplicants.length > 0) {
            this.setState({
                activeKey: filteredApplicants[0].contact._id
            })
        }
    }

    render(){
        return (
            <Component
                handleSelect={this.handleSelect.bind(this)}
                setApplicantRating ={this.setApplicantRating.bind(this)}
                changeApplicantFilter={this.changeApplicantFilter.bind(this)}

                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import Helper from '../shared/helper';

class Component extends React.Component{

    render () {
        let { CABINET_MASTER_LISTING,onSelectAllChange, deleteSelected,onQueryChange } = this.props;
        let { items, isProcessing, query } = CABINET_MASTER_LISTING;
        let noRecordFound;
        if(items.length===0){
            noRecordFound = (
                <tr className="no-record-found ">
                    <td colSpan="5" className="text-center">{isProcessing?'Loading ...':'No items found !'}</td>
                </tr>
            );
        }

        return (
            <div className="page">
                <div className="page-content">
                    <div className="panel panel-transparent contact-listing-panel">
                        <div className="panel-body container-fluid">
                            <div className="row">
                                <div className="col-lg-6">
                                    <button type="button" className="btn btn-sm btn-default">Add</button>
                                </div>
                                <div className="col-lg-6">
                                    <div className="input-search input-search-dark pull-right">
                                        <i className="input-search-icon md-search" aria-hidden="true"></i>
                                        <input value={query.searchText} onChange={(e)=>onQueryChange('query.searchText',e.target.value)} type="text" className="form-control" placeholder="Search ..." />
                                        <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                            <div className="example">
                                <table className="table table-hover table-striped">
                                    <colgroup>
                                        <col width="5%"/>
                                        <col width="10%"/>
                                        <col width="10%"/>
                                        <col width="30%"/>
                                        <col width="15%"/>
                                        <col width="15%"/>
                                        <col width="15%"/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Cabinet Letter</th>
                                        <th>Capacity</th>
                                        <th>Notes</th>
                                        <th>Updated By</th>
                                        <th>Updated At</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {items.map((p,index)=>{
                                        return (
                                            <tr key={p._id}>
                                                <td>{index+1}</td>
                                                <td>{p.cabinetLetter}</td>
                                                <td>{p.capacity}</td>
                                                <td>{p.notes}</td>
                                                <td>{p.updatedBy.forename}</td>
                                                <td>{Helper.formatDateTime(p.updatedAt)}</td>
                                                <td>
                                                    <span className="label label-primary label-outline">{p.status}</span>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    {noRecordFound}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
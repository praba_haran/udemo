import React from 'react';
import _ from 'lodash';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import Alert from '../../lib/Alert';
import Component from './Component';
import CabinetMasterListing from '../../models/CabinetMasterListing';

class Container extends React.Component {

    componentDidMount () {
        this.getItems();
    }

    getItems(){
        CabinetMasterListing.actions.change('isProcessing',true);
        CabinetMasterListing.api.list((result)=>{
            if(result.success){
                CabinetMasterListing.actions.setObjectInPath(['items'],result.data);
            }
            CabinetMasterListing.actions.change('isProcessing', false);
        });
    }

    onSelectAllChange (e){
        e.stopPropagation();
        let checked = e.target.checked;
        CabinetMasterListing.actions.change('allSelected',checked);
        let CONTACT_LISTING = store.getState().CONTACT_LISTING.toJS();
        _.each(CONTACT_LISTING.contacts,function(p,index){
            CabinetMasterListing.actions.change('contacts['+index+'].extra.selected',checked);
        });
    }
    onQueryChange(key,value){
        CabinetMasterListing.actions.change(key,value);
        let CONTACT_LISTING = store.getState().CABINET_MASTER_LISTING.toJS();
        let { contactsBackup, query } = CONTACT_LISTING;
        let items = [];

        let searchText = query.searchText.toLowerCase();
        items = _.filter(contactsBackup, function (p) {
            return (
                    (query.clientType==='All' || p.type === query.clientType)
                &&  (p.type.toLowerCase().indexOf(searchText) > -1
                    || p.firstPerson.forename.toLowerCase().indexOf(searchText) > -1
                    || p.firstPerson.surname.toLowerCase().indexOf(searchText) > -1
                    || p.address.fullAddress.toLowerCase().indexOf(searchText) > -1
                    || p.negotiator.forename.toLowerCase().indexOf(searchText) > -1
                    || p.negotiator.surname.toLowerCase().indexOf(searchText) > -1
                    || p.status.toLowerCase().indexOf(searchText) > -1
                ));
        });
        CabinetMasterListing.actions.setObjectInPath(['items'], items);
    }
    deleteSelected(contacts,e){
        contacts = _.filter(contacts,function(p){
            return p.extra.selected;
        });
        let ids = _.map(contacts,function(p){
            return p._id;
        });
        CabinetMasterListing.api.deleteItems(ids,(result)=>{
            if (result.success) {
                Alert.clearLogs().success(result.message);
                this.getContacts();
            }
            else {
                Alert.clearLogs().error(result.error);
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                onChange={CabinetMasterListing.actions.valueChange}
                onQueryChange={this.onQueryChange.bind(this)}
                onSelectAllChange= {this.onSelectAllChange.bind(this)}
                deleteSelected ={this.deleteSelected.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        CABINET_MASTER_LISTING: store.CABINET_MASTER_LISTING.toJS(),
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

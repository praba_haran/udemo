import React from 'react';
import _ from 'lodash';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import Alert from '../../lib/Alert';
import Component from './Component';
import PropertyListing from '../../models/PropertyListing';
import BasicGrid from '../shared/grid/BasicGrid';

class Container extends BasicGrid{

    componentDidMount () {
        //this.getProperties();
        this.setUrl('property/list/paginate').load();
        this.setState({
            filter : 'Recent',
            marketFor : 'All'
        });
    }

    getProperties(){
        PropertyListing.actions.change('isProcessing',true);
        PropertyListing.api.search((result)=>{
            if(result.success){
                PropertyListing.actions.setObjectInPath(['properties'],result.data);
            }
            PropertyListing.actions.change('isProcessing', false);
        });
    }

    onFilterChange(filter){
        this.setState({
            filter : filter
        });
        if(filter==='Recent'){
            this.removeParam('filter').setPageIndex(1).load();
        }
        else{
            this.setParam('filter',filter).setPageIndex(1).load();
        }
    }

    onMarketChange(marketFor){
        this.setState({
            marketFor : marketFor
        });
        if(marketFor==='All'){
            this.removeParam('marketFor').setPageIndex(1).load();
        }
        else{
            this.setParam('marketFor',marketFor).setPageIndex(1).load();
        }
    }

    remove(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to delete this property?', ()=>{
            PropertyListing.api.deleteItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }
    archive(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to archive this property?', ()=>{
            PropertyListing.api.archiveItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }
    activate(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to activate this property?', ()=>{
            PropertyListing.api.activateItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onChange={PropertyListing.actions.valueChange}
                renderNoRecord={this.renderNoRecord.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                onSearchTextChange={this.onSearchTextChange.bind(this)}
                archive={this.archive.bind(this)}
                activate={this.activate.bind(this)}
                remove={this.remove.bind(this)}
                onFilterChange={this.onFilterChange.bind(this)}
                onMarketChange={this.onMarketChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        CONTACT_LISTING: store.CONTACT_LISTING.toJS(),
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

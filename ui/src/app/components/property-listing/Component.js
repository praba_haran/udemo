import React from 'react';
import classNames from 'classnames';
import Helper from '../shared/helper';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import {Link} from "react-router";

class Component extends React.Component{

    render () {
        let { filter,marketFor,onMarketChange,onFilterChange, items, renderNoRecord,
            renderFooter,searchText,onSearchTextChange, archive, activate, remove } = this.props;

        let statusFilters = [
            'Recent',
            'Appraised',
            'Instructed',
            'Available',
            'Externally Sold',
            'Withdrawn',
            'Suspended',
            'Archived'
        ];

        return (
            <div className="page">
                <div className="page-content">
                    <div className="panel panel-transparent property-listing-panel">
                        <div className="panel-body container-fluid">

                            <div className="basic-grid">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="btn-group margin-right-5">
                                            <button onClick={()=>onMarketChange('All')} type="button" className={classNames({
                                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                                'active': marketFor === 'All'
                                            })}>
                                                All
                                            </button>
                                            <button onClick={()=>onMarketChange('For Sale')} type="button" className={classNames({
                                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                                'active': marketFor==='For Sale'
                                            })}>
                                                Sale
                                            </button>
                                            <button onClick={()=>onMarketChange('To Let')} type="button" className={classNames({
                                                'btn btn-sm btn-default btn-icon waves-effect waves-light':true,
                                                'active': marketFor==='To Let'
                                            })}>
                                                Rent
                                            </button>
                                        </div>
                                        <div className="btn-group">
                                            <DropdownButton bsSize="small" id="dropDown" title={filter}>
                                                {statusFilters.map(function(p){
                                                    return (
                                                        <MenuItem
                                                            key={p}
                                                            eventKey ={p}
                                                            onSelect={onFilterChange}
                                                        >{p}</MenuItem>
                                                    );
                                                })}
                                            </DropdownButton>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="input-search input-search-dark pull-right">
                                            <i className="input-search-icon md-search" aria-hidden="true"></i>
                                            <input value={searchText} onChange={onSearchTextChange} type="text" className="form-control" placeholder="Search ..." />
                                            <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                                <table className="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Price</th>
                                        <th>Address</th>
                                        <th>Category</th>
                                        <th>Market</th>
                                        <th>Negotiator</th>
                                        <th className="hidden-xs">
                                            Status
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {items.map((p,index)=>{

                                        return (
                                            <tr key={p._id}>
                                                <td className="image">
                                                    <a className="avatar" href="javascript:void(0)">
                                                        {p.photos.length>0?(
                                                                <img className="img-responsive" src={p.photos[0].url.replace('upload','upload/w_40,h_30,c_pad,b_black')} alt={p.photos[0].title} />
                                                            ):(
                                                                <img className="img-responsive" src="/dist/images/photos/no-image.png" alt="No Image" />
                                                            )}
                                                    </a>
                                                </td>
                                                <td>{Helper.toCurrency(Helper.property.getPrice(p))}
                                                    <small className="margin-left-5">{p.priceQualifier}</small>
                                                </td>
                                                <td><Link className="address-link" to={'properties/'+p._id+'/overview'}>{p.address.fullAddress}</Link></td>
                                                <td>{p.category}</td>
                                                <td>{p.market}</td>
                                                <td>{p.negotiator.forename}</td>
                                                <td className="hidden-xs">
                                                    <span className="label label-primary label-outline">{Helper.property.getStatus(p)}</span>
                                                </td>
                                                <td>
                                                    <div className="btn-group" role="group">
                                                        <button type="button" className="btn btn-pure btn-xs waves-effect waves-light" id="exampleIconDropdown4" data-toggle="dropdown" aria-expanded="false">
                                                            <i className="icon md-more-vert" aria-hidden="true"></i>
                                                        </button>
                                                        <ul className="dropdown-menu" aria-labelledby="exampleIconDropdown4" role="menu">
                                                            <li role="presentation">
                                                                <a onClick={()=>remove(p._id)} href="javascript:void(0)" role="menuitem">
                                                                    <i className="icon md-delete"></i> Delete
                                                                </a>
                                                            </li>
                                                            {p.recordStatus!=='Archived'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>archive(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-archive"></i> Archive
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                            {p.recordStatus!=='Active'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>activate(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-check"></i> Activate
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    {renderNoRecord({
                                        colSpan:8,
                                        noRecordText : 'No properties found.'
                                    })}
                                    </tbody>
                                </table>
                                {renderFooter()}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
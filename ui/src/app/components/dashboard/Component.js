import React from 'react';
import _ from 'lodash';
import Chart from 'chart.js';
Chart.defaults.global.maintainAspectRatio = true;
import Details from './details/Container';
import BranchPerformance from './branchPerformance/Container';
import BranchActivity from './branchActivity/Container';
import UserActivity from './userActivity/Container';
import Summary from './summary/Container';

class Component extends React.Component{

    render () {

        return (
            <div className="page dashboard-page">
                <div className="page-content">
                    <div className="panel panel-transparent">
                        <div className="panel-body container-fluid">

                            <div className="row">
                                <div className="col-md-7">
                                    <BranchPerformance />
                                </div>
                                <div className="col-md-5">
                                    <Summary />
                                </div>
                            </div>

                            <div className="row margin-top-15">
                                <div className="col-md-7">
                                    <BranchActivity />
                                </div>
                                <div className="col-md-5">

                                </div>
                            </div>

                            <div className="row margin-top-15">
                                <div className="col-md-7">
                                    <UserActivity />
                                </div>
                                <div className="col-md-5">

                                </div>
                            </div>

                            <div className="row margin-top-15">
                                <div className="col-sm-7">
                                    <Details />
                                </div>
                                <div className="col-sm-5">
                                    {/*<Summary />*/}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Component;
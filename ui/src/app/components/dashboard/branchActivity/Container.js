import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../../models/Dashboard';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            branchActivityType : 'property'
        };
    }

    componentDidMount () {
        this.getActivities(this.state.branchActivityType);
    }

    onBranchActivityTypeChange(type){
        this.getActivities(type);
        this.setState({
            branchActivityType : type
        });
    }

    getActivities(type){
        Dashboard.api.getBranchActivity(type, (result)=>{
            if(result.success){
                Dashboard.actions.setObjectInPath(['branchActivity'], result.data);
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onBranchActivityTypeChange={this.onBranchActivityTypeChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        DASHBOARD : store.DASHBOARD.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../../models/Dashboard';

class Container extends React.Component {

    componentDidMount () {
        Dashboard.api.getPropertySummary((result)=>{
            if(result.success){
                Dashboard.actions.setObjectInPath(['summary','items'], result.data);
            }
        });
    }

    onSummaryTypeChange(type){
        let methodName = (type==='property')?'getPropertySummary':'getContactSummary';

        Dashboard.api[methodName]((result)=>{
            if(result.success){
                Dashboard.actions.setObjectInPath(['summary'], {
                    items : result.data,
                    type : type
                });
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onSummaryTypeChange={this.onSummaryTypeChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        DASHBOARD : store.DASHBOARD.toJS()
    };
};

export default connect(mapStateToProps)(Container);

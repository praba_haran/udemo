import React from 'react';
import _ from 'lodash';
import Chart from 'chart.js';
import classNames from 'classnames';

class Component extends React.Component {

    componentDidMount() {
        //console.log('did mount');
        //this.drawCharts();
    }

    componentDidUpdate() {
        let { DASHBOARD  } = this.props;
        let { summary } = DASHBOARD;
        this.drawCharts();
    }

    drawCharts() {
        let { DASHBOARD  } = this.props;
        let { summary } = DASHBOARD;

        let ctx = document
            .getElementById("property-summary-chart")
            .getContext("2d");

        let randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        let chartColors = [
            { name : 'red', value : 'rgb(255, 99, 132)' },
            { name : 'green', value : 'rgb(75, 192, 192)' },
            { name : 'orange', value : 'rgb(255, 159, 64)' },
            { name : 'blue', value : 'rgb(54, 162, 235)' },
            { name : 'yellow', value : 'rgb(255, 205, 86)' },
            { name : 'purple', value : 'rgb(153, 102, 255)' },
            { name : 'grey', value : 'rgb(201, 203, 207)' }
        ];

        let data = [];
        let backGroundColors = [];
        let labels = [];
        _.each(summary.items,(item,index)=>{
            data.push(item.count);
            backGroundColors.push(chartColors[index].value);
            labels.push(item._id);
        });

        let config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: data,
                    backgroundColor: backGroundColors,
                    label: 'Dataset 1'
                }],
                labels: labels
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                    labels:{
                        usePointStyle : true,
                        fontFamily:'Roboto,sans-serif'
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };
        new Chart(ctx, config);
    }

    render() {
        let { DASHBOARD, onSummaryTypeChange } = this.props;
        let { summary } = DASHBOARD;

        return (
            <div className="chart-section">
                <div className="header">
                    <div className="row">
                        <div className="col-sm-6">
                            <h5 className="title">Summary</h5>
                        </div>
                        <div className="col-sm-6">
                            <div className="btn-group pull-right">
                                <button onClick={()=>onSummaryTypeChange('property')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : summary.type==='property'
                                })}>
                                    Property
                                </button>
                                <button onClick={()=>onSummaryTypeChange('contact')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : summary.type==='contact'
                                })}>
                                    Contact
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="chart-container" style={{
                    'width': '260px'
                }}>
                    <canvas id="property-summary-chart"></canvas>
                </div>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import Viewings from './Viewings';
import Appointments from './Appointments';
import * as constants from '../../../constants';
import ViewingModal from '../../viewing/Container';
import ViewingEmailConfirmation from '../../viewing-email-confirmation/Container';
import GeneralAppointmentModal from '../../general-appointment/Container';

class Component extends React.Component{

    render () {
        let { COMMANDS, handleTabChange, activeTab } = this.props;

        return (
            <div className="nav-tabs-horizontal">
                <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                    <li role="presentation" className={(activeTab==='appointments')?'active':''}>
                        <a onClick={()=> handleTabChange('appointments')} role="button" href="javascript:void(0)">Appointments</a>
                    </li>
                    <li role="presentation" className={(activeTab==='viewings')?'active':''}>
                        <a onClick={()=> handleTabChange('viewings')} role="button" href="javascript:void(0)">Viewings</a>
                    </li>
                    <li role="presentation" className={(activeTab==='matches')?'active':''}>
                        <a onClick={()=> handleTabChange('matches')} role="button" href="javascript:void(0)">Matches</a>
                    </li>
                    <li role="presentation" className={(activeTab==='offers')?'active':''}>
                        <a onClick={()=> handleTabChange('offers')} role="button" href="javascript:void(0)">Offers</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <Scrollbars
                        style={{height:420}}
                    >
                        <div className="scrolling-area">
                            <div className="tab-pane active">
                                {activeTab==='appointments'?(<Appointments {...this.props} />):null}
                                {activeTab==='viewings'?(<Viewings {...this.props} />):null}
                                {activeTab==='matches'?('Matches'):null}
                                {activeTab==='offers'?('Offers'):null}
                            </div>
                        </div>
                    </Scrollbars>
                </div>
                <ViewingModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING]} location={this.props.location} params={this.props.params} />
                <ViewingEmailConfirmation canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM]} location={this.props.location} params={this.props.params} />
                <GeneralAppointmentModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_GENERAL_APPOINTMENT]} location={this.props.location} params={this.props.params} />
            </div>
        );
    }
}

export default Component;
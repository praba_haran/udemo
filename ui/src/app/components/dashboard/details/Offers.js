import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import Helper from '../../shared/helper';

class Offers extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: ''
        };
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    getApplicants(offer){
        let isLast = function(index){
            return (offer.applicants.length-1) === index;
        };
        return (
            <span className="applicants">
                {offer.applicants.map((item,index)=>{
                    return (
                        <span key={item._id}>
                            {(!isLast(index) && index!==0)?' , ':''}
                            {(isLast(index) && index!==0)?' and ':''}
                            <a className="applicant">{item.contact.firstPerson.forename+' '+item.contact.firstPerson.surname}</a>
                        </span>
                    );
                })}
            </span>
        );
    }

    getOwners(){
        let { PROPERTY_DETAIL } = this.props;
        let isLast = function(index){
            return (PROPERTY_DETAIL.owners.length-1) === index;
        };
        return (
            <span className="owners">
                {PROPERTY_DETAIL.owners.map((item,index)=>{
                    return (
                        <span key={item._id}>
                            {(!isLast(index) && index!==0)?' , ':''}
                            {(isLast(index) && index!==0)?' and ':''}
                            <a className="owner">{item.contact.firstPerson.forename+' '+item.contact.firstPerson.surname}</a>
                        </span>
                    );
                })}
            </span>
        );
    }

    getPercentageDifference(value1,value2){
        let percentage = (value1/value2)*100;
        let difference = Math.round((100-percentage) * 100) / 100;
        return {
            isLower : difference<0,
            value : Math.abs(difference)
        };
    }

    canCreateOffer(){
        let { PROPERTY_DETAIL } = this.props;
        return PROPERTY_DETAIL.recordStatus === 'Active';
    }

    render(){
        let { PROPERTY_DETAIL, OFFERS,editOffer,isOffersReloading } = this.props;
        return (
            <div className="clear">
                <ul className="list-group list-group-gap offer-list-group">
                    {OFFERS.map((p,index)=>{
                        let difference = this.getPercentageDifference(PROPERTY_DETAIL.proposedPrice,p.price);
                        return (
                            <li key={p._id} className="list-group-item">
                                <div className="offer">
                                    <p onClick={()=>editOffer(p)} className="title">
                                        Offer of {Helper.toCurrency(p.price)} made by {this.getApplicants(p)}
                                        <i className="icon fa-long-arrow-right"></i>
                                        {this.getOwners()}
                                    </p>
                                    {difference.value>0?(
                                            <p className="description">
                                                {difference.isLower?(<i className="icon fa-long-arrow-down"></i>):(<i className="icon fa-long-arrow-up"></i>)}
                                                <span className="highlight">( {difference.value}% ) </span>
                                                {difference.isLower?'Lower':'Higher'} than the Asking Price of
                                                <span className="highlight"> {Helper.toCurrency(PROPERTY_DETAIL.contract.price)}</span>
                                                {this._renderStatus(p)}
                                            </p>
                                        ):(
                                            <p className="description">
                                                <i className="icon fa-arrows-h"></i>
                                                Equal to Asking Price of
                                                <span className="highlight"> {Helper.toCurrency(PROPERTY_DETAIL.contract.price)}</span>
                                                {this._renderStatus(p)}
                                            </p>
                                        )}
                                    <div className="footer">
                                        <p className="appraiser-date">
                                            <span className="time-ago">
                                                <i className="icon fa-clock-o"></i>
                                                {moment(p.startDate).fromNow()}
                                            </span>
                                            <span className="details">
                                                Updated By : {p.updatedBy.forename+' '+p.updatedBy.surname}
                                                <span className="date">Date : {moment(p.updatedAt).format('DD MMM YYYY h:mm a')}</span>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        );
                    })}
                    {isOffersReloading?(
                            <li className="list-group-item no-item-found">
                                <p className="title">Loading offers ...</p>
                            </li>
                        ):null}
                    {(OFFERS.length===0 && !isOffersReloading)?(
                            <li className="list-group-item no-item-found">
                                <p className="title">
                                    No offers found.
                                    {this.canCreateOffer()?(<a onClick={this.props.addOffer} href="javascript:void(0)" role="button">Create New</a>):null}
                                </p>
                            </li>
                        ):null}
                </ul>
            </div>
        );
    }

    _renderStatus(offer){
        let status = null;
        if(offer.status==='Received'){
            status = (<span className="label label-primary label-outline pull-right">Received</span>);
        }
        else if(offer.status==='Accepted'){
            status = (<span className="label label-success label-outline pull-right">Accepted</span>);
        }
        else if(offer.status==='Rejected'){
            status = (<span className="label label-danger label-outline pull-right">Rejected</span>);
        }
        return status;
    }
}

export default Offers;
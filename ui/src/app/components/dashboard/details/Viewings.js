import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import Helper from '../../shared/helper';
import classNames from 'classnames';

class Viewings extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: ''
        };
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    canCreateViewing(){
        let { PROPERTY_DETAIL } = this.props;
        return PROPERTY_DETAIL.recordStatus === 'Active';
    }

    render(){
        let { VIEWINGS,addViewing,editViewing,isViewingReloading,viewingFilter,changeViewingFilter } = this.props;
        let items = _.filter(VIEWINGS,(p)=>{
            let hourMinutes = Helper.getHourMinutesFromTime(p.endTime);
            let date  = moment(p.date).set({
                hour : hourMinutes.hour,
                minute: hourMinutes.minutes,
                second : 0,
                millisecond : 0
            });
            if(viewingFilter==='Upcoming'){
                return (p.status==='Booked' || p.status==='Rescheduled') && date.toDate() > new Date();
            }
            else if(viewingFilter==='Started'){
                return p.status==='Started';
            }
            else if(viewingFilter==='Finished'){
                return p.status==='Finished';
            }
            else if(viewingFilter === 'Expired'){
                return (p.status==='Booked' || p.status==='Rescheduled') && date.toDate() < new Date();
            }
            return  p.status === 'Cancelled';
        });

        return (
            <div className="clear viewing-list-container">
                {VIEWINGS.length>0?(
                        <div className="row">
                            <div className="col-sm-3">
                                <p className="actions">
                                    <span
                                        onClick={addViewing}
                                        className="label label-outline label-default">
                                        <i className="icon md-plus"></i>
                                        Add Viewing
                                    </span>
                                </p>
                            </div>
                            <div className="col-sm-9">
                                <p className="filters">
                                    <span
                                        onClick={()=>changeViewingFilter('Upcoming')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': viewingFilter==='Upcoming'
                                        })}
                                    >
                                        Upcoming
                                    </span>
                                    <span
                                        onClick={()=>changeViewingFilter('Started')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': viewingFilter==='Started'
                                        })}
                                    >
                                        Started
                                    </span>
                                    <span
                                        onClick={()=>changeViewingFilter('Finished')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': viewingFilter==='Finished'
                                        })}
                                    >
                                        Finished
                                    </span>
                                    <span
                                        onClick={()=>changeViewingFilter('Cancelled')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': viewingFilter==='Cancelled'
                                        })}
                                    >
                                        Cancelled
                                    </span>
                                    <span
                                        onClick={()=>changeViewingFilter('Expired')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': viewingFilter==='Expired'
                                        })}
                                    >
                                        Expired
                                    </span>
                                </p>
                            </div>
                        </div>
                    ):null}
                <ul className="list-group list-group-gap viewing-list-group">
                    {items.map((p,index)=>{
                        return (
                            <li key={p._id} className="list-group-item">
                                <div className="viewing">
                                    <a onClick={()=>editViewing(p)} href="javascript:void(0)" role="button" className="title">
                                        Viewing booked at 
                                        <span className="date"> {moment(p.date).format('DD MMM YYYY')} </span>
                                        between <span className="time">{Helper.toTime(p.startTime)} </span>
                                        to <span className="time">{Helper.toTime(p.endTime)}</span>
                                    </a>
                                    <div className="applicants">
                                        {this._renderStatus(p)}
                                        <div className="applicant">
                                            <p className="name">{p.applicants[0].contact.firstPerson.forename+' '+p.applicants[0].contact.firstPerson.surname}</p>
                                            <p className="address">{p.applicants[0].contact.address.fullAddress}</p>
                                        </div>
                                        {p.applicants.length>=2?(
                                            <a onClick={()=>editViewing(p)} href="javascript:void(0)" className="more-link">
                                                <i className="icon fa-angle-down"></i>
                                                {p.applicants.length>2?(
                                                        <span>{p.applicants.length-1} more applicants</span>
                                                    ):'1 more applicant'}
                                            </a>
                                        ):null}
                                    </div>
                                    <div className="footer">
                                        <div className="appraiser-date">
                                            <span className="time-ago">
                                                <i className="icon fa-clock-o"></i>
                                                {moment(p.date).fromNow()}
                                            </span>
                                            <span className="details">
                                                Appraiser : {this.getAppraiser(p.appraiser)}
                                                <span className="date">Date : {moment(p.updatedAt).format('DD MMM YYYY h:mm a')}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        );
                    })}
                    {isViewingReloading?(
                            <li className="list-group-item no-item-found">
                                <p className="title">Loading viewings ...</p>
                            </li>
                        ):null}
                    {(VIEWINGS.length===0 && !isViewingReloading)?(
                            <li className="list-group-item no-item-found">
                                <p className="title">
                                    No viewings found.
                                    {this.canCreateViewing()?(<a onClick={addViewing} href="javascript:void(0)" role="button">Create New</a>):null}
                                </p>
                            </li>
                        ):null}
                </ul>
            </div>
        );
    }

    _renderStatus(viewing){
        let status = null;
        if(viewing.status==='Started'){
            status = (<span className="label label-success label-outline pull-right">Started</span>);
        }
        else if(viewing.status==='Finished'){
            status = (<span className="label label-success label-outline pull-right">Finished</span>);
        }
        else if(viewing.status==='Booked'){
            status = (<span className="label label-primary label-outline pull-right">Booked</span>);

            if(viewing.isConfirmed){
                status = (<span className="label label-success label-outline pull-right">Confirmed</span>);
                if(viewing.confirmationEmailSent) {
                    status = (<span className="label label-success label-outline pull-right">Confirmed / Notified</span>);
                }
            }
        }
        else if(viewing.status==='Rescheduled'){
            status = (<span className="label label-success label-outline pull-right">Rescheduled</span>);
            if(viewing.reScheduledEmailSent) {
                status = (<span className="label label-success label-outline pull-right">Rescheduled / Notified</span>);
            }
        }
        else if(viewing.status==='Cancelled'){
            status = (<span className="label label-danger label-outline pull-right">Cancelled</span>);
            if(viewing.cancellationEmailSent) {
                status = (<span className="label label-danger label-outline pull-right">Cancelled / Notified</span>);
            }
        }
        return status;
    }

    getAppraiser(id){
        let {VIEWING, onChange, APP} = this.props;
        let {agencyDetails} = APP;
        let appraiserName = '';
        if (agencyDetails && agencyDetails.agencyUsers) {
            let appraiser = _.find(agencyDetails.agencyUsers,(p)=>{
                return p._id === id;
            });
            if(appraiser!==undefined){
                appraiserName = appraiser.forename;
            }
        }
        return appraiserName;
    }
}

export default Viewings;
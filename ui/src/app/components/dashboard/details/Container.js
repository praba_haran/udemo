import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../../models/Dashboard';
import Viewing from '../../../models/Viewing';
import Offer from '../../../models/Offer';
import Activity from '../../../models/Activity';
import GeneralAppointment from '../../../models/GeneralAppointment';
import Commands from '../../../models/Commands';
import * as constants from '../../../constants';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            VIEWINGS : [],
            isViewingReloading : false,
            viewingFilter : 'Upcoming',

            APPOINTMENTS : [],
            isAppointmentsReloading : false,
            appointmentFilter : 'Upcoming',

            OFFERS : [],
            isOffersReloading :false,

            activeTab:'appointments'
        };
    }

    componentDidMount(){
        // load
        this.loadViewings(this.props);
        this.loadAppointments(this.props);
        //this.loadOffers(this.props);
    }

    componentWillReceiveProps(newProps){
        let { COMMANDS } = newProps;
        if(COMMANDS.BOOK_VIEWING_CHANGED !==this.props.COMMANDS.BOOK_VIEWING_CHANGED && !this.state.isViewingReloading){
            Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, !COMMANDS.BOOK_VIEWING_CHANGED);
            this.loadViewings(newProps);
        }
        else if(COMMANDS.GENERAL_APPOINTMENT_CHANGED !==this.props.COMMANDS.GENERAL_APPOINTMENT_CHANGED && !this.state.isAppointmentsReloading){
            Commands.actions.change(constants.COMMANDS.GENERAL_APPOINTMENT_CHANGED, !COMMANDS.GENERAL_APPOINTMENT_CHANGED);
            this.loadAppointments(newProps);
        }
        // else if(COMMANDS.MAKE_AN_OFFER_CHANGED !==this.props.COMMANDS.MAKE_AN_OFFER_CHANGED && !this.state.isOffersReloading){
        //     Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER_CHANGED, !COMMANDS.MAKE_AN_OFFER_CHANGED);
        //     this.loadOffers(newProps);
        // }
    }

    // viewings
    loadViewings(props){
        this.setState({
            isViewingReloading : true
        });
        Dashboard.api.getViewings((result)=>{
            if(result.success){
                this.setState({
                    VIEWINGS : result.data,
                    isViewingReloading : false
                });
            }
        });
    }
    editViewing(viewing){
        Viewing.actions.setModel(viewing);
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    addViewing(){
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    changeViewingFilter(filter){
        this.setState({
            viewingFilter : filter
        });
    }

    // appointments
    loadAppointments(props){
        this.setState({
            isAppointmentsReloading : true
        });
        Dashboard.api.getAppointments((result)=>{
            if(result.success){
                this.setState({
                    APPOINTMENTS : result.data,
                    isAppointmentsReloading : false
                });
            }
        });
    }
    editAppointment(appointment){
        GeneralAppointment.actions.setModel(appointment);
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    addAppointment(){
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    changeAppointmentFilter(filter){
        this.setState({
            appointmentFilter : filter
        });
    }

    // offers
    loadOffers(props){
        this.setState({
            isOffersReloading : true
        });
        let params = {
            property : props.params.id
        };
        Offer.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    OFFERS : result.data,
                    isOffersReloading : false
                });
            }
        });
    }
    editOffer(offer){
        Offer.actions.setModel(offer);
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }
    addOffer(){
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }

    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                handleTabChange ={this.handleTabChange.bind(this)}
                editViewing={this.editViewing.bind(this)}
                addViewing={this.addViewing.bind(this)}
                changeViewingFilter={this.changeViewingFilter.bind(this)}
                addAppointment={this.addAppointment.bind(this)}
                editAppointment={this.editAppointment.bind(this)}
                changeAppointmentFilter={this.changeAppointmentFilter.bind(this)}
                addOffer={this.addOffer.bind(this)}
                editOffer={this.editOffer.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        DASHBOARD : store.DASHBOARD.toJS()
    };
};

export default connect(mapStateToProps)(Container);

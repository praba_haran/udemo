import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../models/Dashboard';

class Container extends React.Component {

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        COMMANDS : store.COMMANDS.toJS()
    };
};

//export default connect(mapStateToProps)(Container);
export default Container;

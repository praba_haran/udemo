import React from 'react';
import _ from 'lodash';
import Chart from 'chart.js';
import classNames from 'classnames';

class Component extends React.Component {

    componentDidMount() {
        this.drawCharts();
    }

    componentDidUpdate() {
        this.drawCharts();
    }

    drawCharts() {
        let { DASHBOARD } = this.props;
        let { userActivity } = DASHBOARD;

        let ctx = document
            .getElementById("user-activity-chart")
            .getContext("2d");

        let chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        let labels = [];
        let data = [];
        _.each(userActivity,(item)=>{
            labels.push(item._id.activityType);
            data.push(item.count);
        });

        new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: labels,
                datasets: [{
                    label: "Property",
                    backgroundColor: chartColors.red,
                    borderColor: chartColors.red,
                    data: data
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                    labels:{
                        usePointStyle : true,
                        fontFamily:'Roboto,sans-serif'
                    }
                },
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 0,
                            beginAtZero: true,
                            callback: (value, index, values)=> {
                                if (Math.floor(value) === value) {
                                    return value;
                                }
                            }
                        }
                    }]
                }
            }
        });
    }

    render() {
        let { onUserActivityTypeChange, userActivityType } = this.props;
        return (
            <div className="chart-section">
                <div className="header">
                    <div className="row">
                        <div className="col-sm-4">
                            <h5 className="title">My Activity</h5>
                        </div>
                        <div className="col-sm-8">
                            <div className="btn-group pull-right">
                                <button onClick={()=>onUserActivityTypeChange('property')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : userActivityType==='property'
                                })}>
                                    Property
                                </button>
                                <button onClick={()=>onUserActivityTypeChange('contact')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : userActivityType==='contact'
                                })}>
                                    Contact
                                </button>
                                <button onClick={()=>onUserActivityTypeChange('viewing')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : userActivityType==='viewing'
                                })}>
                                    Viewing
                                </button>
                                <button onClick={()=>onUserActivityTypeChange('appointment')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : userActivityType==='appointment'
                                })}>
                                    Appointment
                                </button>
                                <button onClick={()=>onUserActivityTypeChange('offers')} type="button" className={classNames({
                                    'btn btn-xs':true,
                                    'active' : userActivityType==='offers'
                                })}>
                                    Offers
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="chart-container" style={{
                    'width': '500px',
                    'height': '260px'
                }}>
                    <canvas id="user-activity-chart"></canvas>
                </div>
            </div>
        );
    }

}

export default Component;
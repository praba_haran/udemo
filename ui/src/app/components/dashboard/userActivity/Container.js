import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../../models/Dashboard';

class Container extends React.Component {

    constructor(){
        super();
        this.state = {
            userActivityType : 'property'
        };
    }

    componentDidMount () {
        this.getActivities(this.state.userActivityType);
    }

    onUserActivityTypeChange(type){
        this.getActivities(type);
        this.setState({
            userActivityType : type
        });
    }

    getActivities(type){
        Dashboard.api.getUserActivity(type, (result)=>{
            if(result.success){
                Dashboard.actions.setObjectInPath(['userActivity'], result.data);
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onUserActivityTypeChange={this.onUserActivityTypeChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        DASHBOARD : store.DASHBOARD.toJS()
    };
};

export default connect(mapStateToProps)(Container);

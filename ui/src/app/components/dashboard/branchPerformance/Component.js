import React from 'react';
import _ from 'lodash';
import Chart from 'chart.js';
import classNames from 'classnames';

class Component extends React.Component {

    componentDidMount() {
        this.drawCharts();
    }

    componentDidUpdate() {
        this.drawCharts();
    }

    getPropertyData(){
        let { DASHBOARD } = this.props;
        let { branchPerformance } = DASHBOARD;
        let { properties } = branchPerformance;

        let data = [];
        for(let i=1;i<=12;i++){
            let item = _.find(properties,(p)=>{
                return p._id.month === i;
            });
            if(item!==undefined){
                data.push(item.count);
            }
        }
        return data;
    }

    getContactData(){
        let { DASHBOARD } = this.props;
        let { branchPerformance } = DASHBOARD;
        let { contacts } = branchPerformance;

        let data = [];
        for(let i=1;i<=12;i++){
            let item = _.find(contacts,(p)=>{
                return p._id.month === i;
            });
            if(item!==undefined){
                data.push(item.count);
            }
        }
        return data;
    }

    drawCharts() {
        let ctx = document
            .getElementById("branch-performance-chart")
            .getContext("2d");

        let chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul","Aug","Sep","Oct","Nov","Dec"
                ],
                datasets: [{
                    label: "Property",
                    backgroundColor: chartColors.red,
                    borderColor: chartColors.red,
                    data: this.getPropertyData(),
                    fill: false,
                }, {
                    label: "Contact",
                    fill: false,
                    backgroundColor: chartColors.blue,
                    borderColor: chartColors.blue,
                    data: this.getContactData(),
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                    labels:{
                        usePointStyle : true,
                        fontFamily:'Roboto,sans-serif'
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: true
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: {
                            min: 0,
                            beginAtZero: true,
                            callback: (value, index, values)=> {
                                if (Math.floor(value) === value) {
                                    return value;
                                }
                            }
                        }
                    }]
                }
            }
        });
    }

    render() {
        return (
            <div className="chart-section">
                <div className="header">
                    <div className="row">
                        <div className="col-sm-6">
                            <h5 className="title">Branch Performance</h5>
                        </div>
                    </div>
                </div>
                <div className="chart-container" style={{
                    'width': '500px',
                    'height': '260px'
                }}>
                    <canvas id="branch-performance-chart"></canvas>
                </div>
            </div>
        );
    }
}

export default Component;
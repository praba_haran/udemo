import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import Dashboard from '../../../models/Dashboard';

class Container extends React.Component {

    componentDidMount () {
        Dashboard.api.getBranchPerformance((result)=>{
            if(result.success){
                let { data } = result;
                Dashboard.actions.setObjectInPath(['branchPerformance','properties'], data.properties);
                Dashboard.actions.setObjectInPath(['branchPerformance','contacts'], data.contacts);
            }
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        DASHBOARD : store.DASHBOARD.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import classNames from 'classnames';
import { Select, ValidateTextArea,ValidateText, ValidateSelect,CheckBox } from '../shared/Form';

class Component extends React.Component{

    constructor(){
        super();
        this.state = {
            statusList :{
                'Instructed':['Available','Suspended','Withdrawn'],
                'Externally Sold':['Archived','Available'],
                'Available':['Externally Sold','Suspended','Withdrawn'],
                'Suspended':['Available','Withdrawn'],
                'Withdrawn':['Archived','Available']
            }
        };
    }

    render(){
        let { PROPERTY_STATUS_CHANGE, onChange, PROPERTY_DETAIL,boardContractors ,cancel,submit,canShowModal,isProcessing } = this.props;
        let statusList = this.state.statusList[PROPERTY_STATUS_CHANGE.oldStatus] || [];
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName={classNames({
                'custom-modal':true,
                'modal-lg' : PROPERTY_STATUS_CHANGE.actions.requireBoardChange,
                'modal-md' : !PROPERTY_STATUS_CHANGE.actions.requireBoardChange
            })}>
                <Modal.Header closeButton>
                    <Modal.Title>CHANGE STATUS</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className={classNames({
                                'col-sm-6': PROPERTY_STATUS_CHANGE.actions.requireBoardChange,
                                'col-sm-12' : !PROPERTY_STATUS_CHANGE.actions.requireBoardChange
                            })}>
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-6">PROPERTY STATUS</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Current Status</label>
                                    <ValidateText className="col-sm-6" model={PROPERTY_STATUS_CHANGE} name="oldStatus" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">New Status</label>
                                    <div className="col-sm-6">
                                        <ValidateSelect model={PROPERTY_STATUS_CHANGE} name="status" onChange={onChange}>
                                            {statusList.map(function (p) {
                                                return (
                                                    <option key={p} value={p}>{p}</option>
                                                );
                                            })}
                                        </ValidateSelect>
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Notes</label>
                                    <ValidateTextArea rows="6" className="col-sm-6" model={PROPERTY_STATUS_CHANGE} name="notes" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-4 col-sm-6">
                                        <CheckBox model={PROPERTY_STATUS_CHANGE} name="actions.requireBoardChange" onChange={onChange} label="Require Board Change" />
                                    </div>
                                </div>
                            </div>
                            {PROPERTY_STATUS_CHANGE.actions.requireBoardChange?(
                                    <div className="col-md-6">
                                        <div className="form-group margin-bottom-0">
                                            <h6 className="col-sm-offset-4 col-sm-6">MANAGE BOARD</h6>
                                        </div>
                                        <div className="form-group margin-bottom-10">
                                            <label className="col-sm-4 control-label">Current Board</label>
                                            <ValidateText className="col-sm-6" model={PROPERTY_STATUS_CHANGE} name="boardChangeRequest.currentBoard" onChange={onChange} />
                                        </div>
                                        <div className="form-group margin-bottom-10">
                                            <label className="col-sm-4 control-label">Board Change</label>
                                            <ValidateSelect className="col-sm-6" model={PROPERTY_STATUS_CHANGE} name="boardChangeRequest.newBoard" onChange={onChange} dropKey="boardTypes" />
                                        </div>
                                        <div className="form-group margin-bottom-10">
                                            <label className="col-sm-4 control-label">Board Contractor</label>
                                            <div className="col-sm-6">
                                                <ValidateSelect model={PROPERTY_STATUS_CHANGE} name="boardChangeRequest.boardContractor" onChange={onChange}>
                                                    {boardContractors.map((p)=>{
                                                        return (<option key={p._id} value={p._id}>{p.firstPerson.forename}</option>);
                                                    })}
                                                </ValidateSelect>
                                            </div>
                                        </div>
                                        <div className="form-group margin-bottom-10">
                                            <label className="col-sm-4 control-label">Message</label>
                                            <ValidateTextArea rows="6" className="col-sm-6" model={PROPERTY_STATUS_CHANGE} name="boardChangeRequest.message" onChange={onChange} />
                                        </div>
                                    </div>
                                ):null}
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
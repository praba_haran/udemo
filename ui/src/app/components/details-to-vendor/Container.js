import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import DetailsToVendor from '../../models/DetailsToVendor';
import ContactDetail from '../../models/ContactDetail';
import Component from './Component';


const PROPERTY_MIS_DESCRIPTION_ACT = `PROPERTY MISDESCRIPTIONS ACT 1991

VALIDATION CERTIFICATE

The Property Misdescriptions Act 1991 requires estate agents to ensure that sales particulars are true and accurate. If the attached draft sales particulars are inaccurate in any way please return them with any necessary amendments. If any of the information in the sales particulars change in the future it is important you notify us.

Please return this confirmation as soon as possible, as we will not be able to market your property until we receive this form back and signed by you.

NAME AND ADDRESS OF SELLER(S) 
 
{{applicant.firstPerson.forename}} {{applicant.firstPerson.surname}}
{{applicant.address.fullAddress}}

PROPERTY ADDRESS:                 
{{property.address.fullAddress}}


SELLERS CONFIRMATION 


1. I/we confirm the sales particulars are a true and accurate representation of the property, and the estate agent will be notified of any changes.

2. I/we confirm the estate agents will be notified of the removal of any fixtures and fittings mentioned in the sales particulars.



SIGNED    ........................................................................................................................


DATE      ........................................................................................................................

{{createdAt}}`;

const UNFAIR_TRADING_REGULATIONS = `Consumer Protection from Unfair Trading Regulations 2008 and the Business Protection from Misleading Marketing Regulations 2008

VALIDATION CERTIFICATE

The Consumer Protection from Unfair Trading Regulations 2008 and the Business Protection from Misleading Marketing Regulations 2008 requires estate agents to ensure that sales particulars are true and accurate. If the draft sales particulars are inaccurate in any way please return them with any necessary amendments. If any of the information in the sales particulars change in the future it is important you notify us.

Please return this confirmation in the stamped addressed envelope as soon as possible, as we will not be able to market your property until we receive this form back and signed by you.

NAME AND ADDRESS OF SELLER(S) 
 
{{applicant.firstPerson.forename}} {{applicant.firstPerson.surname}}
{{applicant.address.fullAddress}}

PROPERTY ADDRESS:                 
{{property.address.fullAddress}}


SELLERS CONFIRMATION 


1. I/we confirm the sales particulars are a true and accurate representation of the property, and the estate agent will be notified of any changes.

2. I/we confirm the estate agents will be notified of the removal of any fixtures and fittings mentioned in the sales particulars.



SIGNED  ........................................................................................................................


DATE      ........................................................................................................................

{{createdAt}}`;

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false,

            isSearchingRecipients : false,
            recipients : [],
            recipientsToFilter : []
        };
    }

    cancel(){
        DetailsToVendor.actions.clearModel();
        Commands.actions.change(constants.COMMANDS.SEND_DETAILS_TO_VENDOR,false);
    }

    componentWillReceiveProps(props){
        let { DETAILS_TO_VENDOR,COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.SEND_DETAILS_TO_VENDOR]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.SEND_DETAILS_TO_VENDOR]
            });
            // SET PROPERTY ID
            DetailsToVendor.actions.change('property',PROPERTY_DETAIL._id);
            // LOADING RECENT TERMS SENT
            DetailsToVendor.api.list({
                property : PROPERTY_DETAIL._id
            },(result)=>{
                if(result.success){
                    if(result.data.length>0){
                        DetailsToVendor.actions.setModel(result.data[0]);
                    }
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });

            // IF THIS IS IN PROPERTY DETAIL PAGE, THE PROPERTY ALL OWNERS HAS TO BE ADDED IN VENDORS SECTION BY DEFAULT
            if(! this.state.canShowModal) {
                if (DETAILS_TO_VENDOR.recipients.length === 0 && PROPERTY_DETAIL._id) {
                    _.each(PROPERTY_DETAIL.owners,(owner,index)=>{
                        this.addVendor(owner.contact,index);
                    });
                    // DETAILS_TO_VENDOR.actions.setObjectInPath(['recipients', 0], {
                    //     contact: CONTACT_DETAIL,
                    //     feedback: ''
                    // });
                }
            }
        }
    }

    addVendor(owner,index){
        let { PROPERTY_DETAIL } = this.props;
        let message = PROPERTY_MIS_DESCRIPTION_ACT;
        message = message.replace('{{createdAt}}',moment().format('DD/MM/YYYY h:mm a'));
        message = message.replace('{{property.address.fullAddress}}',PROPERTY_DETAIL.address.fullAddress);
        message = message.replace('{{applicant.address.fullAddress}}',owner.address.fullAddress);
        message = message.replace('{{applicant.firstPerson.forename}}',owner.firstPerson.forename);
        message = message.replace('{{applicant.firstPerson.surname}}',owner.firstPerson.surname);

        let unfairTradingRegulation = UNFAIR_TRADING_REGULATIONS;
        unfairTradingRegulation = unfairTradingRegulation.replace('{{createdAt}}',moment().format('DD/MM/YYYY h:mm a'));
        unfairTradingRegulation = unfairTradingRegulation.replace('{{property.address.fullAddress}}',PROPERTY_DETAIL.address.fullAddress);
        unfairTradingRegulation = unfairTradingRegulation.replace('{{applicant.address.fullAddress}}',owner.address.fullAddress);
        unfairTradingRegulation = unfairTradingRegulation.replace('{{applicant.firstPerson.forename}}',owner.firstPerson.forename);
        unfairTradingRegulation = unfairTradingRegulation.replace('{{applicant.firstPerson.surname}}',owner.firstPerson.surname);

        let obj = {
            contact : owner,
            email : {
                subject :'Particulars for Approval',
                to : owner.firstPerson.emails[0].email,
                message : message,
                isSent : false,
            },
            letter : {
                message : unfairTradingRegulation
            }
        };
        DetailsToVendor.actions.setObjectInPath(['recipients', index], obj);
        DetailsToVendor.actions.change('selectedIndex',index);
    }

    submitHandler(result){
        if (result.success) {
            this.cancel();
            // NOTIFYING PROPERTY UPDATE DETAILS
            // IN CONTAINER, WE ARE RELOADING
            Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
            Alert.clearLogs().success(result.message);
        }
        else{
            Alert.clearLogs().error(result.error);
        }
        this.setState({
            isProcessing: false
        });
    }
    submit(){
        DetailsToVendor.actions.validateModel();
        let model = store.getState().DETAILS_TO_VENDOR.toJS();
        if(model.recipients.length>0) {
            if (model.validator.isValid) {
                this.setState({
                    isProcessing: true
                });
                delete model.validator;
                _.each(model.recipients, (r) => {
                    r.contact = r.contact._id;
                });
                if (model._id) {
                    DetailsToVendor.api.update(model._id,model, this.submitHandler.bind(this));
                }
                else {
                    DetailsToVendor.api.save(model, this.submitHandler.bind(this));
                }
            }
        }
    }
    onRecipientsChange(text){
        if(text.length>1) {
            let { DETAILS_TO_VENDOR,PROPERTY_DETAIL } = this.props;
            text = text.trim();
            this.setState({
                isSearchingRecipients: true
            });
            ContactDetail.api.search({ searchText : text }, (result)=> {
                if(result.success) {
                    let owners = _.map(PROPERTY_DETAIL.owners,(owner)=>{
                        return owner.contact._id;
                    });
                    let existingApplicants = _.map(DETAILS_TO_VENDOR.recipients, (p) => {
                        return p.contact._id;
                    });
                    let applicants = _.map(result.data, (p) => {
                        return {
                            _id: p._id,
                            forename: p.firstPerson.forename,
                            surname: p.firstPerson.surname,
                            fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                            address: p.address.fullAddress
                        }
                    });
                    applicants = _.filter(applicants, (p) => {
                        return existingApplicants.indexOf(p._id) === -1 && owners.indexOf(p._id)>-1;
                    });
                    this.setState({
                        recipientsToFilter : applicants,
                        recipients : result.data,
                        isSearchingRecipients: false
                    });
                }
            });
        }
    }
    onRecipientSelected(items,typeAhead) {
        if (items.length > 0) {
            let { DETAILS_TO_VENDOR,PROPERTY_DETAIL } = this.props;
            let index = DETAILS_TO_VENDOR.recipients.length;
            typeAhead.clear();
            typeAhead.focus();

            let applicant = _.find(DETAILS_TO_VENDOR.recipients,function(p){
                return p.contact._id === items[0]._id;
            });
            if(applicant===undefined) {
                let item = _.find(this.state.recipients,function(p){
                    return p._id === items[0]._id;
                });
                this.addVendor(item,index);
            }
            else{
                Alert.clearLogs().error('This applicant is already added!');
            }
        }
    }
    removeRecipient(index){
        let { DETAILS_TO_VENDOR } = this.props;
        DetailsToVendor.actions.removeObjectInPath(['recipients', index]);
        if(DETAILS_TO_VENDOR.recipients.length>1){
            DetailsToVendor.actions.change('selectedIndex',0);
        }
    }
    selectRecipient(index){
        DetailsToVendor.actions.change('selectedIndex',index);
    }

    render () {
        return(
            <Component
                onChange={DetailsToVendor.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                onRecipientsChange={this.onRecipientsChange.bind(this)}
                onRecipientSelected={this.onRecipientSelected.bind(this)}
                removeRecipient = {this.removeRecipient.bind(this)}
                selectRecipient = {this.selectRecipient.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        DETAILS_TO_VENDOR : store.DETAILS_TO_VENDOR.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
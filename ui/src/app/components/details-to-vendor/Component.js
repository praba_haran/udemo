import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import ApplicantSection from './partials/ApplicantSection';
import WriteEmail from './partials/WriteEmail';
import Letter from './partials/Letter';
import { CheckBox, ValidateTextArea,DatePicker } from '../shared/Form';

class Component extends React.Component{

    constructor(){
        super();
        this.state ={
            activeTab : 'email'
        };
    }
    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    render(){
        let {
            DETAILS_TO_VENDOR,
            onChange,
            cancel,
            submit,
            canShowModal,
            isProcessing
        } = this.props;
        let { activeTab } = this.state;

        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal modal-lg">
                <Modal.Header closeButton>
                    <Modal.Title>SEND DETAILS TO VENDOR</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <div className="row details-to-vendor">
                        <div className="col-sm-4">
                            <div className="clear">
                                <ApplicantSection {...this.props} />
                            </div>
                        </div>
                        <div className="col-sm-8">

                            <div className="nav-tabs-horizontal">
                                <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                                    <li role="presentation" className={(activeTab === 'email') ? 'active' : ''}>
                                        <a onClick={() => this.handleTabChange('email')} role="button" href="javascript:void(0)">Email</a>
                                    </li>
                                    <li role="presentation" className={(activeTab === 'letter') ? 'active' : ''}>
                                        <a onClick={() => this.handleTabChange('letter')} role="button" href="javascript:void(0)">Letter to POST</a>
                                    </li>
                                </ul>
                                <div className="tab-content">
                                    <div className="tab-pane active">
                                        {activeTab === 'email' ? (<WriteEmail {...this.props} />) : null}
                                        {activeTab === 'letter' ? (<Letter {...this.props} />) : null}
                                    </div>
                                </div>
                            </div>
                            {this.renderVendorConfirmation()}
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    {DETAILS_TO_VENDOR._id?(
                            <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>
                                {isProcessing?'Updating ...':'Update'}
                            </Button>
                        ):(
                            <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>
                                {isProcessing?'Sending ...':'Send'}
                            </Button>
                        )}
                </Modal.Footer>
            </Modal>
        );
    }

    renderVendorConfirmation(){
        let { DETAILS_TO_VENDOR, onChange } = this.props;
        if(!DETAILS_TO_VENDOR._id){
            return null;
        }
        return (
            <div className="clear">
                <form className="form-horizontal">
                    <div className="form-group margin-bottom-10">
                        <div className="col-sm-4">
                            <CheckBox model={DETAILS_TO_VENDOR} name="isApproved" onChange={onChange} label="Vendor Accepted" />
                        </div>
                        <label className="col-sm-4 control-label">Accepted Date</label>
                        <div className="col-sm-4">
                            <DatePicker
                                model={DETAILS_TO_VENDOR}
                                name="approvedDate"
                                onChange={onChange}
                                placement="top"
                                options={{
                                    disablePreviousDate: true
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <ValidateTextArea placeholder="Notes ..." className="col-sm-12" rows="5" model={DETAILS_TO_VENDOR} name="notes" onChange={onChange} />
                    </div>
                </form>
            </div>
        );
    }
}

export default Component;
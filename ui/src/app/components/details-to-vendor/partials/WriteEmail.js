import React from 'react';
import _ from 'lodash';
import { ValidateTextArea, ValidateText } from '../../shared/Form';
import { Scrollbars } from 'react-custom-scrollbars';

export default class WriteEmail extends React.Component{

    render(){
        let {
            DETAILS_TO_VENDOR,
            PROPERTY_DETAIL,
            onChange
        } = this.props;
        let { recipients, selectedIndex } = DETAILS_TO_VENDOR;

        return (
            <div className="clear">
                <div className="scroll-area">
                    <form className="form-horizontal">
                        <div className="form-group margin-bottom-10">
                            <ValidateText
                                placeholder="Subject"
                                className="col-sm-12"
                                model={DETAILS_TO_VENDOR}
                                name={'recipients['+selectedIndex+'].email.subject'}
                                onChange={onChange}
                                disabled= {DETAILS_TO_VENDOR._id}
                            />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateText
                                placeholder="Email"
                                className="col-sm-12"
                                model={DETAILS_TO_VENDOR}
                                name={'recipients['+selectedIndex+'].email.to'}
                                onChange={onChange}
                                disabled= {DETAILS_TO_VENDOR._id}
                            />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateTextArea
                                placeholder="Message ..."
                                className="col-sm-12"
                                rows="15"
                                model={DETAILS_TO_VENDOR}
                                name={'recipients['+selectedIndex+'].email.message'}
                                onChange={onChange}
                                disabled= {DETAILS_TO_VENDOR._id}
                            />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
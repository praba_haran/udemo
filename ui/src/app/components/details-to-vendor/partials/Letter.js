import React from 'react';
import { ValidateTextArea } from '../../shared/Form';

export default class Letter extends React.Component{

    render(){
        let {
            DETAILS_TO_VENDOR,
            PROPERTY_DETAIL,
            onChange
        } = this.props;
        let { recipients, selectedIndex } = DETAILS_TO_VENDOR;

        return (
            <div className="clear">
                <div className="scroll-area">
                    <form className="form-horizontal">
                        <div className="form-group margin-bottom-0">
                            <h6 className="col-sm-12 margin-bottom-0">UNFAIR TRADING REGULATION ACT 2008</h6>
                            <h6 className="col-sm-12">BUSINESS PROTECTION FROM MISLEADING MARKETING REGULATIONS ACT 2008</h6>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateTextArea
                                placeholder="Message ..."
                                className="col-sm-12"
                                rows="15"
                                model={DETAILS_TO_VENDOR}
                                name={'recipients['+selectedIndex+'].email.message'}
                                onChange={onChange}
                                disabled= {DETAILS_TO_VENDOR._id}
                            />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
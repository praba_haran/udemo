import React from 'react';
import TypeAhead from 'react-bootstrap-typeahead'
import { Scrollbars } from 'react-custom-scrollbars';
import classNames from 'classnames';
import moment from 'moment';

export default class ApplicantSection extends React.Component{

    renderMenuItem(props,option,idx){
        return (
            <div className="contact-item">
                <p className="name">{option.fullName}</p>
                <p className="address">{option.address}</p>
            </div>
        );
    }

    render(){
        let {
            DETAILS_TO_VENDOR,
            PROPERTY_DETAIL,
            onChange,
            isSearchingRecipients ,
            recipientsToFilter ,
            recipients,
            onRecipientsChange,
            onRecipientSelected,
            removeRecipient,
            selectRecipient
        } = this.props;
        return (
            <div className="clear">
                <div className="content-area">
                    <div className="input-group">
                        <TypeAhead
                            ref="typeahead"
                            multiple={true}
                            placeholder="Search vendors ..."
                            emptyLabel={isSearchingRecipients  ?'Loading ...':'No vendors found.'}
                            minLength={3}
                            filterBy={['forename','surname']}
                            labelKey="fullName"
                            options={recipientsToFilter }
                            onInputChange={onRecipientsChange}
                            onChange ={(items)=> { onRecipientSelected(items,this.refs.typeahead.getInstance()); } }
                            renderMenuItemChildren={this.renderMenuItem.bind(this)}
                        />
                        <span className="input-group-btn">
                            <button type="button" className="btn btn-default waves-effect waves-light">
                                <i className="icon md-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                    {(DETAILS_TO_VENDOR.recipients.length===0 && DETAILS_TO_VENDOR.validator.isSubmitted)?(
                            <p className="help-block text-danger">At least one vendor is required.</p>
                        ):null}
                    {(DETAILS_TO_VENDOR.recipients.length===0 && !DETAILS_TO_VENDOR.validator.isSubmitted && PROPERTY_DETAIL.owners.length>0)?(
                            <p className="help-block">At least one vendor is required.</p>
                        ):null}
                    {(DETAILS_TO_VENDOR.recipients.length===0 && !DETAILS_TO_VENDOR.validator.isSubmitted && PROPERTY_DETAIL.owners.length===0)?(
                            <p className="help-block text-danger">Owners are not linked in property.</p>
                        ):null}
                </div>
                <div className="clear applicant-list">
                    <Scrollbars
                        style={{height:350}}
                    >
                        <div className="scroll-area">
                            <ul className="list-group list-group-gap applicants-list-group">
                                {DETAILS_TO_VENDOR.recipients.map((p,index)=>{
                                    return (
                                        <li key={p.contact._id} className={classNames({
                                            'list-group-item' : true,
                                            'active' : (DETAILS_TO_VENDOR.selectedIndex === index)
                                        })}>
                                            <a onClick={()=> selectRecipient(index)} href="javascript:void(0)" role="button">
                                                <p className="title">{p.contact.firstPerson.forename+' '+p.contact.firstPerson.surname}</p>
                                                <p className="address">{p.contact.address.fullAddress}</p>
                                            </a>
                                            {!DETAILS_TO_VENDOR._id?(
                                                <span className="remove" onClick={()=>removeRecipient(index)}>
                                                    <i className="icon md-close"></i>
                                                </span>
                                                ):null}
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </Scrollbars>
                </div>
                {DETAILS_TO_VENDOR._id?(
                        <div className="clear footer">
                            <p className="updated-by">
                                <span className="details">Updated By : {DETAILS_TO_VENDOR.createdBy.forename}</span>
                                <span className="date">Date : {moment(DETAILS_TO_VENDOR.updatedAt).format('DD MMM YYYY h:mm a')}</span>
                            </p>
                        </div>
                    ):null}
            </div>
        );
    }
}
import React from 'react';
import _ from 'lodash';
import ReactDOM from 'react-dom';
import moment from 'moment';

class FullCalendar extends React.Component{

    componentDidMount(){
        this.drawCalendar();
    }

    componentWillReceiveProps(newProps){
        let calendar = ReactDOM.findDOMNode(this);
        $(calendar).fullCalendar('destroy');
        this.drawCalendar(newProps);
    }

    componentWillUnMount(){
        let calendar = ReactDOM.findDOMNode(this);
        $(calendar).fullCalendar('destroy');
    }

    drawCalendar(props){
        let node = ReactDOM.findDOMNode(this);

        let chunk = function (event,type) {
            let chunked = [];
            //diff = moment.duration(event.end.diff(event.start));
            if (event.start.format('HHmm') > event.end.format('HHmm')) {
                return false; //Can't chunk, starttime > endtime
            }
            for (let day = event.start.clone(); !day.isAfter(event.end,'day'); day.add(1,'day')) {
                chunked.push({
                    start:day.clone(),
                    end:day.clone().hour(event.end.hour()).minute(event.end.minute()),
                    id:"chunked-"+type //Used as a flag in the render function
                })
            }
            return chunked;
        };

        let renderChunkedHelper = (function(){
            let id = 0;
            return function(event){
                console.log('event',event);
                window.clearTimeout(id);
                id = window.setTimeout(function(){
                    let chunked = chunk(event,"helper");
                    //eventToChunk = null;
                    $(node).fullCalendar( "removeEvents", "chunked-helper");
                    for(let i = 0; i < chunked.length; i++){
                        $(node).fullCalendar("renderEvent", chunked[i]); //Manually render each chunk
                    }
                },0); //delay in ms. Could be tweaked for optimal perfomance

            }
        })();
        let { events,calendar,viewRender,defaultView,defaultDate, onSelect } = props || this.props;

        let options = {
            customButtons: {
                prevBtn: {
                    text: 'Prev',
                    click: function () {
                        let node = ReactDOM.findDOMNode('.rc-calendar-prev-month-btn');
                        console.log('clicked',node);
                    }
                }
            },
            header: {
                //left: 'prev,next',
                left:'title',
                //center: 'title',
                right: 'agendaDay,agendaWeek,month'
            },
            navLinks: true,
            editable: true,
            eventLimit: true,
            selectable : true,
            select: function( start, end, jsEvent, view ){
                onSelect(start,end,jsEvent,view);
                // if(window.confirm("Create this event?")){
                //     $(node).fullCalendar( "removeEvents", "chunked-helper");
                //     $(node).fullCalendar( "addEventSource",chunk({start:start,end:end},"event"));
                // }else{
                //     $(node).fullCalendar( "removeEvents", "chunked-helper");
                // }
            },
            // eventRender: function (event,element) {
            //     if(event.className[0] === "fc-helper"){ //if it's the drag event
            //         console.log('render-->',event);
            //         renderChunkedHelper(event);
            //         return false; //don't actually render the select helper
            //     }
            // }
        };

        events = _.map(events,(event)=>{
            return {
                title : event.summary,
                start : moment(event.start.dateTime),
                end : moment(event.end.dateTime)
            };
        });

        options.viewRender = viewRender;
        options.defaultView = defaultView;
        options.defaultDate = defaultDate.format('YYYY-MM-DD');
        if(calendar) {
            options.eventSources = [
                {
                    events: events,
                    color: calendar.backgroundColor,
                    textColor: calendar.foregroundColor
                }
            ];
        }
        else{
            options.events = events;
        }
        $(node).fullCalendar(options);
    }


    render(){
        return (
            <div className="fc fc-ltr fc-unthemed"></div>
        );
    }
}

export default FullCalendar;
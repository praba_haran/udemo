import React from 'react';
import { connect } from 'react-redux';
import Diary from '../../models/Diary';
import Component from './Component';

class Container extends React.Component {

    constructor() {
        super();
        this.state = {
            canShowMarketAppraisalModal : false,
            canShowViewingModal : false,
            canShowAppointmentModal : false
        };
    }

    onViewRender(view,element){
        let { DIARY } = this.props;
        let currentDate = DIARY.options.currentDate;
        Diary.actions.setObjectInPath(['options','view'],view.name);

        if(view.name==='agendaDay') {
            if (!currentDate.isSame(view.calendar.getDate(), 'day')) {
                Diary.actions.setObjectInPath(['options','currentDate'],view.calendar.getDate());
            }
        }
        else if(view.name==='month'){
            if (!currentDate.isSame(view.calendar.getDate(), 'day')) {
                Diary.actions.setObjectInPath(['options','currentDate'],view.calendar.getDate());
            }
        }
    }

    onSelect (start,end,jsEvent,view,resource){
        this.setState({
            canShowMarketAppraisalModal : true
        });
    }

    render() {
        return (
            <Component
                {...this.props}
                onViewRender = {this.onViewRender.bind(this)}
                onSelect = {this.onSelect.bind(this)}
                canShowMarketAppraisalModal={this.state.canShowMarketAppraisalModal}
                canShowViewingModal={this.state.canShowViewingModal}
                canShowAppointmentModal={this.state.canShowAppointmentModal}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        GOOGLE_CALENDARS: store.GOOGLE_CALENDARS.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        DIARY : store.DIARY.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
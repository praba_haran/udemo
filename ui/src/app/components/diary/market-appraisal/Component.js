import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import { Select,DatePicker, TimePicker, ValidateSelect, ValidateTextArea } from '../../shared/Form';

class Component extends React.Component{

    render(){
        let { model, onChange, PROPERTY_DETAIL,cancel,submit,isProcessing,canShow } = this.props;
        return (
            <Modal backdrop="static" show={canShow} onHide={cancel} dialogClassName="custom-modal modal-md">
                <Modal.Header closeButton>
                    <Modal.Title>Book a Market Appraisal</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Type</label>
                                    <ValidateSelect className="col-sm-6" model={model} name="type" onChange={onChange} dropKey="marketAppraisalTypes" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Lead Source</label>
                                    <ValidateSelect className="col-sm-6" model={model} name="leadSource" onChange={onChange} dropKey="contactLeadSource" showLabel="false" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Appraiser</label>
                                    {this._renderAppraiser()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Accompanied By</label>
                                    {this._renderAccompainedBy()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Date</label>
                                    <div className="col-sm-6">
                                        <DatePicker model={model} name="startDate" onChange={onChange} />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Time</label>
                                    <div className="col-sm-3">
                                        <TimePicker model={model} name="startDate" onChange={onChange} />
                                    </div>
                                    <div className="col-sm-3">
                                        <TimePicker model={model} name="endDate" onChange={onChange} />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Reminder</label>
                                    <ValidateSelect className="col-sm-3" model={model} name="reminder" onChange={onChange} dropKey="reminderMinutes" showLabel="false" />
                                    <ValidateSelect className="col-sm-3" model={model} name="reminderChannel" onChange={onChange} dropKey="reminderChannel" showLabel="true" />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Notes</label>
                                    <ValidateTextArea rows="6" className="col-sm-6" model={model} name="notes" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-4 col-sm-6">
                                        <div className="checkbox-custom checkbox-default">
                                            <input type="checkbox" id="matching_filter_disposal_one" />
                                            <label htmlFor="matching_filter_disposal_one">Update Last Contacted Date</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    _renderAppraiser(){
        let { model, onChange, agencyDetails} = this.props;
        if(agencyDetails && agencyDetails.agencyUsers) {
            return (
                <ValidateSelect className="col-sm-6" model={model} name="appraiser"
                                onChange={onChange}>
                    {agencyDetails.agencyUsers.map(function (p) {
                        return (
                            <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                        );
                    })}
                </ValidateSelect>
            );
        }
    }

    _renderAccompainedBy(){
        let { model, onChange, agencyDetails} = this.props;
        if(agencyDetails && agencyDetails.agencyUsers) {
            return (
                <div className="col-sm-6">
                    <Select model={model} name="accompaniedBy" onChange={onChange}>
                        {(model && model.appraiser) ? agencyDetails.agencyUsers.map(function (p) {
                            if (p._id !== model.appraiser) {
                                return (
                                    <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                );
                            }
                        }) : null}
                    </Select>
                </div>
            );
        }
    }
}

export default Component;
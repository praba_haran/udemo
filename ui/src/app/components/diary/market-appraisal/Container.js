import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import * as constants from '../../../constants';
import Commands from '../../../models/Commands';
import MarketAppraisal from '../../../models/MarketAppraisal';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShow : false
        };
    }

    componentWillReceiveProps(props){
        if(props.canShowMarketAppraisalModal !== this.state.canShow){
            this.setState({
                canShow : props.canShowMarketAppraisalModal
            });
        }
    }

    cancel(){
        this.setState({
            canShow : false
        });
    }

    submit(){
        MarketAppraisal.actions.validateModel();
        let model = store.getState().MARKET_APPRAISAL.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            model.property = '58584952f51c5b142c0d1ce2';
            let _this = this;
            MarketAppraisal.api.save(model,(result)=>{
                this.setState({
                    isProcessing: false
                });
                if(result.success) {
                    _this.cancel();
                }
                else{

                }
            });
        }
    }

    render () {
        return(
            <Component
                onChange={MarketAppraisal.actions.valueChange}
                submit={this.submit.bind(this)}
                cancel={this.cancel.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    let app = store.APP.toJS();
    return {
        model : store.MARKET_APPRAISAL.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        agencyDetails : app.agencyDetails,
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
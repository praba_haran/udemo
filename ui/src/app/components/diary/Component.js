import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import FullCalendar from './partials/FullCalendar';
import MarketAppraisal from './market-appraisal/Container';

class Component extends React.Component{

    render(){
        let { DIARY,GOOGLE_CALENDARS } = this.props;
        let calendar = null;
        if(DIARY.selectedCalendarId){
            calendar = _.find(GOOGLE_CALENDARS.items,(p)=>{
               return p.id === DIARY.selectedCalendarId;
            });
        }
        return (
            <div className="page page-diary">
                <div className="page-content">
                    <div className="clear">
                        <div className="header-actions">
                            <div className="row">
                                <div className="col-sm-3">
                                    <h5 className="title">{calendar?calendar.summary:''}</h5>
                                </div>
                                <div className="col-sm-5 text-center">
                                    <div className="label-group">
                                        <span className="label label-outline label-default label-toggle active">
                                            All
                                        </span>
                                        <span className="label label-outline label-default label-toggle">
                                            <i className="icon fa-check"></i>
                                            Meetings
                                        </span>
                                        <span className="label label-outline label-default label-toggle">
                                            <i className="icon fa-check"></i>
                                            Appraisal
                                        </span>
                                        <span className="label label-outline label-default label-toggle">
                                            <i className="icon fa-check"></i>
                                            Viewings
                                        </span>
                                        <span className="label label-outline label-default label-toggle">
                                            <i className="icon fa-check"></i>
                                            Inspections
                                        </span>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="clear text-right">
                                        <div className="label-group label-group-single">
                                            <span className="label label-outline label-default">
                                                <i className="icon fa-print"></i>
                                                Print
                                            </span>
                                        </div>
                                        <div className="label-group label-group-single">
                                            <span className="label label-outline label-default">
                                                More
                                                <i className="icon fa-angle-down"></i>
                                            </span>
                                        </div>
                                        <div className="label-group">
                                            <span className="label label-outline label-default">
                                                <i className="icon fa-calendar"></i>
                                            </span>
                                            <span className="label label-outline label-default">
                                                <i className="icon fa-navicon"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="calendar-container">
                            <FullCalendar
                                calendar={calendar}
                                viewRender={this.props.onViewRender}
                                onSelect = {this.props.onSelect}
                                events={DIARY.events}
                                defaultView={DIARY.options.view}
                                defaultDate ={DIARY.options.currentDate}
                            />
                        </div>
                    </div>
                </div>
                <MarketAppraisal {...this.props} />
            </div>
        );
    }
}

export default Component;
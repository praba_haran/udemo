import React from 'react';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Contact from '../../models/Contact';
import Property from '../../models/Property';
import ErrorMessages from '../../models/ErrorMessages';
import Component from './Component';
import Alert from '../../lib/Alert';

class Container extends React.Component {

    constructor() {
        super();
        this.state = {
            canShowModal : false,
            addresses: [],
            isFetchingAddress: false
        };
    }

    componentWillReceiveProps(props){
        let { COMMANDS, APP }= props;
        if(COMMANDS[constants.COMMANDS.ADD_CONTACT]!==this.state.ADD_CONTACT){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.ADD_CONTACT]
            });
            // BIND NEGOTIATOR AND BRANCH AS DEFAULT
            if(! this.state.canShowModal) {
                let {userInfo} = APP;
                if (userInfo && userInfo._id) {
                    Contact.actions.change('negotiator', userInfo._id);
                    Contact.actions.change('agency', userInfo.agency._id);
                }
            }
        }
    }

    onSubmit() {
        let { COMMANDS } = this.props;
        Contact.actions.validateModel();
        let model = store.getState().CONTACT.toJS();
        // if (model.validator.isValid) {
            delete model.validator;
            Contact.api.save(model,(result)=>{
                if(result.success){
                    Alert.clearLogs().success(result.message);
                    this.cancel();
                    // NOTIFICATION TO ADD SOLICITORS
                    Commands.actions.change(constants.COMMANDS.ADD_CONTACT_CHANGED, true);
                    // REDIRECT ONLY IF THIS FLAG IS SET TO FALSE
                    if(!COMMANDS.ADD_CONTACT_DO_NOT_REDIRECT){
                        hashHistory.push('/contacts/' + result.data._id+'/overview');
                    }
                }
                else{
                    ErrorMessages.actions.change(constants.ERROR_MESSAGES.ADD_CONTACT_ERROR,result.error);
                }
            });
        // }
    }
    cancel() {
        Commands.actions.change(constants.COMMANDS.ADD_CONTACT, false);
        Contact.actions.clearModel();
    }
    addRemoveEmail(length,path,command){
        if(command==='add') {
            if(length<2) {
                Contact.actions.setObjectInPath(path,{
                    email : ''
                });
            }
        }
        else{
            Contact.actions.removeObjectInPath(path);
        }
    }
    addRemoveTelephone(length,path,command){
        if(command==='add') {
            if(length<4) {
                Contact.actions.setObjectInPath(path,{
                    type:'Home',
                    label:'',
                    number:''
                });
            }
        }
        else{
            Contact.actions.removeObjectInPath(path);
        }
    }
    onPostcodeChange(text){
        if(text.length>2){
            text = text.toUpperCase().trim();
            this.setState({
                isFetchingAddress:true
            });
            Property.api.searchAddress(text,(result)=>{
                if(result.success) {
                    this.setState({
                        addresses: result.data,
                        isFetchingAddress: false
                    });
                }
            });
        }
    }
    onPostcodeSelected(items) {
        if (items.length > 0) {
            let address = items[0];
            let matchNumbers = address.street.match(/\d+/);
            if(matchNumbers.length>0) {
                address.nameOrNumber = matchNumbers[0];
            }
            address.street = address.street.replace(address.nameOrNumber,'').trim();
            Contact.actions.setObjectInPath(['address'],address);

            // once the address is populated validate the text box
            let model = store.getState().CONTACT.toJS();
            if (model.validator.isSubmitted) {
                Contact.actions.validateModel();
            }
            // once address is binded clear the error message
            ErrorMessages.actions.change(constants.ERROR_MESSAGES.ADD_CONTACT_ERROR,null);
        }
    }

    render() {
        return (
            <Component
                {...this.props}
                {...this.state}
                onChange={Contact.actions.valueChange}
                onSubmit={this.onSubmit.bind(this)}
                cancel={this.cancel.bind(this)}
                addRemoveTelephone={this.addRemoveTelephone.bind(this)}
                addRemoveEmail={this.addRemoveEmail.bind(this)}
                onPostcodeChange={this.onPostcodeChange.bind(this)}
                onPostcodeSelected={this.onPostcodeSelected.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        model: store.CONTACT.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        error_messages : store.ERROR_MESSAGES.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import Typeahead from 'react-bootstrap-typeahead';
import { TextBox, Select, Validate , ValidateText, ValidateSelect } from '../shared/Form';
import * as constants from '../../constants';

class Component extends React.Component{

    render(){
        let { model,error_messages, isProcessing,canShowModal } = this.props;
        let { canShow, onChange ,onSubmit,cancel, addRemoveEmail , addRemoveTelephone } = this.props;
        return (
            <Modal backdrop="static" bsSize="lg" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Add Contact</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {error_messages[constants.ERROR_MESSAGES.ADD_CONTACT_ERROR]?(
                        <p className="text-danger text-center">{error_messages[constants.ERROR_MESSAGES.ADD_CONTACT_ERROR]}</p>
                    ):null}
                    <div className="row">
                        <div className="col-lg-6">
                            {this.renderAddress()}
                        </div>
                        <div className="col-lg-6">
                            {this.renderPerson('FIRST PERSON')}
                            <div className="margin-top-20">
                                {this.renderPerson('SECOND PERSON')}
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={onSubmit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    renderAddress(){
        let { onChange, model, onPostcodeChange, onPostcodeSelected,isFetchingAddress,addresses,APP } = this.props;
        let { agencyDetails } = APP;
        return (
            <form className="form-horizontal custom-form">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-8">GENERAL</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Type</label>
                    <ValidateSelect className="col-sm-7" model={model} name="type" onChange={onChange} dropKey="contactType" showLabel="false" />
                </div>
                {this.isClient()?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Intension</label>
                            <ValidateSelect className="col-sm-7" model={model} name="intension" onChange={onChange} dropKey="contactIntension" showLabel="false" />
                        </div>
                    ):null}
                {!this.isClient()?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Company</label>
                            <ValidateText className="col-sm-7" model={model} name="companyName" onChange={onChange}/>
                        </div>
                    ):null}
                {!this.isClient()?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Website</label>
                            <ValidateText className="col-sm-7" model={model} name="companyWebsite" onChange={onChange}/>
                        </div>
                    ):null}
                <div className="form-group margin-top-20 margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-8">ADDRESS </h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Find Address</label>
                    <div className="col-sm-7">
                        <Typeahead
                            placeholder="Post Code"
                            emptyLabel={isFetchingAddress?'Loading ...':'No address found.'}
                            minLength={2}
                            filterBy={['fullAddress']}
                            labelKey="postcode"
                            options={addresses}
                            onInputChange={onPostcodeChange}
                            onChange ={ onPostcodeSelected}
                            maxResults={200}
                            paginate={true}
                            renderMenuItemChildren={(props, option, idx) => {
                                return (<span><i className="icon fa-map-marker"></i>{option.fullAddress+', '+option.postcode}</span>);
                            }} />
                        <span className="help-block">Or Enter Address Manually</span>
                    </div>
                </div>
                <div className="clear">
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Dwelling</label>
                        <ValidateText className="col-sm-3" model={model} name="address.dwelling" onChange={onChange}/>
                        <label className="col-sm-2 control-label">Name/No</label>
                        <ValidateText className="col-sm-2" model={model} name="address.nameOrNumber" onChange={onChange} />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Street</label>
                        <ValidateText className="col-sm-7" model={model} name="address.street" onChange={onChange} />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Locality</label>
                        <ValidateText className="col-sm-7" model={model} name="address.locality" onChange={onChange} />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Town</label>
                        <ValidateText className="col-sm-7" model={model} name="address.town" onChange={onChange} type="onlyAlphabet" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">County</label>
                        <ValidateText className="col-sm-7" model={model} name="address.county" onChange={onChange} type="onlyAlphabet" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Postcode</label>
                        <ValidateText className="col-sm-7" model={model} name="address.postcode" onChange={onChange} mask="postcode"/>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Country</label>
                        <ValidateSelect className="col-sm-7" model={model} name="address.country" onChange={onChange} dropKey="country" showLabel="false"/>
                    </div>
                </div>
                {agencyDetails!==null?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Branch</label>
                            <ValidateSelect className="col-sm-7" model={model} name="agency" onChange={onChange}>
                                {agencyDetails.branchDetails.map(function(p){
                                    return (
                                        <option key={p._id} value={p._id}>{p.address.locality+'-'+p.address.town}</option>
                                    );
                                })}
                            </ValidateSelect>
                        </div>
                    ):null}
                {agencyDetails!==null?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Negotiator</label>
                            <ValidateSelect className="col-sm-7" model={model} name="negotiator" onChange={onChange}>
                                {agencyDetails.agencyUsers.map(function(p){
                                    return (
                                        <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                    );
                                })}
                            </ValidateSelect>
                        </div>
                    ):null}
            </form>
        );
    }

    renderPerson(title){
        let { model, onChange ,onSubmit, addRemoveEmail , addRemoveTelephone } = this.props;
        let person;
        let isFirstPerson = false;
        let prefix = 'firstPerson';
        if(title==='FIRST PERSON'){
            person = model.firstPerson;
            isFirstPerson= true;
        }
        else{
            prefix = 'secondPerson';
            person = model.secondPerson;
        }
        return (
            <form className="form-horizontal custom-form">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-3 col-sm-9">{title}</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-3 control-label">Forename</label>
                    <div className="col-sm-3">
                        <Select model={model} name={prefix+'.title'} onChange={onChange} dropKey="contactTitle" showLabel="false" />
                    </div>
                    <ValidateText className="col-sm-5" model={model} name={prefix+'.forename'} onChange={onChange} />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-3 control-label">Surname</label>
                    <ValidateText className="col-sm-8" model={model} name={prefix+'.surname'} onChange={onChange} />
                </div>
                {isFirstPerson?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-3 control-label">Salutation</label>
                            <ValidateText className="col-sm-8" model={model} name={prefix+'.salutation'} onChange={onChange} />
                        </div>
                    ):null}
                {person.emails.map((p,index)=>{
                    let iconClass = index===0?'icon md-plus':'icon md-minus';
                    return (
                        <div key={prefix+'.email.'+index} className="form-group margin-bottom-10">
                            <label className="col-sm-3 control-label">{index===0?'Email':''}</label>
                            <Validate model={model} className="col-sm-8" name={prefix+'.emails['+index+'].email'}>
                                <div className="input-group">
                                    <TextBox model={model} name={prefix+'.emails['+index+'].email'} onChange={onChange}></TextBox>
                                    <span
                                        onClick={()=>{ addRemoveEmail(person.emails.length,[prefix,'emails',(index===0?person.emails.length:index)],index===0?'add':'remove') }}
                                        className="input-group-addon">
                                        <i className={iconClass}></i>
                                    </span>
                                </div>
                            </Validate>
                        </div>
                    );
                })}
                {person.telephones.map((p,index)=>{
                    let iconClass = index===0?'icon md-plus':'icon md-minus';
                    return (
                        <div key={prefix+'.telephones.'+index} className="form-group margin-bottom-10">
                            <label className="col-sm-3 control-label">{index===0?'Telephone':''}</label>
                            <div className="col-sm-3">
                                <Select model={model} name={prefix+'.telephones['+index+'].type'} onChange={onChange} dropKey="contactPhoneType" showLabel="false"/>
                            </div>
                            <Validate model={model} className="col-sm-5" name={prefix+'.telephones['+index+'].number'}>
                                <div className="input-group">
                                    <TextBox model={model} name={prefix+'.telephones['+index+'].number'} onChange={onChange}></TextBox>
                                    <span
                                        onClick={()=>{ addRemoveTelephone(person.telephones.length,[prefix,'telephones',(index===0?person.telephones.length:index)],index===0?'add':'remove') }}
                                        className="input-group-addon">
                                        <i className={iconClass}></i>
                                    </span>
                                </div>
                            </Validate>
                        </div>
                    );
                })}
            </form>
        );
    }

    isClient(){
        let { model } = this.props;
        if(model) {
            return model.type ==='Client';
        }
        return false;
    }
}

export default Component;
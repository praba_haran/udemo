import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import {Link} from "react-router";

class Component extends React.Component{

    render () {
        let { clientType,onClientTypeChange, items, renderNoRecord,
            renderFooter,searchText,onSearchTextChange, archive, activate, remove } = this.props;
        let clientTypes = [
            { key : 'All', text : 'Contacts' },
            { key : 'Client', text : 'Clients' },
            { key : 'Developer', text : 'Developers' },
            { key : 'Solicitor', text : 'Solicitors' },
            { key : 'Bank', text : 'Banks' },
            { key : 'Board Contractor', text : 'Board Contractors' },
            { key : 'Builder', text : 'Builders' },
            { key : 'Building Society', text : 'Building Societies' },
            { key : 'Estate Agent', text : 'Estate Agents' },
            { key : 'Estate Agent (Local)', text : 'Local Estate Agents' }
        ];

        return (
            <div className="page">
                <div className="page-content">
                    <div className="panel panel-transparent contact-listing-panel">
                        <div className="panel-body container-fluid">

                            <div className="basic-grid">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="btn-group">
                                            <DropdownButton bsSize="small" id="dropDown" title={clientType}>
                                                {clientTypes.map(function(p){
                                                    return (
                                                        <MenuItem
                                                            key={p.key}
                                                            eventKey ={p.key}
                                                            onSelect={onClientTypeChange}
                                                        >{p.key}</MenuItem>
                                                    );
                                                })}
                                            </DropdownButton>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="input-search input-search-dark pull-right">
                                            <i className="input-search-icon md-search" aria-hidden="true"></i>
                                            <input value={searchText} onChange={onSearchTextChange} type="text" className="form-control" placeholder="Search ..." />
                                            <button type="button" className="input-search-close icon md-close" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                                <table className="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Type</th>
                                        <th>Negotiator</th>
                                        <th className="hidden-xs">
                                            Status
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {items.map((p,index)=>{
                                        return (
                                            <tr key={p._id}>
                                                <td>
                                                    <Link className="name" to={'contacts/'+p._id+'/overview'}>{p.firstPerson.forename+' '+p.firstPerson.surname}</Link>
                                                </td>
                                                <td>
                                                    <Link className="address-link" to={'contacts/'+p._id+'/overview'}>{p.address.fullAddress}</Link>
                                                </td>
                                                <td>{p.type}</td>
                                                <td>{p.negotiator.forename}</td>
                                                <td className="hidden-xs">
                                                    <span className="label label-primary label-outline">{p.status}</span>
                                                </td>
                                                <td>
                                                    <div className="btn-group" role="group">
                                                        <button type="button" className="btn btn-pure btn-xs waves-effect waves-light" id="exampleIconDropdown4" data-toggle="dropdown" aria-expanded="false">
                                                            <i className="icon md-more-vert" aria-hidden="true"></i>
                                                        </button>
                                                        <ul className="dropdown-menu" aria-labelledby="exampleIconDropdown4" role="menu">
                                                            <li role="presentation">
                                                                <a onClick={()=>remove(p._id)} href="javascript:void(0)" role="menuitem">
                                                                    <i className="icon md-delete"></i> Delete
                                                                </a>
                                                            </li>
                                                            {p.status!=='Archived'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>archive(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-archive"></i> Archive
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                            {p.status!=='Active'?(
                                                                    <li role="presentation">
                                                                        <a onClick={()=>activate(p._id)} href="javascript:void(0)" role="menuitem">
                                                                            <i className="icon md-check"></i> Activate
                                                                        </a>
                                                                    </li>
                                                                ):null}
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    {renderNoRecord({
                                        colSpan:6,
                                        noRecordText : 'No contacts found.'
                                    })}
                                    </tbody>
                                </table>
                                {renderFooter()}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import _ from 'lodash';
import {hashHistory} from 'react-router';
import { connect } from 'react-redux';
import store from '../../store';
import Alert from '../../lib/Alert';
import Component from './Component';
import ContactListing from '../../models/ContactListing';
import BasicGrid from '../shared/grid/BasicGrid';

class Container extends BasicGrid{

    componentDidMount () {
        this.getContacts();
        this.setUrl('contact/list/paginate').load();
        this.setState({
            clientType : 'All'
        });
    }

    getContacts(){
        ContactListing.actions.change('isProcessing',true);
        ContactListing.api.list((result)=>{
            if(result.success){
                ContactListing.actions.setObjectInPath(['contacts'],result.data);
            }
            ContactListing.actions.change('isProcessing', false);
        });
    }

    onClientTypeChange(key){
        this.setState({
            clientType : key
        });
        if(key==='All'){
            this.removeParam('type').setPageIndex(1).load();
        }
        else{
            this.setParam('type',key).setPageIndex(1).load();
        }
    }

    remove(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to delete this contact?', ()=>{
            ContactListing.api.deleteItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }
    archive(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to archive this contact?', ()=>{
            ContactListing.api.archiveItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }
    activate(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to activate this contact?', ()=>{
            ContactListing.api.activateItems([id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.load();
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                onChange={ContactListing.actions.valueChange}
                renderNoRecord={this.renderNoRecord.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                onSearchTextChange={this.onSearchTextChange.bind(this)}
                archive={this.archive.bind(this)}
                activate={this.activate.bind(this)}
                remove={this.remove.bind(this)}
                onClientTypeChange={this.onClientTypeChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        CONTACT_LISTING: store.CONTACT_LISTING.toJS(),
        commands : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);

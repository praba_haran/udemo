import React from 'react';
import _ from 'lodash';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import dropDownValues from '../../../components/shared/dropdown-values';
import PropertyDetail from '../../../models/PropertyDetail';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.selectFeature = this.selectFeature.bind(this);
    }

    componentDidMount(){
        this.setFeatures();
    }

    componentWillUpdate(){
        this.setFeatures();
    }

    setFeatures(){
        let { model } = this.props;
        if(model._id && model.features && model.features.list.length===0){
            PropertyDetail.actions.setObjectInPath(['features'],{
                selectedIndex : 0,
                list : dropDownValues.propertyFeatures
            });
        }
    }

    onSubmit(){
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    selectFeature(e,index){
        PropertyDetail.actions.change('features.selectedIndex',index);
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                selectFeature={this.selectFeature.bind(this)}
                onSubmit={this.onSubmit}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import uuid from 'uuid';
import { CheckBox } from '../../../components/shared/Form';
import ActionButtonGroup from '../helper/ActionButtonGroup';

class Features extends React.Component{

    getSelectedCount(feature){
        return _.filter(feature.values,(p)=>{
            return p.selected;
        }).length;
    }

    render () {
        let { model, onChange,selectFeature } = this.props;
        let features = model.features;
        let selectedFeature = {
            values : []
        };
        if(features.list.length>0) {
            selectedFeature = features.list[features.selectedIndex];
        }
        return (
            <div className="tab-pane active">
                <div className="row features-areas">
                    <div className="col-lg-4 col-md-4 col-xs-6">
                        <div className="list-group">
                            {model.features.list.map((p,index)=>{
                                let cssClass= 'list-group-item waves-effect waves-block waves-classic';
                                if(p.key === selectedFeature.key){
                                    cssClass+=' active';
                                }
                                let count = this.getSelectedCount(p);
                                return (
                                    <a key={p._id} onClick={(e)=>selectFeature(e,index)} className={cssClass} href="javascript:void(0)">
                                        <i className="icon fa-angle-right"></i>
                                        {p.key}
                                        {count>0?(<span className="badge badge-default">{count}</span>):null}
                                    </a>
                                );
                            })}
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-xs-6">
                        {selectedFeature.values.map((p,index)=>{
                            let inputId = uuid.v1();
                            let name = 'features.list['+features.selectedIndex+'].values['+index+'].selected';
                            return (
                                <CheckBox onChange={onChange} name={name} key={inputId} model={model} label={p.name} />
                            );
                        })}
                    </div>
                </div>
                <ActionButtonGroup {...this.props} />
            </div>
        );
    }
}

export default Features;
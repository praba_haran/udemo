import React from 'react';
import NavLink from '../shared/NavLink';
import * as constants from '../../constants';
import BrochureSetupModal from './brochure-setup/Container';
import ViewingModal from '../viewing/Container';
import ViewingEmailConfirmation from '../viewing-email-confirmation/Container';
import GeneralAppointmentModal from '../general-appointment/Container';
import MakeAnOffer from '../offer/Container';
import PropertyStatus from '../property-status/Container';
import ChangeBoard from '../change-board/Container';
import InstructProperty from '../instruct-property/Container';
import TermsSent from '../terms-sent/Container';
import TermsSigned from '../terms-signed/Container';
import DetailsToVendor from '../details-to-vendor/Container';
import PropertySlideShow from '../property-media-slide-show/Container';

class Property extends React.Component{

    render(){
        let id = this.props.params.id;
        let { model, COMMANDS ,userInfo } = this.props;
        return (
            <div className="page property-detail-page">
                <div className="page-content">
                    <div className="panel nav-tabs-horizontal panel-transparent">
                        <ul className="nav nav-tabs nav-tabs-line">
                            <NavLink activeClassName="active" to={'/properties/'+id+'/overview'}>Overview</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/details'}>Details</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/features'}>Features</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/media'}>Media</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/rooms'}>Rooms</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/mapping'}>Mapping</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/matching'}>Matching</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/keys'}>Keys</NavLink>
                            <NavLink activeClassName="active" to={'/properties/'+id+'/brochures'}>Brochures</NavLink>
                        </ul>
                        <div className="panel-body">
                            <div className="tab-content">{this.props.children}</div>
                        </div>
                    </div>
                </div>
                <BrochureSetupModal canShowModal={COMMANDS[constants.COMMANDS.SHOW_BROCHURE_SETUP]} location={this.props.location} params={this.props.params} />
                <ViewingModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING]} location={this.props.location} params={this.props.params} />
                <ViewingEmailConfirmation canShowModal={COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM]} location={this.props.location} params={this.props.params} />
                <GeneralAppointmentModal canShowModal={COMMANDS[constants.COMMANDS.BOOK_GENERAL_APPOINTMENT]} location={this.props.location} params={this.props.params} />
                <MakeAnOffer canShowModal={COMMANDS[constants.COMMANDS.MAKE_AN_OFFER]} location={this.props.location} params={this.props.params} />
                <PropertyStatus canShowModal={COMMANDS[constants.COMMANDS.PROPERTY_STATUS]} location={this.props.location} params={this.props.params} />
                <ChangeBoard canShowModal={COMMANDS[constants.COMMANDS.CHANGE_BOARD]} location={this.props.location} params={this.props.params} />
                <InstructProperty canShowModal={COMMANDS[constants.COMMANDS.INSTRUCT_PROPERTY]} location={this.props.location} params={this.props.params} />
                <TermsSent canShowModal={COMMANDS[constants.COMMANDS.TERMS_SENT]} location={this.props.location} params={this.props.params} />
                <TermsSigned canShowModal={COMMANDS[constants.COMMANDS.TERMS_SIGNED]} location={this.props.location} params={this.props.params} />
                <DetailsToVendor canShowModal={COMMANDS[constants.COMMANDS.SEND_DETAILS_TO_VENDOR]} location={this.props.location} params={this.props.params} />
                <PropertySlideShow canShowModal={COMMANDS[constants.COMMANDS.SHOW_PROPERTY_SLIDE_SHOW]} location={this.props.location} params={this.props.params} />
            </div>
        );
    }
}

export default Property;
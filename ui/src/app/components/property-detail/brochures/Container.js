import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import axiosStatic from '../../../api/axios-static';
import PropertyDetail from '../../../models/PropertyDetail';

class Container extends React.Component{

    getBrochureUrl(){
        let { model, APP } = this.props;
        let { userInfo} = APP;
        let defaultBrochureUrl;
        if(userInfo && userInfo.agency) {
            let { brochures } = userInfo.agency.settings;
            let defaultBrochure = _.find(brochures, function (o) {
                return o.isDefault;
            });
            if(defaultBrochure){
                defaultBrochureUrl = axiosStatic.getBrochureUrl('doc/brochure/'+model._id+'/'+defaultBrochure.template);
                defaultBrochureUrl = defaultBrochureUrl+'#zoom=60';
            }
        }
        return defaultBrochureUrl;
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                getBrochureUrl={this.getBrochureUrl.bind(this)}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
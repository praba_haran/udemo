import React from 'react';

class Component extends React.Component{

    render(){
        let url = this.props.getBrochureUrl();
        return (
            <div className="clear brochure-viewer">
                {url?(
                    <iframe frameBorder={0} src={url} height="440px" width="100%"></iframe>
                ):null}
            </div>
        );
    }
}

export default Component;
import React from 'react';
import _ from 'lodash';
import Matches from './Matches';
import Offers from './Offers';
import Viewings from './Viewings';
import Appointments from './Appointments';
import Activities from './Activities';
import { Scrollbars } from 'react-custom-scrollbars';

class SectionRight extends React.Component{

    constructor(){
        super();
        this.state ={
            activeTab:'activities'
        };
        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    render(){
        let { activeTab } = this.state;
        return (
            <div className="col-lg-6">
                <div className="nav-tabs-horizontal">
                    <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                        <li role="presentation" className={(activeTab==='activities')?'active':''}>
                            <a onClick={()=>this.handleTabChange('activities')} role="button" href="javascript:void(0)">Activities</a>
                        </li>
                        <li role="presentation" className={(activeTab==='appointments')?'active':''}>
                            <a onClick={()=>this.handleTabChange('appointments')} role="button" href="javascript:void(0)">Appointments</a>
                        </li>
                        <li role="presentation" className={(activeTab==='matches')?'active':''}>
                            <a onClick={()=>this.handleTabChange('matches')} role="button" href="javascript:void(0)">Matches</a>
                        </li>
                        <li role="presentation" className={(activeTab==='offers')?'active':''}>
                            <a onClick={()=>this.handleTabChange('offers')} role="button" href="javascript:void(0)">Offers</a>
                        </li>
                        <li role="presentation" className={(activeTab==='viewings')?'active':''}>
                            <a onClick={()=>this.handleTabChange('viewings')} role="button" href="javascript:void(0)">Viewings</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <Scrollbars
                            style={{height:420}}
                        >
                            <div className="scrolling-area">
                                <div className="tab-pane active">
                                    {activeTab==='activities'?(<Activities {...this.props} />):null}
                                    {activeTab==='appointments'?(<Appointments {...this.props} />):null}
                                    {activeTab==='matches'?(<Matches {...this.props} />):null}
                                    {activeTab==='offers'?(<Offers {...this.props} />):null}
                                    {activeTab==='viewings'?(<Viewings {...this.props} />):null}
                                </div>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        );
    }
}

export default SectionRight;
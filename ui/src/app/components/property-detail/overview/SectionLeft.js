import React from 'react';
import _ from 'lodash';
import Helper from '../../../components/shared/helper';
import MarketAppraisalBlock from './market-appraisal/Container';

class SectionLeft extends React.Component{

    render(){
        let { PROPERTY_DETAIL, openSlideShow} = this.props;
        let getPrice = ()=>{
            if(PROPERTY_DETAIL.contract.instructId){
                return PROPERTY_DETAIL.contract.price;
            }
            return PROPERTY_DETAIL.proposedPrice || PROPERTY_DETAIL.price;
        };
        return (
            <div className="col-lg-6 section-left">
                <div className="clear">
                    <h4>{PROPERTY_DETAIL.address.fullAddress}</h4>
                    <h5 className="price">{Helper.toCurrency(getPrice())} - {PROPERTY_DETAIL.type}</h5>
                    {/*<div className="owners">*/}
                        {/*<div className="row">*/}
                            {/*<div className="col-sm-6">*/}
                                {/*<div className="owner">*/}
                                    {/*<h5 className="heading">Miss Swethaa Parthiban</h5>*/}
                                    {/*<p className="email">*/}
                                        {/*<a href="">aruljothiparthiban@hotmail.com</a>*/}
                                    {/*</p>*/}
                                    {/*<p className="telephone">*/}
                                        {/*<a href="#">+447405927851</a>*/}
                                    {/*</p>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                            {/*<div className="col-sm-6">*/}
                                {/*<div className="owner">*/}
                                    {/*<h5 className="heading">Miss Swethaa Parthiban</h5>*/}
                                    {/*<p className="email">*/}
                                        {/*<a href="">aruljothiparthiban@hotmail.com</a>*/}
                                    {/*</p>*/}
                                    {/*<p className="telephone">*/}
                                        {/*<a href="#">+447405927851</a>*/}
                                    {/*</p>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                </div>
                <div className="clear">
                    <ul className="measurement-list clearfix">
                        {/*<li className="list-item">*/}
                            {/*<p className="icon-container"><img src="/dist/images/icons/property.png" /></p>*/}
                            {/*<p className="unit">PROPERTY AGE</p>*/}
                            {/*<p className="title">BUILT YEAR</p>*/}
                            {/*<p className="value">{model.age}</p>*/}
                        {/*</li>*/}
                        <li className="list-item">
                            <p className="icon-container"><img src="/dist/images/icons/sq_ft.png" /></p>
                            <p className="unit">IN {PROPERTY_DETAIL.landAreaUnit}</p>
                            <p className="title">AREA</p>
                            <p className="value">{PROPERTY_DETAIL.landArea}</p>
                        </li>
                        <li className="list-item">
                            <p className="icon-container"><img src="/dist/images/icons/bed.png" /></p>
                            <p className="unit">NUMBER OF</p>
                            <p className="title">BEDROOM</p>
                            <p className="value">{PROPERTY_DETAIL.bedrooms}</p>
                        </li>
                        <li className="list-item">
                            <p className="icon-container"><img src="/dist/images/icons/bath.png" /></p>
                            <p className="unit">NUMBER OF</p>
                            <p className="title">BATHROOM</p>
                            <p className="value">{PROPERTY_DETAIL.bathrooms}</p>
                        </li>
                        <li className="list-item">
                            <p className="icon-container"><img src="/dist/images/icons/floor.png" /></p>
                            <p className="unit">TOTAL</p>
                            <p className="title">FLOOR</p>
                            <p className="value">2</p>
                        </li>
                        <li className="list-item">
                            <p className="icon-container"><img src="/dist/images/icons/garage.png" /></p>
                            <p className="unit">NUMBER OF</p>
                            <p className="title">PARKING</p>
                            <p className="value">{PROPERTY_DETAIL.parking}</p>
                        </li>
                    </ul>
                </div>
                <div className="clear">
                    <div className="photos-viewer clearfix">
                        <div className="main-photo" onClick={openSlideShow}>
                            {this._renderPhotoByIndex(0)}
                        </div>
                        <div className="photo-thumbnails">
                            {this._renderPhotoByIndex(1)}
                            {this._renderPhotoByIndex(2)}
                            {this._renderPhotoByIndex(3)}
                            {this._renderPhotoByIndex(4)}
                            {this._renderPhotoByIndex(5)}
                            {this._renderPhotoByIndex(6)}
                        </div>
                    </div>
                </div>
                <div className="clear">
                    <MarketAppraisalBlock {...this.props} />
                </div>
            </div>
        );
    }

    _renderPhotoByIndex(index){
        let { PROPERTY_DETAIL, openSlideShow } = this.props;
        let url = '/dist/images/photos/no-image-354x255.png';
        if(PROPERTY_DETAIL.photos[index]){
            url = PROPERTY_DETAIL.photos[index].url;
            url = url.replace('upload','upload/w_354,h_255,c_pad,b_black');
        }
        if(index===0){
            return (<img src={url}/>);
        }
        return (
            <div className="item" onClick={openSlideShow}>
                <img src={url}/>
            </div>
        );
    }
}

export default SectionLeft;
import React from 'react';
import SectionLeft from './SectionLeft';
import SectionRight from './SectionRight';

class Component extends React.Component{

    render(){
        return (
            <div className="tab-pane active overview-section">
                <div className="row">
                    <SectionLeft {...this.props} />
                    <SectionRight {...this.props} />
                </div>
            </div>
        );
    }
}

export default Component;
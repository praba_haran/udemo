import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import classNames from 'classnames';
import Helper from '../../shared/helper';

class Appointments extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: ''
        };
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    canCreateAppointment(){
        let { PROPERTY_DETAIL } = this.props;
        return PROPERTY_DETAIL.recordStatus === 'Active';
    }

    render(){
        let { APPOINTMENTS, addAppointment,editAppointment,changeAppointmentFilter,
            appointmentFilter ,isAppointmentsReloading } = this.props;

        let items = _.filter(APPOINTMENTS,(p)=>{
            let hourMinutes = Helper.getHourMinutesFromTime(p.endTime);
            let date  = moment(p.date).set({
                hour : hourMinutes.hour,
                minute: hourMinutes.minutes,
                second : 0,
                millisecond : 0
            });
            if(appointmentFilter==='Upcoming'){
                return (p.status==='Booked' || p.status==='Rescheduled') && date.toDate() > new Date();
            }
            else if(appointmentFilter === 'Expired'){
                return (p.status==='Booked' || p.status==='Rescheduled') && date.toDate() < new Date();
            }
            return  p.status === 'Cancelled';
        });

        return (
            <div className="clear appointment-list-container">
                {APPOINTMENTS.length>0?(
                        <div className="row">
                            <div className="col-sm-3">
                                <p className="actions">
                                    <span
                                        onClick={addAppointment}
                                        className="label label-outline label-default">
                                        <i className="icon md-plus"></i>
                                        Add Appointment
                                    </span>
                                </p>
                            </div>
                            <div className="col-sm-9">
                                <p className="filters">
                                    <span
                                        onClick={()=>changeAppointmentFilter('Upcoming')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': appointmentFilter==='Upcoming'
                                        })}
                                    >
                                        Upcoming
                                    </span>
                                    <span
                                        onClick={()=>changeAppointmentFilter('Cancelled')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': appointmentFilter==='Cancelled'
                                        })}
                                    >
                                        Cancelled
                                    </span>
                                    <span
                                        onClick={()=>changeAppointmentFilter('Expired')}
                                        className={classNames({
                                            'label label-outline label-default' : true,
                                            'active': appointmentFilter==='Expired'
                                        })}
                                    >
                                        Expired
                                    </span>
                                </p>
                            </div>
                        </div>
                    ):null}
                <ul className="list-group list-group-gap">
                    {items.map((p,index)=>{
                        return (
                            <li key={p._id} className="list-group-item">
                                <div className="viewing">
                                    <a onClick={()=>editAppointment(p)} href="javascript:void(0)" role="button" className="title">
                                        Appointment booked at
                                        <span className="date"> {moment(p.date).format('DD MMM YYYY')} </span>
                                        between <span className="time">{Helper.toTime(p.startTime)} </span>
                                        to <span className="time">{Helper.toTime(p.endTime)}</span>
                                    </a>
                                    <div className="applicants">
                                        {this._renderStatus(p)}
                                        {p.applicants.map((item)=>{
                                            return (
                                                <div key={item._id} className="applicant">
                                                    <p className="name">{item.contact.firstPerson.forename+' '+item.contact.firstPerson.surname}</p>
                                                    <p className="address">{item.contact.address.fullAddress}</p>
                                                </div>
                                            );
                                        })}
                                    </div>
                                    <div className="footer">
                                        <p className="appraiser-date">
                                            <span className="time-ago">
                                                <i className="icon fa-clock-o"></i>
                                                {moment(p.date).fromNow()}
                                            </span>
                                            <span className="details">
                                                Appraiser : {this.getAppraiser(p.appraiser)}
                                                <span className="date">Date : {moment(p.updatedAt).format('DD MMM YYYY h:mm a')}</span>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        );
                    })}
                    {isAppointmentsReloading?(
                            <li className="list-group-item no-item-found">
                                <p className="title">Loading appointments ...</p>
                            </li>
                        ):null}
                    {(APPOINTMENTS.length===0 && !isAppointmentsReloading)?(
                            <li className="list-group-item no-item-found">
                                <p className="title">No appointments found.
                                    {this.canCreateAppointment()?(<a onClick={this.props.addAppointment} href="javascript:void(0)" role="button">Create New</a>):null}
                                </p>
                            </li>
                        ):null}
                </ul>
            </div>
        );
    }

    _renderStatus(appointment){
        let status = null;
        if(appointment.status==='Booked'){
            status = (<span className="label label-primary label-outline pull-right">Booked</span>);
        }
        else if(appointment.status==='Cancelled'){
            status = (<span className="label label-danger label-outline pull-right">Cancelled</span>);
        }
        return status;
    }

    getAppraiser(id){
        let {onChange, APP} = this.props;
        let {agencyDetails} = APP;
        let appraiserName = '';
        if (agencyDetails && agencyDetails.agencyUsers) {
            let appraiser = _.find(agencyDetails.agencyUsers,(p)=>{
                return p._id === id;
            });
            if(appraiser!==undefined){
                appraiserName = appraiser.forename;
            }
        }
        return appraiserName;
    }
}

export default Appointments;
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component'
import PropertyDetail from '../../../models/PropertyDetail';
import Viewing from '../../../models/Viewing';
import Offer from '../../../models/Offer';
import Activity from '../../../models/Activity';
import GeneralAppointment from '../../../models/GeneralAppointment';
import Commands from '../../../models/Commands';
import * as constants from '../../../constants';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            VIEWINGS : [],
            isViewingReloading : false,
            viewingFilter : 'Upcoming',

            APPOINTMENTS : [],
            isAppointmentsReloading : false,
            appointmentFilter : 'Upcoming',

            OFFERS : [],
            isOffersReloading :false,
            isActivityReloading : false,
            ACTIVITIES : [],
            activityFilters : ['Appointments','Viewings','Offers','Others']
        };
    }

    componentDidMount(){
        // load
        this.loadViewings(this.props);
        this.loadAppointments(this.props);
        this.loadOffers(this.props);
        this.loadActivities(this.props);
    }

    componentWillReceiveProps(newProps){
        let { COMMANDS } = newProps;
        if(newProps.params.id!==this.props.params.id){
            this.loadViewings(newProps);
            this.loadAppointments(newProps);
            this.loadOffers(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.BOOK_VIEWING_CHANGED !==this.props.COMMANDS.BOOK_VIEWING_CHANGED && !this.state.isViewingReloading){
            Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, !COMMANDS.BOOK_VIEWING_CHANGED);
            this.loadViewings(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.GENERAL_APPOINTMENT_CHANGED !==this.props.COMMANDS.GENERAL_APPOINTMENT_CHANGED && !this.state.isAppointmentsReloading){
            Commands.actions.change(constants.COMMANDS.GENERAL_APPOINTMENT_CHANGED, !COMMANDS.GENERAL_APPOINTMENT_CHANGED);
            this.loadAppointments(newProps);
            this.loadActivities(newProps);
        }
        else if(COMMANDS.MAKE_AN_OFFER_CHANGED !==this.props.COMMANDS.MAKE_AN_OFFER_CHANGED && !this.state.isOffersReloading){
            Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER_CHANGED, !COMMANDS.MAKE_AN_OFFER_CHANGED);
            this.loadOffers(newProps);
            this.loadActivities(newProps);
        }
    }

    openSlideShow(url){
        Commands.actions.change(constants.COMMANDS.SHOW_PROPERTY_SLIDE_SHOW, true);
    }

    // viewings
    loadViewings(props){
        this.setState({
            isViewingReloading : true
        });
        let params = {
            property : props.params.id
        };
        Viewing.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    VIEWINGS : result.data,
                    isViewingReloading : false
                });
            }
        });
    }
    editViewing(viewing){
        Viewing.actions.setModel(viewing);
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    addViewing(){
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING,true);
    }
    changeViewingFilter(filter){
        this.setState({
            viewingFilter : filter
        });
    }

    // appointments
    loadAppointments(props){
        this.setState({
            isAppointmentsReloading : true
        });
        let params = {
            property : props.params.id
        };
        GeneralAppointment.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    APPOINTMENTS : result.data,
                    isAppointmentsReloading : false
                });
            }
        });
    }
    editAppointment(appointment){
        GeneralAppointment.actions.setModel(appointment);
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    addAppointment(){
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,true);
    }
    changeAppointmentFilter(filter){
        this.setState({
            appointmentFilter : filter
        });
    }
    // offers
    loadOffers(props){
        this.setState({
            isOffersReloading : true
        });
        let params = {
            property : props.params.id
        };
        Offer.api.search(params,(result)=>{
            if(result.success){
                this.setState({
                    OFFERS : result.data,
                    isOffersReloading : false
                });
            }
        });
    }
    editOffer(offer){
        Offer.actions.setModel(offer);
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }
    addOffer(){
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,true);
    }
    // activities
    loadActivities(props){
        this.setState({
            isActivityReloading : true
        });
        let params = {
            property : props.params.id
        };
        Activity.api.list(params,(result)=>{
            if(result.success){
                this.setState({
                    ACTIVITIES : result.data,
                    isActivityReloading : false
                });
            }
        });
    }
    activityFilterChange(filter){
        let { activityFilters } = this.state;
        let index = _.findIndex(activityFilters,(f)=>{
           return f === filter;
        });
        if(index>-1){
            activityFilters = _.filter(activityFilters,(f)=>{
                return f!==filter;
            });
        }
        else{
            activityFilters.push(filter);
        }
        this.setState({
            activityFilters : activityFilters
        });
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                openSlideShow={this.openSlideShow.bind(this)}
                editViewing={this.editViewing.bind(this)}
                addViewing={this.addViewing.bind(this)}
                changeViewingFilter={this.changeViewingFilter.bind(this)}
                addAppointment={this.addAppointment.bind(this)}
                editAppointment={this.editAppointment.bind(this)}
                changeAppointmentFilter={this.changeAppointmentFilter.bind(this)}
                addOffer={this.addOffer.bind(this)}
                editOffer={this.editOffer.bind(this)}
                activityFilterChange = {this.activityFilterChange.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import Helper from '../../../shared/helper';

class Component extends React.Component{

    render(){
        let { MARKET_APPRAISAL } = this.props;
        if(!MARKET_APPRAISAL._id){
            return null;
        }
        return (
            <ul className="list-group list-group-gap market-appraisal-group">
                <li className="list-group-item">
                    <div className="market-appraisal">
                        <a onClick={()=>this.props.edit(MARKET_APPRAISAL)} href="javascript:void(0)" role="button" className="title">
                            Market Appraisal booked at
                            <span className="date"> {moment(MARKET_APPRAISAL.date).format('DD MMM YYYY')} </span>
                            between <span className="time">{Helper.toTime(MARKET_APPRAISAL.startTime)} </span>
                            to <span className="time">{Helper.toTime(MARKET_APPRAISAL.endTime)}</span>
                        </a>
                        <table className="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Type</td>
                                    <td>{MARKET_APPRAISAL.type}</td>
                                </tr>
                                <tr>
                                    <td>Lead Source</td>
                                    <td>{MARKET_APPRAISAL.leadSource}</td>
                                </tr>
                                <tr>
                                    <td>Notes</td>
                                    <td>{MARKET_APPRAISAL.notes}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="footer">
                            <p className="appraiser-date">
                                    <span className="time-ago">
                                        <i className="icon fa-clock-o"></i>
                                        {moment(MARKET_APPRAISAL.startDate).fromNow()}
                                    </span>
                                <span className="details">
                                        Appraiser : {this._getAppraiser()}
                                    <span className="date">Date : {moment(MARKET_APPRAISAL.updatedAt).format('DD MMM YYYY h:mm a')}</span>
                                    </span>
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        );
    }

    _getAppraiser(){
        let { MARKET_APPRAISAL, APP} = this.props;
        let { agencyDetails } = APP;
        let appraiserName = '';
        if(agencyDetails && agencyDetails.agencyUsers) {
            let appraiser = _.find(agencyDetails.agencyUsers,(user)=>{
                return user._id === MARKET_APPRAISAL.appraiser;
            });
            if(appraiser!==undefined){
                appraiserName = (appraiser.forename+' '+appraiser.surname);
            }
        }
        return appraiserName;
    }
}

export default Component;
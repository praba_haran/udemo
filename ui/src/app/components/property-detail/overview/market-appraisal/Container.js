import React from 'react';
import { connect } from 'react-redux';
import store from '../../../../store';
import * as constants from '../../../../constants';
import Commands from '../../../../models/Commands';
import MarketAppraisal from '../../../../models/MarketAppraisal';
import Component from './Component';

class Container extends React.Component{

    edit(model){
        Commands.actions.change(constants.COMMANDS.BOOK_MARKET_APPRAISAL,true);
    }

    componentWillReceiveProps(props){
        let { COMMANDS } = props;
        if(COMMANDS[constants.COMMANDS.MARKET_APPRAISAL_SAVED]){
            Commands.actions.change(constants.COMMANDS.MARKET_APPRAISAL_SAVED,false);
            this.loadMarketAppraisal(props);
        }
        // if user clicked on side menu
        if(props.params.id!==this.props.params.id){
            this.loadMarketAppraisal(props);
        }
    }

    componentDidMount(){
        this.loadMarketAppraisal(this.props);
    }

    loadMarketAppraisal(props){
        let params = {
            propertyId : props.params.id
        };
        MarketAppraisal.api.search(params,(result)=>{
            if(result.success){
                if(result.data.length>0) {
                    MarketAppraisal.actions.setModel(result.data[0]);
                }
                else{
                    MarketAppraisal.actions.clearModel();
                }
            }
        });
    }

    render () {
        return(
            <Component
                edit ={this.edit.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        MARKET_APPRAISAL : store.MARKET_APPRAISAL.toJS()
    };
};

export default connect(mapStateToProps)(Container);
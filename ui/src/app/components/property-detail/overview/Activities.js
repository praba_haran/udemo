import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import classNames from 'classnames';
import Helper from '../../shared/helper';

class Activities extends React.Component{

    render(){
        let { ACTIVITIES,isActivityReloading,activityFilters,activityFilterChange } = this.props;
        let filteredActivities = [];
        _.each(ACTIVITIES,(p)=>{
            if(activityFilters.indexOf('Appointments')>-1 && p.appointment){
                filteredActivities.push(p);
            }
            else if(activityFilters.indexOf('Viewings')>-1 && p.viewing){
                filteredActivities.push(p);
            }
            else if(activityFilters.indexOf('Offers')>-1 && p.offer){
                filteredActivities.push(p);
            }
            else if(activityFilters.indexOf('Others')>-1 && p.offer){
                filteredActivities.push(p);
            }
        });
        return (
            <div className="activity-container">
                {(!isActivityReloading && ACTIVITIES.length>0)?(
                        <p className="filters">
                            <span
                                onClick={()=>activityFilterChange('Appointments')}
                                className={classNames({
                                    'label label-outline label-default' : true,
                                    'active': activityFilters.indexOf('Appointments')>-1
                                })}
                            >
                                Appointments
                                <i className="icon md-check"></i>
                            </span>
                            <span
                                onClick={()=>activityFilterChange('Viewings')}
                                className={classNames({
                                    'label label-outline label-default' : true,
                                    'active': activityFilters.indexOf('Viewings')>-1
                                })}
                            >
                                Viewings
                                <i className="icon md-check"></i>
                            </span>
                            <span
                                onClick={()=>activityFilterChange('Offers')}
                                className={classNames({
                                    'label label-outline label-default' : true,
                                    'active': activityFilters.indexOf('Offers')>-1
                                })}
                            >
                                Offers
                                <i className="icon md-check"></i>
                            </span>
                            <span
                                onClick={()=>activityFilterChange('Others')}
                                className={classNames({
                                    'label label-outline label-default' : true,
                                    'active': activityFilters.indexOf('Others')>-1
                                })}
                            >
                                Others
                                <i className="icon md-check"></i>
                            </span>
                        </p>
                    ):null}
                <ul className="list-group list-group-gap">
                    {filteredActivities.map((p)=>{
                        if(p.appointment) {
                            return this._renderAppointment(p);
                        }
                        else if(p.viewing){
                            return this._renderViewing(p);
                        }
                    })}
                    {isActivityReloading?(
                            <li className="list-group-item no-item-found">
                                <p className="title">Loading activities ...</p>
                            </li>
                        ):null}
                    {(filteredActivities.length===0 && !isActivityReloading)?(
                            <li className="list-group-item no-item-found">
                                <p className="title">
                                    No activities found.
                                </p>
                            </li>
                        ):null}
                </ul>
            </div>
        );
    }

    _renderAppointment(activity){
        let appointment  = activity.appointment_ref;
        return (
            <li key={activity._id} className="list-group-item">
                <div className="activity">
                    <p className="title">
                        Appointment at
                        <span className="date"> {moment(appointment.date).format('DD MMM YYYY')} </span>
                        <span className="time">{Helper.toTime(appointment.startTime)}</span>
                        <i className="icon fa-long-arrow-right"></i>
                        {this._getApplicants(appointment.applicants)}
                    </p>
                    <div className="footer">
                        <p>
                            <span className="time-ago">
                                <i className="icon fa-clock-o"></i>
                                {moment(appointment.date).fromNow()}
                            </span>
                            <span className="details">
                                <span className="appraiser">Appraiser : {appointment.appraiser.forename}</span>
                                <span className="status-text">Status : {appointment.status}</span>
                            </span>
                        </p>
                    </div>
                </div>
            </li>
        );
    }

    _renderViewing(activity){
        let viewing  = activity.viewing_ref;
        return (
            <li key={activity._id} className="list-group-item">
                <div className="activity">
                    <p className="title">
                        Viewing at
                        <span className="date"> {moment(viewing.date).format('DD MMM YYYY')} </span>
                        <span className="time">{Helper.toTime(viewing.startTime)} </span>
                        <i className="icon fa-long-arrow-right"></i>
                        {this._getApplicants(viewing.applicants)}
                    </p>
                    <div className="footer">
                        <p>
                            <span className="time-ago">
                                <i className="icon fa-clock-o"></i>
                                {moment(viewing.startDate).fromNow()}
                            </span>
                            <span className="details">
                                <span className="appraiser">Appraiser : {viewing.appraiser.forename}</span>
                                <span className="status-text">Status : {viewing.status}</span>
                            </span>
                        </p>
                    </div>
                </div>
            </li>
        );
    }

    _getApplicants(applicants){
        let isLast = function(index){
            return (applicants.length-1) === index;
        };
        return (
            <span className="applicants">
                {applicants.map((item,index)=>{
                    return (
                        <span key={item._id}>
                            {(!isLast(index) && index!==0)?' , ':''}
                            {(isLast(index) && index!==0)?' and ':''}
                            <a className="applicant">{item.contact.firstPerson.forename+' '+item.contact.firstPerson.surname}</a>
                        </span>
                    );
                })}
            </span>
        );
    }
}

export default Activities;
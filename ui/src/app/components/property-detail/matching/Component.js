import React from 'react';
import _ from 'lodash';
import Filter from './partials/Filter';
import MatchingList from './partials/MatchingList';

class Component extends React.Component{

    render () {
        let { MATCHING,addMatchingList,chooseMatchingList} = this.props;
        let shortListCount = 0;
        return (
            <div className="tab-pane active property-matching">
                <div className="row">
                    <div className="col-md-4">
                        <div className="label-manager">
                            {MATCHING.matching.map((p,index)=>{
                                let cssClass = 'label label-outline label-default';
                                if(MATCHING.selectedId === p._clientId){
                                    cssClass+=' active';
                                }
                                return (
                                    <span key={p.name} onClick={()=>{ chooseMatchingList(p,p._clientId) }} className={cssClass}>{p.name?p.name:'List '+(index+1)}</span>
                                );
                            })}
                            {(MATCHING.matching.length<5)? (
                                    <span className="label label-outline label-default" onClick={addMatchingList}>
                                        <i className="icon md-plus"></i>
                                    </span>
                                ):null}
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="label-manager align-right">
                            <span className="label label-outline label-default short-list-btn">
                                <i className="icon md-favorite-outline"></i>
                                Shortlisted
                                {shortListCount>0?(<span className="badge badge-radius badge-default">{shortListCount}</span>):null}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <Filter {...this.props} />
                    <MatchingList {...this.props} />
                </div>
            </div>
        );
    }
}

export default Component;
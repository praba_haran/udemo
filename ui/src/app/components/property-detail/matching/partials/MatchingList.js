import React from 'react';
import ListItem from './MatchingListItem';
import _ from 'lodash';
import classNames from 'classnames';

export default class MatchingList extends React.Component {

    render() {
        let {
            MATCHING,
            goToContact,
            pageIndex,
            pageSize,
            nextPage,
            prevPage
        }= this.props;

        let { matches } = MATCHING;
        let noMatchFound;
        if(matches.length===0){
            noMatchFound = (
                <li className="list-group-item no-item-found">
                    <p className="img-container"><img src="/dist/images/photos/no-matches-found.png" /></p>
                    <h4 className="text">No matches to see here ...</h4>
                </li>
            );
        }
        let matchList = _(matches).slice((pageIndex-1)*pageSize).take(pageSize).value();

        return (
            <div className="col-lg-8">
                <div className="matching-body">
                    <div className="header">
                        <div className="row">
                            <div className="col-md-6 col-xs-8">
                                <span className="help-block">{matches.length} Matches found</span>
                            </div>
                            <div className="col-md-6 col-xs-4">
                                <div className="label-group pull-right">
                                    <span onClick={prevPage} className={classNames({
                                        'label label-outline label-default':true,
                                        'disabled':pageIndex===1
                                    })}>
                                        <i className="icon fa-angle-left"></i>
                                    </span>
                                    <span onClick={nextPage} className={classNames({
                                        'label label-outline label-default':true,
                                        'disabled':(matches.length/pageSize)<pageIndex
                                    })}>
                                        <i className="icon fa-angle-right"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <ul className="list-group list-group-dividered property-list">
                                {matchList.map((contact)=>{
                                    return (
                                        <ListItem goToContact={goToContact} contact={contact} key={contact._id}/>
                                    );
                                })}
                                {noMatchFound}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
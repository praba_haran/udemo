import React from 'react';
import _ from 'lodash';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import { TilePicker, TextBox,CheckBox } from '../../../shared/Form';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Filter extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: 'matchOn',
            essentialIndex: 0
        };
        this.handleSelect = this.handleSelect.bind(this);
        this.onEssentialChange = this.onEssentialChange.bind(this);
        this.getSelectedCount = this.getSelectedCount.bind(this);
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    onEssentialChange(index){
        this.setState({
            essentialIndex : index
        });
    }

    getSelectedCount(category){
        let selectedItems = _.filter(category.values,function(p){
            return p.selected;
        });
        return selectedItems.length;
    }

    render (){
        let { model,MATCHING , onMatchingChange, saveMatching,removeMatchingList,onEssentialItemChange } = this.props;
        let index = _.findIndex(MATCHING.matching,(p)=>{
           return p._clientId === MATCHING.selectedId;
        });
        index = (index<0)?0:index;
        let essentialCategory = MATCHING.matching[index].essentials[this.state.essentialIndex];
        return (
            <div className="col-lg-4">
                <div className="matching-filter">
                    <div className="header">
                        <div className="row">
                            <div className="col-md-8 col-xs-8">
                                <span className="help-block">
                                    <TextBox model={MATCHING} name={'matching['+index+'].name'} onChange={onMatchingChange} />
                                </span>
                            </div>
                            <div className="col-md-4 col-xs-4 text-right">
                                <button onClick={saveMatching} className="btn btn-xs margin-right-5">
                                    <i className="icon fa-check"></i>
                                </button>
                                {(MATCHING.matching[index] && MATCHING.matching[index]._id)?(
                                        <button onClick={()=>removeMatchingList(MATCHING.matching[index])} className="btn btn-xs">
                                            <i className="icon fa-trash"></i>
                                        </button>
                                    ):null}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <PanelGroup bsClass="panel-group panel-group-continuous" activeKey={this.state.activeKey} onSelect={this.handleSelect} accordion>
                            <Panel header="Match On" eventKey="matchOn">
                                <div className="tile-manager">
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchPropertyType'} onChange={onMatchingChange} tileHeading="Property Type" type="toggle" tileOptions="Yes,No" />
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchBedrooms'} onChange={onMatchingChange} tileHeading="Bedrooms" type="toggle" tileOptions="Yes,No" />
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchBathrooms'} onChange={onMatchingChange} tileHeading="Bathrooms" type="toggle" tileOptions="Yes,No" />
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchReceptions'} onChange={onMatchingChange} tileHeading="Receptions" type="toggle" tileOptions="Yes,No" />
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchParking'} onChange={onMatchingChange} tileHeading="Parking" type="toggle" tileOptions="Yes,No" />
                                    <TilePicker model={MATCHING} name={'matching['+index+'].matchFeatures'} onChange={onMatchingChange} tileHeading="Features" type="toggle" tileOptions="Yes,No" />
                                </div>
                            </Panel>
                            <Panel header="Essentials" eventKey="essentials">
                                <Scrollbars
                                    style={{height:300}}
                                    autoHide
                                    autoHideTimeout={1000}
                                    autoHideDuration={200}
                                >
                                    <div className="clear">
                                        <div className="col-sm-5 features-list-group">
                                            <div className="list-group">
                                                {MATCHING.matching[index].essentials.map((p,fIndex)=>{
                                                    let cssClass= 'list-group-item waves-effect waves-block waves-classic';
                                                    if(fIndex===this.state.essentialIndex){
                                                        cssClass+=' selected';
                                                    }
                                                    let count = this.getSelectedCount(MATCHING.matching[index].essentials[fIndex]);
                                                    return (
                                                        <a onClick={()=>this.onEssentialChange(fIndex)} key={'type_'+fIndex} className={cssClass}>
                                                            {p.key}
                                                            {count>0?(<span className="badge badge-default">{count}</span>):null}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                        <div className="col-sm-7 features-list-group-details">
                                            <div className="list-group">
                                                {essentialCategory.values.map((p,itemIndex)=>{
                                                    let name = 'matching['+index+'].essentials['+this.state.essentialIndex+'].values['+itemIndex+'].selected';
                                                    return (
                                                        <a onClick={()=>onEssentialItemChange(name,p.selected)} key={name} className="list-group-item waves-effect waves-block waves-classic">
                                                            {p.selected?(<i className="icon md-check-square"></i>):(<i className="icon md-square-o"></i>)}
                                                            {p.name}
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </Scrollbars>
                            </Panel>
                            <Panel header="Filters" eventKey="filters">
                                <Scrollbars
                                    style={{height:300}}
                                    autoHide
                                    autoHideTimeout={1000}
                                    autoHideDuration={200}
                                >
                                    <div className="clear">
                                        {MATCHING.matching[index].filters.map((p,pIndex)=>{
                                            let name = 'matching['+index+'].filters['+pIndex+'].selected';
                                            return (
                                                <span key={name} className="checkbox-custom checkbox-default">
                                                    <CheckBox model={MATCHING} name={name} label={p.name} onChange={onMatchingChange} />
                                                </span>
                                            );
                                        })}
                                    </div>
                                </Scrollbars>
                            </Panel>
                        </PanelGroup>
                    </div>
                </div>
            </div>
        );
    }
}
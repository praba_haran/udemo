import React from 'react';
import {Link} from "react-router";

export default class MatchingListItem extends React.Component{

    render(){
        let { contact, goToContact } = this.props;
        return (
            <li className="list-group-item">
                <div className="media">
                    <div className="media-left">
                        <Link to={'/contacts/'+contact._id+'/overview'} className="avatar">
                            {contact.firstPerson.forename[0]+contact.firstPerson.surname[0]}
                        </Link>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading" onClick={()=> goToContact(contact._id)}>{contact.firstPerson.title+' '+contact.firstPerson.forename+' '+contact.firstPerson.surname}</h4>
                        <p className="address" onClick={()=> goToContact(contact._id)}>{contact.address.fullAddress}</p>
                        <p className="email-phone">
                            <Link to={'/contacts/'+contact._id+'/overview'} className="margin-right-15">
                                <i className="icon fa-envelope-o"></i>
                                {contact.firstPerson.emails[0].email}
                            </Link>
                            <Link to={'/contacts/'+contact._id+'/overview'}>
                                <i className="icon fa-mobile-phone"></i>
                                {contact.firstPerson.telephones[0].number}
                            </Link>
                        </p>
                        {(contact.secondPerson.emails.length>0
                            && contact.secondPerson.telephones.length>0
                            && contact.secondPerson.emails[0].email
                            && contact.secondPerson.telephones[0].number
                            )?(
                                <p className="email-phone">
                                    <a href="#" className="margin-right-15">
                                        <i className="icon fa-envelope-o"></i>
                                        {contact.secondPerson.emails[0].email}
                                    </a>
                                    <a href="#">
                                        <i className="icon fa-mobile-phone"></i>
                                        {contact.secondPerson.telephones[0].number}
                                    </a>
                                </p>
                            ):null}
                        <div className="row footer-summary">
                            <div className="col-md-8">
                                <p className="action-buttons">
                                    <a className="margin-right-10 label label-outline label-default">
                                        0 Matches
                                    </a>
                                    <a className="margin-right-10 label label-outline label-default">
                                        0 Viewings
                                    </a>
                                    <a className="margin-right-10 label label-outline label-default">
                                        0 Offers
                                    </a>
                                </p>
                            </div>
                            <div className="col-md-4">
                                <small>
                                    <span className="help-block margin-right-10">Last Contacted By: {contact.negotiator.forename}</span>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}
import React from 'react';
import _ from 'lodash';
import {hashHistory} from 'react-router';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import Matching from '../../../models/Matching';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            pageIndex : 1,
            pageSize : 10
        };
    }

    nextPage(){
        let { MATCHING } = this.props;
        let { matches } =  MATCHING;
        let pageIndex = this.state.pageIndex+1;
        if((matches.length/this.state.pageSize)>0){
            this.setState({
                pageIndex: pageIndex
            });
        }
    }
    prevPage(){
        if(this.state.pageIndex>1) {
            this.setState({
                pageIndex: --this.state.pageIndex
            });
        }
    }

    onMatchingChange(e){
        Matching.actions.valueChange(e);
        let list = this.getCurrentMatchingList();
        Matching.api.getMatches(this.props.params.id,list);
    }
    onEssentialItemChange(name,selected){
        Matching.actions.change(name,!selected);
    }
    getCurrentMatchingList(){
        let MATCHING = store.getState().MATCHING.toJS();
        let { matching, selectedId } = MATCHING;
        return _.find(matching,function (p) {
            return p._clientId ===  selectedId;
        });
    }
    addMatchingList(){
        let list = this.getCurrentMatchingList();
        if(list && list._id){
            Matching.api.addList();
        }
        else{
            Alert.clearLogs().error('Please save '+list.name+' and add another.')
        }
    }
    removeMatchingList(list){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove the list named "'+list.name+'".', ()=>{
            Matching.api.remove(this.props.params.id,list);
        },()=> {
            // user clicked "cancel"
        });
    }
    chooseMatchingList(matching,clientId){
        Matching.actions.change('selectedId',clientId);
        Matching.api.getMatches(this.props.params.id,matching);
    }
    saveMatching(){
        let matching = this.getCurrentMatchingList();
        if(matching){
            Matching.api.save(this.props.params.id,matching);
        }
    }
    goToContact(id){
        hashHistory.push('/contacts/'+id+'/overview');
    }

    render(){
        return (
            <Component
                removeMatchingList={this.removeMatchingList.bind(this)}
                onMatchingChange={this.onMatchingChange.bind(this)}
                onEssentialItemChange={this.onEssentialItemChange.bind(this)}
                chooseMatchingList={this.chooseMatchingList.bind(this)}
                saveMatching={this.saveMatching.bind(this)}
                addMatchingList={this.addMatchingList.bind(this)}
                goToContact={this.goToContact.bind(this)}
                nextPage={this.nextPage.bind(this)}
                prevPage={this.prevPage.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        MATCHING : store.MATCHING.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
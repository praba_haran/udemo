import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import axiosStatic from '../../../api/axios-static';
import PropertyDetail from '../../../models/PropertyDetail';
import Commands from '../../../models/Commands';
import * as constants from '../../../constants';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            canShowModal : false
        }
    }
    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.SHOW_BROCHURE_SETUP]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.SHOW_BROCHURE_SETUP]
            });
        }
    }
    cancel(){
        Commands.actions.change(constants.COMMANDS.SHOW_BROCHURE_SETUP,false);
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                cancel={this.cancel.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import axiosStatic from '../../../api/axios-static';
import * as constants from '../../../constants';
import Commands from '../../../models/Commands';

class Component extends React.Component{

    constructor(){
        super();
        this.state = {
            previewUrl : null
        };
    }

    onEnter(){
        setTimeout(()=>{
            let { model,APP} = this.props;
            let { userInfo } = APP;
            if(userInfo && userInfo.agency) {
                let {brochures} = userInfo.agency.settings;
                if(brochures.length>0) {
                    $('#cascade-slider').cascadeSlider({
                        onChange: (index)=>{
                            this.setState({
                                previewUrl : axiosStatic.getBrochureUrl('doc/brochure/'+model._id+'/'+brochures[index].template)
                            });
                        }
                    });
                }
            }
        },300);
    }

    render(){
        let { model,canShowModal,cancel } = this.props;
        return (
            <Modal bsSize="large" onEnter={this.onEnter.bind(this)} show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal brochure-setup-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Brochure Setup</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="cascade-slider_container" id="cascade-slider">
                                {this._renderTemplates()}
                                <span className="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev">
                                    <i className="icon fa-angle-left"></i>
                                </span>
                                <span className="cascade-slider_arrow cascade-slider_arrow-right" data-action="next">
                                    <i className="icon fa-angle-right"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.cancel}>Cancel</Button>
                    <a href={this.state.previewUrl} target="_blank" className="btn btn-primary">Preview</a>
                </Modal.Footer>
            </Modal>
        );
    }

    _renderTemplates(){
        let { model,APP} = this.props;
        let {userInfo} = APP;
        if(userInfo && userInfo.agency){
            let { brochures } = userInfo.agency.settings;
            brochures = _.sortBy(brochures,[function(o) {
                return !o.isDefault;
            }]);
            return (
                <div className="cascade-slider_slides">
                    {brochures.map((p)=>{
                        let url = axiosStatic.getImageUrl('doc/preview/'+p.template);
                        return (
                            <div key={p.template} className="cascade-slider_item">
                                <h3 className="title">{p.template}</h3>
                                <div className="image-container">
                                    <img src={url}/>
                                    {p.isDefault?(
                                        <div className="ribbon ribbon-badge ribbon-warning">
                                            <span className="ribbon-inner">Default</span>
                                        </div>
                                    ):null}
                                </div>
                            </div>
                        );
                    })}
                </div>
            );
        }
    }
}

export default Component;
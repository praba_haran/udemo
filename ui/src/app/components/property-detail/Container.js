import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import store from '../../store';
import Alert from '../../lib/Alert';
import PropertyDetail from '../../models/PropertyDetail';
import PropertyListing from '../../models/PropertyListing';
import Matching from '../../models/Matching';
import Commands from '../../models/Commands';
import Component from './Property';
import * as constants from '../../constants';

class Container extends React.Component{

    constructor(){
        super();
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            isPropertyReloading : false
        }
    }

    getPropertyById(props){
        this.setState({
            isPropertyReloading : true
        });
        PropertyDetail.api.get(props.params.id,(result)=>{
            if(result.success){
                PropertyDetail.actions.setModel(result.data);
                // WE ARE SHOWING MATCHES IN DASHBOARD, SO WE NEED TO CALL MATCHES HERE
                Matching.api.getList(props.params.id);
            }
            this.setState({
                isPropertyReloading : false
            });
        });
    }

    componentDidMount(){
        this.getPropertyById(this.props);
    }

    componentWillReceiveProps(newProps){
        let { COMMANDS } = newProps;
        if(this.props.params.id!==newProps.params.id){
            this.getPropertyById(newProps);
        }
        else if(COMMANDS.PROPERTY_UPDATED !== this.props.COMMANDS.PROPERTY_UPDATED && !this.state.isPropertyReloading){
            Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED, !COMMANDS.PROPERTY_UPDATED);
            this.getPropertyById(newProps);
        }
        else if(COMMANDS.ARCHIVE_PROPERTY){
            Commands.actions.change(constants.COMMANDS.ARCHIVE_PROPERTY, !COMMANDS.ARCHIVE_PROPERTY);
            this.archive();
        }
        else if(COMMANDS.ACTIVATE_PROPERTY){
            Commands.actions.change(constants.COMMANDS.ACTIVATE_PROPERTY, !COMMANDS.ACTIVATE_PROPERTY);
            this.activate();
        }
    }

    onSubmit(e) {
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    archive(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to archive this property ?', ()=>{
            let { params } = this.props;
            PropertyListing.api.archiveItems([params.id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.getPropertyById(this.props);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        });
    }

    activate(){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to activate this property ?', ()=>{
            let { params } = this.props;
            PropertyListing.api.activateItems([params.id],(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                    this.getPropertyById(this.props);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        });
    }

    render () {
        return(
            <Component
                onChange={PropertyDetail.actions.valueChange}
                onSubmit={this.onSubmit}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    let app = store.APP.toJS();
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        MATCHING : store.MATCHING.toJS()
    };
};

export default connect(mapStateToProps)(Container);
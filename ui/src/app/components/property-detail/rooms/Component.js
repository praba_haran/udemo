import React from 'react';
import _ from 'lodash';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import PhotosModal from './PhotosModal';
import ActionButtonGroup from '../helper/ActionButtonGroup';
import PropertyDetail from '../../../models/PropertyDetail';
import {Link} from "react-router";
import {ValidateTextArea, ValidateText} from '../../shared/Form';

class Component extends React.Component{

    render () {
        let { model,onSubmit, onChange ,isProcessing } = this.props;
        let rooms =  model.rooms;
        return (
            <div className="tab-pane active property-rooms">
                <div className="row">
                    <div className="col-sm-8">
                        {model.photos.length===0?(
                                <p className="text-warning">No photos are attached to this property.</p>
                            ):null}
                    </div>
                    <div className="col-sm-4">
                        <p className="text-right">
                            <button disabled={isProcessing} onClick={this.props.addRoom} type="button" className="btn btn-sm margin-right-5">
                                <i className="icon md-plus" aria-hidden="true"></i>
                                <span className="text hidden-xs">Add Room</span>
                            </button>
                            <button disabled={isProcessing} onClick={onSubmit} type="button" className="btn btn-sm btn-primary">
                                <i className="icon md-check" aria-hidden="true"></i>
                                <span className="text hidden-xs">{isProcessing?'Saving ...':'Save'}</span>
                            </button>
                        </p>
                    </div>
                </div>
                <PanelGroup bsClass="panel-group panel-group-continuous" activeKey={this.props.roomIndex} onSelect={this.props.onIndexChange} accordion>
                    {rooms.map((room,index)=>{
                        return (
                            <Panel key={'room_index_'+index} header={room.name || ('Room ' + (index + 1))} eventKey={index}>
                                <form className="form-horizontal custom-form">
                                    <div className="row">
                                        <div className="col-lg-8">
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Name</label>
                                                <ValidateText className="col-sm-5" name={'rooms[' + index + '].name'}
                                                              model={model} onChange={onChange}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Size (meter)</label>
                                                <ValidateText className="col-sm-5" name={'rooms[' + index + '].sizeInMeter'}
                                                              model={model} onChange={onChange} type="positiveNumber"
                                                              onChangeCallback={this.calculateSizeInFeet.bind(this)}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Size (feet)</label>
                                                <ValidateText className="col-sm-5" name={'rooms[' + index + '].sizeInFeet'}
                                                              model={model} onChange={onChange} type="positiveNumber"
                                                              onChangeCallback={this.calculateSizeInMeter.bind(this)}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Description</label>
                                                <ValidateTextArea className="col-sm-9" rows="8" model={model}
                                                                  name={'rooms[' + index + '].description'}
                                                                  placeholder="Room description here..."
                                                                  onChange={onChange}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                {room._id?(
                                                        <div className="col-sm-offset-3 col-sm-9">
                                                            <button type="button" disabled={isProcessing} onClick={()=>this.props.removeRoom(index)} className="btn btn-sm btn-danger">Remove</button>
                                                        </div>
                                                    ):null}
                                            </div>
                                        </div>
                                        <div className="col-lg-4">
                                            <h5>ROOM PHOTOS</h5>
                                            <ul className="blocks blocks-100 blocks-lg-3 blocks-md-3 blocks-sm-3 blocks-xs-3">
                                                {this._renderPhoto(room.photos[0])}
                                                {this._renderPhoto(room.photos[1])}
                                                {this._renderPhoto(room.photos[2])}
                                                {this._renderPhoto(room.photos[3])}
                                                {this._renderPhoto(room.photos[4])}
                                                {this._renderPhoto(room.photos[5])}
                                            </ul>
                                            {this._renderMorePhotosLink(room,index)}
                                        </div>
                                    </div>
                                </form>
                            </Panel>
                        );
                    })}
                </PanelGroup>
                {this.props.canShowModal?(
                    <PhotosModal
                        property={model}
                        canShowModal={this.props.canShowModal}
                        linkPhotos={this.props.linkPhotos}
                        roomIndex={this.props.roomIndex}
                        cancel={this.props.cancel}
                    />
                ):null}
            </div>
        );
    }

    _renderPhoto(photo){
        if(photo){
            let thumbUrl = photo.url.replace('upload','upload/w_354,h_255,c_pad,b_black');
            return (
                <li className="room-photo-item">
                    <div className="panel">
                        <figure
                            className="overlay overlay-hover animation-hover">
                            <img className="overlay-figure" src={thumbUrl}/>
                            <figcaption
                                className="overlay-panel overlay-background overlay-fade text-center vertical-align">
                                <div className="btn-group btn-group-flat">
                                    <button onClick={(e) => this.props.removeLinkedPhoto(photo)} type="button"
                                            className="btn btn-icon btn-pure btn-default waves-effect waves-classic"
                                            title="Remove">
                                        <i className="icon fa-trash"
                                           aria-hidden="true"></i>
                                    </button>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </li>
            );
        }
        return (
            <li className="room-photo-item">
                <div className="panel">
                    <figure className="overlay overlay-hover animation-hover">
                        <img className="overlay-figure" src="/dist/images/photos/no-image-354x255.png"/>
                    </figure>
                </div>
            </li>
        );
    }

    _renderMorePhotosLink(room,index){
        let {model, openModal } = this.props;
        if(room.photos.length>0 && room.photos.length<6){
            return(
                <div className="clear no-photos-linked-message">
                    <a className="label label-primary label-outline" onClick={(e) => openModal(index)} href="javascript:void(0)">Link more photos</a>
                </div>
            );
        }
        else if(room.photos.length>0 && room.photos.length>=6){
            return(
                <div className="clear">
                    <p className="text-warning">Maximum of 6 photos allowed.</p>
                </div>
            );
        }
        else if(model.photos.length>0){
            return (
                <div className="clear">
                    <a className="label label-primary label-outline" onClick={(e) => openModal(index)} href="javascript:void(0)">Link photos</a>
                </div>
            );
        }
        return (
            <div className="clear">
                <Link className="label label-primary label-outline" to={'/properties/' + model._id + '/media'}>Upload photos</Link>
            </div>
        );
    }

    calculateSizeInFeet(e) {
        let value = e.target.value;
        let feet = Number(value) * 3.2808;
        if (!isNaN(feet)) {
            let name = e.target.name.replace('sizeInMeter', 'sizeInFeet');
            PropertyDetail.actions.change(name, feet.toFixed(2));
        }
    }

    calculateSizeInMeter(e) {
        let value = e.target.value;
        let meter = Number(value) / 3.2808;
        if (!isNaN(meter)) {
            let name = e.target.name.replace('sizeInFeet', 'sizeInMeter');
            PropertyDetail.actions.change(name, meter.toFixed(2));
        }
    }
}

export default Component;
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import PropertyDetail from '../../../models/PropertyDetail';
import Commands from '../../../models/Commands';
import * as constants from '../../../constants';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            canShowModal : false,
            roomIndex : 0,
            isProcessing : false
        };
    }

    componentDidMount(){
        this.setDefaultRoom();
    }

    componentWillUpdate(){
        this.setDefaultRoom();
    }

    setDefaultRoom(){
        let { model } = this.props;
        if(model._id && (!model.rooms || model.rooms.length===0)){
            PropertyDetail.actions.setObjectInPath(['rooms'],[this.getRoom()]);
        }
    }

    addRoom(){
        let { model } = this.props;
        let index =  model.rooms.length;
        if(model._id){
            PropertyDetail.actions.setObjectInPath(['rooms',index],this.getRoom());
            this.onIndexChange(index);
        }
    }

    removeRoom(index){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this room?', ()=>{
            PropertyDetail.actions.removeObjectInPath(['rooms',index]);
            this.onIndexChange(0);
            this.onSubmit('Room removed successfully.');
        },()=> {
            // user clicked "cancel"
        });
    }

    onIndexChange(index){
        this.setState({
            roomIndex : index
        });
    }

    getRoom(){
        let { model } = this.props;
        let index =  model.rooms.length;
        return {
            name :'Room '+(++index),
            sizeInMeter : 0,
            sizeInFeet : 0,
            description:'',
            photos:[]
        };
    }

    openModal(index){
        this.setState({
            canShowModal : true,
            roomIndex : index
        });
    }

    linkPhotos(photos){
        PropertyDetail.actions.setObjectInPath(['rooms',this.state.roomIndex,'photos'],photos);
    }

    removeLinkedPhoto(photo){
        let { model } = this.props;
        let room = model.rooms[this.state.roomIndex];
        if(room) {
            Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this photo?', ()=>{
                let photos = _.filter(room.photos,(r)=>{
                    return r.mediaId !==photo.mediaId;
                });
                PropertyDetail.actions.setObjectInPath(['rooms',this.state.roomIndex,'photos'],photos);
            },()=> {
                // user clicked "cancel"
            });
        }
    }

    onSubmit() {
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success('Rooms saved successfully.');
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing : false
                });
            });
        }
    }

    cancel() {
        this.setState({
            canShowModal : false
        });
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                removeLinkedPhoto={this.removeLinkedPhoto.bind(this)}
                openModal={this.openModal.bind(this)}
                linkPhotos={this.linkPhotos.bind(this)}
                cancel={this.cancel.bind(this)}
                onIndexChange={this.onIndexChange.bind(this)}
                addRoom={this.addRoom.bind(this)}
                removeRoom={this.removeRoom.bind(this)}
                onSubmit={this.onSubmit.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

class PhotosModal extends React.Component{

    constructor(){
        super();
        this.selectPhoto = this.selectPhoto.bind(this);
        this.state = {
            selectedItems : [],
            error : null
        };
    }

    componentDidMount(){
        let { property, roomIndex } = this.props;
        this.setState({
            selectedItems : _.cloneDeep(property.rooms[roomIndex].photos)
        });
    }

    isSelected(item){
        let oldItem = _.find(this.state.selectedItems,(p)=>{
            return item.id === p.mediaId;
        });
        return oldItem!==undefined;
    }

    selectPhoto(item){
        let { selectedItems } =  this.state;
        this.setState({
            error : null
        });
        if(!this.isSelected(item)){
            if(selectedItems.length<6) {
                selectedItems.push({
                    mediaId: item.id,
                    url: item.url
                });
            }
            else{
                this.setState({
                    error : 'Maximum of 6 photos can be linked to a room.'
                });
            }
        }
        else{
            selectedItems = _.filter(selectedItems,(p)=>{
                return p.mediaId!==item.id;
            });
        }
        this.setState({
            selectedItems : selectedItems
        });
    }

    onClickLink(e){
        let { linkPhotos,cancel} = this.props;
        linkPhotos(this.state.selectedItems);
        cancel();
    }

    cancel(){
        let { cancel} = this.props;
        this.setState({
            selectedItems : []
        });
        cancel();
    }

    render(){
        let { canShowModal ,property } = this.props;
        return (
            <Modal bsSize="lg" show={canShowModal} onHide={this.cancel.bind(this)} dialogClassName="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Choose Photos</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p className="text-danger text-center">{this.state.error}</p>
                    <div className="row">
                        <div className="col-sm-12 choose-photos-modal">
                            <ul className="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">
                                {property.photos.map((p)=>{
                                    let thumbUrl = p.url.replace('upload','upload/w_354,h_255,c_pad,b_black');
                                    let selected = this.isSelected(p);
                                    return (
                                        <li key={p.id} className="gallery-item">
                                            <div className="panel" onClick={(e)=>{this.selectPhoto(p)}}>
                                                <figure className="overlay overlay-hover animation-hover">
                                                    <img className="overlay-figure" src={thumbUrl} alt={p.title} />
                                                </figure>
                                                {selected?(
                                                    <div className="ribbon ribbon-corner ribbon-primary">
                                                        <span className="ribbon-inner">
                                                            <i className="icon fa-check-square-o" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                ):null}
                                            </div>
                                        </li>
                                    );
                                },this)}
                            </ul>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.cancel.bind(this)}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={this.onClickLink.bind(this)} disabled={this.state.selectedItems.length===0 && this.state.selectedItems.length>6}>Link to Room</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default PhotosModal;
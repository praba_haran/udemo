import React from 'react';

class NoExternalBrochure extends React.Component{

    render() {
        let { items } = this.props;
        if(items.length===0) {
            return (
                <div className="clear no-item-found">
                    <p className="img-container"><img src="/dist/images/photos/no-matches-found.png"/></p>
                    <h4 className="text">No links to see here ...</h4>
                </div>
            );
        }
        return null;
    }
}

export default NoExternalBrochure;
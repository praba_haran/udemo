import React from 'react';
import ReactDOM from 'react-dom';
import { TextBox } from '../../../shared/Form';

class Photo extends React.Component{

    constructor(){
        super();
        this.editTitle = this.editTitle.bind(this);
        this.saveTitle = this.saveTitle.bind(this);
        this.state = {
            canEditTitle : false
        };
    }

    editTitle(){
        this.setState({
            canEditTitle:true
        });
    }

    saveTitle(){
        let {submit,photo} = this.props;
        this.setState({
            canEditTitle:false
        });
        if(photo.title){
            submit('Photo saved successfully.');
        }
    }

    render() {
        let { photo,index,model,onChange,remove } = this.props;
        let thumbUrl = photo.url.replace('upload','upload/w_354,h_255,c_pad,b_black');
        setTimeout(()=>{
            if(this.state.canEditTitle){
                ReactDOM.findDOMNode(this.refs.title).focus();
            }
        },300);
        return (
            <li className="gallery-item">
                <div className="panel">
                    <figure className="overlay overlay-hover animation-hover">
                        <img className="overlay-figure" src={thumbUrl} alt={photo.title} />
                        <figcaption className="overlay-panel overlay-background overlay-fade text-center vertical-align">
                            <div className="btn-group btn-group-flat">
                                <div className="dropdown pull-left">
                                    <button type="button" className="btn btn-icon btn-pure btn-default dropdown-toggle waves-effect waves-classic" title="Setting">
                                        <i className="icon md-settings"></i>
                                    </button>
                                    <ul className="dropdown-menu" role="menu">
                                        <li><a href={photo.url} download>Download</a></li>
                                        <li><a href="">Link to Room</a></li>
                                    </ul>
                                </div>
                                <button onClick={()=>{ remove(photo.id) }} type="button" className="btn btn-icon btn-pure btn-default waves-effect waves-classic" title="Delete">
                                    <i className="icon md-delete"></i>
                                </button>
                            </div>
                        </figcaption>
                    </figure>
                    {!this.state.canEditTitle?(
                            <div className="pull-right action-icons">
                                <button onClick={this.editTitle} type="button" className="btn btn-icon btn-pure btn-default waves-effect waves-classic" title="Edit">
                                    <i className="icon md-edit"></i>
                                </button>
                            </div>
                        ):null}
                    <div>
                        {this.state.canEditTitle?(
                                <div className="input-group title-group">
                                    <TextBox model={model} ref="title" name={'photos['+index+'].title'} onChange={onChange}></TextBox>
                                    <span onClick={this.saveTitle} className="input-group-addon">
                                        <i className="icon fa-check"></i>
                                    </span>
                                </div>
                            ):(
                                <span>{photo.title || 'Untitled'}</span>
                            )}
                    </div>
                    {(index===0)?(
                            <div className="ribbon ribbon-badge ribbon-warning">
                                <span className="ribbon-inner">Main Photo</span>
                            </div>
                        ):null}
                </div>
            </li>
        );
    }
}

export default Photo;
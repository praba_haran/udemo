import React from 'react';
import Slider from 'rc-slider';
import { TextBox } from '../../shared/Form';
import ActionButtonGroup from '../helper/ActionButtonGroup';

class CustomHandle extends React.Component{

    getColor(value){
        let { colors } = this.props;
        if(value <=20){
            return colors[6];
        }
        else if( value >=21 && value <=38){
            return colors[5];
        }
        else if(value >=39 && value <=54){
            return colors[4];
        }
        else if(value >=55 && value <=68){
            return colors[3];
        }
        else if(value >=69 && value <=80){
            return colors[2];
        }
        else if( value >=81 && value <=91){
            return colors[1];
        }
        else if(value >=92 && value <=100){
            return colors[0];
        }
        return 'transparent';
    }

    render(){
        const props = this.props;
        let bgColor = this.getColor(props.value);
        return (
            <div className="rc-slider-custom-handle" style={{ bottom : `${props.offset}%` }}>
                <div className="left" style={{ borderRightColor: bgColor}}></div>
                <div className="right" style={{ backgroundColor: bgColor}}>{props.value}</div>
            </div>
        );
    }
}

class EPC extends React.Component{

    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(name,value){
        let { onChange } = this.props;
        onChange({
            target:{
                name : name,
                value : value
            },
            type:'change'
        });
    }

    render(){
        let { model, onChange } = this.props;
        let eerColors = ['#007e2d','#2c9f29','#a1d20e','#f8f400','#f1c300','#e77c17','#df0024'];
        let eirColors = ['#cde3f4','#97c0e6','#73a2d6','#4e84c4','#a5a6a9','#919194','#807f83'];
        return (
            <div className="clear">
                <div className="row">
                    <div className="col-sm-5">
                        <h5 className="text-center">Energy Efficiency Rating</h5>
                        <table className="table table-bordered epc-table">
                            <colgroup>
                                <col width="60%" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Current</th>
                                    <th>Potential</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="scales">
                                        <div className="scale scale-a" style={{backgroundColor:eerColors[0]}}>
                                            <span className="value">(92 - 100)</span>
                                            <span className="letter">A</span>
                                        </div>
                                        <div className="scale scale-b" style={{backgroundColor:eerColors[1]}}>
                                            <span className="value">(81 - 91)</span>
                                            <span className="letter">B</span>
                                        </div>
                                        <div className="scale scale-c" style={{backgroundColor:eerColors[2]}}>
                                            <span className="value">(69 - 80)</span>
                                            <span className="letter">C</span>
                                        </div>
                                        <div className="scale scale-d" style={{backgroundColor:eerColors[3]}}>
                                            <span className="value">(55 - 68)</span>
                                            <span className="letter">D</span>
                                        </div>
                                        <div className="scale scale-e" style={{backgroundColor:eerColors[4]}}>
                                            <span className="value">(39 - 54)</span>
                                            <span className="letter">E</span>
                                        </div>
                                        <div className="scale scale-f" style={{backgroundColor:eerColors[5]}}>
                                            <span className="value">(21 - 38)</span>
                                            <span className="letter">F</span>
                                        </div>
                                        <div className="scale scale-g" style={{backgroundColor:eerColors[6]}}>
                                            <span className="value">(1 - 20)</span>
                                            <span className="letter">G</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="slider">
                                            <div className="slider-inner">
                                                <Slider handle={<CustomHandle colors={eerColors} />} value={model.epc.EnergyEfficiencyRating.current} vertical tipTransitionName="rc-slider-tooltip-zoom-down" onChange={(value)=> this.handleChange("epc.EnergyEfficiencyRating.current",value)} />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="slider">
                                            <div className="slider-inner">
                                                <Slider handle={<CustomHandle colors={eerColors} />} value={model.epc.EnergyEfficiencyRating.potential} vertical tipTransitionName="rc-slider-tooltip-zoom-down" onChange={(value)=> this.handleChange("epc.EnergyEfficiencyRating.potential",value)} />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <TextBox model={model} name="epc.EnergyEfficiencyRating.current" onChange={onChange} />
                                    </td>
                                    <td>
                                        <TextBox model={model} name="epc.EnergyEfficiencyRating.potential" onChange={onChange} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className="col-sm-5">
                        <h5 className="text-center">Environmental Impact Rating</h5>
                        <table className="table table-bordered epc-table">
                            <colgroup>
                                <col width="60%" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th></th>
                                <th>Current</th>
                                <th>Potential</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="scales">
                                    <div className="scale scale-a" style={{backgroundColor:eirColors[0]}}>
                                        <span className="value">(92 - 100)</span>
                                        <span className="letter">A</span>
                                    </div>
                                    <div className="scale scale-b" style={{backgroundColor:eirColors[1]}}>
                                        <span className="value">(81 - 91)</span>
                                        <span className="letter">B</span>
                                    </div>
                                    <div className="scale scale-c" style={{backgroundColor:eirColors[2]}}>
                                        <span className="value">(69 - 80)</span>
                                        <span className="letter">C</span>
                                    </div>
                                    <div className="scale scale-d" style={{backgroundColor:eirColors[3]}}>
                                        <span className="value">(55 - 68)</span>
                                        <span className="letter">D</span>
                                    </div>
                                    <div className="scale scale-e" style={{backgroundColor:eirColors[4]}}>
                                        <span className="value">(39 - 54)</span>
                                        <span className="letter">E</span>
                                    </div>
                                    <div className="scale scale-f" style={{backgroundColor:eirColors[5]}}>
                                        <span className="value">(21 - 38)</span>
                                        <span className="letter">F</span>
                                    </div>
                                    <div className="scale scale-g" style={{backgroundColor:eirColors[6]}}>
                                        <span className="value">(1 - 20)</span>
                                        <span className="letter">G</span>
                                    </div>
                                </td>
                                <td>
                                    <div className="slider">
                                        <div className="slider-inner">
                                            <Slider handle={<CustomHandle colors={eirColors} />} value={model.epc.EnvironmentalImpactRating.current} vertical tipTransitionName="rc-slider-tooltip-zoom-down" onChange={(value)=> this.handleChange("epc.EnvironmentalImpactRating.current",value)} />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className="slider">
                                        <div className="slider-inner">
                                            <Slider handle={<CustomHandle colors={eirColors} />} value={model.epc.EnvironmentalImpactRating.potential} vertical tipTransitionName="rc-slider-tooltip-zoom-down" onChange={(value)=> this.handleChange("epc.EnvironmentalImpactRating.potential",value)} />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <TextBox model={model} name="epc.EnvironmentalImpactRating.current" onChange={onChange} />
                                </td>
                                <td>
                                    <TextBox model={model} name="epc.EnvironmentalImpactRating.potential" onChange={onChange} />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                <ActionButtonGroup {...this.props} />
            </div>
        );
    }
}

export default EPC;
import React from 'react';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import Panel from 'react-bootstrap/lib/Panel';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';
import NoExternalBrochure from './external-brochures/NoExternalBrochure';
import helper from '../../shared/helper';

class ExternalBrochures extends React.Component{

    render(){
        let { model,remove } = this.props;
        return (
            <div className="clear">
                {!this.props.showDropZone?(
                        <p className="text-right">
                            <button onClick={this.props.toggleDropZone} type="button" className="btn btn-sm btn-outline btn-primary btn-round waves-effect waves-light waves-round">
                                <span className="text hidden-xs">Upload</span>
                                <i className="icon md-upload" aria-hidden="true"></i>
                            </button>
                        </p>
                    ):null}
                <div className="drop-zone-container">
                    <Panel collapsible expanded={this.props.showDropZone}>
                        <a onClick={this.props.toggleDropZone} className="close">
                            <i className="icon md-close"></i>
                        </a>
                        <Dropzone
                            disableClick={true}
                            ref={ (node)=> {
                                this.dropzone = node;
                            } }
                            className="drop-zone"
                            activeClassName="active"
                            multiple={true}
                            onDrop={this.props.onFilesDrop}>
                            {this.props.isProcessing?(
                                    <div>
                                        <p>{this.props.uploadingMessage}</p>
                                        <p>
                                            <small>Please wait ...</small>
                                        </p>
                                        <ProgressBar bsStyle="success" now={this.props.progress} />
                                    </div>
                                ):(
                                    <div>
                                        <p>Drop your brochures here</p>
                                        <p>
                                            <small>Or, if you prefer ...</small>
                                        </p>
                                        <button onClick={()=>this.props.onClickingDropZone(this.dropzone)} className="btn btn-sm btn-primary margin-top-5">Choose files to upload</button>
                                    </div>
                                )}
                        </Dropzone>
                    </Panel>
                </div>
                {model.externalBrochures.length>0?(
                        <div className="clear">
                            <table className="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>Uploaded At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {model.externalBrochures.map((p,index)=> {
                                    let format = p.id.split('.').pop();
                                    let cssDict = {
                                        pdf : 'fa-file-pdf-o',
                                        docx : 'fa-file-word-o',
                                        doc : 'fa-file-word-o',
                                        xls : 'fa-file-excel-o',
                                        xlsx : 'fa-file-excel-o'
                                    };
                                    let iconCss = 'icon margin-right-5 ';
                                    iconCss+=cssDict[format];
                                    return (
                                        <tr key={p.id}>
                                            <td>{index+1}</td>
                                            <td>
                                                <a className="tdn" href={p.url} target="_blank">
                                                    <i className={iconCss}></i>
                                                    {p.fileName}
                                                </a>
                                            </td>
                                            <td>{helper.bytesToSize(p.size)}</td>
                                            <td>{helper.formatDateTime(p.createdAt)}</td>
                                            <td className="hidden-xs text-right">
                                                <a href={p.url} className="btn btn-pure btn-xs waves-effect waves-light" download>
                                                    <i className="icon md-download"></i>
                                                </a>
                                                <button onClick={(e)=>remove(p.id)} className="btn btn-pure btn-xs waves-effect waves-light">
                                                    <i className="icon fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    );
                                })}
                                </tbody>
                            </table>
                        </div>
                    ):null}
                <NoExternalBrochure items={model.externalBrochures}/>
            </div>
        );
    }
}

export default ExternalBrochures;
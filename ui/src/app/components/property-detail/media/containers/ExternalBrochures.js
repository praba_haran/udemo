import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../../../store';
import Alert from '../../../../lib/Alert';
import Cloudinary from '../../../../lib/Cloudinary';
import ExternalBrochures from '../ExternalBrochures';
import PropertyDetail from '../../../../models/PropertyDetail';

class Container extends React.Component{

    constructor() {
        super();
        this.onClickingDropZone = this.onClickingDropZone.bind(this);
        this.onFilesDrop = this.onFilesDrop.bind(this);
        this.toggleDropZone = this.toggleDropZone.bind(this);
        this.remove = this.remove.bind(this);
        this.state = {
            showDropZone: false,
            isProcessing :false,
            uploadingMessage : '',
            progress : 10
        };
    }

    toggleDropZone() {
        this.setState({
            showDropZone: !this.state.showDropZone,
            isProcessing : false,
            uploadingMessage : '',
            progress :10
        });
    }

    onClickingDropZone(dropzone) {
        dropzone.open();
    }

    onFilesDrop(files) {
        let _this = this;
        let { model,APP } = this.props;
        let { userInfo } = APP;
        let fileIndex = model.externalBrochures.length;
        Cloudinary.upload(files,{
            onProgress : function(e){
                _this.setState({
                    progress: e.percent
                });
            },
            onFinished: function(data){
                _.each(data,(file)=>{
                    let doc = {
                        id : file.public_id,
                        url : file.url,
                        fileName : file.original_filename,
                        size : file.bytes,
                        title : '',
                        description:'',
                        createdAt: new Date(),
                        createdBy : userInfo._id
                    };
                    PropertyDetail.actions.setObjectInPath(['externalBrochures',fileIndex],doc);
                    fileIndex++;
                });
                _this.toggleDropZone();
                _this.submit(data.length>1?'Files uploaded successfully.':'File uploaded successfully.');
            },
            onError: function(err){
                Alert.clearLogs().error(err);
            }
        });
        let uploadingMessage = files.length+' Brochures are uploading';
        if(files.length===1){
            uploadingMessage = '1 Brochure is uploading';
        }
        this.setState({
            isProcessing : true,
            uploadingMessage : uploadingMessage
        });
    }

    remove(public_id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this file?', ()=>{
            let _this = this;
            let { model } = this.props;
            Cloudinary.destroy([public_id],{
                onFinished: function(data){
                    _.each(data,function(res){
                        if(res.result==='ok' || res.result==='not found'){
                            let docIndex = _.findIndex(model.externalBrochures,(doc)=>{
                                return doc.id === public_id;
                            });
                            if(docIndex>-1){
                                PropertyDetail.actions.removeObjectInPath(['externalBrochures',docIndex]);
                                _this.submit('File removed successfully.');
                            }
                        }
                        else{
                            Alert.clearLogs().error(res.result);
                        }
                    });
                },
                onError: function(err){
                    Alert.clearLogs().error(err);
                }
            });
        },()=> {
            // user clicked "cancel"
        });
    }

    submit(message){
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(message);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    render(){
        return (
            <ExternalBrochures
                dropzone={this.state.dropzone}
                showDropZone={this.state.showDropZone}
                isProcessing={this.state.isProcessing}
                uploadingMessage={this.state.uploadingMessage}
                progress={this.state.progress}
                toggleDropZone={this.toggleDropZone}
                onClickingDropZone={this.onClickingDropZone}
                onFilesDrop={this.onFilesDrop}
                remove={this.remove}
                onChange={PropertyDetail.actions.valueChange}
                submit={this.submit.bind(this)}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
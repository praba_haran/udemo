import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from '../EPC';
import store from '../../../../store';
import Alert from '../../../../lib/Alert';
import PropertyDetail from '../../../../models/PropertyDetail';

class Container extends React.Component{

    onSubmit(){
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    Alert.clearLogs().success(result.message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    onChangeHandler(e){
        let value = Number(e.target.value);
        if(value<=100){
            PropertyDetail.actions.valueChange(e);
        }
    }

    render(){
        return (
            <Component
                onChange={this.onChangeHandler.bind(this)}
                onSubmit={this.onSubmit.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
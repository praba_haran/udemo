import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import WebLinks from '../WebLinks';
import Alert from '../../../../lib/Alert';
import store from '../../../../store';
import PropertyDetail from '../../../../models/PropertyDetail';
import Commands from '../../../../models/Commands';
import * as constants from '../../../../constants';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            index : 0,
            isProcessing : false
        };
    }

    onIndexChange(index){
        this.setState({
            index : index
        });
    }

    addWebLink(){
        let { model,APP } = this.props;
        let index = model.webLinks.length;
        if(model.webLinks.length<10) {
            let webLink = {
                url: '',
                title: '',
                description: ''
            };
            PropertyDetail.actions.setObjectInPath(['webLinks', index], webLink);
            this.onIndexChange(index);
        }
        else{
            Alert.clearLogs().error('Maximum of 10 links allowed.');
        }
    }

    onSubmit(message) {
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            model.webLinks = _.filter(model.webLinks, (p) => {
                return p.url;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    PropertyDetail.actions.setModel(result.data);
                    Alert.clearLogs().success(message);
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing : false
                });
            });
        }
    }

    removeWebLink(index){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this link?', ()=>{
            PropertyDetail.actions.removeObjectInPath(['webLinks', index]);
            this.onSubmit('Web link removed successfully.');
        },()=> {
            // user clicked "cancel"
        });
    }

    render(){
        return (
            <WebLinks
                onChange={PropertyDetail.actions.valueChange}
                addWebLink={this.addWebLink.bind(this)}
                onIndexChange={this.onIndexChange.bind(this)}
                onSubmit={this.onSubmit.bind(this)}
                removeWebLink={this.removeWebLink.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
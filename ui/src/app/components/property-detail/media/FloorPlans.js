import React from 'react';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import Panel from 'react-bootstrap/lib/Panel';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';
import FloorPlan from './floorplans/FloorPlan';
import NoFloorPlan from './floorplans/NoFloorPlan';

class FloorPlans extends React.Component{

    render(){
        let { model } = this.props;
        return (
            <div className="clear">
                {!this.props.showDropZone?(
                        <p className="text-right">
                            <button onClick={this.props.toggleDropZone} type="button" className="btn btn-sm btn-outline btn-primary btn-round waves-effect waves-light waves-round">
                                <span className="text hidden-xs">Upload</span>
                                <i className="icon md-upload" aria-hidden="true"></i>
                            </button>
                        </p>
                    ):null}
                <div className="drop-zone-container">
                    <Panel collapsible expanded={this.props.showDropZone}>
                        <a onClick={this.props.toggleDropZone} className="close">
                            <i className="icon md-close"></i>
                        </a>
                        <Dropzone
                            disableClick={true}
                            ref={ (node)=> {
                                this.dropzone = node;
                            } }
                            className="drop-zone"
                            activeClassName="active"
                            multiple={true}
                            accept="image/*"
                            onDrop={this.props.onFilesDrop}>
                            {this.props.isProcessing?(
                                    <div>
                                        <p>{this.props.uploadingMessage}</p>
                                        <p>
                                            <small>Please wait ...</small>
                                        </p>
                                        <ProgressBar bsStyle="success" now={this.props.progress} />
                                    </div>
                                ):(
                                    <div>
                                        <p>Drop your floorplans here</p>
                                        <p>
                                            <small>Or, if you prefer ...</small>
                                        </p>
                                        <button onClick={()=>this.props.onClickingDropZone(this.dropzone)} className="btn btn-sm btn-primary margin-top-5">Choose files to upload</button>
                                    </div>
                                )}
                        </Dropzone>
                    </Panel>
                </div>
                <div id="app-projects">
                    <ul className="blocks blocks-100 blocks-lg-5 blocks-md-4 blocks-sm-3 blocks-xs-3">
                        {model.floorPlans.map((value,index)=>{
                            return (<FloorPlan
                                key={value.id}
                                index={index}
                                photo={value}
                                {...this.props}
                            />);
                        })}
                    </ul>
                </div>
                <NoFloorPlan items={model.floorPlans}/>
            </div>
        );
    }
}
export default FloorPlans;
import React from 'react';
import NavLink from '../../shared/NavLink';
import { connect } from 'react-redux';

class Media extends React.Component{

    render () {
        let { model } = this.props;
        return (
            <div className="tab-pane active">
                <div className="nav-tabs-horizontal nav-tabs-inverse">
                    <ul className="nav nav-tabs nav-tabs-solid">
                        <NavLink to={'/properties/'+model._id+'/media'} disableContainsCheck={true}>Photos</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/floorplans'}>Floorplans</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/virtual-tours'}>Virtual Tours</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/external-brochures'}>External Brochures</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/epc'}>EPC</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/web-links'}>Web Links</NavLink>
                        <NavLink to={'/properties/'+model._id+'/media/other-files'}>Other Files</NavLink>
                    </ul>
                    <div className="tab-content">{this.props.children}</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    let app = store.APP.toJS();
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        MATCHING : store.MATCHING.toJS()
    };
};

export default connect(mapStateToProps)(Media);
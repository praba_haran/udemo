import React from 'react';
import _ from 'lodash';
import NoWebLinks from './web-links/NoWebLinks';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import { ValidateTextArea,ValidateText } from '../../shared/Form';

class WebLinks extends React.Component{

    render(){
        let {model,onChange,addWebLink,removeWebLink,onSubmit,isProcessing} = this.props;
        return (
            <div className="clear web-links">
                <p className="text-right">
                    <button onClick={addWebLink} type="button" className="btn btn-sm btn-outline btn-primary btn-round waves-effect waves-light waves-round">
                        <i className="icon md-plus" aria-hidden="true"></i>
                        <span className="text hidden-xs">Add</span>
                    </button>
                </p>
                <div className="row">
                    <div className="col-sm-12">
                        <PanelGroup bsClass="panel-group panel-group-continuous" activeKey={this.props.index} onSelect={this.props.onIndexChange} accordion>
                            {model.webLinks.map((link,index)=>{
                                return (
                                    <Panel key={'media_web_link_' + index} header={link.title || 'Untitled'}
                                           eventKey={index}>
                                        <form className="form-horizontal custom-form">
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Title</label>
                                                <ValidateText className="col-sm-5" name={'webLinks[' + index + '].title'}
                                                              model={model} onChange={onChange}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Url</label>
                                                <ValidateText className="col-sm-5" name={'webLinks[' + index + '].url'}
                                                              model={model} onChange={onChange}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <label className="col-sm-3 control-label">Description</label>
                                                <ValidateTextArea className="col-sm-8" rows="6" model={model}
                                                                  name={'webLinks[' + index + '].description'}
                                                                  placeholder="Description here..."
                                                                  onChange={onChange}/>
                                            </div>
                                            <div className="form-group margin-bottom-10">
                                                <div className="col-sm-offset-3 col-sm-9">
                                                    {link._id?(
                                                            <button onClick={()=>onSubmit('Web link updated successfully.')} disabled={isProcessing} type="button" className="btn btn-sm margin-right-5">{isProcessing?'Updating ...':'Update'}</button>
                                                        ):(
                                                            <button onClick={()=>onSubmit('Web link saved successfully.')} disabled={isProcessing} type="button" className="btn btn-sm margin-right-5">{isProcessing?'Saving ...':'Save'}</button>
                                                        )}
                                                    {link._id?(<button disabled={isProcessing} onClick={()=> removeWebLink(index)} type="button" className="btn btn-sm btn-danger">Remove</button>):null}
                                                </div>
                                            </div>
                                        </form>
                                    </Panel>
                                );
                            })}
                        </PanelGroup>
                        <NoWebLinks items={model.webLinks} />
                    </div>
                </div>
            </div>
        );
    }
}

export default WebLinks;
import React from 'react';
import * as constants from '../../../constants';

class ActionButtonGroup extends React.Component{

    render(){
        let { model, COMMANDS, onSubmit,addRoom } = this.props;
        return (
            <div className="site-action">
                <button onClick={onSubmit} type="button" className="btn btn-floating btn-primary btn-sm waves-effect waves-float waves-light">
                    <i className="icon md-check" aria-hidden="true"></i>
                </button>
                <button type="button" className="btn btn-floating btn-default btn-sm waves-effect waves-float waves-light">
                    <i className="icon md-close" aria-hidden="true"></i>
                </button>
            </div>
        );
    }
}

export default ActionButtonGroup;
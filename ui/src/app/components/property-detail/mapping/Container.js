import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import scriptLoader from 'react-async-script-loader';
import PropertyDetail from '../../../models/PropertyDetail';

class Container extends React.Component{

    componentDidMount () {
        const { isScriptLoaded, isScriptLoadSucceed } = this.props
        if (isScriptLoaded && isScriptLoadSucceed) {
            this.getGeocode();
        }
    }

    componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
        if (isScriptLoaded && !this.props.isScriptLoaded) {
            if (isScriptLoadSucceed) {
                this.getGeocode();
            }
        }
    }

    getAddressToGeoCode(address){
        let text = '';
        let fields = ['street','town','county','postcode'];
        _.each(fields,(p)=>{
            if(address[p]){
                text +=address[p];
                text +=',';
            }
        });
        return text.substr(0,text.length-1);
    }

    getGeocode(){
        let { model } = this.props;
        let google = window.google;
        let geocoder = new google.maps.Geocoder();
        let latLng = new google.maps.LatLng(51.509865, -0.118092);
        if(model.address) {
            let address = this.getAddressToGeoCode(model.address);
            geocoder.geocode({'address': address}, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    this.loadMap(results[0].geometry.location, true);
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                    this.loadMap(latLng, false);
                }
            });
        }
        else{
            this.loadMap(latLng, false);
        }
    }

    loadMap(location,isCorrectAddress){
        this.loadMapView(location);
        this.loadStreetView(location);
    }

    loadMapView(location){
        let node = document.getElementById('markersGmap');
        let map = new google.maps.Map(node, {
            center: {
                lat : location.lat(),
                lng : location.lng()
            },
            zoom: 14,
            disableDefaultUI:true
        });
        map.setCenter(location);
        let marker = new google.maps.Marker( {
            map     : map,
            position: location
        });
    }

    loadStreetView(location){
        let node = document.getElementById('street-map');
        let map = new google.maps.Map(node, {
            center: {
                lat : location.lat(),
                lng : location.lng()
            },
            zoom: 14,
            disableDefaultUI:true
        });
        map.setCenter(location);
        let panorama = new google.maps.StreetViewPanorama(document.getElementById('street-map'), {
            position: location,
            pov: {
                heading: 34,
                pitch: 10
            }
        });
        map.setStreetView(panorama);
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

Container =  scriptLoader(["https://maps.googleapis.com/maps/api/js?key=AIzaSyD-yv-f-OdQmTT-OFOQXKAjdV-_qNZ5ndY"])(Container);

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import Panel from 'react-bootstrap/lib/Panel';

class Component extends React.Component{

    render () {
        let mapStyle = {
            height: '450px'
        };
        return (
            <div className="tab-pane active" role="tabpanel">
                <div className="row">
                    <div className="col-lg-6">
                        <h6>MARKERS</h6>
                        <Panel>
                            <div style={mapStyle} id="markersGmap"></div>
                        </Panel>
                    </div>
                    <div className="col-lg-6">
                        <h6>STREET VIEW</h6>
                        <Panel>
                            <div style={mapStyle} id="street-map"></div>
                        </Panel>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
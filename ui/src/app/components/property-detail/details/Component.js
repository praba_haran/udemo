import React from 'react';
import NavLink from '../../shared/NavLink';
import ActionButtonGroup from '../helper/ActionButtonGroup';

class Component extends React.Component{

    render () {
        let { model } = this.props;
        return (
            <div className="tab-pane active">
                <div className="nav-tabs-horizontal nav-tabs-inverse">
                    <ul className="nav nav-tabs nav-tabs-solid">
                        <NavLink to={'/properties/'+model._id+'/details'} disableContainsCheck={true}>Pre Appraisal</NavLink>
                        <NavLink to={'/properties/'+model._id+'/details/post-appraisal'}>Post Appraisal</NavLink>
                        <NavLink to={'/properties/'+model._id+'/details/owners'}>Owners</NavLink>
                        <NavLink to={'/properties/'+model._id+'/details/agency'}>Agency</NavLink>
                        <NavLink to={'/properties/'+model._id+'/details/summary'}>Summary</NavLink>
                    </ul>
                    <div className="tab-content">{this.props.children}</div>
                </div>
                <ActionButtonGroup {...this.props} />
            </div>
        );
    }
}

export default Component;

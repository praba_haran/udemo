import React from 'react';
import { Select, TextBox , ValueUnitPicker , DatePicker, TilePicker, ValidateText, ValidateSelect,ValidatePriceInput } from '../../../shared/Form';

class Component extends React.Component{

    renderAddress(){
        let { model ,onChange } = this.props;
        if(model!==null) {
            return (
                <form className="form-horizontal custom-form">
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-offset-4 col-sm-8">ADDRESS</h6>
                    </div>
                    {!model.address.fullAddress?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Find Address</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control input-sm" placeholder="Post Code" />
                                <a href="" className="help-block">Or Enter Address Manually</a>
                            </div>
                        </div>
                    ):null}
                    <div className="clear">
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Dwelling</label>
                            <ValidateText className="col-sm-8" name="address.dwelling" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Name/No</label>
                            <ValidateText className="col-sm-8" name="address.nameOrNumber" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Street</label>
                            <ValidateText className="col-sm-8" name="address.street" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Locality</label>
                            <ValidateText className="col-sm-8" name="address.locality" model={model} onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Town</label>
                            <ValidateText className="col-sm-8" name="address.town" model={model} onChange={onChange} type="onlyAlphabet" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">County</label>
                            <ValidateText className="col-sm-8" name="address.county" model={model} onChange={onChange} type="onlyAlphabet" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Postcode</label>
                            <ValidateText className="col-sm-8" name="address.postcode" model={model} onChange={onChange} mask="postcode" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Country</label>
                            <ValidateSelect className="col-sm-8" model={model} name="address.country" onChange={onChange} dropKey="country" showLabel="false"/>
                        </div>
                    </div>
                </form>
            );
        }
    }

    renderBasicInformation(){
        let { model ,onChange } = this.props;
        if(model!==null){
            return (
                <form className="form-horizontal custom-form">
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-offset-4 col-sm-8">BASIC INFORMATION</h6>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Type</label>
                        <ValidateSelect className="col-sm-8" model={model} name="type" onChange={onChange} dropKey="propertyType" showLabel="false" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Market</label>
                        <ValidateSelect className="col-sm-8" model={model} name="market" onChange={onChange} dropKey="propertyMarket" showLabel="false" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Category</label>
                        <ValidateSelect className="col-sm-8" model={model} name="category" onChange={onChange} dropKey="propertyCategory" showLabel="false" />
                    </div>
                    {model.market==="For Sale"?(
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Age</label>
                            <ValidateSelect className="col-sm-8" model={model} name="age" onChange={onChange} dropKey="propertyAge" />
                        </div>
                    ):null}
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Marketing</label>
                        <ValidateSelect className="col-sm-8" model={model} name="marketing" onChange={onChange} dropKey="propertyMarketing" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Time On Market</label>
                        <ValidateSelect className="col-sm-8" model={model} name="timeOnMarket" onChange={onChange} dropKey="propertyTimeOnTheMarket" />
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">Current Price</label>
                        <ValidatePriceInput className="col-sm-8" model={model} name="price" onChange={onChange}/>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <label className="col-sm-4 control-label">SA Expire On</label>
                        <div className="col-sm-8">
                            <DatePicker
                                model={model}
                                name="saExpireOn"
                                onChange={onChange}
                                placement="top"
                                options={{
                                    disablePreviousDate: true
                                }}
                            />
                        </div>
                    </div>
                </form>
            );
        }
    }

    renderMeasurements(){
        let { model ,onChange } = this.props;
        if(model!==null){
            return(
                <form className="form-horizontal custom-form margin-left-30">
                    <div className="form-group margin-bottom-0">
                        <h6 className="col-sm-12">MEASUREMENTS</h6>
                    </div>
                    <div className="form-group margin-bottom-10">
                        <div className="col-sm-10 tile-manager tile-odd-even">
                            <TilePicker model={model} name="bedrooms" onChange={onChange} tileHeading="Bedrooms" type="numeric" />
                            <TilePicker model={model} name="bathrooms" onChange={onChange} tileHeading="Bathrooms" type="numeric" />
                            <TilePicker model={model} name="receptions" onChange={onChange} tileHeading="Receptions" type="numeric" />
                            <TilePicker model={model} name="parking" onChange={onChange} tileHeading="Parking" type="numeric" />
                            <TilePicker model={model} name="floorArea" onChange={onChange} tileHeading="Floor Area" type="numeric" />
                            <ValueUnitPicker model={model} name="landArea" unitName="landAreaUnit" units={['Acre','Sq.Ft']} onChange={onChange} tileHeading="Land Area" />
                        </div>
                    </div>
                </form>
            );
        }
    }

    render () {
        return (
            <div className="row">
                <div className="col-lg-4">
                    {this.renderAddress()}
                </div>
                <div className="col-lg-4">
                    {this.renderBasicInformation()}
                </div>
                <div className="col-lg-4">
                    {this.renderMeasurements()}
                </div>
            </div>
        );
    }
}

export default Component;
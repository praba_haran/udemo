import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import {hashHistory} from 'react-router';
import Component from './Component';
import PropertyDetail from '../../../models/PropertyDetail';
import Commands from '../../../models/Commands';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import * as constants from '../../../constants';

class Container extends React.Component{

    submit(){
        PropertyDetail.actions.validateModel();
        let model = store.getState().PROPERTY_DETAIL.toJS();
        console.log(model);
        if (model.validator.isValid) {
            delete model.validator;
            delete model.__v;
            _.each(model.owners, (p) => {
                p.contact = p.contact._id;
            });
            PropertyDetail.api.update(model._id, model,(result)=>{
                if (result.success) {
                    PropertyDetail.actions.setModel(result.data);
                    Alert.clearLogs().success(result.message);
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                onSubmit={this.submit.bind(this)}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
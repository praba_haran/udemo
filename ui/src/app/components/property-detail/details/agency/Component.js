import React from 'react';
import { ValidateTextArea,ValidateSelect, DatePicker  } from '../../../shared/Form';

class Component extends React.Component{

    render () {
        let {model, onChange, APP } = this.props;
        let { agencyDetails } = APP;
        return (
            <div className="row">
                <div className="col-lg-6">
                    {model!==null && agencyDetails!==null?
                        (
                            <form className="form-horizontal custom-form">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-6">BRANCH INFORMATION</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Branch</label>
                                    <ValidateSelect className="col-sm-6" model={model} name="agency" onChange={onChange}>
                                        {agencyDetails.branchDetails.map(function(p){
                                            return (
                                                <option key={p._id} value={p._id}>{p.address.locality+'-'+p.address.town}</option>
                                            );
                                        })}
                                    </ValidateSelect>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Negotiator</label>
                                    <ValidateSelect className="col-sm-6" model={model} name="negotiator" onChange={onChange}>
                                        {agencyDetails.agencyUsers.map(function(p){
                                            return (
                                                <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                            );
                                        })}
                                    </ValidateSelect>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Review Date</label>
                                    <div className="col-sm-6">
                                        <DatePicker
                                            model={model}
                                            name="reviewDate"
                                            onChange={onChange}
                                            options={{
                                                disablePreviousDate: true
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Contacted Date</label>
                                    <div className="col-sm-6">
                                        <DatePicker
                                            model={model}
                                            name="lastContactedDate"
                                            onChange={onChange}
                                            options={{
                                                disablePreviousDate: true
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Review Notes</label>
                                    <ValidateTextArea className="col-sm-6" rows="4" model={model} name="reviewNotes" onChange={onChange} />
                                </div>
                            </form>
                        ):null
                    }
                </div>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import PropertyDetail from '../../../../models/PropertyDetail';

class Container extends React.Component{

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
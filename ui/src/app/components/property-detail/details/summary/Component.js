import React from 'react';
import {  ValidateTextArea,ValidateSelect, DatePicker } from '../../../shared/Form';

class Component extends React.Component{

    render () {
        let {model,onChange } = this.props;
        return (
            <div className="row">
                <div className="col-sm-12">
                    <form className="form-horizontal">
                        <div className="form-group margin-bottom-0">
                            <div className="col-sm-12">
                                <h6>SUMMARY</h6>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateTextArea className="col-sm-12" rows="10" model={model} name="summary" onChange={onChange} />

                            {/*<div className="col-sm-12">*/}
                            {/**/}
                            {/*<textarea rows="10" className="form-control input-sm"></textarea>*/}
                            {/*<span className="pull-right help-block">1500 Characters remaining.</span>*/}
                            {/*</div>*/}
                        </div>
                        <div className="form-group margin-bottom-0">
                            <div className="col-sm-12">
                                <h6>ADVERT SUMMARY</h6>
                            </div>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <ValidateTextArea className="col-sm-12" rows="10" model={model} name="advertSummary" onChange={onChange} />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Component;
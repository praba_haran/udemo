import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import Component from './Component';
import Alert from '../../../../lib/Alert';
import PropertyDetail from '../../../../models/PropertyDetail';
import ContactDetail from '../../../../models/ContactDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSearchingOwners : false,
            owners : [],
            ownersToFilter : []
        };
    }

    onOwnersFocus(){
        this.onOwnersChange('');
    }

    getAllOwners(typeAhead){
        typeAhead.focus();
    }

    onOwnersChange(text){
        let { model } = this.props;
        text = text.trim();
        this.setState({
            isSearchingOwners:true
        });
        ContactDetail.api.search( { searchText : text },(result)=>{
            if(result.success) {
                let existingOwners = _.map(model.owners, (p) => {
                    return p.contact._id;
                });
                let owners = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        forename: p.firstPerson.forename,
                        surname: p.firstPerson.surname,
                        fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                        address: p.address.fullAddress
                    }
                });
                owners = _.filter(owners, (p) => {
                    return existingOwners.indexOf(p._id) === -1;
                });
                this.setState({
                    owners: result.data,
                    ownersToFilter: owners,
                    isSearchingOwners: false
                });
            }
        });
    }
    onOwnerSelected(items,typeAhead) {
        if (items.length > 0) {
            let { model } = this.props;
            let index = model.owners.length;
            typeAhead.clear();

            let owner = _.find(model.owners,function(p){
                return p.contact._id === items[0]._id;
            });
            if(owner===undefined) {
                let item = _.find(this.state.owners,function(p){
                    return p._id === items[0]._id;
                });
                PropertyDetail.actions.setObjectInPath(['owners', index,'contact'], item);
            }
            else{
                Alert.clearLogs().error('This owner is already added!');
            }
        }
    }
    removeOwner(id){
        Alert.okBtn('Yes').cancelBtn('No').confirm('Would you like to remove this owner?', ()=>{
            let { model } = this.props;
            let index = _.findIndex(model.owners,function(p){
                return p.id === id;
            });
            PropertyDetail.actions.removeObjectInPath(['owners', index]);
        },()=> {
            // user clicked "cancel"
        });
    }

    render(){
        return (
            <Component
                onChange={PropertyDetail.actions.valueChange}
                getAllOwners={this.getAllOwners.bind(this)}
                onOwnersFocus={this.onOwnersFocus.bind(this)}
                onOwnersChange={this.onOwnersChange.bind(this)}
                onOwnerSelected={this.onOwnerSelected.bind(this)}
                removeOwner = {this.removeOwner.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        model : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
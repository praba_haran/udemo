import React from 'react';
import _ from 'lodash';
import {Link} from "react-router";
import TypeAhead from 'react-bootstrap-typeahead';
import Helper from '../../../shared/helper';

class Component extends React.Component{

    renderMenuItem(props,option,idx){
        return (
            <div className="contact-item">
                <p className="name">{option.fullName}</p>
                <p className="address">{option.address}</p>
            </div>
        );
    }

    render () {
        let {
            model,
            isSearchingOwners,
            ownersToFilter,
            owners,
            onOwnersChange,
            onOwnersFocus,
            getAllOwners,
            onOwnerSelected,
            removeOwner
        } = this.props;

        let disableOwners = Helper.property.disableAddOwnersToProperty(model);
        let noRecordFound;
        if(model.owners.length===0){
            noRecordFound = (
                <tr className="no-record-found ">
                    <td colSpan="6" className="text-center">No Owners Linked !</td>
                </tr>
            );
        }
        return (
            <div className="clear">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="input-group">
                            <TypeAhead
                                ref="typeahead"
                                placeholder="Search owners by name ..."
                                emptyLabel={isSearchingOwners?'Loading ...':'No owners found.'}
                                minLength={0}
                                filterBy={['forename','surname']}
                                labelKey="fullName"
                                options={ownersToFilter}
                                onInputChange={onOwnersChange}
                                onChange ={(items)=> { onOwnerSelected(items,this.refs.typeahead.getInstance()); } }
                                onFocus = {onOwnersFocus}
                                renderMenuItemChildren={this.renderMenuItem.bind(this)}
                                disabled={disableOwners}
                            />
                            <span className="input-group-btn">
                                <button onClick={()=>getAllOwners(this.refs.typeahead.getInstance())} type="button" className="btn btn-default waves-effect waves-light">
                                    <i className="icon md-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <p className="help-block margin-top-10">Link owners here.</p>
                        <div className="example">
                            <table className="table table-hover table-striped owners-table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Type</th>
                                    <th className="hidden-xs">
                                        Status
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {model.owners.map((p,index)=>{
                                    return (
                                        <tr key={p.contact._id} onClick={(e)=>{}}>
                                            <td>{p.contact.firstPerson.forename+' '+p.contact.firstPerson.surname}</td>
                                            <td>
                                                <Link className="address-link" to={'contacts/'+p.contact._id+'/overview'}>{p.contact.address.fullAddress}</Link>
                                            </td>
                                            <td>{p.contact.type}</td>
                                            <td className="hidden-xs">
                                                <span className="label label-primary label-outline">{p.contact.status}</span>
                                            </td>
                                            <td className="hidden-xs">
                                                <span disabled={disableOwners} onClick={()=>{ removeOwner(p.contact._id)}} className="label label-default label-outline">
                                                    <i className="icon fa-trash"></i>
                                                </span>
                                            </td>
                                        </tr>
                                    );
                                })}
                                {noRecordFound}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import {
    ValidateTextArea,
    ValidateSelect,
    ValidatePriceInput,
    PercentageInput,
    DatePicker
} from '../../../shared/Form';
import Helper from '../../../shared/helper';

class Component extends React.Component{

    render () {
        let { model,onChange, agencyDetails } = this.props;
        let isInstructed = Helper.property.isInstructed(model);
        return (
            <div className="row">
                <div className="col-lg-5">
                    <form className="form-horizontal custom-form">
                        <div className="form-group margin-bottom-0">
                            <h6 className="col-sm-offset-4 col-sm-8">PRICING</h6>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Low Price</label>
                            <ValidatePriceInput disabled={isInstructed} className="col-sm-7" model={model} name="lowPrice" onChange={onChange}/>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">High Price</label>
                            <ValidatePriceInput disabled={isInstructed} className="col-sm-7" model={model} name="highPrice" onChange={onChange}/>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Owner Price</label>
                            <ValidatePriceInput disabled={isInstructed} className="col-sm-7" model={model} name="ownerPrice" onChange={onChange}/>
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Proposed</label>
                            <ValidatePriceInput disabled={isInstructed} className="col-sm-7" model={model} name="proposedPrice" onChange={onChange} />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Furnishing</label>
                            <ValidateSelect disabled={isInstructed} className="col-sm-7" model={model} name="furnishing" onChange={onChange} dropKey="furnishing" />
                        </div>
                        <div className="form-group margin-bottom-10">
                            <label className="col-sm-4 control-label">Appraiser Notes</label>
                            <ValidateTextArea disabled={isInstructed} className="col-sm-7" rows="6" model={model} name="appraiserNotes" onChange={onChange} />
                        </div>
                    </form>
                </div>
                {(model.market==="To Let")?(
                        <div className="col-lg-6">
                            <form className="form-horizontal custom-form">
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-8">MANAGEMENT AND AVAILABILITY</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Deposit</label>
                                    <ValidatePriceInput disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.deposit" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Management</label>
                                    <ValidateSelect disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.type" onChange={onChange} dropKey="managementTypes"/>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Available</label>
                                    <div className="col-sm-5">
                                        <DatePicker
                                            model={model}
                                            name="contract.management.availableOn"
                                            onChange={onChange}
                                            placement="top"
                                            options={{
                                                disablePreviousDate: true
                                            }}
                                            disabled={isInstructed}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-8">FEE IN PERCENT</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Upfront Fee</label>
                                    <PercentageInput disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.percentage.upfrontFee" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Recurring Fee</label>
                                    <PercentageInput disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.percentage.recurringFee" onChange={onChange} />
                                    <ValidateSelect disabled={isInstructed} className="col-sm-3" model={model} name="contract.management.percentage.recurringPeriod" onChange={onChange} dropKey="recurringPeriod" />
                                </div>
                                <div className="form-group margin-bottom-0">
                                    <h6 className="col-sm-offset-4 col-sm-8">FEE IN FIXED</h6>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Upfront Fee</label>
                                    <ValidatePriceInput disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.fixed.upfrontFee" onChange={onChange} />
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Recurring Fee</label>
                                    <ValidatePriceInput disabled={isInstructed} className="col-sm-5" model={model} name="contract.management.fixed.recurringFee" onChange={onChange} />
                                    <ValidateSelect disabled={isInstructed} className="col-sm-3" model={model} name="contract.management.fixed.recurringPeriod" onChange={onChange} dropKey="recurringPeriod" />
                                </div>
                            </form>
                        </div>
                    ):null}
            </div>
        );
    }
}

export default Component;
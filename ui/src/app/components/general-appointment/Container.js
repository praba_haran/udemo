import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import store from '../../store';
import Alert from '../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import * as constants from '../../constants';
import GeneralAppointment from '../../models/GeneralAppointment';
import Commands from '../../models/Commands';
import PropertyDetail from '../../models/PropertyDetail';
import ContactDetail from '../../models/ContactDetail';
import Helper from '../../components/shared/helper';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            activeTab:'applicants',
            isProcessing : false,
            isDeleting : false,
            canShowModal : false,
            isSearchingApplicants : false,
            applicants : [],
            applicantsToFilter : [],
            isSearchingProperties : false,
            properties : [],
            propertiesToFilter :[]
        };
    }

    componentWillReceiveProps(props){
        let { APP,COMMANDS,CONTACT_DETAIL, PROPERTY_DETAIL, GENERAL_APPOINTMENT }= props;
        if(COMMANDS[constants.COMMANDS.BOOK_GENERAL_APPOINTMENT]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.BOOK_GENERAL_APPOINTMENT]
            });
            // IF THIS IS IN CONTACT DETAIL PAGE, THEN THE CONTACT HAS TO BE ADDED IN APPLICANT SECTION
            // AND IF THIS IS IN PROPERTY DETAIL PAGE, THE PROPERTY HAS TO BE ADDED IN PROPERTIES SECTION
            if(! this.state.canShowModal) {
                if (GENERAL_APPOINTMENT.applicants.length === 0 && Helper.isContactDetailPage(props) && CONTACT_DETAIL._id) {
                    GeneralAppointment.actions.setObjectInPath(['applicants', 0], {
                        contact: CONTACT_DETAIL,
                        feedback: ''
                    });
                }
                if (GENERAL_APPOINTMENT.properties.length === 0 && Helper.isPropertyDetailPage(props) && PROPERTY_DETAIL._id) {
                    GeneralAppointment.actions.setObjectInPath(['properties', 0], {
                        property: PROPERTY_DETAIL
                    });
                }
                // BIND NEGOTIATOR AND BRANCH AS DEFAULT IF IT IS NOT SAVED
                if(!GENERAL_APPOINTMENT._id) {
                    let {userInfo} = APP;
                    if (userInfo && userInfo._id) {
                        GeneralAppointment.actions.change('appraiser', userInfo._id);
                    }
                }
            }
        }
    }

    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT,false);
        GeneralAppointment.actions.clearModel();
    }

    submitHandler(result){
        if (result.success) {
            this.cancel();
            Alert.clearLogs().success(result.message);
            Commands.actions.change(constants.COMMANDS.GENERAL_APPOINTMENT_CHANGED, true);
        }
        else {
            console.log(result);
        }
        this.setState({
            isProcessing: false
        });
    }

    submit(){
        GeneralAppointment.actions.validateModel();
        let model = store.getState().GENERAL_APPOINTMENT.toJS();
        if (model.validator.isValid) {
            let hasError = false;
            if(model.applicants.length===0){
                hasError = true;
                this.handleTabChange('applicants');
            }
            if(!hasError && model.properties.length===0){
                hasError = true;
                this.handleTabChange('properties');
            }

            if(!hasError) {
                this.setState({
                    isProcessing: true
                });
                delete model.validator;
                _.each(model.applicants, (p) => {
                    p.contact = p.contact._id;
                });
                _.each(model.properties, (p) => {
                    p.property = p.property._id;
                });
                if (model._id) {
                    GeneralAppointment.api.update(model._id, model, this.submitHandler.bind(this));
                }
                else {
                    GeneralAppointment.api.save(model, this.submitHandler.bind(this));
                }
            }
        }
    }

    remove(e){
        let { GENERAL_APPOINTMENT } = this.props;
        this.setState({
            isDeleting: true
        });
        GeneralAppointment.api.delete(GENERAL_APPOINTMENT._id, (result)=>{
            if (result.success) {
                this.cancel();
                Alert.clearLogs().success(result.message);
                Commands.actions.change(constants.COMMANDS.GENERAL_APPOINTMENT_CHANGED, true);
            }
            else {
                Alert.clearLogs().error(result.message);
            }
            this.setState({
                isDeleting: false
            });
        });
    }

    onApplicantsFocus(){
        this.onApplicantsChange('');
    }
    getAllApplicants(typeAhead){
        typeAhead.focus();
    }
    onApplicantsChange(text){
        let { GENERAL_APPOINTMENT } = this.props;
        text = text.trim();
        this.setState({
            isSearchingOwners: true
        });
        ContactDetail.api.search({
            searchText : text,
            status : 'Active'
        }, (result)=> {
            if(result.success) {
                let existingApplicants = _.map(GENERAL_APPOINTMENT.applicants, (p) => {
                    return p.contact._id;
                });
                let propertyOwners = [];
                if(GENERAL_APPOINTMENT.properties) {
                    _.each(GENERAL_APPOINTMENT.properties,(p)=>{
                        propertyOwners = propertyOwners.concat(
                            _.map(p.property.owners, (owner) => {
                                return (typeof owner.contact)==='object'?owner.contact._id:owner.contact;
                            })
                        );
                    });
                }

                let applicants = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        forename: p.firstPerson.forename,
                        surname: p.firstPerson.surname,
                        fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                        address: p.address.fullAddress,
                        type : p.type
                    }
                });
                applicants = _.filter(applicants, (p) => {
                    return existingApplicants.indexOf(p._id) === -1 && propertyOwners.indexOf(p._id) === -1;
                });
                this.setState({
                    applicantsToFilter: applicants,
                    applicants: result.data,
                    isSearchingApplicants: false
                });
            }
        });
    }
    onApplicantSelected(items,typeAhead) {
        if (items.length > 0) {
            let { GENERAL_APPOINTMENT } = this.props;
            let index = GENERAL_APPOINTMENT.applicants.length;
            typeAhead.focus();
            typeAhead.clear();

            let applicant = _.find(GENERAL_APPOINTMENT.applicants,function(p){
                return p.contact._id === items[0]._id;
            });
            if(applicant===undefined) {
                let item = _.find(this.state.applicants,function(p){
                    return p._id === items[0]._id;
                });
                let obj = {
                    contact : item,
                    feedback : ''
                };
                GeneralAppointment.actions.setObjectInPath(['applicants', index], obj);
            }
            else{
                Alert.clearLogs().error('This applicant is already added!');
            }
        }
    }
    removeApplicant(id){
        let { GENERAL_APPOINTMENT } = this.props;
        let index = _.findIndex(GENERAL_APPOINTMENT.applicants,function(p){
            return p.contact._id === id;
        });
        if(index>-1) {
            GeneralAppointment.actions.removeObjectInPath(['applicants', index]);
        }
    }
    // properties tab
    onPropertiesFocus(){
        this.onPropertiesChange('');
    }
    getAllProperties(typeAhead){
        typeAhead.focus();
    }
    onPropertiesChange(text){
        let { GENERAL_APPOINTMENT } = this.props;
        text = text.trim();
        this.setState({
            isSearchingProperties: true
        });
        PropertyDetail.api.search({ searchText : text }, (result)=> {
            if(result.success) {
                let existingProperties = _.map(GENERAL_APPOINTMENT.properties, (p) => {
                    return p.property._id;
                });
                let properties = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        price: p.price.toString(),
                        status: p.status,
                        address: p.address.fullAddress,
                        owners : p.owners
                    }
                });
                properties = _.filter(properties, (p) => {
                    return existingProperties.indexOf(p._id) === -1;
                });
                this.setState({
                    propertiesToFilter: properties,
                    properties: result.data,
                    isSearchingProperties: false
                });
            }
        });
    }
    onPropertySelected(items,typeAhead) {
        if (items.length > 0) {
            let { GENERAL_APPOINTMENT } = this.props;
            let index = GENERAL_APPOINTMENT.properties.length;
            typeAhead.focus();
            typeAhead.clear();

            let property = _.find(GENERAL_APPOINTMENT.properties,function(p){
                return p.property._id === items[0]._id;
            });
            if(property===undefined) {
                let item = _.find(this.state.properties,function(p){
                    return p._id === items[0]._id;
                });
                let obj = {
                    property : item
                };
                GeneralAppointment.actions.setObjectInPath(['properties', index], obj);
            }
            else{
                Alert.clearLogs().error('This property is already added!');
            }
        }
    }
    removeProperty(id){
        let { GENERAL_APPOINTMENT } = this.props;
        let index = _.findIndex(GENERAL_APPOINTMENT.properties,function(p){
            return p.property._id === id;
        });
        if(index>-1) {
            GeneralAppointment.actions.removeObjectInPath(['properties', index]);
        }
    }

    render(){
        return (
            <Component
                onChange={GeneralAppointment.actions.valueChange}
                cancel = {this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                remove={this.remove.bind(this)}
                handleTabChange={this.handleTabChange.bind(this)}
                onApplicantsFocus={this.onApplicantsFocus.bind(this)}
                getAllApplicants={this.getAllApplicants.bind(this)}
                onApplicantsChange={this.onApplicantsChange.bind(this)}
                onApplicantSelected={this.onApplicantSelected.bind(this)}
                removeApplicant = {this.removeApplicant.bind(this)}
                onPropertiesFocus={this.onPropertiesFocus.bind(this)}
                getAllProperties={this.getAllProperties.bind(this)}
                onPropertiesChange={this.onPropertiesChange.bind(this)}
                onPropertySelected={this.onPropertySelected.bind(this)}
                removeProperty = {this.removeProperty.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        GENERAL_APPOINTMENT : store.GENERAL_APPOINTMENT.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        DIARY : store.DIARY.toJS(),
        GOOGLE_CALENDARS : store.GOOGLE_CALENDARS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
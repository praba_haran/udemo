import React from 'react';
import {Link} from "react-router";
import TypeAhead from 'react-bootstrap-typeahead';
import { Scrollbars } from 'react-custom-scrollbars';

class Applicants extends React.Component{

    renderMenuItem(props,option,idx){
        return (
                <div className="contact-item">
                    <p className="name">{option.fullName}
                        <span className="contact-type">{option.type}</span>
                    </p>
                    <p className="address">{option.address}</p>
                </div>
        );
    }

    render(){
        let {
            GENERAL_APPOINTMENT,
            isSearchingApplicants,
            applicantsToFilter,
            applicants,
            onApplicantsFocus,
            getAllApplicants,
            onApplicantsChange,
            onApplicantSelected,
            removeApplicant
        } = this.props;
        return (
            <div className="clear">
                <div className="content-area">
                    <div className="input-group">
                        <TypeAhead
                            ref="typeahead"
                            multiple={true}
                            placeholder="Search applicants by name ..."
                            emptyLabel={isSearchingApplicants?'Loading ...':'No applicants found.'}
                            minLength={0}
                            filterBy={['forename','surname']}
                            labelKey="fullName"
                            options={applicantsToFilter}
                            onInputChange={onApplicantsChange}
                            onChange ={(items)=> onApplicantSelected(items,this.refs.typeahead.getInstance()) }
                            onFocus = {onApplicantsFocus}
                            renderMenuItemChildren={this.renderMenuItem.bind(this)}
                        />
                        <span className="input-group-btn">
                            <button onClick={()=>getAllApplicants(this.refs.typeahead.getInstance())} type="button" className="btn btn-default waves-effect waves-light">
                                <i className="icon md-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                    {(GENERAL_APPOINTMENT.applicants.length===0 && GENERAL_APPOINTMENT.validator.isSubmitted)?(
                            <p className="help-block text-danger">At least one applicant is required.</p>
                        ):null}
                    {(GENERAL_APPOINTMENT.applicants.length===0 && !GENERAL_APPOINTMENT.validator.isSubmitted)?(
                            <p className="help-block">At least one applicant is required.</p>
                        ):null}
                </div>
                <div className="clear">
                    <Scrollbars
                        style={{height:250}}
                    >
                        <div className="scroll-area">
                            <ul className="list-group list-group-gap applicants-list-group">
                                {GENERAL_APPOINTMENT.applicants.map((p,index)=>{
                                    return (
                                        <li key={p.contact._id} className="list-group-item">
                                            <a href="javascript:void(0)" role="button">
                                                <p className="title">{p.contact.firstPerson.forename+' '+p.contact.firstPerson.surname}</p>
                                                <p className="address">{p.contact.address.fullAddress}</p>
                                            </a>
                                            <span className="remove" onClick={()=>removeApplicant(p.contact._id)}>
                                                <i className="icon md-close"></i>
                                            </span>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </Scrollbars>
                </div>
            </div>
        );
    }
}

export default Applicants;
import React from 'react';
import {Link} from "react-router";
import Helper from '../../shared/helper';
import TypeAhead from 'react-bootstrap-typeahead';
import { Scrollbars } from 'react-custom-scrollbars';

class Properties extends React.Component{

    renderMenuItem(props,option,idx){
        return (
                <div className="contact-item">
                    <p className="name">{option.fullName}</p>
                    <p className="address">{option.address}</p>
                </div>
        );
    }

    render(){
        let {
            GENERAL_APPOINTMENT,
            isSearchingProperties,
            propertiesToFilter,
            properties,
            onPropertiesFocus,
            getAllProperties,
            onPropertiesChange,
            onPropertySelected,
            removeProperty
        } = this.props;
        return (
            <div className="clear">
                <div className="content-area">
                    <div className="input-group">
                        <TypeAhead
                            ref="typeahead"
                            multiple={true}
                            placeholder="Search properties by address ..."
                            emptyLabel={isSearchingProperties?'Loading ...':'No properties found.'}
                            minLength={0}
                            filterBy={['price','address','_id']}
                            labelKey="_id"
                            options={propertiesToFilter}
                            onInputChange={onPropertiesChange}
                            onChange ={(items)=> onPropertySelected(items,this.refs.typeahead.getInstance()) }
                            onFocus = {onPropertiesFocus}
                            renderMenuItemChildren={this.renderMenuItem.bind(this)}
                        />
                        <span className="input-group-btn">
                            <button onClick={()=>getAllProperties(this.refs.typeahead.getInstance())} type="button" className="btn btn-default waves-effect waves-light">
                                <i className="icon md-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                    {(GENERAL_APPOINTMENT.properties.length===0 && GENERAL_APPOINTMENT.validator.isSubmitted)?(
                            <p className="help-block text-danger">At least one property is required.</p>
                        ):null}
                    {(GENERAL_APPOINTMENT.properties.length===0 && !GENERAL_APPOINTMENT.validator.isSubmitted)?(
                            <p className="help-block">At least one property is required.</p>
                        ):null}
                </div>
                <div className="clear">
                    <Scrollbars
                        style={{height:250}}
                    >
                        <div className="scroll-area">
                            <ul className="list-group list-group-gap applicants-list-group">
                                {GENERAL_APPOINTMENT.properties.map((p,index)=>{
                                    return (
                                        <li key={p.property._id} className="list-group-item">
                                            <a href="javascript:void(0)" role="button">
                                                <p className="title">{p.property.address.fullAddress}</p>
                                                <p className="address">{Helper.property.getStatus(p.property)}</p>
                                            </a>
                                            <span className="remove" onClick={()=>removeProperty(p.property._id)}>
                                                <i className="icon md-close"></i>
                                            </span>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </Scrollbars>
                </div>
            </div>
        );
    }
}

export default Properties;
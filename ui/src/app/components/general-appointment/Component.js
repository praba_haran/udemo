import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import Applicants from './partials/Applicants';
import Properties from './partials/Properties';
import {Select, DatePicker, TimePicker, ValidateSelect, ValidateTextArea, CheckBox} from '../shared/Form';
import { Scrollbars } from 'react-custom-scrollbars';

class Component extends React.Component {

    render() {
        let {
            GENERAL_APPOINTMENT,
            onChange,
            PROPERTY_DETAIL,
            cancel,
            submit,
            canShowModal,
            isProcessing,
            activeTab,
            handleTabChange,
            isDeleting,
            remove
        } = this.props;

        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false}
                   dialogClassName="modal-lg">
                <Modal.Header closeButton>
                    <Modal.Title>Book A General Appointment</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Type</label>
                                    <ValidateSelect className="col-sm-8" model={GENERAL_APPOINTMENT} name="type" onChange={onChange} dropKey="appointmentType" showLabel="false"/>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Appraiser</label>
                                    {this._renderAppraiser()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Accompanied By</label>
                                    {this._renderAccompainedBy()}
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Date</label>
                                    <div className="col-sm-8">
                                        <DatePicker
                                            model={GENERAL_APPOINTMENT}
                                            name="date"
                                            onChange={onChange}
                                            options={{
                                                disablePreviousDate: true
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Time</label>
                                    <div className="col-sm-4">
                                        <TimePicker
                                            model={GENERAL_APPOINTMENT}
                                            name="startTime"
                                            onChange={onChange}
                                        />
                                    </div>
                                    <div className="col-sm-4">
                                        <TimePicker
                                            model={GENERAL_APPOINTMENT}
                                            name="endTime"
                                            onChange={onChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Reminder</label>
                                    <ValidateSelect className="col-sm-4" model={GENERAL_APPOINTMENT} name="reminder" onChange={onChange} dropKey="reminderMinutes" showLabel="false"/>
                                    <ValidateSelect className="col-sm-4" model={GENERAL_APPOINTMENT} name="reminderChannel" onChange={onChange} dropKey="reminderChannel" showLabel="true"/>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <label className="col-sm-4 control-label">Notes</label>
                                    <ValidateTextArea rows="9" className="col-sm-8" model={GENERAL_APPOINTMENT} name="notes" onChange={onChange}/>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="nav-tabs-horizontal">
                                    <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                                        <li role="presentation" className={(activeTab === 'applicants') ? 'active' : ''}>
                                            <a onClick={() => handleTabChange('applicants')} role="button" href="javascript:void(0)">Applicants</a>
                                        </li>
                                        <li role="presentation" className={(activeTab === 'properties') ? 'active' : ''}>
                                            <a onClick={() => handleTabChange('properties')} role="button" href="javascript:void(0)">Properties</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content">
                                        <div className="tab-pane active">
                                            {activeTab === 'applicants' ? (<Applicants {...this.props} />) : null}
                                            {activeTab === 'properties' ? (<Properties {...this.props} />) : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <div className="row">
                        <div className="col-sm-6 text-left">
                            {GENERAL_APPOINTMENT._id?(
                                    <Button onClick={remove} disabled={isProcessing || isDeleting} className="btn btn-danger">{isDeleting ? 'Cancelling ...' : 'Cancel Appointment'}</Button>
                                ):null}
                        </div>
                        <div className="col-sm-6 text-right">
                            <Button onClick={cancel}>Cancel</Button>
                            <Button className="btn btn-primary" disabled={isProcessing || isDeleting} onClick={submit}>{isProcessing ? 'Saving ...' : 'Save'}</Button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }

    _renderAppraiser() {
        let {GENERAL_APPOINTMENT, onChange, APP} = this.props;
        let {agencyDetails} = APP;
        if (agencyDetails && agencyDetails.agencyUsers) {
            return (
                <ValidateSelect className="col-sm-8" model={GENERAL_APPOINTMENT} name="appraiser" onChange={onChange}>
                    {agencyDetails.agencyUsers.map(function (p) {
                        return (
                            <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                        );
                    })}
                </ValidateSelect>
            );
        }
    }

    _renderAccompainedBy() {
        let {GENERAL_APPOINTMENT, onChange, APP} = this.props;
        let {agencyDetails} = APP;
        if (agencyDetails && agencyDetails.agencyUsers) {
            return (
                <div className="col-sm-8">
                    <Select model={GENERAL_APPOINTMENT} name="accompaniedBy" onChange={onChange}>
                        {(GENERAL_APPOINTMENT && GENERAL_APPOINTMENT.appraiser) ? agencyDetails.agencyUsers.map(function (p) {
                                if (p._id !== GENERAL_APPOINTMENT.appraiser) {
                                    return (
                                        <option key={p._id} value={p._id}>{p.forename} {p.surname}</option>
                                    );
                                }
                            }) : null}
                    </Select>
                </div>
            );
        }
    }
}

export default Component;
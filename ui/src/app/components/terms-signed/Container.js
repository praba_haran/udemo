import React from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import TermsSigned from '../../models/TermsSigned';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false
        };
    }

    cancel(){
        TermsSigned.actions.clearModel();
        Commands.actions.change(constants.COMMANDS.TERMS_SIGNED,false);
    }

    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.TERMS_SIGNED]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.TERMS_SIGNED]
            });
            // SET PROPERTY ID
            TermsSigned.actions.change('property',PROPERTY_DETAIL._id);
            // LOADING RECENT TERMS SENT
            TermsSigned.api.list({
                property : PROPERTY_DETAIL._id
            },(result)=>{
                if(result.success){
                    if(result.data.length>0){
                        TermsSigned.actions.setModel(result.data[0]);
                    }
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
            });
        }
    }

    submit(){
        TermsSigned.actions.validateModel();
        let model = store.getState().TERMS_SIGNED.toJS();
        if (model.validator.isValid) {
            this.setState({
                isProcessing : true
            });
            delete model.validator;
            TermsSigned.api.save(model,(result)=>{
                if (result.success) {
                    this.cancel();
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                    Alert.clearLogs().success(result.message);
                }
                else{
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing: false
                });
            });
        }
    }

    render () {
        return(
            <Component
                onChange={TermsSigned.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        TERMS_SIGNED : store.TERMS_SIGNED.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
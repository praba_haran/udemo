import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import CommissionAndFees from './partials/CommissionAndFees';
import Discount from './partials/Discount';
import ForSaleContract from './partials/ForSaleContract';
import ToLetContract from './partials/ToLetContract';
import Management from './partials/Management';
import Board from './partials/Board';

class Component extends React.Component{

    render(){
        let { INSTRUCT_PROPERTY,PROPERTY_DETAIL, onChange,cancel,submit,canShowModal,isProcessing,boardContractors } = this.props;
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false} dialogClassName="custom-modal modal-lg">
                <Modal.Header closeButton>
                    <Modal.Title>INSTRUCT</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PropertySection {...this.props}/>
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-6">
                                {PROPERTY_DETAIL.market==='To Let'?(
                                        <ToLetContract {...this.props} />
                                    ):(
                                        <ForSaleContract {...this.props} />
                                    )}
                                {INSTRUCT_PROPERTY.actions.requireBoardChange?(<Board {...this.props} />):null}
                            </div>
                            <div className="col-md-6">
                                {PROPERTY_DETAIL.market==='To Let'?(
                                        <Management {...this.props} />
                                    ):(
                                        <CommissionAndFees {...this.props} />
                                    )}
                                <Discount {...this.props} />
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={cancel}>Cancel</Button>
                    <Button className="btn btn-primary" onClick={submit} disabled={isProcessing}>{isProcessing?'Saving ...':'Save'}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
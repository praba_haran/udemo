import React from 'react';
import {
    ValidateSelect,
    ValidatePriceInput,
    ValidateTextArea,
    DatePicker,
    CheckBox
} from '../../shared/Form';

export default class Management extends React.Component {

    render() {
        let {INSTRUCT_PROPERTY, onChange} = this.props;
        return (
            <div className="clear">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-6">MANAGEMENT</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Management</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="management.type" onChange={onChange} dropKey="managementTypes"/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Maintenance</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="management.maintenanceType" onChange={onChange} dropKey="maintenanceTypes"/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Overseas Landlord</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="management.overseasLandlordType" onChange={onChange} dropKey="overseasLandlordTypes"/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Available</label>
                    <div className="col-sm-6">
                        <DatePicker
                            model={INSTRUCT_PROPERTY}
                            name="management.availableOn"
                            onChange={onChange}
                            placement="top"
                            options={{
                                disablePreviousDate: true
                            }}
                        />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Notes</label>
                    <ValidateTextArea rows="4" className="col-sm-6" model={INSTRUCT_PROPERTY} name="management.notes" onChange={onChange} />
                </div>
            </div>
        );
    }
}
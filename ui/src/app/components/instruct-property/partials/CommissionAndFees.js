import React from 'react';
import {
    TextBox,
    ValidatePriceInput,
    PercentageInput,
    CheckBox,
    Validate
} from '../../shared/Form';

class CommissionAndFees extends React.Component {

    render() {
        let {INSTRUCT_PROPERTY, onChange} = this.props;
        return (
            <div className="clear">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-6">COMMISSION AND FEES</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Commission Min</label>
                    <div className="col-sm-6">
                        <Validate
                            model={INSTRUCT_PROPERTY}
                            name="commissionAndFees.commissionMin">
                            <PercentageInput
                                model={INSTRUCT_PROPERTY}
                                name="commissionAndFees.commissionMin"
                                onChange={onChange}
                            />
                        </Validate>                       
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Commission Max</label>
                    <div className="col-sm-6">
                        <PercentageInput
                            model={INSTRUCT_PROPERTY}
                            name="commissionAndFees.commissionMax"
                            onChange={onChange}
                        />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Fee</label>
                    <ValidatePriceInput className="col-sm-6" model={INSTRUCT_PROPERTY} name="commissionAndFees.fee"
                                        onChange={onChange}/>
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-4 col-sm-6">
                        <CheckBox model={INSTRUCT_PROPERTY} name="commissionAndFees.isVatInclusive" onChange={onChange}
                                  label="Inclusive"/>
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Minimum Fee</label>
                    <ValidatePriceInput className="col-sm-6" model={INSTRUCT_PROPERTY} name="commissionAndFees.minFee"
                                        onChange={onChange}/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Withdrawal Fee</label>
                    <ValidatePriceInput className="col-sm-6" model={INSTRUCT_PROPERTY}
                                        name="commissionAndFees.withdrawalFee" onChange={onChange}/>
                </div>
            </div>
        );
    }
}

export default CommissionAndFees;
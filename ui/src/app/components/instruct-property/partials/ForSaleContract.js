import React from 'react';
import {
    ValidateSelect,
    ValidatePriceInput,
    ValidateTextArea,
    DatePicker,
    CheckBox
} from '../../shared/Form';

export default class ForSaleContract extends React.Component {

    render() {
        let {INSTRUCT_PROPERTY, onChange} = this.props;
        return (
            <div className="clear">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-6">CONTRACT</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Asking Price</label>
                    <ValidatePriceInput className="col-sm-6" model={INSTRUCT_PROPERTY} name="price" onChange={onChange} />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Sale Terms</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="saleTerms" onChange={onChange} dropKey="saleTerms"/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Contract Length</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="contractLength" onChange={onChange} dropKey="contractLengths"/>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Contract Ends On</label>
                    <div className="col-sm-6">
                        <DatePicker
                            model={INSTRUCT_PROPERTY}
                            name="contractEndsOn"
                            onChange={onChange}
                            placement="top"
                            options={{
                                disablePreviousDate: true
                            }}
                        />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Available</label>
                    <div className="col-sm-6">
                        <DatePicker
                            model={INSTRUCT_PROPERTY}
                            name="management.availableOn"
                            onChange={onChange}
                            placement="top"
                            options={{
                                disablePreviousDate: true
                            }}
                        />
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Notes</label>
                    <ValidateTextArea rows="4" className="col-sm-6" model={INSTRUCT_PROPERTY} name="notes" onChange={onChange} />
                </div>
                <div className="form-group margin-bottom-10">
                    <div className="col-sm-offset-4 col-sm-6">
                        <CheckBox model={INSTRUCT_PROPERTY} name="actions.requireBoardChange" onChange={onChange} label="Require Board Change" />
                    </div>
                </div>
            </div>
        );
    }
}
import React from 'react';
import {
    ValidateText,
    ValidateSelect,
    ValidateTextArea
} from '../../shared/Form';

export default class Board extends React.Component {

    render() {
        let {INSTRUCT_PROPERTY, onChange, boardContractors} = this.props;
        return (
            <div className="clear">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-6">MANAGE BOARD</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Current Board</label>
                    <ValidateText className="col-sm-6" model={INSTRUCT_PROPERTY} name="boardChangeRequest.currentBoard" onChange={onChange} />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Board Change</label>
                    <ValidateSelect className="col-sm-6" model={INSTRUCT_PROPERTY} name="boardChangeRequest.newBoard" onChange={onChange} dropKey="boardTypes" />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Board Contractor</label>
                    <div className="col-sm-6">
                        <ValidateSelect model={INSTRUCT_PROPERTY} name="boardChangeRequest.boardContractor" onChange={onChange}>
                            {boardContractors.map((p)=>{
                                return (<option key={p._id} value={p._id}>{p.firstPerson.forename}</option>);
                            })}
                        </ValidateSelect>
                    </div>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Message</label>
                    <ValidateTextArea rows="4" className="col-sm-6" model={INSTRUCT_PROPERTY} name="boardChangeRequest.message" onChange={onChange} />
                </div>
            </div>
        );
    }
}
import React from 'react';
import _ from 'lodash';
import Helper from '../../shared/helper';

export default class DescribePropertySection extends React.Component{
    
    render(){
        let { PROPERTY_DETAIL } = this.props;
        let getPrice = ()=>{
            return PROPERTY_DETAIL.proposedPrice || PROPERTY_DETAIL.price;
        };
        return (
            <div className="clear describe-property">
                <ul className="list-group">
                    <li className="list-group-item">
                        <div className="media">
                            <div className="media-left">{this._renderThumbnail()}</div>
                            <div className="media-body">
                                <p className="media-heading margin-bottom-0">{PROPERTY_DETAIL.address.fullAddress}</p>
                                <p className="margin-bottom-0">
                                    <span className="text-danger price">{Helper.toCurrency(getPrice())}</span>
                                    <small className="margin-left-5">{PROPERTY_DETAIL.priceQualifier}</small>
                                </p>
                                <p className="margin-bottom-0"><small>{Helper.property.getStatus(PROPERTY_DETAIL)}</small></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }

    _renderThumbnail(){
        let { PROPERTY_DETAIL } = this.props;
        if(PROPERTY_DETAIL) {
            if(PROPERTY_DETAIL.photos.length>0) {
                return (
                    <a className="avatar" href="javascript:void(0)">
                        <img className="img-responsive" src={PROPERTY_DETAIL.photos[0].url.replace('upload', 'upload/w_100')}
                             alt={PROPERTY_DETAIL.photos[0].title}/>
                    </a>
                );
            }
            return (
                <a className="avatar" href="javascript:void(0)">
                    <img className="overlay-figure" src="/dist/images/photos/no-image.png"/>
                </a>
            );
        }
    }
}
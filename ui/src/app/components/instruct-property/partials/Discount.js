import React from 'react';
import {
    TextBox,
    ValidatePriceInput,
    DatePicker,
    PercentageInput
} from '../../shared/Form';

export default class Discount extends React.Component {

    render() {
        let {INSTRUCT_PROPERTY, onChange, onDiscountPercentChanged, onDiscountFeeChanged} = this.props;
        return (
            <div className="clear">
                <div className="form-group margin-bottom-0">
                    <h6 className="col-sm-offset-4 col-sm-6">DISCOUNT</h6>
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Percent</label>
                    <PercentageInput
                        className="col-sm-6"
                        model={INSTRUCT_PROPERTY}
                        name="discount.percent"
                        onChange={onChange}
                        onChangeFinished={onDiscountPercentChanged}
                        disabled={INSTRUCT_PROPERTY.discount.fee > 0}
                    />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Or Fee</label>
                    <ValidatePriceInput
                        className="col-sm-6"
                        model={INSTRUCT_PROPERTY}
                        name="discount.fee"
                        onChange={onChange}
                        onChangeFinished={onDiscountFeeChanged}
                        disabled={INSTRUCT_PROPERTY.discount.percent > 0}
                    />
                </div>
                <div className="form-group margin-bottom-10">
                    <label className="col-sm-4 control-label">Applies From</label>
                    <div className="col-sm-6">
                        <DatePicker
                            model={INSTRUCT_PROPERTY}
                            name="discount.appliesFrom"
                            onChange={onChange}
                            placement="top"
                            options={{
                                disablePreviousDate: true
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
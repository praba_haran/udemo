import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import ContactDetail from '../../models/ContactDetail';
import InstructProperty from '../../models/InstructProperty';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isProcessing : false,
            canShowModal : false,
            boardContractors : []
        };
    }

    cancel(){
        InstructProperty.actions.clearModel();
        Commands.actions.change(constants.COMMANDS.INSTRUCT_PROPERTY,false);
    }

    componentWillReceiveProps(props){
        let { COMMANDS,PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.INSTRUCT_PROPERTY]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.INSTRUCT_PROPERTY]
            });
            // SET PROPERTY ID AND PROPERTY'S MANAGEMENT AND AVAILABILITY
            InstructProperty.actions.change('property',PROPERTY_DETAIL._id);
            InstructProperty.actions.change('contractType',PROPERTY_DETAIL.market);
            InstructProperty.actions.change('management.type',PROPERTY_DETAIL.contract.management.type);
            InstructProperty.actions.change('management.availableOn',PROPERTY_DETAIL.contract.management.availableOn);
            // LOADING BOARD CONTRACTORS
            this.loadBoardContractors();
        }
    }

    loadBoardContractors(){
        ContactDetail.api.search({ type : 'Board Contractor' },(result)=>{
            if(result.success){
                this.setState({
                    boardContractors : result.data
                });
            }
            else{
                Alert.clearLogs().error(result.error);
            }
        });
    }

    submit(){
        let { PROPERTY_DETAIL }= this.props;
        InstructProperty.actions.validateModel();
        let model = store.getState().INSTRUCT_PROPERTY.toJS();
        if (model.validator.isValid) {
            delete model.validator;
            this.setState({
                isProcessing : true
            });
            InstructProperty.api.save(model,(result)=>{
                if (result.success) {
                    this.cancel();
                    // NOTIFYING PROPERTY UPDATE DETAILS
                    // IN CONTAINER, WE ARE RELOADING
                    Commands.actions.change(constants.COMMANDS.PROPERTY_UPDATED,true);
                    Alert.clearLogs().success(result.message);
                }
                else {
                    Alert.clearLogs().error(result.error);
                }
                this.setState({
                    isProcessing : false
                });
            });
        }
    }

    onDiscountPercentChanged(value){
        if(value>0){
            InstructProperty.actions.valueChange({
                target: {
                    name: 'discount.fee',
                    value: 0
                },
                type: 'change'
            });
        }
    }
    onDiscountFeeChanged(value){
        if(value>0){
            InstructProperty.actions.valueChange({
                target: {
                    name: 'discount.percent',
                    value: 0
                },
                type: 'change'
            });
        }
    }

    render () {
        return(
            <Component
                onChange={InstructProperty.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                onDiscountPercentChanged = {this.onDiscountPercentChanged.bind(this)}
                onDiscountFeeChanged = {this.onDiscountFeeChanged.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        INSTRUCT_PROPERTY : store.INSTRUCT_PROPERTY.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import AllParties from './all-parties/Component';
import Email from './all-parties/Email';


class Component extends React.Component {
    constructor(){
        super();
        this.state ={
            activeTab : 'email'
        };
    }
    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    render() {
        let {
            VIEWING,
            onChange,
            cancel,
            submit,
            isSendingEmail,
            selectedApplicantIndex
        } = this.props;
        let { activeTab } = this.state;
        return (
            <Modal
                keyboard={false}
                backdrop="static"
                show={this.props.canShowModal}
                onHide={cancel}
                dialogClassName={classNames({
                    'modal-viewing-confirmation' : true
                })}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Viewing Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="clear">
                        <div className="row">
                            <AllParties {...this.props} />
                            <Email {...this.props} />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className="row">
                        <div className="col-sm-12 text-right">
                            <Button disabled={isSendingEmail} onClick={cancel}>Cancel</Button>
                            <Button className="btn btn-primary" disabled={isSendingEmail} onClick={submit}>{isSendingEmail?'Sending ...':'Send'}</Button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default Component;
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import store from '../../store';
import * as constants from '../../constants';
import Commands from '../../models/Commands';
import Alert from '../../lib/Alert';
import Viewing from '../../models/Viewing';
import Component from './Component';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSendingEmail : false,
            canShowModal : false,
            error : null,
            activeTab : 'applicants',
            selectedIndex : 0,
            applicantId : null
        };
    }

    handleTabChange(tab){
        this.setState({
            activeTab : tab,
            selectedIndex : 0
        });
    }

    selectApplicantAndOwner(index){
        this.setState({
            selectedIndex : index
        });
    }

    cancel(){
        this.setState({
            canShowModal : false
        });
        Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM,false);
        Viewing.actions.clearModel();
    }

    componentWillReceiveProps(props){
        let { BOOK_VIEWING_EMAIL_CONFIRMATION,COMMANDS,VIEWING }= props;
        if(COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM]
            });
        }
        if(typeof COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM_APPLICANT_ID]==='string') {
            this.setState({
                applicantId: COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM_APPLICANT_ID]
            });
            // updating selected index
            if(this.state.activeTab==='applicants'){
                let selectedIndex = _.findIndex(VIEWING.applicants,(p)=>{
                    return p._id === COMMANDS[constants.COMMANDS.BOOK_VIEWING_EMAIL_CONFIRM_APPLICANT_ID];
                });
                this.setState({
                    selectedIndex: selectedIndex
                });
            }
        }
    }

    submit(){
        Viewing.actions.validateModel();
        let model = store.getState().VIEWING.toJS();
        if (model.validator.isValid && model.applicants.length>0) {
            this.setState({
                isSendingEmail: true,
                error : null
            });
            delete model.validator;
            model.property = model.property._id;

            Viewing.api.sendConfirmationEmail(model, (result)=>{
                if (result.success) {
                    //Viewing.actions.setModel(result.data);
                    Alert.clearLogs().success(result.message);
                    // TO AVOID RE RENDERING, I HAVE DUPLICATED SET STATE HERE
                    this.setState({
                        isSendingEmail: false
                    });
                    this.cancel();
                    // IF WE SET THIS TRUE, VIEWING WILL RELOAD
                    Commands.actions.change(constants.COMMANDS.BOOK_VIEWING_CHANGED, true);
                }
                else{
                    this.setState({
                        error: result.error,
                        isSendingEmail: false
                    });
                }
            });
        }
    }

    isMailSentToAllApplicants(){
        return false;
    }

    render () {
        return(
            <Component
                onChange={Viewing.actions.valueChange}
                cancel={this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                handleTabChange={this.handleTabChange.bind(this)}
                selectApplicantAndOwner = {this.selectApplicantAndOwner.bind(this)}
                {...this.state}
                {...this.props}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        VIEWING : store.VIEWING.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);
import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

class Applicants extends React.Component {

    render() {
        let {
            VIEWING,
            activeTab,
            selectedIndex,
            selectApplicantAndOwner,
            applicantId
        } = this.props;

        return (
            <div className="clear applicant-list">
                <ul className="list-group list-group-gap applicants-list-group">
                    {VIEWING.applicants.map((item,index)=>{
                        return (
                            <li key={item.contact._id} className={classNames({
                                'list-group-item' : true,
                                'active' : (activeTab==='applicants' && selectedIndex===index)
                            })}>
                                <a onClick={()=> selectApplicantAndOwner(index)} href="javascript:void(0)" role="button">
                                    <p className="title">{item.contact.firstPerson.forename+' '+item.contact.firstPerson.surname}</p>
                                    <p className="email">{item.contact.firstPerson.emails[0].email}</p>
                                    <p className="phone">{item.contact.firstPerson.telephones[0].number}</p>
                                </a>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

export default Applicants;
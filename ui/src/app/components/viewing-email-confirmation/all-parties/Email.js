import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import Helper from '../../shared/helper';
import ContentEditable from "react-contenteditable";

export default class Email extends React.Component{

    render(){
        let {
            VIEWING,
            getNotificationOn
        } = this.props;
        let contactObj = this.getContactObj();
        if(!contactObj){
            return null;
        }
        let notification = this.getNotificationInfo(contactObj);
        let notifyOn = notification.type;
        return (
            <div className="col-sm-8">
                <div className="scroll-area">
                    <form className="form-horizontal">
                        <div className="form-group margin-bottom-0">
                            <div className="col-sm-12">
                                <h5 className="heading">{notification.heading}</h5>
                                {this.renderDeliveryInfo(contactObj,notifyOn)}
                                <div className="clear email-editor">
                                    <table className="container content float-center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table className="row">
                                                    <tbody>
                                                    <tr>
                                                        <th className="small-12 large-12 columns first last">
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <th>
                                                                        <h1>Hi, {contactObj.contact.firstPerson.forename}</h1>
                                                                        {this.renderLine(contactObj,notifyOn,'line1')}
                                                                    </th>
                                                                    <th className="expander"></th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table className="row">
                                                    <tbody>
                                                    <tr>
                                                        <th className="small-12 large-12 columns first last">
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <th>
                                                                        <table className="table table-hovered">
                                                                            <colgroup>
                                                                                <col width="50%"/>
                                                                                <col width="50%"/>
                                                                            </colgroup>
                                                                            <thead>
                                                                            <tr>
                                                                                <th colSpan="2">Viewing Details</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Date &amp; Time</td>
                                                                                <td>{moment(VIEWING.date).format('DD/MM/YYYY')} {Helper.toTime(VIEWING.startTime)}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Location</td>
                                                                                <td>{VIEWING.property.address.fullAddress}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Appraiser</td>
                                                                                <td>{this.getAppraiser(VIEWING.appraiser)}</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </th>
                                                                    <th className="expander"></th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table className="row">
                                                    <tbody>
                                                    <tr>
                                                        <th className="small-12 large-12 columns first last">
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <th>
                                                                        {this.renderLine(contactObj,notifyOn,'line2')}
                                                                        {this.renderLine(contactObj,notifyOn,'line3')}
                                                                        <p>
                                                                            Thanks
                                                                            <br />
                                                                            {VIEWING.updatedBy.forename}
                                                                        </p>
                                                                    </th>
                                                                    <th className="expander"></th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                {this.props.error?(
                        <p className="text-danger error-message">
                            <strong>{this.props.error}</strong>
                        </p>
                    ):null}
            </div>
        );
    }

    getContactObj(){
        let  { VIEWING,selectedIndex,activeTab } = this.props;
        if(activeTab === 'applicants'){
            return VIEWING.applicants[selectedIndex];
        }
        return VIEWING.owners[selectedIndex];
    }


    getNotificationInfo(applicant){
        let { VIEWING } = this.props;
        let notification = {};
        if(VIEWING.status === 'Booked'){
            if(applicant.status==='Active'){
                notification.type = 'onConfirmation';
                notification.heading = 'Confirmation Email';
                notification.canSendEmail = !applicant.onConfirmation.isEmailDelivered;
            }
            else if(applicant.status==='Cancelled'){
                notification.type = 'onCancelled';
                notification.heading = 'Cancellation Email';
                notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                    && !applicant.onCancelled.isEmailDelivered;
            }
        }
        if(VIEWING.status === 'Rescheduled'){
            if(applicant.status==='Active'){
                if(!applicant.onConfirmation.isEmailDelivered){
                    notification.type = 'onConfirmation';
                    notification.heading = 'Confirmation Email';
                    notification.canSendEmail = true;
                }
                else{
                    notification.type = 'onRescheduled';
                    notification.heading = 'Rescheduled Email';
                    notification.canSendEmail = !applicant.onRescheduled.isEmailDelivered;
                }
            }
            else if(applicant.status==='Cancelled'){
                notification.type = 'onCancelled';
                notification.heading = 'Cancellation Email';
                notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                    && !applicant.onCancelled.isEmailDelivered;
            }
        }
        else if(VIEWING.status==='Cancelled'){
            notification.type = 'onCancelled';
            notification.heading = 'Cancellation Email';
            notification.canSendEmail = (applicant.onConfirmation.isEmailDelivered || applicant.onRescheduled.isEmailDelivered)
                && !applicant.onCancelled.isEmailDelivered;
        }
        return notification;
    }

    extractContent(s) {
        let span= document.createElement('span');
        span.innerHTML= s;
        return span.textContent || span.innerText;
    }

    renderDeliveryInfo(contactObj,notifyOn){
        if(contactObj[notifyOn].isEmailDelivered){
            return (
                <div className="email-delivery-info">Delivered at : {moment(contactObj[notifyOn].deliveredAt).format('DD/MM/YYYY')} {Helper.toTime(contactObj[notifyOn].deliveredAt)}</div>
            );
        }
        return null;
    }

    renderLine(contactObj,notifyOn,line){
        if(contactObj[notifyOn].emailMessage[line]){
            if(contactObj[notifyOn].isEmailDelivered){
                return (
                    <p>{contactObj[notifyOn].emailMessage[line]}</p>
                );
            }
            return (
                <ContentEditable
                    tagName="p"
                    className="editable-block"
                    html={contactObj[notifyOn].emailMessage[line]}
                    disabled={false}
                    onChange={(e)=> this.lineChange(notifyOn,line,e)}
                />
            )
        }
        return null;
    }

    lineChange(notifyOn,lineName,evt){
        let {
            onChange,
            selectedIndex,
            activeTab
        } = this.props;

        let name = 'applicants['+selectedIndex+'].'+notifyOn+'.emailMessage.'+lineName;
        if(activeTab==='owners'){
            name = 'owners['+selectedIndex+'].'+notifyOn+'.emailMessage.'+lineName;
        }
        let value = this.extractContent(evt.target.value);
        onChange({
            target : {
                value : value,
                name : name
            },
            type:'change'
        });
    }


    getAppraiser(id){
        let {APP} = this.props;
        let {agencyDetails} = APP;
        let appraiserName = '';
        if (agencyDetails && agencyDetails.agencyUsers) {
            let appraiser = _.find(agencyDetails.agencyUsers,(p)=>{
                return p._id === id;
            });
            if(appraiser!==undefined){
                appraiserName = appraiser.forename;
            }
        }
        return appraiserName;
    }
}
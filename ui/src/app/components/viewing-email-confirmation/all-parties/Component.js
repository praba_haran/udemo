import React from 'react';
import Applicants from './sections/Applicants';
import Owners from './sections/Owners';

class Component extends React.Component {

    render() {
        let {
            VIEWING,
            onChange,
            activeTab,
            handleTabChange,
            applicantId
        } = this.props;
        return (
            <div className="col-sm-4">
                <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                    <li role="presentation" className={(activeTab === 'applicants') ? 'active' : ''}>
                        <a onClick={() => handleTabChange('applicants')} role="button"
                           href="javascript:void(0)">Applicants</a>
                    </li>
                    <li role="presentation" className={(activeTab === 'owners') ? 'active' : ''}>
                        <a onClick={() => handleTabChange('owners')} role="button" href="javascript:void(0)">Owners</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane active">
                        {(activeTab === 'applicants' && VIEWING.applicants.length > 0) ? (<Applicants {...this.props} />) : null}
                        {(activeTab === 'owners' && VIEWING.applicants.length > 0) ? (<Owners {...this.props} />) : null}
                    </div>
                </div>
            </div>
        );
    }
}

export default Component;
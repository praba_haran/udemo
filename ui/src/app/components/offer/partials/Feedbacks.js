import React from 'react';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';
import { ValidateTextArea } from '../../shared/Form';

class Feedbacks extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: ''
        };
    }

    componentDidMount(){
        let { OFFER } = this.props;
        if(OFFER.applicants.length>0){
            this.setState({
                activeKey : OFFER.applicants[0].contact._id
            })
        }
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    render(){
        let { OFFER,onChange } = this.props;
        return (
            <div className="clear">
                <div className="scroll-area">

                    <PanelGroup bsClass="panel-group" activeKey={this.state.activeKey} onSelect={this.handleSelect.bind(this)} accordion>
                        {OFFER.applicants.map((p,index)=>{
                            let name = 'applicants['+index+'].feedback';
                            let placeHolder = p.contact.firstPerson.forename+"'s feedback here...";
                            return (
                                <Panel key={p.contact._id} header={p.contact.firstPerson.forename+' '+p.contact.firstPerson.surname} eventKey={p.contact._id}>
                                    <ValidateTextArea placeholder={placeHolder} rows="4" model={OFFER} name={name} onChange={onChange}/>
                                </Panel>
                            );
                        })}
                    </PanelGroup>
                </div>
            </div>
        );
    }
}

export default Feedbacks;
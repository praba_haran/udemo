import React from 'react';
import { ValidateTextArea } from '../../shared/Form';
import PanelGroup from 'react-bootstrap/lib/PanelGroup';
import Panel from 'react-bootstrap/lib/Panel';

class Notes extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            activeKey: 'aboutAnOffer'
        };
    }

    componentDidMount(){
        let { OFFER } = this.props;
        if(OFFER.applicants.length>0){
            this.setState({
                activeKey : OFFER.applicants[0].contact._id
            })
        }
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    render(){
        let { OFFER,onChange } = this.props;
        return (
            <div className="clear">
                <div className="scroll-area">

                    <PanelGroup bsClass="panel-group" activeKey={this.state.activeKey} onSelect={this.handleSelect.bind(this)} accordion>
                        <Panel header="About an Offer" eventKey="aboutAnOffer">
                            <ValidateTextArea rows="6" model={OFFER} name="aboutAnOffer" onChange={onChange}/>
                        </Panel>
                        <Panel header="Chain" eventKey="chainNotes">
                            <ValidateTextArea rows="6" model={OFFER} name="chainNotes" onChange={onChange}/>
                        </Panel>
                        <Panel header="Mortgage/Financial" eventKey="mortgageAndFinancialNotes">
                            <ValidateTextArea rows="6" model={OFFER} name="mortgageAndFinancialNotes" onChange={onChange}/>
                        </Panel>
                        <Panel header="Fixture And Fittings" eventKey="fixtureAndFittingsNotes">
                            <ValidateTextArea rows="6" model={OFFER} name="fixtureAndFittingsNotes" onChange={onChange}/>
                        </Panel>
                        <Panel header="Estimated Dates" eventKey="estimatedDates">
                            <ValidateTextArea rows="6" model={OFFER} name="estimatedDates" onChange={onChange}/>
                        </Panel>
                    </PanelGroup>
                </div>
            </div>
        );
    }
}

export default Notes;
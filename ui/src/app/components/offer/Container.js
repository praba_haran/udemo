import React from 'react';
import _ from 'lodash';
import store from '../../store';
import Alert from '../../lib/Alert';
import { connect } from 'react-redux';
import Component from './Component';
import * as constants from '../../constants';
import Offer from '../../models/Offer';
import Commands from '../../models/Commands';
import PropertyDetail from '../../models/PropertyDetail';
import ContactDetail from '../../models/ContactDetail';

class Container extends React.Component{

    constructor(){
        super();
        this.state = {
            isSaving : false,
            isAccepting : false,
            isRejecting : false,
            isSearchingApplicants : false,
            applicants : [],
            applicantsToFilter : [],
            canShowModal : false,

            isSearchingProperties : false,
            properties : [],
            propertiesToFilter : []
        };
    }

    componentWillReceiveProps(props){
        let { COMMANDS,OFFER,CONTACT_DETAIL, PROPERTY_DETAIL }= props;
        if(COMMANDS[constants.COMMANDS.MAKE_AN_OFFER]!==this.state.canShowModal){
            this.setState({
                canShowModal : COMMANDS[constants.COMMANDS.MAKE_AN_OFFER]
            });
            // IF THIS IS IN CONTACT DETAIL PAGE, THEN THE CONTACT HAS TO BE ADDED IN APPLICANT SECTION
            // AND IF THIS IS IN PROPERTY DETAIL PAGE, THE PROPERTY HAS TO BE ADDED IN PROPERTIES SECTION
            if(! this.state.canShowModal) {
                if (OFFER.applicants.length === 0 && CONTACT_DETAIL._id) {
                    Offer.actions.setObjectInPath(['applicants', 0], {
                        contact: CONTACT_DETAIL,
                        feedback: ''
                    });
                }
                // IF WE ARE IN PROPERTY DETAIL PAGE, THEN SET THIS PROPERTY AS DEFAULT
                if (OFFER.property === null && this.isPropertyDetailPage(props) && PROPERTY_DETAIL._id) {
                    Offer.actions.setObjectInPath(['property'],PROPERTY_DETAIL);
                }
            }
        }
    }

    isPropertyDetailPage(props){
        props = props || this.props;
        return props.location.pathname.indexOf('/properties/'+props.params.id)>-1;
    }

    cancel(){
        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER,false);
        Offer.actions.clearModel();
    }

    submitHandler(result){
        if (result.success) {
            this.cancel();
            Alert.clearLogs().success(result.message);
            Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER_CHANGED, true);
        }
        else {
            console.log(result);
        }
        this.setState({
            isSaving: false
        });
    }

    submit(){
        Offer.actions.validateModel();
        let model = store.getState().OFFER.toJS();
        if (model.validator.isValid && model.applicants.length>0 && model.property) {
            this.setState({
                isSaving: true
            });
            delete model.validator;
            model.property = model.property._id;
            if (model._id) {
                Offer.api.update(model._id, model, this.submitHandler.bind(this));
            }
            else {
                Offer.api.save(model, this.submitHandler.bind(this));
            }
        }
    }

    accept(){
        this.changeStatus('isAccepting','Accepted');
    }
    reject(){
        this.changeStatus('isRejecting','Rejected');
    }
    changeStatus(key,status){
        Offer.actions.change('status',status);
        //let { PROPERTY_DETAIL } = this.props;
        Offer.actions.validateModel();
        let model = store.getState().OFFER.toJS();
        if (model.validator.isValid && model.applicants.length>0) {
            this.setState({
                [key] : true
            });
            delete model.validator;
            //model.property = PROPERTY_DETAIL._id;
            if (model._id) {
                Offer.api.update(model._id, model,(result)=>{
                    if (result.success) {
                        this.cancel();
                        Alert.clearLogs().success('Offer '+status+' Successfully.');
                        Commands.actions.change(constants.COMMANDS.MAKE_AN_OFFER_CHANGED, true);
                    }
                    this.setState({
                        [key] : false
                    });
                });
            }
        }
    }

    onApplicantsFocus(){
        this.onApplicantsChange('');
    }
    getAllApplicants(typeAhead){
        typeAhead.focus();
    }
    onApplicantsChange(text){
        let { OFFER } = this.props;
        text = text.trim();
        this.setState({
            isSearchingOwners: true
        });
        ContactDetail.api.search({
            searchText: text,
            status : 'Active'
        }, (result)=> {
            if(result.success) {
                let propertyOwners = _.map(OFFER.property.owners,(p)=>{
                   return p.contact._id;
                });
                let existingApplicants = _.map(OFFER.applicants, (p) => {
                    return p.contact._id;
                });
                let applicants = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        forename: p.firstPerson.forename,
                        surname: p.firstPerson.surname,
                        fullName: p.firstPerson.forename + ' ' + p.firstPerson.surname,
                        address: p.address.fullAddress,
                        type : p.type
                    }
                });
                applicants = _.filter(applicants, (p) => {
                    return existingApplicants.indexOf(p._id) === -1 &&
                        propertyOwners.indexOf(p._id) === -1;
                });
                this.setState({
                    applicantsToFilter: applicants,
                    applicants: result.data,
                    isSearchingApplicants: false
                });
            }
        });
    }
    onApplicantSelected(items,typeAhead) {
        if (items.length > 0) {
            let { OFFER } = this.props;
            let index = OFFER.applicants.length;
            typeAhead.focus();
            typeAhead.clear();

            let applicant = _.find(OFFER.applicants,function(p){
                return p.contact._id === items[0]._id;
            });
            if(applicant===undefined) {
                let item = _.find(this.state.applicants,function(p){
                    return p._id === items[0]._id;
                });
                let obj = {
                    contact : item,
                    feedback : ''
                };
                Offer.actions.setObjectInPath(['applicants', index], obj);
            }
            else{
                Alert.clearLogs().error('This applicant is already added!');
            }
        }
    }
    removeApplicant(id){
        let { OFFER } = this.props;
        let index = _.findIndex(OFFER.applicants,function(p){
            return p.contact._id === id;
        });
        if(index>-1) {
            Offer.actions.removeObjectInPath(['applicants', index]);
        }
    }

    // properties typeahead
    onPropertiesFocus(typeAhead){
        typeAhead.focus();
        this.onPropertiesChange('');
    }
    onPropertiesChange(text){
        let {OFFER} = this.props;
        text = text.trim();
        this.setState({
            isSearchingProperties: true
        });
        PropertyDetail.api.search({
            searchText: text,
            statusList : ['Available']
        }, (result) => {
            if (result.success) {
                let properties = _.map(result.data, (p) => {
                    return {
                        _id: p._id,
                        price: p.price.toString(),
                        status: p.status,
                        address: p.address.fullAddress
                    }
                });
                if(OFFER.property!==null) {
                    properties = _.filter(properties, (p) => {
                        return p._id !== OFFER.property._id;
                    });
                }
                this.setState({
                    propertiesToFilter: properties,
                    properties: result.data,
                    isSearchingProperties: false
                });
            }
        });
    }
    onPropertySelected(items,typeAhead) {
        if (items.length > 0) {
            typeAhead.clear();

            let item = _.find(this.state.properties,function(p){
                return p._id === items[0]._id;
            });
            Offer.actions.setObjectInPath(['property'], item);
        }
    }

    render(){
        return (
            <Component
                onChange={Offer.actions.valueChange}
                cancel = {this.cancel.bind(this)}
                submit={this.submit.bind(this)}
                accept={this.accept.bind(this)}
                reject={this.reject.bind(this)}
                onApplicantsFocus={this.onApplicantsFocus.bind(this)}
                getAllApplicants={this.getAllApplicants.bind(this)}
                onApplicantsChange={this.onApplicantsChange.bind(this)}
                onApplicantSelected={this.onApplicantSelected.bind(this)}
                removeApplicant = {this.removeApplicant.bind(this)}
                onPropertiesFocus={this.onPropertiesFocus.bind(this)}
                onPropertiesChange={this.onPropertiesChange.bind(this)}
                onPropertySelected={this.onPropertySelected.bind(this)}
                isPropertyDetailPage={this.isPropertyDetailPage.bind(this)}
                {...this.props}
                {...this.state}
            />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        OFFER : store.OFFER.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS(),
        APP : store.APP.toJS(),
        COMMANDS : store.COMMANDS.toJS()
    };
};

export default connect(mapStateToProps)(Container);
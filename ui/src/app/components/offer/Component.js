import React from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import PropertySection from './partials/PropertySection';
import Applicants from './partials/Applicants';
import Feedbacks from './partials/Feedbacks';
import Notes from './partials/Notes';
import TypeAhead from 'react-bootstrap-typeahead';
import classNames from 'classnames';
import {ValidateText, CheckBox, PriceInput} from '../shared/Form';
import Helper from '../shared/helper';
import Knob from 'react-canvas-knob';

class Component extends React.Component {

    constructor(){
        super();
        this.state ={
            activeTab:'applicants'
        };
        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabChange(tab){
        this.setState({
            activeTab : tab
        });
    }

    onPriceChange(e){
        let { OFFER, onChange } = this.props;
        let { property } = OFFER;
        if(/^\d*(\.(\d{0,2})?)?$/.test(e.target.value)){
            let value = Number(e.target.value);
            onChange({
                target: {
                    name: 'price',
                    value: value
                },
                type: 'change'
            });
            if (property && value >0) {
                let diff = this.getPercentageDifference(property.proposedPrice, value);
                onChange({
                    target: {
                        name: 'priceInPercent',
                        value: (100 - diff.value)
                    },
                    type: 'change'
                });
            }
        }
    }

    handleKnobChange(value){
        let { OFFER,onChange } = this.props;
        let { property } = OFFER;
        onChange({
            target:{
                name:'priceInPercent',
                value : value
            },
            type:'change'
        });
        if(property) {
            onChange({
                target: {
                    name: 'price',
                    value: (value * (property.proposedPrice / 100))
                },
                type: 'change'
            });
        }
    }
    renderMenuItem(props,option,idx){
        return (
            <div className="contact-item">
                <p className="name">{option.fullName}</p>
                <p className="address">{option.address}</p>
            </div>
        );
    }

    render() {
        let {
            OFFER,
            onChange,
            cancel,
            submit,
            canShowModal,
            isSaving,
            isSearchingProperties,
            propertiesToFilter,
            onPropertiesFocus,
            onPropertiesChange,
            onPropertySelected,
            isPropertyDetailPage
        } = this.props;
        let { property } = OFFER;
        let { activeTab } = this.state;
        return (
            <Modal backdrop="static" show={canShowModal} onHide={cancel} keyboard={false}
                   dialogClassName="modal-lg">
                <Modal.Header closeButton>
                    <Modal.Title>Make An Offer</Modal.Title>
                </Modal.Header>
                <Modal.Body className="make-an-offer-page">
                    <PropertySection {...this.props} />
                    <form className="form-horizontal custom-form">
                        <div className="row">
                            <div className="col-md-6">
                                {(!OFFER._id && !isPropertyDetailPage())?(
                                    <div className="form-group margin-bottom-10">
                                        <label className="col-sm-offset-2 col-sm-9">Property</label>
                                        <div className={classNames({
                                            'col-sm-offset-2 col-sm-9':true,
                                            'has-error': (OFFER.validator.isSubmitted && OFFER.property===null)
                                        })}>
                                            <TypeAhead
                                                ref="typeahead"
                                                placeholder="Search property ..."
                                                emptyLabel={isSearchingProperties?'Loading ...':'No property found.'}
                                                minLength={0}
                                                filterBy={['price','address','_id']}
                                                labelKey="address"
                                                options={propertiesToFilter}
                                                onInputChange={onPropertiesChange}
                                                onChange ={(items)=> { onPropertySelected(items,this.refs.typeahead.getInstance()); } }
                                                onFocus={()=> onPropertiesFocus(this.refs.typeahead.getInstance())}
                                                renderMenuItemChildren={this.renderMenuItem.bind(this)}
                                            />
                                            {(OFFER.validator.isSubmitted && OFFER.property===null)?(<label className="control-label">Choose a property</label>):null}
                                        </div>
                                    </div>
                                    ): null}
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-2 col-sm-9">
                                        <PriceInput inputClass="form-control input-lg" model={OFFER} name="price" value={OFFER.price} onChange={this.onPriceChange.bind(this)} />
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-1 col-sm-10 text-center">
                                        <Knob
                                            value={OFFER.priceInPercent}
                                            onChange={this.handleKnobChange.bind(this)}
                                            thickness={0.15}
                                        />
                                        {this._renderOfferSummary()}
                                    </div>
                                </div>
                                <div className="form-group margin-bottom-10">
                                    <div className="col-sm-offset-2 col-sm-9">
                                        <CheckBox model={OFFER} name="updateApplicantLastContactedDate" onChange={onChange} label="Update Applicant's Contacted Date"/>
                                        <CheckBox model={OFFER} name="updateVendorLastContactedDate" onChange={onChange} label="Update Owner's Contacted Date"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="nav-tabs-horizontal">
                                    <ul className="nav nav-tabs nav-tabs-solid" role="tablist">
                                        <li role="presentation" className={(activeTab === 'applicants') ? 'active' : ''}>
                                            <a onClick={() => this.handleTabChange('applicants')} role="button" href="javascript:void(0)">Applicants</a>
                                        </li>
                                        {OFFER.applicants.length>0?(
                                                <li role="presentation" className={(activeTab === 'feedbacks') ? 'active' : ''}>
                                                    <a onClick={() => this.handleTabChange('feedbacks')} role="button" href="javascript:void(0)">Feedbacks</a>
                                                </li>
                                            ):null}
                                        <li role="presentation" className={(activeTab === 'notes') ? 'active' : ''}>
                                            <a onClick={() => this.handleTabChange('notes')} role="button" href="javascript:void(0)">Notes</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content">
                                        <div className="tab-pane active">
                                            {activeTab === 'applicants' ? (<Applicants {...this.props} />) : null}
                                            {activeTab === 'feedbacks' ? (<Feedbacks {...this.props} />) : null}
                                            {activeTab === 'notes' ? (<Notes {...this.props} />) : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <div className="row">
                        <div className="col-sm-6 text-left">
                            {OFFER._id?(
                                    <Button onClick={this.props.accept} className="btn btn-success">{this.props.isAccepting ? 'Accepting ...' : 'Accept'}</Button>
                                ):null}
                            {OFFER._id?(
                                    <Button onClick={this.props.reject} className="btn btn-danger">{this.props.isRejecting ? 'Rejecting ...' : 'Reject'}</Button>
                                ):null}
                        </div>
                        <div className="col-sm-6 text-right">
                            <Button onClick={cancel}>Cancel</Button>
                            <Button className="btn btn-primary" onClick={submit} disabled={isSaving}>{isSaving ? 'Saving ...' : 'Save'}</Button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }

    _renderOfferSummary(){
        let { OFFER } = this.props;
        let { property } = OFFER;
        if(property===null){
            return null;
        }
        if(OFFER.price===0){
            return null;
        }
        let difference = this.getPercentageDifference(property.contract.price,OFFER.price);
        if(difference.value>0){
            return (
                <p className="offer-description">
                    This offer <span className="highlight">{Helper.toCurrency(OFFER.price)} </span>
                    is <span className="highlight">({difference.value}%)</span> {difference.isLower?'Lower':'Higher'} than the Asking Price of
                    <span className="highlight"> {Helper.toCurrency(property.contract.price)}</span>
                </p>
            );
        }
        return (
            <p className="offer-description">
                This offer is equal to Asking Price of
                <span className="highlight"> {Helper.toCurrency(property.contract.price)}</span>
            </p>
        );
    }

    getPercentageDifference(value1,value2){
        let percentage = (value1/value2)*100;
        let difference = Math.round((100-percentage) * 100) / 100;
        return {
            isLower : difference<0,
            value : Math.abs(difference)
        };
    }
}

export default Component;
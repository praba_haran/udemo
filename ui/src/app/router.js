import React from 'react';
import { Router, Route, browserHistory, hashHistory, IndexRoute } from 'react-router';

// Layouts
import LoginLayout from './layouts/LoginLayout';
import PublicLayout from './layouts/PublicLayout';
import PrivateLayout from './layouts/PrivateLayout';

// Components
import Login from './components/account/login/Container';
import ForgotPassword from './components/account/forgot-password/Container';
import ChangePassword from './components/account/change-password/Container';
import DashboardContainer from './components/dashboard/Container';
import ContactListing from './components/contact-listing/Container';
import PropertyListing from './components/property-listing/Container';

import Contact from './components/contact-detail/Container';
import ContactOverview from './components/contact-detail/overview/Container';
import ContactDetails from './components/contact-detail/details/Container';
import ContactDetailsAbout from './components/contact-detail/details/about/Container';
import ContactDetailsInterestedProperties from './components/contact-detail/details/interested-properties/Container';
import ContactDetailsPosition from './components/contact-detail/details/position/Container';
import ContactDetailsSolicitor from './components/contact-detail/details/solicitor/Container';
import ContactDetailsAgency from './components/contact-detail/details/agency/Container';
import ContactProperties from './components/contact-detail/properties/Container';
import ContactRequirement from './components/contact-detail/requirements/Container';
import ContactDocuments from './components/contact-detail/documents/Container';

import Diary from './components/diary/Container';
import Property from './components/property-detail/Container';
import PropertyOverview from './components/property-detail/overview/Container';
import PropertyDetails from './components/property-detail/details/Container';
import PreAppraisal from './components/property-detail/details/pre-appraisal/Container';
import PostAppraisal from './components/property-detail/details/post-appraisal/Container';
import Owners from './components/property-detail/details/owners/Container';
import Summary from './components/property-detail/details/summary/Container';
import Agency from './components/property-detail/details/agency/Container';

import Mapping from './components/property-detail/mapping/Container';
import Media from './components/property-detail/media/Media';
import Photos from './components/property-detail/media/containers/Photos';
import FloorPlans from './components/property-detail/media/containers/FloorPlans';
import VirtualTours from './components/property-detail/media/VirtualTours';
import ExternalBrochures from './components/property-detail/media/containers/ExternalBrochures';
import OtherFiles from './components/property-detail/media/containers/OtherFiles';
import WebLinks from './components/property-detail/media/containers/WebLinks';
import EPC from './components/property-detail/media/containers/EPC';

import Matching from './components/property-detail/matching/Container';
import Features from './components/property-detail/features/Container';
import Rooms from './components/property-detail/rooms/Container';

import Brochures from './components/property-detail/brochures/Container';
import Settings from './components/settings/Settings';
import Users from './components/settings/users/Container';
import InvitePeople from './components/settings/invite-people/Container';
import Preferences from './components/settings/preferences/Container';
import GoogleCalendars from './components/settings/diary/GoogleCalendars';
import CalendarListing from './components/settings/diary/listing/Container';
import CalendarSetting from './components/settings/diary/CalendarContainer';

import LoginModel from './models/Login';
import PropertyTrans from './route-transition/Property';
import ContactTrans from './route-transition/Contact';
import DiaryTrans from './route-transition/Diary';

import KeyCabinets from './components/key-cabinets/Container';
import Keys from './components/keys/Container';

function requireAuth(nextState, replace) {
    if(!LoginModel.api.isLoggedIn()){
        replace('/login');
    }
}

export default (
    <Router history={hashHistory}>
        <Route component={LoginLayout}>
            <Route path="/login" component={Login}></Route>
        </Route>
        <Route component={PublicLayout}>
            <Route path="/forgot-password" component={ForgotPassword}></Route>
        </Route>
        <Route component={PublicLayout}>
            <Route path="/change-password/(:token)" component={ChangePassword}></Route>
        </Route>
        <Route component={PrivateLayout} onEnter={requireAuth}>
            <Route path="/" component={DashboardContainer}></Route>
            <Route path="contacts" component={ContactListing}></Route>
            <Route path="contacts/(:id)/" component={Contact} onEnter={ContactTrans.onEnteringContactDetail} onLeave={ContactTrans.onLeavingContactDetail}>
                <Route path="overview" component={ContactOverview}></Route>
                <Route path="details" component={ContactDetails}>
                    <IndexRoute component={ContactDetailsAbout} />
                    <Route path="interested-properties" component={ContactDetailsInterestedProperties}></Route>
                    <Route path="position" component={ContactDetailsPosition}></Route>
                    <Route path="solicitor" component={ContactDetailsSolicitor}></Route>
                    <Route path="agency" component={ContactDetailsAgency}></Route>
                </Route>
                <Route path="properties" component={ContactProperties}></Route>
                <Route path="requirements" component={ContactRequirement}></Route>
                <Route path="documents" component={ContactDocuments}></Route>
            </Route>
            <Route path="diary" component={Diary} onEnter={DiaryTrans.onEnteringDiary} onLeave={DiaryTrans.onLeavingDiary}></Route>

            <Route path="properties" component={PropertyListing}></Route>
            <Route path="properties/(:id)/" component={Property} onEnter={PropertyTrans.onEnteringPropertyDetail} onLeave={PropertyTrans.onLeavingPropertyDetail}>
                <Route path="overview" component={PropertyOverview}></Route>
                <Route path="details" component={PropertyDetails}>
                    <IndexRoute component={PreAppraisal} />
                    <Route path="post-appraisal" component={PostAppraisal}></Route>
                    <Route path="owners" component={Owners}></Route>
                    <Route path="summary" component={Summary}></Route>
                    <Route path="agency" component={Agency}></Route>
                </Route>
                <Route path="mapping" component={Mapping}></Route>
                <Route path="media" component={Media} onEnter={PropertyTrans.onEnteringMedia} onLeave={PropertyTrans.onLeavingMedia}>
                    <IndexRoute component={Photos} />
                    <Route path="floorplans" component={FloorPlans}></Route>
                    <Route path="virtual-tours" component={VirtualTours}></Route>
                    <Route path="external-brochures" component={ExternalBrochures}></Route>
                    <Route path="epc" component={EPC}></Route>
                    <Route path="other-files" component={OtherFiles}></Route>
                    <Route path="web-links" component={WebLinks}></Route>
                </Route>
                <Route path="matching" component={Matching}></Route>
                <Route path="features" component={Features}></Route>
                <Route path="rooms" component={Rooms} onEnter={PropertyTrans.onEnteringRooms} onLeave={PropertyTrans.onLeavingRooms}></Route>
                {/*<Route path="keys" component={PropertyKeys}></Route>*/}
                <Route path="brochures" component={Brochures}></Route>
            </Route>
            <Route path="key-management/cabinets" component={KeyCabinets}></Route>
            <Route path="key-management/keys" component={Keys}></Route>
            <Route path="settings" component={Settings}>
                <Route path="users" component={Users}></Route>
                <Route path="invite-people" component={InvitePeople}></Route>
                <Route path="preferences" components={Preferences}></Route>
                <Route path="diary-google" component={GoogleCalendars}>
                    <IndexRoute component={CalendarListing} />
                    <Route path="(:id)" component={CalendarSetting}></Route>
                </Route>
            </Route>
        </Route>
    </Router>
);
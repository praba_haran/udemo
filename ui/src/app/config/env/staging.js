
export default {
    APP_ENV : process.env.NODE_ENV,
    API_BASE_URL : 'https://a3eff612.ngrok.io/api/',

    cloudinary :{
        upload :{
            preset:'c8yobmcp',
            baseUrl :'https://api.cloudinary.com/v1_1/ultima-plus/upload'
        }
    },
    diary:{
        google:{
            clientId :'522798595284-v3abinpes7bk4vacthun1ujh886s75mo.apps.googleusercontent.com',
            scopes :[
                'https://www.googleapis.com/auth/calendar',
                'https://www.googleapis.com/auth/plus.login',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        }
    }
};

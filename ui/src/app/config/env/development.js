
export default {
    APP_ENV : process.env.NODE_ENV,
    API_BASE_URL : 'http://localhost:3003/api/',

    cloudinary :{
        upload :{
            preset:'veykyygx',
            baseUrl :'https://api.cloudinary.com/v1_1/ultimaplus/upload'
        }
    },
    diary:{
        google:{
            clientId :'522798595284-v3abinpes7bk4vacthun1ujh886s75mo.apps.googleusercontent.com',
            scopes :[
                'https://www.googleapis.com/auth/calendar',
                'https://www.googleapis.com/auth/plus.login',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        }
    }
};

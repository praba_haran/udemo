
export default {
    APP_ENV : process.env.NODE_ENV,
    // API_BASE_URL : 'https://ultima-plus-api.herokuapp.com/api/',
    API_BASE_URL : 'http:localhost:8082/api/',
    //API_BASE_URL : 'https://a3eff612.ngrok.io/api/',

    cloudinary :{
        upload :{
            preset:'c8yobmcp',
            baseUrl :'https://api.cloudinary.com/v1_1/ultima-plus/upload'
        }
    },
    diary:{
        google:{
            clientId :'442046038763-63qkrkg00007e8rtq9imgbtskg0n8h54.apps.googleusercontent.com',
            scopes :[
                'https://www.googleapis.com/auth/calendar',
                'https://www.googleapis.com/auth/plus.login',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        }
    }
};

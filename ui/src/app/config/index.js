
// const env = process.env.NODE_ENV;
const env = 'development';
import development from './env/development';
import production from './env/production';
import staging from './env/staging';

let config = null;

if(env==='development'){
    config = development;
}
else if(env==='staging'){
    config = staging;
}
else{
    config = production;
}

export default config;

import _ from 'lodash';
import ruleConfig from './rules';
import validators from './validators';

let validationHistory = {};

function hasErrorAlready(key,rule){
    let errors = validationHistory[key];
    let hasError = false;
    if(errors){
        let index = _.findIndex(errors,(p)=>{
            return p.name === rule.name;
        });
    }
    return hasError;
}

function validateRule(key,rules,model,errors){
    _.each(rules, function (r,index) {
        let value = _.get(model, r.name);
        let validator = validators[r.type] || r.getValidator();

        if(validator){
            let isValid = validator(model,r,value);
            if(isValid){
                if(r.getChildRules){
                    let subRules = r.getChildRules(model);
                    validateRule(key,subRules,model,errors);
                }
                if(r.next&& r.next.length>0){
                    validateRule(key,r.next,model,errors);
                }
            }
            else{
                errors.push({
                    name : r.name,
                    message : r.message,
                    type : r.type
                });
            }
        }
    });
}

export function validate(key, model) {
    let errors = [];
    let rules = ruleConfig[key];
    if(rules){
        validateRule(key,rules,model,errors);
    }
    return {
        isSubmitted : true,
        isValid : (errors.length===0),
        errors : errors
    };
}

export function validateControl(key,controlName,model) {
    let errors = [];
    let rules = ruleConfig[key];
    if(rules){
        let allRules = [];
        _.each(rules,function (p) {
            if(typeof(p.getChildRules)==='function'){
                allRules = _.concat(allRules,p.getChildRules(model));
            }
            else{
                allRules.push(p);
            }
        });
        allRules = _.filter(allRules,function(r){
           return r.name === controlName;
        });
        validateRule(allRules,model,errors);
    }
    //set Focus
    // if(errors.length>0){
    //     let error = errors[0];
    //     let elements = document.getElementsByName(error.name);
    //     if(elements.length>0){
    //         console.log('focus');
    //         elements[0].focus();
    //     }
    // }
    return {
        isSubmitted : true,
        isValid : (errors.length===0),
        errors : errors
    };
}
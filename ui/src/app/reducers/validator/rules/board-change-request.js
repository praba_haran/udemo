import _ from 'lodash';

export default [
    {
        dataType: "string",
        name: "property",
        message: "Please choose property.",
        type: "required"
    },
    {
        dataType: "string",
        name: 'currentBoard',
        message: "Current Board is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: 'newBoard',
        message: "New Board is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: 'boardContractor',
        message: "Board Contractor is required.",
        type: "required"
    }
];
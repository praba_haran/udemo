import _ from 'lodash';

export default [
    {
        dataType: "string",
        name: "property",
        message: "Please choose property.",
        type: "required"
    },
    {
        dataType: "string",
        name: 'notes',
        message: "Notes is required.",
        type: "required"
    }
];
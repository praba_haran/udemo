import _ from 'lodash';

export default [
    {
        dataType:"string",
        name: "type",
        message: "Property type is required.",
        type: "required"
    },
    {
        dataType:"string",
        name: "address.street",
        message: "Street is required.",
        type: "required"
    },
    {
        dataType:"string",
        name: "address.town",
        message: "Town is required.",
        type: "required",
        next :[
            {
                dataType:"string",
                name: "address.town",
                message: "Please enter valid letters.",
                type: "validName"
            }
        ]
    },
    {
        dataType:"string",
        name: "address.county",
        message: "Please enter valid letters.",
        type: "validName"
    },
    {
        dataType:"string",
        name: "address.country",
        message: "Country is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "address.postcode",
        message: "Postcode is required.",
        type: "required"
    },
    {
        dataType:"string",
        name: "agency",
        message: "Agency is required.",
        type: "required"
    },
    // {
    //     dataType:"string",
    //     name: "negotiator",
    //     message: "Negotiator is required.",
    //     // type: "required"
    // },
    {
        dataType: "array",
        name: "rooms",
        message: "Rooms is required.",
        type: "requiredArray",
        getChildRules:function(model){
            let rules = [];
            _.each(model.rooms, function (p, index) {
                rules.push({
                    dataType: "string",
                    name: 'rooms[' + index + '].name',
                    message: "Room name is required.",
                    type: "required"
                });
            });
            return rules;
        },
        getValidator:function(){
            return function (model) {
                if(model.rooms.length===0) return true;
                return true;
            };
        }
    },
    {
        dataType: "array",
        name: "webLinks",
        message: "Web Links is required.",
        type: "optionalArray",
        getChildRules:function(model){
            let rules = [];
            _.each(model.webLinks, function (p, index) {
                rules.push({
                    dataType: "string",
                    name: 'webLinks[' + index + '].title',
                    message: "Title is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'webLinks[' + index + '].url',
                    message: "Url is required.",
                    type: "required",
                    next : [{
                        dataType: "string",
                        name: 'webLinks[' + index + '].url',
                        message: "Please enter valid url.",
                        type: "url"
                    }]
                });
            });
            return rules;
        },
        getValidator:function(){
            return function (model) {
                return true;
            };
        }
    },
    {
        dataType:"number",
        name: "highPrice",
        message: "Should be higher than the low price.",
        type: "optional",
        getValidator : function(){
            return function(model){
                return model.highPrice >= model.lowPrice;
            }
        }
    }
];
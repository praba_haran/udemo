import _ from 'lodash';

export default [
    {
        dataType: "string",
        name: 'calendar.summary',
        message: "Calendar name is required.",
        type: "required"
    },
    {
        dataType: "array",
        name: "permissions",
        message: "Permission is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.calendar) {
                _.each(model.calendar.permissions, function (p, index) {
                    rules.push({
                        dataType: "string",
                        name: 'permissions[' + index + '].email',
                        message: "Please enter valid email.",
                        type: "email"
                    });
                });
            }
            return rules;
        }
    }
];
import _ from 'lodash';

let isEmailDuplicated = function (index, emails) {
    let emailArray = _.map(emails, function (p) {
        return p.email;
    });
    if (index > 0) {
        return emailArray[index].toLowerCase() !== emailArray[index - 1].toLowerCase();
    }
    return true;
};

let isPhoneNoDuplicated = function (currentIndex, phones) {
    if (currentIndex > 0) {
        let hasPhone = _.find(phones, function (p, index) {
            return currentIndex !== index && currentIndex > index && phones[currentIndex].number === p.number;
        });
        return hasPhone === undefined;
    }
    return true;
};

export default [
    {
        dataType: "string",
        name: "type",
        message: "Contact type is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "companyName",
        message: "Company is required.",
        type: "optional",
        getValidator : function(){
            return function(model){
                return true;
            }
        },
        getChildRules : function(model){
            let rules = [];
            if(model.type !== 'Client'){
                rules.push({
                    dataType: "string",
                    name: 'companyName',
                    message: "Company is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'companyWebsite',
                    message: "Website is required.",
                    type: "required",
                    next : [{
                        dataType: "string",
                        name: 'companyWebsite',
                        message: "Please enter valid url.",
                        type: "url"
                    }]
                });
            }
            return rules;
        }
    },
    {
        dataType:"string",
        name: "agency",
        message: "Agency is required.",
        type: "required"
    },
    // {
    //     dataType:"string",
    //     name: "negotiator",
    //     message: "Negotiator is required.",
    //     type: "required"
    // },
    {
        dataType: "string",
        name: "address.street",
        message: "Street is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "address.town",
        message: "Town is required.",
        type: "required",
        next :[
            {
                dataType:"string",
                name: "address.town",
                message: "Please enter valid letters.",
                type: "validName"
            }
        ]
    },
    {
        dataType: "string",
        name: "address.country",
        message: "Country is required.",
        type: "required"
    },
    {
        dataType:"string",
        name: "address.county",
        message: "Please enter valid letters.",
        type: "validName"
    },
    {
        dataType: "string",
        name: "address.postcode",
        message: "Postcode is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "firstPerson.forename",
        message: "Forename is required.",
        type: "required",
        next: [{
            dataType: "string",
            name: "firstPerson.forename",
            message: "Please enter valid name.",
            type: "validName"
        }
        ]
    },
    {
        dataType: "string",
        name: "firstPerson.surname",
        message: "Surname is required.",
        type: "required",
        next: [{
            dataType: "string",
            name: "firstPerson.surname",
            message: "Please enter valid name.",
            type: "validName"
        }
        ]
    },
    {
        dataType: "string",
        name: "firstPerson.salutation",
        message: "Salutation is required.",
        type: "required",
        next :[{
            dataType: "string",
            name: "firstPerson.salutation",
            message: "Please enter valid salutation.",
            type: "validName"
        }]
    },
    {
        dataType: "array",
        name: "firstPerson.emails",
        message: "Email is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.firstPerson && model.firstPerson.emails) {
                _.each(model.firstPerson.emails, function (p, index) {
                    rules.push({
                        dataType: "string",
                        name: 'firstPerson.emails[' + index + '].email',
                        message: "Email is required.",
                        type: "required",
                        next: [
                            {
                                dataType: "string",
                                name: 'firstPerson.emails[' + index + '].email',
                                message: "Please enter valid email.",
                                type: "email"
                            },
                            {
                                dataType: "string",
                                name: 'firstPerson.emails[' + index + '].email',
                                message: "This email is already exist.",
                                type: "emailDuplicate",
                                getValidator: function () {
                                    return function (model) {
                                        return isEmailDuplicated(index, model.firstPerson.emails);
                                    }
                                }
                            }
                        ]
                    })
                });
            }
            return rules;
        }
    },
    {
        dataType: "array",
        name: "firstPerson.telephones",
        message: "Number is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.firstPerson && model.firstPerson.telephones) {
                _.each(model.firstPerson.telephones, function (p, index) {
                    rules.push({
                        dataType: "string",
                        name: 'firstPerson.telephones[' + index + '].number',
                        message: "Number is required.",
                        type: "required",
                        next: [
                            {
                                dataType: "string",
                                name: 'firstPerson.telephones[' + index + '].number',
                                message: "Please enter valid phone no.",
                                type: "phone"
                            },
                            {
                                dataType: "string",
                                name: 'firstPerson.telephones[' + index + '].number',
                                message: "Phone no is already exist.",
                                type: "phoneDuplicate",
                                getValidator: function () {
                                    return function (model) {
                                        return isPhoneNoDuplicated(index, model.firstPerson.telephones);
                                    }
                                }
                            }
                        ]
                    })
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "secondPerson.forename",
        message: "Please enter valid name.",
        type: "validName",
        getChildRules: function (model) {
            let rules = [];
            if(model.secondPerson.forename){
                rules.push({
                    dataType: "string",
                    name: "secondPerson.surname",
                    message: "Please enter surname.",
                    type: "required"
                });
            }
            rules.push({
                dataType: "string",
                name: "secondPerson.forename",
                message: "Already exist in first person.",
                type: "fornameDuplicate",
                getValidator: function () {
                    return function (model) {
                        if(model.secondPerson.surname && model.firstPerson.surname === model.secondPerson.surname){
                            return (model.firstPerson.forename !== model.secondPerson.forename);
                        }
                        return true;
                    }
                }
            });
            return rules;
        }
    },
    {
        dataType: "string",
        name: "secondPerson.surname",
        message: "Please enter valid name.",
        type: "validName",
        getChildRules: function (model) {
            let rules = [];
            if(model.secondPerson.surname){
                rules.push({
                    dataType: "string",
                    name: "secondPerson.forename",
                    message: "Please enter forename.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "array",
        name: "secondPerson.emails",
        message: "Email is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.secondPerson && model.secondPerson.emails) {
                _.each(model.secondPerson.emails, function (p, index) {
                    if(model.secondPerson.forename || model.secondPerson.surname) {
                        rules.push({
                            dataType: "string",
                            name: 'secondPerson.emails[' + index + '].email',
                            message: "Email is required.",
                            type: "required",
                            next: [{
                                dataType: "string",
                                name: 'secondPerson.emails[' + index + '].email',
                                message: "Please enter valid email.",
                                type: "email",
                                next: [
                                    {
                                        dataType: "string",
                                        name: 'secondPerson.emails[' + index + '].email',
                                        message: "This email is already exist.",
                                        type: "emailDuplicate",
                                        getValidator: function () {
                                            return function (model) {
                                                return isEmailDuplicated(index, model.secondPerson.emails);
                                            }
                                        },
                                        next: [
                                            {
                                                dataType: "string",
                                                name: 'secondPerson.emails[' + index + '].email',
                                                message: "Already exist in first person.",
                                                type: "emailDuplicate",
                                                getValidator: function () {
                                                    return function (model) {
                                                        if (!model.secondPerson.emails[index].email) return true;

                                                        let emailIndex = _.findIndex(model.firstPerson.emails, function (p) {
                                                            return p.email === model.secondPerson.emails[index].email;
                                                        });
                                                        return emailIndex === -1;
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }]
                        });
                    }
                });
            }
            return rules;
        }
    },
    {
        dataType: "array",
        name: "secondPerson.telephones",
        message: "Number is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.secondPerson && model.secondPerson.telephones) {
                _.each(model.secondPerson.telephones, function (p, index) {
                    if(model.secondPerson.forename || model.secondPerson.surname) {
                        rules.push({
                            dataType: "string",
                            name: 'secondPerson.telephones[' + index + '].number',
                            message: "Number is required.",
                            type: "required",
                            next: [{
                                dataType: "string",
                                name: 'secondPerson.telephones[' + index + '].number',
                                message: "Please enter valid phone no.",
                                type: "phone",
                                next: [
                                    {
                                        dataType: "string",
                                        name: 'secondPerson.telephones[' + index + '].number',
                                        message: "This phone no is already exist.",
                                        type: "phoneDuplicate",
                                        getValidator: function () {
                                            return function (model) {
                                                return isPhoneNoDuplicated(index, model.secondPerson.telephones);
                                            }
                                        },
                                        next: [
                                            {
                                                dataType: "string",
                                                name: 'secondPerson.telephones[' + index + '].number',
                                                message: "Already exist in first person.",
                                                type: "phoneDuplicate",
                                                getValidator: function () {
                                                    return function (model) {
                                                        if (!model.secondPerson.telephones[index].number) return true;

                                                        let phoneIndex = _.findIndex(model.firstPerson.telephones, function (p) {
                                                            return p.number === model.secondPerson.telephones[index].number;
                                                        });
                                                        return phoneIndex === -1;
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }]
                        });
                    }
                });
            }
            return rules;
        }
    }
];
import contact from './contact';
import property from './property';
import inviteUsers from './invite-users';
import googleCalendar from './google-calendar';
import marketAppraisal from './market-appraisal';
import viewing from './viewing';
import generalAppointment from './general-appointment';
import propertyStatusChange from './property-status-change';
import instructProperty from './instruct-property';
import boardChangeRequest from './board-change-request';
import termsSent from './terms-sent';
import termsSigned from './terms-signed';
import detailsToVendor from './details-to-vendor';

export default {
    CONTACT : contact,
    CONTACT_DETAIL : contact,
    PROPERTY : property,
    PROPERTY_DETAIL : property,
    INVITE_USERS : inviteUsers,
    GOOGLE_CALENDAR : googleCalendar,
    MARKET_APPRAISAL : marketAppraisal,
    VIEWING : viewing,
    GENERAL_APPOINTMENT : generalAppointment,
    PROPERTY_STATUS_CHANGE : propertyStatusChange,
    INSTRUCT_PROPERTY : instructProperty,
    BOARD_CHANGE_REQUEST : boardChangeRequest,
    TERMS_SENT : termsSent,
    TERMS_SIGNED : termsSigned,
    DETAILS_TO_VENDOR : detailsToVendor
};


import _ from 'lodash';

export default [
    {
        dataType: "array",
        name: "users",
        message: "Email is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.users) {
                _.each(model.users, function (p, index) {
                    rules.push({
                        dataType: "string",
                        name: 'users[' + index + '].forename',
                        message: "Please enter valid name.",
                        type: "validName"
                    });
                    rules.push({
                        dataType: "string",
                        name: 'users[' + index + '].surname',
                        message: "Please enter valid name.",
                        type: "validName"
                    });
                    rules.push({
                        dataType: "string",
                        name: 'users[' + index + '].email',
                        message: "Email is required.",
                        type: "needEmail",
                        getValidator: function () {
                            return function (model) {
                                if(!model.users[index].forename && !model.users[index].surname) return true;
                                return model.users[index].email!=='';
                            }
                        }
                    });
                    rules.push({
                        dataType: "string",
                        name: 'users[' + index + '].email',
                        message: "Please enter valid email.",
                        type: "email",
                        next: [
                            {
                                dataType: "string",
                                name: 'users[' + index + '].email',
                                message: "This email is already exist.",
                                type: "emailDuplicate",
                                getValidator: function () {
                                    return function (model) {
                                        if(!model.users[index].email) return true;

                                        let emailArray = _.map(model.users, function (p) {
                                            return p.email;
                                        });
                                        if (index > 0) {
                                            return emailArray[index].toLowerCase() !== emailArray[index - 1].toLowerCase();
                                        }
                                        return true;
                                    }
                                }
                            }
                        ]
                    });
                });
            }
            return rules;
        }
    }
];
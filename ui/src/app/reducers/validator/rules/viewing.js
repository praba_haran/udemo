import moment from 'moment';
import Helper from '../../../components/shared/helper';

export default [
    // {
    //     dataType: "string",
    //     name: "property",
    //     message: "Please choose property.",
    //     type: "required"
    // },
    {
        dataType: "string",
        name: "appraiser",
        message: "Appraiser is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "date",
        message: "Date is required.",
        type: "required"
    },
    {
        dataType: "time",
        name: "startTime",
        message: "Start time is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            // IF VIEWING IS ALREADY BOOKED DON'T ADD THIS RULE
            if(model.date && !model._id){
                rules.push({
                    dataType: "time",
                    name: 'startTime',
                    message: "Select future time.",
                    type: "conditional",
                    getValidator : function () {
                        return function (model){
                            let hourMinutes = Helper.getHourMinutesFromTime(model.startTime);
                            let date = moment(model.date).set({
                                hour : hourMinutes.hour,
                                minute: hourMinutes.minutes,
                            });
                            return moment(date).isAfter(moment());
                        }
                    }
                });
                rules.push({
                    dataType: "time",
                    name: 'endTime',
                    message: "Select future time.",
                    type: "conditional",
                    getValidator : function () {
                        return function (model){
                            let hourMinutes = Helper.getHourMinutesFromTime(model.endTime);
                            let date = moment(model.date).set({
                                hour : hourMinutes.hour,
                                minute: hourMinutes.minutes,
                            });
                            return moment(date).isAfter(moment());
                        }
                    }
                });
                rules.push({
                    dataType: "time",
                    name: 'endTime',
                    message: "Set minimum 30 mins",
                    type: "conditional",
                    getValidator : function () {
                        return function (model){
                            let hourMinutes = Helper.getHourMinutesFromTime(model.startTime);
                            let startDate = moment(model.date).set({
                                hour : hourMinutes.hour,
                                minute: hourMinutes.minutes,
                            });

                            hourMinutes = Helper.getHourMinutesFromTime(model.endTime);
                            let endDate = moment(model.date).set({
                                hour : hourMinutes.hour,
                                minute: hourMinutes.minutes,
                            });

                            return moment(endDate).isAfter(startDate);
                        }
                    }
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "notes",
        message: "Notes is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "reminder",
        message: "Reminder is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if(model.reminder!=='None'){
                rules.push({
                    dataType: "string",
                    name: 'reminderChannel',
                    message: "Channel is required.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "reminderChannel",
        message: "Channel is required.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules: function (model) {
            let rules = [];
            if(model.reminderChannel!==''){
                rules.push({
                    dataType: "string",
                    name: 'reminder',
                    message: "Reminder is required.",
                    type: "conditional",
                    getValidator : function () {
                        return function (model){
                            return model.reminder!=='None';
                        }
                    }
                });
            }
            return rules;
        }
    }
];
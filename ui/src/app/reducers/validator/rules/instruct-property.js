import _ from 'lodash';
import store from '../../../store'

export default [
    {
        dataType: "string",
        name: "property",
        message: "Please choose property.",
        type: "required"
    },
    {
        dataType: "positiveNumber",
        name: "price",
        message: "Price is required.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules : function(model){
            let rules = [];
            let PROPERTY_DETAIL = store.getState().PROPERTY_DETAIL.toJS();
            if(PROPERTY_DETAIL.market==='To Let'){
                rules.push({
                    dataType: "positiveNumber",
                    name: 'price',
                    message: "Rent Amount is required.",
                    type: "required"
                });
            }
            else{
                rules.push({
                    dataType: "positiveNumber",
                    name: 'price',
                    message: "Asking Price is required.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "contractLength",
        message: "Contract Length is required.",
        type: "required"
    },
    {
        dataType: "date",
        name: "contractEndsOn",
        message: "Contract End Date is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "saleTerms",
        message: "Sale Terms is required when contract type is For Sale.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules : function(model){
            let rules = [];
            if(model.contractType === 'For Sale'){
                rules.push({
                    dataType: "string",
                    name: 'saleTerms',
                    message: "Sale Terms is required.",
                    type: "required"
                });
            }
            else{
                rules.push({
                    dataType: "string",
                    name: 'rentalFrequency',
                    message: "Rental Frequency is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'priceQualifier',
                    message: "Price Qualifier is required.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "requireBoardChange",
        message: "Require Board Change is optional.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules: function (model) {
            let rules = [];
            if(model.actions.requireBoardChange){
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.currentBoard',
                    message: "Current Board is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.newBoard',
                    message: "New Board is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.boardContractor',
                    message: "Board Contractor is required.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "object",
        name: "discount",
        message: "Discount is optional.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules: function (model) {
            let rules = [];
            if(model.discount.appliesFrom!==null && model.discount.percent===0 && model.discount.fee===0){
                rules.push({
                    dataType: "number",
                    name: 'discount.fee',
                    message: "Percent or Fee is required.",
                    type: "required"
                });
            }
            else if(model.discount.appliesFrom===null && (model.discount.percent>0 || model.discount.fee>0)){
                rules.push({
                    dataType: "date",
                    name: 'discount.appliesFrom',
                    message: "Date is required.",
                    type: "required"
                });
            }
            return rules;
        }
    },
    {
        dataType: "string",
        name: "management.availableOn",
        message: "Available Date is required.",
        type: "required"
    },
    {
        dataType: "number",
        name: "commissionAndFees.commissionMin",
        message: "Should be less than the Max.",
        type: "custom",
        getValidator : function(){
            return function (model) {
                var commissionMin = Number(model.commissionAndFees.commissionMin);
                var commissionMax = Number(model.commissionAndFees.commissionMax);
                return ( commissionMax ===0 || commissionMin <= commissionMax );
            }
        },
        getChildRules: function (model) {
            let rules = [];
            var commissionMax = Number(model.commissionAndFees.commissionMax);
            if(commissionMax>0){
                rules.push({
                    dataType: "number",
                    name: 'commissionAndFees.commissionMin',
                    message: "Commission Min is required.",
                    type: "required"
                });
            }
            return rules;
        }
    }
];
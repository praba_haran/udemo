import _ from 'lodash';

export default [
    {
        dataType: "string",
        name: "property",
        message: "Please choose property.",
        type: "required"
    },
    {
        dataType: "array",
        name: "recipients",
        message: "Recipients is required.",
        type: "required",
        getChildRules: function (model) {
            let rules = [];
            if (model.recipients && model.recipients.length>0) {
                _.each(model.recipients, function (p, index) {
                    rules.push({
                        dataType: "string",
                        name: 'recipients['+index+'].email.subject',
                        message: "Subject is required.",
                        type: "required"
                    });
                    rules.push({
                        dataType: "string",
                        name: 'recipients['+index+'].email.to',
                        message: "Email is required.",
                        type: "required",
                        next: [
                            {
                                dataType: "string",
                                name: 'recipients['+index+'].email.to',
                                message: "Please enter valid email.",
                                type: "email"
                            }
                        ]
                    });
                    rules.push({
                        dataType: "string",
                        name: 'recipients['+index+'].email.message',
                        message: "Message is required.",
                        type: "required"
                    });
                    rules.push({
                        dataType: "string",
                        name: 'recipients['+index+'].letter.message',
                        message: "Message is required.",
                        type: "required"
                    });
                });
            }
            return rules;
        }
    }
];
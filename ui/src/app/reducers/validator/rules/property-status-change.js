import _ from 'lodash';

export default [
    {
        dataType: "string",
        name: "property",
        message: "Please choose property.",
        type: "required"
    },
    {
        dataType: "string",
        name: "oldStatus",
        message: "Old Status is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "status",
        message: "Status is required.",
        type: "required"
    },
    {
        dataType: "string",
        name: "requireBoardChange",
        message: "Require Board Change is optional.",
        type: "optional",
        getValidator : function(){
            return function (model) {
                return true;
            }
        },
        getChildRules: function (model) {
            let rules = [];
            if(model.actions.requireBoardChange){
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.currentBoard',
                    message: "Current Board is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.newBoard',
                    message: "New Board is required.",
                    type: "required"
                });
                rules.push({
                    dataType: "string",
                    name: 'boardChangeRequest.boardContractor',
                    message: "Board Contractor is required.",
                    type: "required"
                });
            }
            return rules;
        }
    }
];
export default function isValid(model,rule,value){
    if(rule.dataType==='string') {
        if (value) {
            value = value.trim();
        }
        return (value !== null && value !== undefined && value !== '');
    }
    else if(rule.dataType==='number') {
        return (value !== null && value !== undefined && value !== '' && value!==0);
    }
    else if(rule.dataType==='positiveNumber') {
        return (value !== null && value !== undefined && value !== '' && value>0);
    }
    else if(rule.dataType==='date') {
        return (value !== null && value !== undefined && value !== '');
    }
    else if(rule.dataType==='array'){
        return value.length>0;
    }
    else if(rule.dataType ==='time'){
        return value>0;
    }
    return false;
}
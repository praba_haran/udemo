import validator from 'validator';

export default function isValid(model,rule,value){
    if(rule.dataType==='string') {
        if (!value) return true;
        value = value.trim();

        //let regEx = /^(http[s]:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        //return regEx.test(value);
        return validator.isURL(value,{
            protocols:['http','https'],
            require_protocol : false
        });
    }
    return false;
}
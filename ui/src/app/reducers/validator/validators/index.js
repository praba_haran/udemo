import required from './required';
import email from './email';
import phone from './phone';
import validName from './validName';
import url from './url';

export default {
    required : required,
    email : email,
    phone : phone,
    url : url,
    validName : validName
};
export default function isValid(model,rule,value){
    if(rule.dataType==='string') {
        if (!value) return true;
        value = value.trim();
        
        let regEx = /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/i;
        return regEx.test(value);
    }
    return false;
}
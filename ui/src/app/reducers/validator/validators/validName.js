export default function isValid(model,rule,value){
    if(rule.dataType==='string') {
        if (!value) return true;
        value = value.trim();

        let regEx = /^[a-zA-Z ,.'-]+$/i;
        return regEx.test(value);
    }
    return false;
}
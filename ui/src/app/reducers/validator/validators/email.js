export default function isValid(model,rule,value){
    if(rule.dataType==='string') {
        if (!value) return true;
        value = value.trim();
        
        let regEx = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regEx.test(value);
    }
    return false;
}
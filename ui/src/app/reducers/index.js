import { combineReducers } from 'redux';
import App from '../models/App';
import MarketAppraisal from '../models/MarketAppraisal';
import Viewing from '../models/Viewing';
import FileNotes from '../models/FileNotes';
import Offer from '../models/Offer';
import GeneralAppointment from '../models/GeneralAppointment';
import Login from '../models/Login';
import ForgotPassword from '../models/ForgotPassword';
import ChangePassword from '../models/ChangePassword';
import Dashboard from '../models/Dashboard';
import Commands  from '../models/Commands';
import Property from '../models/Property';
import PropertyDetail from '../models/PropertyDetail';
import PropertyListing from '../models/PropertyListing';
import Matching from '../models/Matching';
import Contact from '../models/Contact';
import ContactDetail from '../models/ContactDetail';
import ContactListing from '../models/ContactListing';
import Requirement from '../models/Requirement';
import Diary from '../models/Diary';
import Users from '../models/Users';
import InviteUsers from '../models/InviteUsers';
import GoogleCalendar from '../models/GoogleCalendar';
import GoogleCalendars from '../models/GoogleCalendars';
import GoogleDiary from '../models/GoogleDiary';

import KeyListing from '../models/KeyListing';
import PropertyStatusChange from '../models/PropertyStatusChange';
import InstructProperty from '../models/InstructProperty';
import TermsSent from '../models/TermsSent';
import TermsSigned from '../models/TermsSigned';
import BoardChangeRequest from '../models/BoardChangeRequest';
import DetailsToVendor from '../models/DetailsToVendor';
import Activity from '../models/Activity';
import CabinetMasterListing from '../models/CabinetMasterListing';
import ErrorMessages from '../models/ErrorMessages';

const reducers = combineReducers({

    APP : App.reducer,
    LOGIN : Login.reducer,
    FORGOT_PASSWORD : ForgotPassword.reducer,
    CHANGE_PASSWORD : ChangePassword.reducer,
    DASHBOARD : Dashboard.reducer,
    MARKET_APPRAISAL : MarketAppraisal.reducer,
    VIEWING : Viewing.reducer,
    FILE_NOTES : FileNotes.reducer,
    OFFER : Offer.reducer,
    GENERAL_APPOINTMENT : GeneralAppointment.reducer,
    COMMANDS : Commands.reducer,
    PROPERTY : Property.reducer,
    PROPERTY_DETAIL: PropertyDetail.reducer,
    PROPERTY_LISTING : PropertyListing.reducer,
    MATCHING : Matching.reducer,
    CONTACT : Contact.reducer,
    CONTACT_REQUIREMENT : Requirement.reducer,
    CONTACT_DETAIL : ContactDetail.reducer,
    CONTACT_LISTING : ContactListing.reducer,
    DIARY : Diary.reducer,
    USERS : Users.reducer,
    INVITE_USERS : InviteUsers.reducer,
    GOOGLE_CALENDAR : GoogleCalendar.reducer,
    GOOGLE_CALENDARS : GoogleCalendars.reducer,
    GOOGLE_DIARY : GoogleDiary.reducer,

    KEY_LISTING : KeyListing.reducer,
    PROPERTY_STATUS_CHANGE : PropertyStatusChange.reducer,
    BOARD_CHANGE_REQUEST : BoardChangeRequest.reducer,
    INSTRUCT_PROPERTY : InstructProperty.reducer,
    TERMS_SENT : TermsSent.reducer,
    TERMS_SIGNED : TermsSigned.reducer,
    DETAILS_TO_VENDOR : DetailsToVendor.reducer,
    ACTIVITY : Activity.reducer,
    CABINET_MASTER_LISTING : CabinetMasterListing.reducer,
    ERROR_MESSAGES : ErrorMessages.reducer
});

export default reducers;

let alertify = window.alertify;
alertify.theme('bootstrap');
alertify.delay(3000);
alertify.logPosition("top right");

export default alertify;
import uuid from 'uuid';
import sha1 from 'sha1';
import async from 'async';
import request from 'superagent';

class Cloudinary{

    constructor(){
        this.preset = 'veykyygx';
        this.secret = 'FtUdBWCR2F3H00rqdTfwEwfBjkU';
        this.apiKey = '489831626177541';
    }

    getSignature(timeStamp,public_id){
        let message = 'public_id='+public_id+'&timestamp='+timeStamp+this.secret;
        return sha1(message);
    }

    upload(files,config){

        let _this = this;
        let tasks = _.map(files, function (file) {
            return function (cb) {
                let timeStamp = new Date().getTime();
                let public_id = uuid.v1();
                let upload = request.post('https://api.cloudinary.com/v1_1/ultimaplus/upload')
                    .field('timestamp',timeStamp)
                    .field('public_id',public_id)
                    .field('api_key',_this.apiKey)
                    .field('file', file)
                    .field('signature',_this.getSignature(timeStamp,public_id));

                if(config.onProgress){
                    upload.on('progress',config.onProgress);
                }
                upload.end((err,res)=>{
                    if(err){
                        cb(err,null);
                    }
                    else{
                        let data = res.body;
                        cb(null,data);
                    }
                });
            }
        });
        async.parallel(tasks, (err, data)=> {
            if (err) {
                config.onError(err);
            }
            else {
                config.onFinished(data);
            }
        });
    }

    destroy(public_ids,config){
        let _this = this;
        let tasks = _.map(public_ids, function (public_id) {
            let timeStamp = new Date().getTime();
            return function (cb) {
                let isImage = public_id.split('.').length === 1;
                let destroyUrl = 'https://api.cloudinary.com/v1_1/ultima-plus/raw/destroy';
                if(isImage){
                    destroyUrl = 'https://api.cloudinary.com/v1_1/ultima-plus/image/destroy';
                }
                let upload = request.post(destroyUrl)
                    .field('timestamp',timeStamp)
                    .field('public_id',public_id)
                    .field('api_key',_this.apiKey)
                    .field('signature',_this.getSignature(timeStamp,public_id));

                if(!isImage){
                    upload.field('resource_type','raw');
                }

                if(config.onProgress){
                    upload.on('progress',config.onProgress);
                }
                upload.end((err,res)=>{
                    if(err){
                        cb(err,null);
                    }
                    else{
                        let data = res.body;
                        cb(null,data);
                    }
                });
            }
        });
        async.parallel(tasks, (err, data)=> {
            if (err) {
                config.onError(err);
            }
            else {
                config.onFinished(data);
            }
        });
    }
}

export default new Cloudinary();

import config from '../config';
import async from 'async';
import _ from 'lodash';

let getUserProfile = function (cb){
    gapi.client.load('oauth2','v2',function(){
        gapi.client.oauth2.userinfo.get().execute(function(data) {
            cb(data.result);
        });
    });
};

let getAddPermissionHandler = function(calendarId,model){

    return function(cb){
        let payload = {};
        if(model.status==='active') {
            if (model.id !== '') {
                payload.calendarId = calendarId;
                payload.ruleId = model.id;
                payload.role = model.role;
                payload.scope = {
                    type: model.scope.type,// default|user|group|domain
                    value: model.scope.value
                };
                let request = gapi.client.calendar.acl.update(payload);

                request.execute(function (resp) {
                    if (cb) cb(null, resp);
                });
            }
            else {
                payload.calendarId = calendarId;
                payload.role = model.role;
                payload.scope = {
                    type: model.scope.type,// default|user|group|domain
                    value: model.scope.value
                };
                let request = gapi.client.calendar.acl.insert(payload);

                request.execute(function (resp) {
                    if (cb) cb(null, resp);
                });
            }
        }
        else if(model.status==='deleted' && model.id!==''){
            let request = gapi.client.calendar.acl.delete({
                calendarId : calendarId,
                ruleId : model.id
            });

            request.execute(function (resp) {
                if (cb) cb(null, resp);
            });
        }
    };
};

class GoogleCalendar{

    constructor(){
        this.authResult = null;
    }

    setToken(token,cb){
        gapi.client.load('oauth2','v2',function(){
            gapi.auth.setToken({
                access_token: token.access_token
            });
            if(cb) cb();
        });
    }

    checkIsUserLoggedIn(cb){
        gapi.client.load('oauth2','v2',()=>{
            gapi.client.oauth2.userinfo.get().execute(function(result) {
                if(result.code===401){
                    cb(false)
                }
                else{
                    cb(true);
                }
            });
        });
    }

    logout(callblack){
        gapi.client.load('oauth2','v2',function(){
            gapi.auth.setToken(null);
            gapi.auth.signOut();
            callblack();
        });
    }

    login(callback){
        let options = {
            client_id : config.diary.google.clientId,
            scope: config.diary.google.scopes,
            immediate : false
        };
        gapi.auth.authorize(options,(authResult)=>{
            if(authResult.access_token){
                this.authResult = authResult;
                getUserProfile((profile)=>{
                    profile.token = {
                        access_token:authResult.access_token,
                        client_id : authResult.client_id,
                        cookie_policy : authResult.cookie_policy,
                        g_user_cookie_policy: authResult.g_user_cookie_policy,
                        expires_at: authResult.expires_at,
                        expires_in: authResult.expires_in,
                        issued_at :authResult.issued_at,
                        response_type : authResult.response_type,
                        scope : authResult.scope,
                        state : authResult.state,
                        token_type : authResult.token_type,
                        status: authResult.status
                    };
                    if(callback) callback(profile);
                });
            }
            else{
                console.log('authResult',authResult);
            }
        });
    }

    authorize(callback){
        let options = {
            client_id : config.diary.google.clientId,
            scope: config.diary.google.scopes.join(' '),
            immediate : false
        };
        gapi.auth.authorize(options,(authResult)=>{
            if(callback) callback(authResult);
        });
    }

    getCalendars(callback){
        gapi.client.load('calendar', 'v3', function(){
            let request = gapi.client.calendar.calendarList.list({
            });

            request.execute(function (resp) {
                if(callback) callback(resp);
            });
        });
    }

    saveCalendar(model,callback){
        gapi.client.load('calendar', 'v3', function(){
            let payload = {
                kind : model.kind,
                summary : model.summary,
                description : model.description
            };
            let request = gapi.client.calendar.calendars.insert(payload);

            request.execute(function (resp) {
                if(callback) callback(resp);
            });
        });
    }

    // update background and foreground colors
    updateCalendarList(model,callback){
        gapi.client.load('calendar', 'v3', function(){
            let payload = {
                calendarId : model.id,
                backgroundColor : model.backgroundColor,
                foregroundColor : model.foregroundColor,
                colorId : model.colorId,
                selected : true,
                hidden : false,
                colorRgbFormat : true
            };
            let request = gapi.client.calendar.calendarList.update(payload);

            request.execute(function (resp) {
                if(callback) callback(resp);
            });
        });
    }

    // update calendar title and description
    updateCalendar(model,callback){
        gapi.client.load('calendar', 'v3', function(){
            let payload = {
                calendarId : model.id,
                summary : model.summary,
                description : model.description
            };
            let request = gapi.client.calendar.calendars.update(payload);

            request.execute(function (resp) {
                if(callback) callback(resp);
            });
        });
    }

    addPermissions(calendarId,list,callback){
        gapi.client.load('calendar', 'v3', function(){
            let handlers = [];
            _.each(list,(model)=>{
                handlers.push(getAddPermissionHandler(calendarId,model));
            });

            async.parallel(handlers,(err,results)=>{
                if(callback) callback(results);
            });
        });
    }

    getPermissions(calendarId,callback){
        gapi.client.load('calendar', 'v3', function(){
            let request = gapi.client.calendar.acl.list({
                calendarId : calendarId
            });

            request.execute(function (resp) {
                if(callback) callback(resp);
            });
        });
    }

    getEvents(calendar,callback){
        gapi.client.load('calendar', 'v3', function(){
            let request = gapi.client.calendar.events.list({
                'calendarId': calendar.id,
                'showDeleted': false,
                'singleEvents': true,
                'maxResults': 10,
                'orderBy': 'startTime'
            });

            request.execute(callback);
        });
    }

    saveEvent(calendar,event,callback){
        gapi.client.load('calendar', 'v3', function(){
            let eventObj = {
                'summary': event.summary,
                'description': event.description,
                'start': {
                    'dateTime': event.start.dateTime,
                    'timeZone': event.start.timeZone
                },
                'end': {
                    'dateTime': event.end.dateTime,
                    'timeZone': event.end.timeZone
                },
                'orderBy': 'startTime'
            };

            let request = gapi.client.calendar.events.insert({
                'calendarId': calendar.id,
                'resource': eventObj
            });

            request.execute(callback);
        });
    }
}

let InstanceManager = (function () {

    let instance = null;

    return {
        getCalendar : function(){
            if(!instance){
                instance = new GoogleCalendar();
            }
            return instance;
        },
        getInstance : function(){
            if(!instance){
                instance = new GoogleCalendar();
            }
            return instance;
        }
    };
})();

export default InstanceManager;
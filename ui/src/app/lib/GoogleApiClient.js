import config from '../config';
import async from 'async';
import _ from 'lodash';


class GoogleCalendar{

    constructor(){
        this.authResult = null;
    }

    setToken(token,cb){
        gapi.client.load('oauth2','v2',function(){
            gapi.auth.setToken({
                access_token: token.access_token
            });
            if(cb) cb();
        });
    }

    checkIsUserLoggedIn(cb){
        gapi.client.load('oauth2','v2',()=>{
            gapi.client.oauth2.userinfo.get().execute(function(result) {
                if(result.code===401){
                    cb(false)
                }
                else{
                    cb(true);
                }
            });
        });
    }

    logout(callblack){
        gapi.client.load('oauth2','v2',function(){
            gapi.auth.setToken(null);
            gapi.auth.signOut();
            callblack();
        });
    }

    login(callback){
        let options = {
            client_id : config.diary.google.clientId,
            scope: config.diary.google.scopes,
            immediate : false
        };

        gapi.auth.authorize(options,(authResult)=> {
            if (authResult.access_token) {
                this.authResult = authResult;
                this.getUserProfile(callback);
            }
            else{
                console.log('err',authResult);
            }
        });
    }

    getUserProfile (cb){
        gapi.client.load('oauth2','v2',()=>{
            gapi.client.oauth2.userinfo.get().execute((data)=> {
                let profile = data.result;
                profile.token = {
                    access_token:this.authResult.access_token,
                    client_id : this.authResult.client_id,
                    cookie_policy : this.authResult.cookie_policy,
                    g_user_cookie_policy: this.authResult.g_user_cookie_policy,
                    expires_at: this.authResult.expires_at,
                    expires_in: this.authResult.expires_in,
                    issued_at :this.authResult.issued_at,
                    response_type : this.authResult.response_type,
                    scope : this.authResult.scope,
                    state : this.authResult.state,
                    token_type : this.authResult.token_type,
                    status: this.authResult.status
                };
                cb(profile);
            });
        });
    }

    authorize(callback){
        let options = {
            client_id : config.diary.google.clientId,
            scope: config.diary.google.scopes.join(' '),
            immediate : false
        };
        gapi.auth.authorize(options,(authResult)=>{
            if(callback) callback(authResult);
        });
    }
}

let InstanceManager = (function () {

    let instance = null;

    return {
        getInstance : function(){
            if(!instance){
                instance = new GoogleCalendar();
            }
            return instance;
        }
    };
})();

export default InstanceManager;
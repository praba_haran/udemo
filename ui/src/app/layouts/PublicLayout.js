import React from 'react';

export default class PublicLayout extends React.Component{

    componentDidMount () {
        $('body').removeClass().addClass('page-forgot-password layout-full');
        Breakpoints();
        window.Site.run();
    }

    render(){
        return (
            <div>
                <div>{this.props.children}</div>
            </div>
        );
    }
}
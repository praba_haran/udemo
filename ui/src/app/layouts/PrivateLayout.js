import React from 'react';
import { hashHistory } from 'react-router';
import localStorage  from 'localStorage';
import NavBarContainer from './nav-bar/Container';
import SideMenuContainer from './side-menu/Container';
import Footer from '../components/shared/Footer';
import AddContactModal from '../components/add-contact/Container';
import AddPropertyModal from '../components/add-property/Container';
import BookMarketAppraisal from '../components/market-appraisal/Container';

export default class PrivateLayout extends React.Component{

    constructor(){
        super();
        this.state = {
            contentBodyHeight: 300
        };
    }

    componentDidMount(){
        $('body').removeClass().addClass('app-gallery site-menubar-keep site-menubar-unfold');
        this.adjustContentBody();
        Breakpoints();
        window.Site.run();
    }

    adjustContentBody(){
        let headerHeight = $('.navbar-header').height();
        let footerHeight = $('.site-footer').height();
        let totalHeight = $(window).height();
        let contentBodyHeight = totalHeight - (headerHeight+footerHeight+21);
        this.setState({
            contentBodyHeight: contentBodyHeight
        });
    }

    render(){
        return (
            <div>
                <NavBarContainer {...this.props} />
                <SideMenuContainer {...this.props}/>
                <div id="content-body" style={{minHeight:this.state.contentBodyHeight+'px'}}>{this.props.children}</div>
                <Footer {...this.props}/>

                <AddContactModal />
                <AddPropertyModal />
                <BookMarketAppraisal />
            </div>
        );
    }
}
import React from 'react';

export default class Notifications extends React.Component {

    render() {
        return (
            <li className="dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications"
                   aria-expanded="false"
                   data-animation="scale-up" role="button">
                    <i className="icon md-notifications" aria-hidden="true"></i>
                    <span className="badge badge-danger up">5</span>
                </a>
                <ul className="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                    <li className="dropdown-menu-header" role="presentation">
                        <h5>NOTIFICATIONS</h5>
                        <span className="label label-round label-danger">New 5</span>
                    </li>
                    <li className="list-group" role="presentation">
                        <div data-role="container">
                            <div data-role="content">
                                <a className="list-group-item" href="javascript:void(0)" role="menuitem">
                                    <div className="media">
                                        <div className="media-left padding-right-10">
                                            <i className="icon md-receipt bg-red-600 white icon-circle"
                                               aria-hidden="true"></i>
                                        </div>
                                        <div className="media-body">
                                            <h6 className="media-heading">A new order has been placed</h6>
                                            <time className="media-meta">5
                                                hours ago
                                            </time>
                                        </div>
                                    </div>
                                </a>
                                <a className="list-group-item" href="javascript:void(0)" role="menuitem">
                                    <div className="media">
                                        <div className="media-left padding-right-10">
                                            <i className="icon md-account bg-green-600 white icon-circle"
                                               aria-hidden="true"></i>
                                        </div>
                                        <div className="media-body">
                                            <h6 className="media-heading">Completed the task</h6>
                                            <time className="media-meta">2
                                                days ago
                                            </time>
                                        </div>
                                    </div>
                                </a>
                                <a className="list-group-item" href="javascript:void(0)" role="menuitem">
                                    <div className="media">
                                        <div className="media-left padding-right-10">
                                            <i className="icon md-settings bg-red-600 white icon-circle"
                                               aria-hidden="true"></i>
                                        </div>
                                        <div className="media-body">
                                            <h6 className="media-heading">Settings updated</h6>
                                            <time className="media-meta">2
                                                days ago
                                            </time>
                                        </div>
                                    </div>
                                </a>
                                <a className="list-group-item" href="javascript:void(0)" role="menuitem">
                                    <div className="media">
                                        <div className="media-left padding-right-10">
                                            <i className="icon md-calendar bg-blue-600 white icon-circle"
                                               aria-hidden="true"></i>
                                        </div>
                                        <div className="media-body">
                                            <h6 className="media-heading">Event started</h6>
                                            <time className="media-meta">3
                                                days ago
                                            </time>
                                        </div>
                                    </div>
                                </a>
                                <a className="list-group-item" href="javascript:void(0)" role="menuitem">
                                    <div className="media">
                                        <div className="media-left padding-right-10">
                                            <i className="icon md-comment bg-orange-600 white icon-circle"
                                               aria-hidden="true"></i>
                                        </div>
                                        <div className="media-body">
                                            <h6 className="media-heading">Message received</h6>
                                            <time className="media-meta">3
                                                days ago
                                            </time>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li className="dropdown-menu-footer" role="presentation">
                        <a className="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                            <i className="icon md-settings" aria-hidden="true"></i>
                        </a>
                        <a href="javascript:void(0)" role="menuitem">
                            All notifications
                        </a>
                    </li>
                </ul>
            </li>
        );
    }
}
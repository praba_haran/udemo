import React from 'react';
import _ from 'lodash';
import {Link} from "react-router";
import * as constants from '../../../constants';
import Commands from '../../../models/Commands';

export default class PropertyActions extends React.Component{

    OnClick(cmd){
        Commands.actions.change(cmd,true);
    }

    canBookAppointment(){
        let { PROPERTY_DETAIL } = this.props;
        return PROPERTY_DETAIL.recordStatus ==='Active';
    }

    canShowInstruct(){
        let { PROPERTY_DETAIL } = this.props;
        return (PROPERTY_DETAIL.recordStatus ==='Active' && PROPERTY_DETAIL.status.toLowerCase()==='appraised');
    }

    canShowChangeStatus(){
        let { PROPERTY_DETAIL } = this.props;
        let statusList = ['pre appraisal','appraised','archived'];
        return PROPERTY_DETAIL.recordStatus ==='Active' && statusList.indexOf(PROPERTY_DETAIL.status.toLowerCase())===-1;
    }

    canShowMarketAppraisal(){
        let { MARKET_APPRAISAL, PROPERTY_DETAIL } = this.props;
        return MARKET_APPRAISAL.property === null && PROPERTY_DETAIL.recordStatus==='Active' &&  PROPERTY_DETAIL.status.toLowerCase()==='pre appraisal';
    }

    render(){
        let { onLinkClick , userInfo, PROPERTY_DETAIL, routes } = this.props;
        let isPropertyDetailPage = false;
        _.each(routes,(p)=>{
            if(!isPropertyDetailPage && PROPERTY_DETAIL && p.path==='properties/(:id)/'){
                isPropertyDetailPage = true;
            }
        });
        return (
            <ul className="nav navbar-toolbar">
                {isPropertyDetailPage?(
                        <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                               data-animation="scale-up"
                               aria-expanded="false" role="button">
                                <i className="icon fa-gear margin-right-5"></i>
                                Actions
                                <i className="icon md-chevron-down margin-left-5"></i>
                            </a>
                            <ul className="dropdown-menu" role="menu">
                                {this.canBookAppointment()?(
                                        <li role="presentation">
                                            <a href="javascript:void(0)"
                                               onClick={(e)=>this.OnClick(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT)}
                                               role="menuitem">
                                                <i className="icon ion-ios-calendar"></i>
                                                Book a General Appointment
                                            </a>
                                        </li>
                                    ):null}
                                {this.canShowMarketAppraisal()?(
                                        <li role="presentation">
                                            <a href="javascript:void(0)"
                                               onClick={(e)=>this.OnClick(constants.COMMANDS.BOOK_MARKET_APPRAISAL)}
                                               role="menuitem">
                                                <i className="icon ion-ios-calendar"></i>
                                                Book a Market Appraisal
                                            </a>
                                        </li>
                                    ):null}
                                {this.canShowChangeStatus()?(
                                        <li role="presentation">
                                            <a href="javascript:void(0)"
                                               onClick={(e)=>this.OnClick(constants.COMMANDS.PROPERTY_STATUS)}
                                               role="menuitem">
                                                <i className="icon fa-pencil"></i>
                                                Change Status
                                            </a>
                                        </li>
                                    ):null}
                                {this.canShowInstruct()?(
                                        <li role="presentation">
                                            <a href="javascript:void(0)"
                                               onClick={(e)=>this.OnClick(constants.COMMANDS.INSTRUCT_PROPERTY)}
                                               role="menuitem">
                                                <i className="icon ion-ios-calendar"></i>
                                                Instruct ...
                                            </a>
                                        </li>
                                    ):null}
                                {this._renderActionsOnAvailable()}
                                {this._renderTermRelatedActions()}
                                {this._renderOtherActions()}
                            </ul>
                        </li>
                    ):null}
                {isPropertyDetailPage?(
                        <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                               data-animation="scale-up"
                               aria-expanded="false" role="button">
                                <i className="icon fa-wrench margin-right-5"></i>
                                Setting
                                <i className="icon md-chevron-down margin-left-5"></i>
                            </a>
                            <ul className="dropdown-menu" role="menu">
                                <li role="presentation">
                                    <a href="javascript:void(0)"
                                       onClick={(e)=>this.OnClick(constants.COMMANDS.SHOW_BROCHURE_SETUP)}
                                       role="menuitem">
                                        <i className="icon fa-gear"></i>
                                        Brochure Setup
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="javascript:void(0)"
                                       onClick={(e)=>this.OnClick(constants.COMMANDS.CLONE_PROPERTY)}
                                       role="menuitem">
                                        <i className="icon ion-ios-copy"></i>
                                        Clone Property
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="javascript:void(0)"
                                       onClick={(e)=>this.OnClick(constants.COMMANDS.TRANSFER_PROPERTY_TO_ANOTHER_BRANCH)}
                                       role="menuitem">
                                        <i className="icon ion-android-arrow-forward"></i>
                                        Transfer to Another Branch
                                    </a>
                                </li>
                            </ul>
                        </li>
                    ):null}
            </ul>
        );
    }

    _renderActionsOnAvailable(){
        let actions = [];
        let { PROPERTY_DETAIL } = this.props;
        if(PROPERTY_DETAIL.status==='Available' && PROPERTY_DETAIL.recordStatus==='Active') {
            actions.push(<li key="divider_2" role="presentation" className="divider"></li>);
            actions.push(
                <li role="presentation" key="BOOK_VIEWING">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.BOOK_VIEWING)}
                       role="menuitem">
                        <i className="icon ion-eye"></i>
                        Book a Viewing
                    </a>
                </li>
            );
            actions.push(
                <li role="presentation" key="MAKE_AN_OFFER">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.MAKE_AN_OFFER)}
                       role="menuitem">
                        <i className="icon fa-money"></i>
                        Make an Offer
                    </a>
                </li>
            );
            actions.push(
                <li role="presentation" key="CHANGE_BOARD">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.CHANGE_BOARD)}
                       role="menuitem">
                        <i className="icon fa-pencil"></i>
                        Change Board
                    </a>
                </li>
            );
        }
        return actions;
    }

    _renderTermRelatedActions(){
        let actions = [];
        let { PROPERTY_DETAIL } = this.props;
        if(PROPERTY_DETAIL.status==='Available' && PROPERTY_DETAIL.recordStatus==='Active') {
            actions.push(<li key="divider_2" role="presentation" className="divider"></li>);

            // SHOW ONLY IF TERMS HAS NOT SENT TO VENDOR OR LANDLORD
            if(!PROPERTY_DETAIL.termsSent.isTermsSent) {
                actions.push(
                    <li role="presentation" key="TERMS_SENT">
                        <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.TERMS_SENT)}
                           role="menuitem">
                            <i className="icon ion-eye"></i>
                            Terms Sent ...
                        </a>
                    </li>
                );
            }

            // SHOW ONLY IF TERMS HAS NOT SIGNED
            if(!PROPERTY_DETAIL.termsSigned.isTermsSigned) {
                actions.push(
                    <li role="presentation" key="TERMS_SIGNED">
                        <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.TERMS_SIGNED)}
                           role="menuitem">
                            <i className="icon ion-eye"></i>
                            Terms Signed ...
                        </a>
                    </li>
                );
            }

            actions.push(
                <li role="presentation" key="SEND_DETAILS_TO_VENDOR">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.SEND_DETAILS_TO_VENDOR)}
                       role="menuitem">
                        <i className="icon fa-paper-plane"></i>
                        Details To Vendor (Draft)
                    </a>
                </li>
            );
        }
        return actions;
    }

    _renderOtherActions(){
        let actions = [];
        let { PROPERTY_DETAIL } = this.props;
        //actions.push(<li key="divider_2" role="presentation" className="divider"></li>);

        if(PROPERTY_DETAIL.recordStatus==='Active') {

            actions.push(
                <li role="presentation" key="ARCHIVE_PROPERTY">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.ARCHIVE_PROPERTY)}
                       role="menuitem">
                        <i className="icon ion-archive"></i>
                        Archive
                    </a>
                </li>
            );
        }
        else {

            actions.push(
                <li role="presentation" key="ACTIVATE_PROPERTY">
                    <a href="javascript:void(0)" onClick={(e) => this.OnClick(constants.COMMANDS.ACTIVATE_PROPERTY)}
                       role="menuitem">
                        <i className="icon ion-android-done"></i>
                        Activate
                    </a>
                </li>
            );
        }

        return actions;
    }
}
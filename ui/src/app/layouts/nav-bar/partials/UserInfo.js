import React from 'react';
import localStorage from 'localStorage';
import {Link} from "react-router";
import { hashHistory } from 'react-router';
import GoogleClient from '../../../lib/GoogleClient';

export default class UserInfo extends React.Component{

    logOut (e){
        e.preventDefault();
        GoogleClient.getCalendar().logout(()=>{
            localStorage.removeItem('x-access-token');
            hashHistory.push('/login');
        });
    }

    renderUser(){
        let { APP } = this.props;
        let { userInfo,userProfile } = APP;
        if(userProfile){
            return (
                <a className="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#"
                   aria-expanded="false"
                   data-animation="scale-up" role="button">
                    {userProfile.name}
                    <span className="avatar avatar-online">
                        <img src={userProfile.picture} />
                    </span>
                </a>
            );
        }
        return (
            <a className="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#"
               aria-expanded="false"
               data-animation="scale-up" role="button">
                {userInfo.forename}
                <span className="avatar avatar-online">
                    <img className="margin-left-5" src="/dist/images/photos/user-profile.png" />
                </span>
            </a>
        );
    }

    render(){
        let { APP } = this.props;
        let { userInfo,userProfile } = APP;
        if(!userInfo){
            return null;
        }
        return(
            <li className="dropdown">
                {this.renderUser()}
                <ul className="dropdown-menu" role="menu">
                    <li role="presentation">
                        <a href="javascript:void(0)" role="menuitem">
                            <i className="icon md-account" aria-hidden="true"></i>
                            Profile
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)" role="menuitem">
                            <i className="icon md-card" aria-hidden="true"></i>
                            Billing
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)" role="menuitem">
                            <i className="icon md-settings" aria-hidden="true"></i>
                            Settings
                        </a>
                    </li>
                    <li className="divider" role="presentation"></li>
                    <li role="presentation">
                        <a onClick={this.logOut.bind(this)} href="#" role="menuitem">
                            <i className="icon md-power" aria-hidden="true"></i>
                            Logout
                        </a>
                    </li>
                </ul>
            </li>
        );
    }
}
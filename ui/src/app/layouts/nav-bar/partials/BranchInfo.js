import React from 'react';

export default class BranchInfo extends React.Component{

    render (){
        let { APP } = this.props;
        let { userInfo } = APP;
        if(!userInfo){
            return null;
        }
        return (
            <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                   data-animation="scale-up"
                   aria-expanded="false" role="button">
                    Branch - {userInfo.agency.address.town}
                </a>
                {userInfo.agency.branchDetails ? (
                    <ul className="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="javascript:void(0)" role="menuitem">Harrow</a>
                        </li>
                        <li role="presentation">
                            <a href="javascript:void(0)" role="menuitem">Harrow on the Hill</a>
                        </li>
                    </ul>
                ) : null}
            </li>
        );
    }
}
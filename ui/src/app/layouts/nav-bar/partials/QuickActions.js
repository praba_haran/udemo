import React from 'react';
import * as constants from '../../../constants';
import Commands from '../../../models/Commands';

export default class QuickActions extends React.Component {

    render() {
        return (
            <ul className="nav navbar-toolbar">
                <li className="hidden-xs">
                    <a href="javascript:void(0)"
                       onClick={(e)=>Commands.actions.change(constants.COMMANDS.ADD_PROPERTY,true)}
                       role="menuitem">
                        <i className="icon ion-home"></i>
                        <span className="margin-left-5">+</span>
                    </a>
                </li>
                <li className="hidden-xs">
                    <a href="javascript:void(0)"
                       onClick={(e)=>Commands.actions.change(constants.COMMANDS.ADD_CONTACT,true)}
                       role="menuitem">
                        <i className="icon ion-android-person"></i>
                        <span className="margin-left-5">+</span>
                    </a>
                </li>
            </ul>
        );
    }
}
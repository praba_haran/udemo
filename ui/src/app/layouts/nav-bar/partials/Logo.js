import React from 'react';

export default class Logo extends React.Component{

    render (){
        return(
            <div className="navbar-header">
                <button type="button" className="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                        data-toggle="menubar">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="hamburger-bar"></span>
                </button>
                <button type="button" className="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                        data-toggle="collapse">
                    <i className="icon md-more" aria-hidden="true"></i>
                </button>
                <a href="#/" className="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                    <img className="navbar-brand-logo" src="/dist/images/logo-white.png" title="Ultima Plus" />
                </a>
                <button type="button" className="navbar-toggle collapsed" data-target="#site-navbar-search"
                        data-toggle="collapse">
                    <span className="sr-only">Toggle Search</span>
                    <i className="icon md-search" aria-hidden="true"></i>
                </button>
            </div>
        );
    }
}
import React from 'react';
import _ from 'lodash';
import * as constants from '../../../constants';
import Commands from '../../../models/Commands';

export default class ContactActions extends React.Component{

    OnClick(cmd){
        Commands.actions.change(cmd,true);
    }

    isActiveContact(){
        let { CONTACT_DETAIL } = this.props;
        return (CONTACT_DETAIL.status.toLowerCase()==='active');
    }

    renderSubMenus (){
        let { onLinkClick } = this.props;

        if(this.isActiveContact()){
            return (
                <ul className="dropdown-menu" role="menu">
                    <li role="presentation">
                        <a href="javascript:void(0)"
                            onClick={(e)=>this.OnClick(constants.COMMANDS.BOOK_GENERAL_APPOINTMENT)}
                            role="menuitem">
                            <i className="icon ion-ios-calendar"></i>
                            Book a General Appointment
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)"
                            onClick={(e)=>this.OnClick(constants.COMMANDS.BOOK_VIEWING)}
                            role="menuitem">
                            <i className="icon ion-eye"></i>
                            Book a Viewing
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)" onClick={(e)=>this.OnClick(constants.COMMANDS.MAKE_AN_OFFER)}
                            role="menuitem">
                            <i className="icon ion-key"></i>
                            Make an Offer
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)" onClick={(e)=>this.OnClick(constants.COMMANDS.ARCHIVE_CONTACT)}
                            role="menuitem">
                            <i className="icon ion-archive"></i>
                            Archive
                        </a>
                    </li>
                    <li role="presentation" className="divider"></li>
                    <li role="presentation">
                        <a href="javascript:void(0)" onClick={(e)=>this.OnClick(constants.COMMANDS.ADD_NOTES)}
                           role="menuitem">
                            <i className="icon fa-file"></i>
                            Add Notes
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="javascript:void(0)" onClick={(e)=>this.OnClick(constants.COMMANDS.LOG_PHONE_CALLS)}
                           role="menuitem">
                            <i className="icon fa-phone"></i>
                            Log Phone Calls
                        </a>
                    </li>
                </ul>
            );
        }
        return (
            <ul className="dropdown-menu" role="menu">
                <li role="presentation">
                    <a href="javascript:void(0)"
                       onClick={(e)=>this.OnClick(constants.COMMANDS.ACTIVATE_CONTACT)}
                       role="menuitem">
                        <i className="icon ion-android-done"></i>
                        Activate
                    </a>
                </li>
            </ul>
        );
    }

    render(){
        let { onLinkClick , userInfo, CONTACT_DETAIL, routes } = this.props;
        let isContactDetailPage = false;
        _.each(routes,(p)=>{
            if(!isContactDetailPage && CONTACT_DETAIL && p.path==='contacts/(:id)/'){
                isContactDetailPage = true;
            }
        });
        return (
            <ul className="nav navbar-toolbar">
                {isContactDetailPage?(
                        <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                               data-animation="scale-up"
                               aria-expanded="false" role="button">
                                <i className="icon fa-gear margin-right-5"></i>
                                Actions
                                <i className="icon md-chevron-down margin-left-5"></i>
                            </a>
                            {this.renderSubMenus()}
                        </li>
                    ):null}
                {isContactDetailPage?(
                        <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                               data-animation="scale-up"
                               aria-expanded="false" role="button">
                                <i className="icon fa-wrench margin-right-5"></i>
                                Setting
                                <i className="icon md-chevron-down margin-left-5"></i>
                            </a>
                            <ul className="dropdown-menu" role="menu">
                                <li role="presentation">
                                    <a href="javascript:void(0)"
                                       onClick={(e)=>this.OnClick(constants.COMMANDS.CLONE_CONTACT)}
                                       role="menuitem">
                                        <i className="icon ion-ios-copy"></i>
                                        Clone Contact
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="javascript:void(0)"
                                       onClick={(e)=>this.OnClick(constants.COMMANDS.TRANSFER_CONTACT_TO_ANOTHER_BRANCH)}
                                       role="menuitem">
                                        <i className="icon ion-android-arrow-forward"></i>
                                        Transfer to Another Branch
                                    </a>
                                </li>
                            </ul>
                        </li>
                    ):null}
            </ul>
        );
    }
}
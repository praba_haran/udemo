import React from 'react';
import Component from './Component';
import { connect } from 'react-redux';
import App from '../../models/App';
import scriptLoader from 'react-async-script-loader';
import GoogleClient from '../../lib/GoogleClient';
import GoogleDiary from '../../models/GoogleDiary';

class Container extends React.Component{

    componentDidMount () {
        // GoogleDiary.api.login((profile)=>{
        //     App.api.getUserInfo();
        //     App.api.getAgencyDetails();
        // });

        // const { isScriptLoaded, isScriptLoadSucceed } = this.props
        // if (isScriptLoaded && isScriptLoadSucceed) {
        //     // need to write
        //     setTimeout(()=>{
        //         App.actions.change('meta.isGoogleApiLoaded', isScriptLoadSucceed);
        //     },2500);
        // }
        // else{
        //
        // }
    }

    // componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed, GOOGLE_DIARY }) {
    //     if (isScriptLoaded && !this.props.isScriptLoaded) {
    //         if (isScriptLoadSucceed) {
    //             // connect to google
    //             console.log('GOOGLE_DIARY',GoogleDiary);
    //             GoogleDiary.api.login();
    //             setTimeout(()=>{
    //                 App.actions.change('meta.isGoogleApiLoaded', isScriptLoadSucceed);
    //             },2500);
    //             this.setGoogleToken();
    //         }
    //     }
    // }

    setGoogleToken(){
        let { APP} = this.props;
        let { userProfile } = APP;
        // setting token
        if (userProfile && userProfile.token) {
            setTimeout(()=>{
                // if user profile is not null then user may logged in using google
                // then we have to set the token to prevent repetitive authorization
                GoogleClient.getInstance().setToken(userProfile.token, function () {
                    GoogleClient.getInstance().checkIsUserLoggedIn((isUserLoggedIn) => {
                        App.actions.change('meta.isGoogleAuthDone', isUserLoggedIn);
                    });
                });
            },1500);
        }
    }

    render(){
        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = function (store) {
    let state = store.COMMANDS.toJS();
    return {
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS(),
        MARKET_APPRAISAL : store.MARKET_APPRAISAL.toJS(),
        commands : state.model,
        APP : store.APP.toJS(),
        GOOGLE_DIARY : store.GOOGLE_DIARY.toJS()
    };
};

// export default scriptLoader([
//     'https://apis.google.com/js/client:plusone.js'
// ])(connect(mapStateToProps)(Container));

export default connect(mapStateToProps)(Container);
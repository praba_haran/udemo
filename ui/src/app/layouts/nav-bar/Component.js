import React from 'react';
import _ from 'lodash';
import {Link} from "react-router";
import Logo from './partials/Logo';
import UserInfo from './partials/UserInfo';
import QuickActions from './partials/QuickActions';
import Notifications from './partials/Notifications';
import BranchInfo from './partials/BranchInfo';
import PropertyActions from './partials/PropertyActions';
import ContactActions from './partials/ContactActions';

export default class NavBar extends React.Component{

    render (){
        return(
            <nav className="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
                <Logo />
                <div className="navbar-container container-fluid">
                    <div className="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                        <div className="row">
                            <div className="col-lg-4">
                                <QuickActions {...this.props}/>
                            </div>
                            <div className="col-lg-3">
                                <PropertyActions {...this.props}/>
                                <ContactActions {...this.props}/>
                            </div>
                            <div className="col-lg-5">
                                <ul className="nav navbar-toolbar navbar-right navbar-toolbar-right">
                                    <BranchInfo {...this.props} />
                                    <Notifications {...this.props} />
                                    <UserInfo {...this.props}/>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="collapse navbar-search-overlap" id="site-navbar-search">
                        <form role="search">
                            <div className="form-group">
                                <div className="input-search">
                                    <i className="input-search-icon md-search" aria-hidden="true"></i>
                                    <input type="text" className="form-control" name="site-search" placeholder="Search..." />
                                    <button type="button" className="input-search-close icon md-close" data-target="#site-navbar-search"
                                            data-toggle="collapse" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        );
    }
}
import React from 'react';
import { connect } from 'react-redux';
import Component from './Component';
import BasicGrid from '../../../components/shared/grid/BasicGrid';

class Container extends BasicGrid{

    componentDidMount () {
        this.setUrl('contact/list/paginate').load();
        this.setState({
            type : 'All'
        });
    }

    onClientTypeChange(key){
        this.setState({
            type : key,
            pageIndex : 1
        });
        if(key==='All'){
            this.removeParam('type').setPageIndex(1).load();
        }
        else{
            this.setParam('type',key).setPageIndex(1).load();
        }
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                renderNoRecord={this.renderNoRecord.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                prev={this.prev.bind(this)}
                next={this.next.bind(this)}
                getEndIndex={this.getEndIndex.bind(this)}
                getPageSummary={this.getPageSummary.bind(this)}
                onClientTypeChange={this.onClientTypeChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        commands : store.COMMANDS.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS()
    };
};

export default connect(mapStateToProps)(Container);

import React from 'react';
import classNames from 'classnames';
import {Link} from "react-router";
import SiteMenuLink from '../../../components/shared/SiteMenuLink';

export default class Component extends React.Component{

    render(){
        let { items,isLoading, type,onClientTypeChange,
            getPageSummary, count,prev,next,pageIndex,getEndIndex,
            CONTACT_DETAIL } = this.props;

        let noItemFound;
        if(items.length === 0){
            if(isLoading){
                noItemFound = (
                    <li className="no-item-found">
                        <p>Loading ...</p>
                    </li>
                );
            }
            else {
                noItemFound = (
                    <li className="no-item-found">
                        <p>No contacts found.</p>
                    </li>
                );
            }
        }
        return (
            <div>
                <div className="panel nav-tabs-horizontal panel-transparent">
                    <ul className="site-menu">
                        <li className="site-menu-item active">
                            <Link to="/contacts" className="clearfix animsition-link">
                                <i className="site-menu-icon fa-user" aria-hidden="true"></i>
                                <span className="site-menu-title">Contacts</span>
                            </Link>
                        </li>
                    </ul>
                    <ul className="nav nav-tabs nav-tabs-line">
                        <li onClick={()=> onClientTypeChange('All')} className={classNames({
                            'active':type==='All'
                        })}>
                            <a href="javascript:void(0)">All</a>
                        </li>
                        <li onClick={()=> onClientTypeChange('Client')} className={classNames({
                            'active':type==='Client'
                        })}>
                            <a href="javascript:void(0)">Client</a>
                        </li>
                        <li onClick={()=> onClientTypeChange('Others')} className={classNames({
                            'active':type==='Others'
                        })}>
                            <a href="javascript:void(0)">Others</a>
                        </li>
                    </ul>
                    <div className="clear">
                        <ul className="site-menu contact-listing">
                            {items.map((item)=>{
                                let p = (item._id === CONTACT_DETAIL._id)? CONTACT_DETAIL: item;
                                return (
                                    <SiteMenuLink routeId={p._id} key={p._id} id={p._id} to={'/contacts/'+p._id+'/overview'} activeClassName="active">
                                        <div className="left">
                                            <div className="avatar">{p.firstPerson.forename[0]+p.firstPerson.surname[0]}</div>
                                        </div>
                                        <div className="right">
                                            <p className="title">{p.firstPerson.forename+' '+p.firstPerson.surname}</p>
                                            <p className="address">{p.address.nameOrNumber+', '+p.address.street}</p>
                                            <p className="contact-type">{p.type}</p>
                                        </div>
                                    </SiteMenuLink>
                                );
                            })}
                            {!isLoading?(
                                    <li className="page-summary">
                                        <div className="clear">
                                            <div className="col-sm-8">
                                                <p>{getPageSummary()}</p>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="btn-group">
                                                    <button type="button" onClick={prev} disabled={(items.length=== count || pageIndex===1)} className="btn btn-sm">
                                                        <i className="icon fa-angle-left"></i>
                                                    </button>
                                                    <button type="button" onClick={next} disabled={getEndIndex()=== count} className="btn btn-sm">
                                                        <i className="icon fa-angle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ):null}
                            {noItemFound}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
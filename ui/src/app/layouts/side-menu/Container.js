import React from 'react';
import Component from './Component';
import { connect } from 'react-redux';
import PropertyListing from '../../models/PropertyListing';
import ContactListing from '../../models/ContactListing';
import Requirement from '../../models/Requirement';
import Matching from '../../models/Matching';

class Container extends React.Component{

    componentDidMount(){
        //this.loadProperties();
        //this.loadContacts();
    }

    componentWillReceiveProps(newProps){
        if(newProps.params.id !==this.props.params.id){
            //this.loadProperties();
            //this.loadContacts();
            Requirement.actions.clearModel();
            Matching.actions.clearModel();
        }
    }

    // loadProperties(){
    //     PropertyListing.actions.change('isProcessing',true);
    //     PropertyListing.api.search({},(result)=>{
    //         if(result.success){
    //             PropertyListing.actions.setObjectInPath(['properties'],result.data);
    //
    //         }
    //         PropertyListing.actions.change('isProcessing', false);
    //     });
    // }

    loadContacts(){
        ContactListing.actions.change('isProcessing',true);
        ContactListing.api.list((result)=>{
            if(result.success){
                ContactListing.actions.setObjectInPath(['contacts'],result.data);

            }
            ContactListing.actions.change('isProcessing', false);
        });
    }

    render(){
        return (
            <Component {...this.props}/>
        );
    }
}

const mapStateToProps = function (store) {

    return {
        commands : store.COMMANDS.toJS(),
        PROPERTY_LISTING : store.PROPERTY_LISTING.toJS(),
        CONTACT_LISTING : store.CONTACT_LISTING.toJS(),
        property : store.PROPERTY_DETAIL.toJS(),
        CONTACT_DETAIL : store.CONTACT_DETAIL.toJS()
    };
};

export default connect(mapStateToProps)(Container);
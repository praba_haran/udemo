import React from 'react';
import { connect } from 'react-redux';
import Diary from '../../../models/Diary';
import GoogleCalendars from '../../../models/GoogleCalendars';
import Component from './Component';

class Container extends React.Component {

    componentDidMount(){
        let { APP } = this.props;
        if(APP.meta.isGoogleApiLoaded){
            GoogleCalendars.api.getCalendars(this.loadDefaultCalendar.bind(this));
        }
    }

    componentWillReceiveProps(newProps){
        let { APP, GOOGLE_CALENDARS } = newProps;

        if(APP.meta.isGoogleAuthDone){
            if(GOOGLE_CALENDARS.items.length===0){
                GoogleCalendars.api.getCalendars(this.loadDefaultCalendar.bind(this));
            }
            if(!this.props.APP.meta.isGoogleApiLoaded){
                console.log('loading calendars');
                GoogleCalendars.api.getCalendars(this.loadDefaultCalendar.bind(this));
            }
        }
    }

    connectGoogle(){
        GoogleCalendars.api.connectToGoogle((result)=>{
            GoogleCalendars.api.getCalendars();
        });
    }

    loadDefaultCalendar(items){
        if(items.length>0){
            this.getGoogleCalendarEvents(items[0]);
        }
    }

    getGoogleCalendarEvents(calendar){
        Diary.api.getEvents(calendar);
    }

    onDateChange(value){
        Diary.actions.setObjectInPath(['options','currentDate'],value);
    }

    render() {
        return (
            <Component
                {...this.props}
                connectGoogle={this.connectGoogle.bind(this)}
                getGoogleCalendarEvents={this.getGoogleCalendarEvents.bind(this)}
                onDateChange = {this.onDateChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        GOOGLE_CALENDARS: store.GOOGLE_CALENDARS.toJS(),
        COMMANDS : store.COMMANDS.toJS(),
        DIARY : store.DIARY.toJS(),
        APP : store.APP.toJS()
    };
};

export default connect(mapStateToProps)(Container);

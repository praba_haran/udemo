import React from 'react';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover from 'react-bootstrap/lib/Popover';

class CalendarSettingPopover extends React.Component{

    render(){
        const popoverRight = (
            <Popover id="popover-positioned-right" title="Popover right">
                <strong>Holy guacamole!</strong> Check this info.
            </Popover>
        );

        return (
            <OverlayTrigger trigger="click" placement="right" rootClose overlay={popoverRight}>
                <span className="pull-right">
                    <i className="icon fa-gear"></i>
                </span>
            </OverlayTrigger>
        );
    }
}

export default CalendarSettingPopover;
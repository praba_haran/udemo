import React from 'react';
import _ from 'lodash';
import Calendar from 'rc-calendar';
import SiteMenuLink from '../../../components/shared/SiteMenuLink';
import CalendarSettingPopover from './CalendarSettingPopover';

class Component extends React.Component{

    constructor(){
        super();
        this.state = {
            market : null
        };
        this.onTabClick = this.onTabClick.bind(this);
    }

    onTabClick (market){
        this.setState({
            market : market
        });
    }

    dateRender(current) {
        let { DIARY } = this.props;
        const selectedValue = DIARY.options.currentDate;
        if(DIARY.options.view==='month'){
            if(selectedValue && current.month() === selectedValue.month() && current.year() === selectedValue.year()){
                return (<div className="rc-calendar-selected-day">
                    <div className="rc-calendar-date">
                        {current.date()}
                    </div>
                </div>);
            }
        }
        else if(DIARY.options.view==='agendaWeek'){
            if (selectedValue && current.year() === selectedValue.year() &&
                current.week() === selectedValue.week()) {
                return (<div className="rc-calendar-selected-day">
                    <div className="rc-calendar-date">
                        {current.date()}
                    </div>
                </div>);
            }
        }
        else if(DIARY.options.view==='agendaDay'){
            if (selectedValue && current.year() === selectedValue.year() &&
                current.month() === selectedValue.month() && current.dayOfYear()===selectedValue.dayOfYear()) {
                return (<div className="rc-calendar-selected-day">
                    <div className="rc-calendar-date">
                        {current.date()}
                    </div>
                </div>);
            }
        }

        return (
            <div className="rc-calendar-date">
                {current.date()}
            </div>
        );
    }

    render(){
        let { connectGoogle,getGoogleCalendarEvents,onDateChange } = this.props;
        let { DIARY,APP,GOOGLE_CALENDARS } = this.props;
        return (
            <div className="side-menu-diary">
                <div className="panel nav-tabs-horizontal panel-transparent">
                    <ul className="site-menu">
                        <SiteMenuLink to="/diary" activeClassName="active">
                            <i className="site-menu-icon fa-calendar" aria-hidden="true"></i>
                            <span className="site-menu-title">Diary</span>
                        </SiteMenuLink>
                    </ul>
                    <div className="week-calendar">
                        <Calendar
                            showDateInput={false}
                            showToday={false}
                            selectedValue ={DIARY.options.currentDate}
                            dateRender={this.dateRender.bind(this)}
                            onChange={onDateChange}
                        />
                    </div>
                    <ul className="site-menu">
                        {GOOGLE_CALENDARS.items.map((p)=>{
                            let cssClass = "site-menu-item";
                            if(p.id===DIARY.selectedCalendarId){
                                cssClass += " active";
                            }
                            return (
                                <li key={p.id} className={cssClass}>
                                    <a onClick={(e)=> getGoogleCalendarEvents(p)} className="clearfix animsition-link" href="javascript:void(0)">
                                        <i style={{color:p.backgroundColor}} className="site-menu-icon fa-square" aria-hidden="true"></i>
                                        <span className="site-menu-title">{p.summary}</span>
                                    </a>
                                </li>
                            );
                        })}
                        {GOOGLE_CALENDARS.isProcessing?(
                                <li className="site-menu-item">
                                    <a className="clearfix animsition-link" href="javascript:void(0)">
                                        <i className="site-menu-icon fa-spinner fa-spin" aria-hidden="true"></i>
                                        <span className="site-menu-title">Loading Calendars ...</span>
                                    </a>
                                </li>
                            ):null}
                        {(!APP.meta.isGoogleAuthDone)?(
                                <li className="site-menu-item">
                                    <a className="clearfix animsition-link" href="javascript:void(0)" onClick={connectGoogle}>
                                        <i className="site-menu-icon md-google" aria-hidden="true"></i>
                                        <span className="site-menu-title">Connect Google Calendar</span>
                                    </a>
                                </li>
                            ):null}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Component;
import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import Alert from '../../../lib/Alert';
import Component from './Component';
import BasicGrid from '../../../components/shared/grid/BasicGrid';

class Container extends BasicGrid{

    componentDidMount () {
        this.setUrl('property/list/paginate').load();
        this.setState({
            marketFor : 'All'
        });
    }

    onMarketChange(marketFor){
        this.setState({
            marketFor : marketFor,
            pageIndex : 1
        });
        if(marketFor==='All'){
            this.removeParam('marketFor').setPageIndex(1).load();
        }
        else{
            this.setParam('marketFor',marketFor).setPageIndex(1).load();
        }
    }

    render () {
        return(
            <Component
                {...this.props}
                {...this.state}
                renderNoRecord={this.renderNoRecord.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                prev={this.prev.bind(this)}
                next={this.next.bind(this)}
                getEndIndex={this.getEndIndex.bind(this)}
                getPageSummary={this.getPageSummary.bind(this)}
                onMarketChange={this.onMarketChange.bind(this)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        commands : store.COMMANDS.toJS(),
        PROPERTY_DETAIL : store.PROPERTY_DETAIL.toJS()
    };
};

export default connect(mapStateToProps)(Container);

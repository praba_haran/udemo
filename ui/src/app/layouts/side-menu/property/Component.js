import React from 'react';
import classNames from 'classnames';
import {Link} from "react-router";
import Helper from '../../../components/shared/helper';
import SiteMenuLink from '../../../components/shared/SiteMenuLink';

export default class Component extends React.Component{

    render(){
        let { items,isLoading, marketFor,onMarketChange,getPageSummary,
            count,prev,next,pageIndex,getEndIndex,
            PROPERTY_DETAIL } = this.props;
        let noItemFound;
        if(items.length === 0){
            if(isLoading){
                noItemFound = (
                    <li className="no-item-found">
                        <p>Loading ...</p>
                    </li>
                );
            }
            else {
                noItemFound = (
                    <li className="no-item-found">
                        <p>No properties found.</p>
                    </li>
                );
            }
        }
        return (
            <div>
                <div className="panel nav-tabs-horizontal panel-transparent">
                    <ul className="site-menu">
                        <li className="site-menu-item active">
                            <Link to="/properties" className="clearfix animsition-link">
                                <i className="site-menu-icon fa-home" aria-hidden="true"></i>
                                <span className="site-menu-title">Properties</span>
                            </Link>
                        </li>
                    </ul>
                    <ul className="nav nav-tabs nav-tabs-line">
                        <li onClick={()=> onMarketChange('All')} className={classNames({
                            'active':marketFor==='All'
                        })}>
                            <a href="javascript:void(0)">All</a>
                        </li>
                        <li onClick={()=> onMarketChange('For Sale')} className={classNames({
                            'active':marketFor==='For Sale'
                        })}>
                            <a href="javascript:void(0)">Sale</a>
                        </li>
                        <li onClick={()=> onMarketChange('To Let')} className={classNames({
                            'active':marketFor==='To Let'
                        })}>
                            <a href="javascript:void(0)">Rent</a>
                        </li>
                    </ul>
                    <div className="clear">
                        <ul className="site-menu property-listing">
                            {items.map((item)=>{
                                let p = (item._id === PROPERTY_DETAIL._id)? PROPERTY_DETAIL: item;
                                return (
                                    <SiteMenuLink routeId={p._id} key={p._id} id={p._id} to={'/properties/'+p._id+'/overview'} activeClassName="active">
                                        <div className="left">
                                            {p.photos.length>0?(
                                                <img width="40px" height="auto" src={p.photos[0].url.replace('upload','upload/w_40,h_30,c_pad,b_black')} alt={p.photos[0].title} />
                                            ):(
                                                <img width="40px" height="auto" src="/dist/images/photos/no-image-354x255.png" alt="No Image" />
                                            )}
                                        </div>
                                        <div className="right">
                                            <p className="address">{p.address.nameOrNumber+', '+p.address.street}</p>
                                            <p className="price">
                                                <span className="text-danger">{Helper.toCurrency(Helper.property.getPrice(p))}</span>
                                                <small className="margin-left-10">{Helper.property.getStatus(p)}</small>
                                            </p>
                                        </div>
                                    </SiteMenuLink>
                                );
                            })}
                            {!isLoading?(
                                    <li className="page-summary">
                                        <div className="clear">
                                            <div className="col-sm-8">
                                                <p>{getPageSummary()}</p>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="btn-group">
                                                    <button type="button" onClick={prev} disabled={(items.length=== count || pageIndex===1)} className="btn btn-sm">
                                                        <i className="icon fa-angle-left"></i>
                                                    </button>
                                                    <button type="button" onClick={next} disabled={getEndIndex()=== count} className="btn btn-sm">
                                                        <i className="icon fa-angle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ):null}
                            {noItemFound}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
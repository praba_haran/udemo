import React from 'react';
import SiteMenuLink from '../../../components/shared/SiteMenuLink';
import {Link} from "react-router";
import Calendar from 'rc-calendar'

export default class DefaultMenus extends React.Component{

    render(){
        return(
            <div className="side-menu-default">
                <div className="inner-container">
                    <ul className="site-menu">
                        <SiteMenuLink to="/" activeClassName="active">
                            <i className="site-menu-icon fa-desktop" aria-hidden="true"></i>
                            <span className="site-menu-title">Dashboard</span>
                        </SiteMenuLink>
                        <SiteMenuLink to="/properties" activeClassName="active">
                            <i className="site-menu-icon fa-home" aria-hidden="true"></i>
                            <span className="site-menu-title">Properties</span>
                        </SiteMenuLink>
                        <SiteMenuLink to="/contacts" activeClassName="active">
                            <i className="site-menu-icon fa-user" aria-hidden="true"></i>
                            <span className="site-menu-title">Contacts</span>
                        </SiteMenuLink>
                        <SiteMenuLink to="/diary" activeClassName="active">
                            <i className="site-menu-icon fa-calendar" aria-hidden="true"></i>
                            <span className="site-menu-title">Diary</span>
                        </SiteMenuLink>
                        <SiteMenuLink to="/keys" activeClassName="active" subMenus={this.getKeySubMenus()}>
                            <i className="site-menu-icon fa-key" aria-hidden="true"></i>
                            <span className="site-menu-title">Manage Keys</span>
                        </SiteMenuLink>
                        <SiteMenuLink to="/settings" activeClassName="active">
                            <i className="site-menu-icon fa-gear" aria-hidden="true"></i>
                            <span className="site-menu-title">Settings</span>
                        </SiteMenuLink>
                    </ul>

                    {/* Week calendar commented */}
                    {/*<div className="week-calendar">*/}
                        {/*<Calendar*/}
                            {/*showDateInput={false}*/}
                            {/*showToday={false}*/}
                            {/*selectedValue ={new Date()}*/}
                        {/*/>*/}
                    {/*</div>*/}

                </div>
            </div>
        );
    }

    getKeySubMenus(){
        return (
            <ul className="site-menu-sub is-shown">
                <li className="site-menu-item has-sub">
                    <Link className="waves-effect waves-classic" to="/key-management/cabinets">
                        <span className="site-menu-title">Cabinets</span>
                    </Link>
                </li>
                <li className="site-menu-item has-sub">
                    <Link className="waves-effect waves-classic" to="/key-management/keys">
                        <span className="site-menu-title">Keys</span>
                    </Link>
                </li>
            </ul>
        );
    }
}
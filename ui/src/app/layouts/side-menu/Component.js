import React from 'react';
import localStorage from 'localStorage';
import { hashHistory } from 'react-router';
import DefaultMenus from './default/DefaultMenus';
import PropertyMenus from './property/Container';
import ContactMenus from './contact/Container';
import DiaryMenus from './diary/Container';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Component extends React.Component{

    logOut (e){
        e.preventDefault();
        localStorage.removeItem('x-access-token');
        hashHistory.push('/login');
    }

    renderSideMenu(){
        let { commands } = this.props;
        if(commands.SHOW_SIDE_MENU_PROPERTIES){
            return (<PropertyMenus {...this.props}/>);
        }
        else if(commands.SHOW_SIDE_MENU_CONTACTS){
            return (<ContactMenus {...this.props}/>);
        }
        else if(commands.SHOW_SIDE_MENU_DIARY){
            return (<DiaryMenus {...this.props}/>);
        }
        return (<DefaultMenus {...this.props}/>);
    }

    render(){
        return(
            <div className="site-menubar">
                <div className="site-menubar-body">
                    <Scrollbars
                        autoHide
                        autoHideTimeout={1000}
                        autoHideDuration={200}
                        universal={true}
                    >
                        {this.renderSideMenu()}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}
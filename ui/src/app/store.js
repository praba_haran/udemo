import { createStore ,compose } from 'redux';
import reducers from './reducers';

const finalCreateStore = compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

//const store = createStore(reducers);
const store = finalCreateStore(reducers);

export default store;

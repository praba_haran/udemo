import axios from 'axios';
import localStorage from 'localStorage';
import config from '../config';

export default {
    getImageUrl: function (url) {
        let token = localStorage.getItem('x-access-token');
        url = config.API_BASE_URL+url;
        url += '?token=';
        url += token;
        return url;
    },
    getBrochureUrl:function(url){
        let token = localStorage.getItem('x-access-token');
        url = config.API_BASE_URL+url;
        url += '?token=';
        url += token;
        if(config.APP_ENV==='staging'){
            url +='&cache=true';
        }
        return url;
    }
};
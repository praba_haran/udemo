import axios from 'axios';
import localStorage from 'localStorage';
import { hashHistory } from 'react-router';
import Alert from '../lib/Alert';
import config from '../config';

export function getInstance()
{

    let axiosInstance = axios.create({
        baseURL: config.API_BASE_URL,
        // headers:{
        //     'x-access-token':localStorage.getItem('x-access-token'),
        //     'Content-Type':'application/json;charset=UTF-8'
        // }
    });

    axiosInstance.defaults.headers.common['x-access-token'] = localStorage.getItem('x-access-token');
    axiosInstance.defaults.headers.common['Content-Type'] = 'application/json';

    axiosInstance.interceptors.response.use(function (response) {
        // handle success response here
        if (response.status === 200 && response.data.success) {
            if (response.data.message) {
                Alert.clearLogs().success(response.data.message);
            }
        }
        // else if(response.status===200 && !response.data.success){
        //     if(response.data.error){
        //         Alert.error(response.data.error);
        //     }
        // }
        return response;
    }, function (error) {
        if (error && error.response) {
            if (error.response.status === 403) {
                localStorage.removeItem('x-access-token');
                hashHistory.push('/login');
            }
        }
        return Promise.reject(error);
    });

    return axiosInstance;
}

export function ajax(options){
    options.contentType = 'application/json';
    options.dataType = 'json';
    options.async = true;
    options.cache = true;
    options.crossDomain = true;
    options.url = config.API_BASE_URL+options.url;
    if(options.method.toLowerCase()==='post' || options.method.toLowerCase() === 'put'){
        options.data = JSON.stringify(options.data);
    }
    options.headers = {};
    if(localStorage.getItem('x-access-token')!==undefined && localStorage.getItem('x-access-token')!==null){
        options.headers['x-access-token'] =  unescape(encodeURIComponent(localStorage.getItem('x-access-token')));
    }
    $.support.cors = true;
    let instance  = $.ajax(options);

    instance.fail(function(jqXHR){
        if(jqXHR.status===403){
            hashHistory.replace('/login');
        }
    });
    return instance;
}
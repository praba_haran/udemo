import _ from 'lodash';
import Immutable from 'immutable';
import store from './store';
import { validate } from './reducers/validator';

const commonTypes = {
    SET_MODEL:'SET_MODEL',
    CLEAR_MODEL : 'CLEAR_MODEL',
    VALIDATE_MODEL:'VALIDATE_MODEL',
    VALUE_CHANGE:'VALUE_CHANGE',
    SET_OBJECT_IN_PATH :'SET_OBJECT_IN_PATH',
    REMOVE_OBJECT_IN_PATH:'REMOVE_OBJECT_IN_PATH',
    SEARCH_MODEL : 'SEARCH_MODEL'
};

class CollectionBase {

    constructor(collection,getSchemaFn){
        this.collection = collection;
        this.types = {};
        this.actions = {};
        this.reducer = null;
        this._getSchemaFn = getSchemaFn;
        this.defaultState = this._getDefaultState();
        this.api = {};
        this.context = {};

        // this._initTypes = this._initTypes.bind(this);
        // this._initActions = this._initActions.bind(this);
        // this._initReducer = this._initReducer.bind(this);

        this._initTypes();
        this._initActions();
        this._initReducer();
    }

    _getDefaultState(){
        let model = this._getSchemaFn() || {};
        model.validator = Immutable.Map({
            isSubmitted : false,
            isValid : false,
            errors : Immutable.List([])
        });
        return Immutable.fromJS(model);
    }

    _initTypes(){
        let _this = this;
        _.each(commonTypes,function (type) {
            _this.types[type] = _this.collection+'_'+type;
        });
    }

    _initActions(){
        let types = this.types;

        this.actions.setModel = function(model){
            if(!model){
                throw new Error('The argument model is missing.');
            }
            store.dispatch({
                type : types[commonTypes.SET_MODEL],
                model
            });
        };

        this.actions.clearModel = function () {
            store.dispatch({
                type : types[commonTypes.CLEAR_MODEL]
            });
        };

        this.actions.validateModel = function () {
            store.dispatch({
                type : types[commonTypes.VALIDATE_MODEL]
            });
        };

        this.actions.valueChange = function(event){
            if(!event){
                throw new Error('The argument event is missing.');
            }
            store.dispatch({
                type : types[commonTypes.VALUE_CHANGE],
                event
            });
        };

        this.actions.change = function(key,value){
            if(!key){
                throw new Error('The argument key is missing.');
            }
            if(value===undefined){
                throw new Error('The argument value is missing.');
            }

            let payload = {
                target :{
                    name : key,
                    value : value
                },
                type:'change'
            };
            store.dispatch({
                type : types[commonTypes.VALUE_CHANGE],
                event : payload
            });
        };

        this.actions.search = function(key,value){
            if(!key){
                throw new Error('The argument key is missing.');
            }
            if(value===undefined){
                throw new Error('The argument value is missing.');
            }

            let payload = {
                target :{
                    name : key,
                    value : value
                },
                type:'change'
            };
            store.dispatch({
                type : types[commonTypes.VALUE_CHANGE],
                event : payload
            });
            store.dispatch({
                type : types[commonTypes.SEARCH_MODEL],
                key : key,
                value : value
            });
        };

        this.actions.setObjectInPath = function(path,obj){
            if(!path){
                throw new Error('The argument path is missing.');
            }
            if(!obj){
                throw new Error('The argument obj is missing.');
            }

            store.dispatch({
                type : types[commonTypes.SET_OBJECT_IN_PATH],
                path,
                obj
            });
        };

        this.actions.removeObjectInPath = function(path){
            if(!path){
                throw new Error('The argument path is missing.');
            }

            store.dispatch({
                type : types[commonTypes.REMOVE_OBJECT_IN_PATH],
                path
            });
        };
    }

    _initReducer(){
        let types = this.types;
        let collection = this.collection;
        let _this = this;

        this.reducer = function(state,action){
            if(!state){
                state = _this._getDefaultState();
            }
            let newState;
            switch (action.type){

                case types[commonTypes.SET_MODEL]:
                    action.model.validator = {
                        isSubmitted : false,
                        isValid : false,
                        errors : []
                    };
                    return Immutable.fromJS(action.model);
                    break;

                case types[commonTypes.CLEAR_MODEL]:
                    return _this._getDefaultState();
                    break;

                case types[commonTypes.VALIDATE_MODEL]:
                    let validatorState = validate(collection,state.toJS());
                    return state.set('validator',Immutable.fromJS(validatorState));
                    break;

                case types[commonTypes.VALUE_CHANGE]:
                    let target = action.event.target;
                    if(action.event.type==='change' || action.event.type==='propertychange') {
                        let field = target.name.replace(/\[/g, '.');
                        field = field.replace(/\]/g, '');
                        let path = field.split('.');

                        let value = target.value;
                        if (target.type === 'checkbox') {
                            value = target.checked;
                        }
                        return state.setIn(path, value);
                    }
                    else{
                        let currentState = state.toJS();
                        if(currentState.validator.isSubmitted){
                            let validator = validate(collection,currentState);
                            return state.set('validator',Immutable.fromJS(validator));
                        }
                    }
                    break;

                case types[commonTypes.SET_OBJECT_IN_PATH]:
                    return state.setIn(action.path,Immutable.fromJS(action.obj));
                    break;

                case types[commonTypes.REMOVE_OBJECT_IN_PATH]:
                    return state.removeIn(action.path);
                    break;
            }
            return state;
        }
    }
}

let CollectionFactory = (function () {

    let collectionDict = {};

    return {
        getCollection : function(collectionName,getSchemaFn){
            if(!collectionDict[collectionName]){
                collectionDict[collectionName] = new CollectionBase(collectionName,getSchemaFn);
            }
            return collectionDict[collectionName];
        }
    };
})();

export default CollectionFactory;

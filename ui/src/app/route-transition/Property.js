import Commands from '../models/Commands';
import * as constants from '../constants';

class Transition {

    onEnteringPropertyDetail(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_PROPERTIES,true);
    }
    onLeavingPropertyDetail(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_PROPERTIES,false);
    }

    onEnteringRooms(){

    }
    onLeavingRooms(){

    }
}

export default new Transition();
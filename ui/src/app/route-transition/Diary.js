import PropertyDetail from '../models/PropertyDetail';
import Commands from '../models/Commands';
import * as constants from '../constants';

class Transition {

    onEnteringDiary(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_DIARY,true);
    }
    onLeavingDiary(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_DIARY,false);
    }
}

export default new Transition();
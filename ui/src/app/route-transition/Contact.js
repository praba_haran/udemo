import Commands from '../models/Commands';
import * as constants from '../constants';

class Transition {

    onEnteringContactDetail(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_CONTACTS,true);
    }
    onLeavingContactDetail(){
        Commands.actions.change(constants.COMMANDS.SHOW_SIDE_MENU_CONTACTS,false);
    }
}

export default new Transition();
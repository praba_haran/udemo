import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        contractType : 'For Sale',
        price : 0,
        saleTerms : '',
        rentalFrequency : '',
        priceQualifier : '',
        contractLength : '',
        contractEndsOn : null,
        notes: '',
        actions : Immutable.Map({
            requireBoardChange : false
        }),
        commissionAndFees: Immutable.Map({
            commissionMin : 0,
            commissionMax : 0,
            commissionAdditional : 0,
            fee : 0,
            minFee : 0,
            withdrawalFee : 0,
            isVatInclusive : false
        }),
        discount : Immutable.Map({
            percent : 0,
            fee : 0,
            appliesFrom : null
        }),
        management : Immutable.Map({
            deposit: 0,
            type : '',
            percentage:Immutable.Map({
                upfrontFee : 0,
                recurringFee : 0,
                recurringPeriod : ''
            }),
            fixed :Immutable.Map({
                upfrontFee : 0,
                recurringFee : 0,
                recurringPeriod : ''
            }),
            maintenanceType : '',
            overseasLandlordType : '',
            availableOn : null,
            notes : ''
        }),
        boardChangeRequest : Immutable.Map({
            currentBoard : 'No Board',
            newBoard :'',
            boardContractor : '',
            message : '',
            includeOwnerInMail : false
        }),
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
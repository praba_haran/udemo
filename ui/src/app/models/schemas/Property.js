import Immutable from 'immutable';
import dropDownValues from '../../components/shared/dropdown-values';

export default () => {
    return {
        type: 'House',
        category: 'Residential',
        market: 'For Sale',
        age: '',
        marketing: '',
        timeOnMarket: '',
        bedrooms: 2,
        bathrooms: 2,
        receptions: 0,
        parking: 0,
        floorArea: 0,
        landArea: 0,
        landAreaUnit: 'Acre',
        summary: '',
        advertSummary: '',
        furnishing: '',
        appraiserNotes: '',
        price: 0,
        lowPrice: 0,
        highPrice: 0,
        ownerPrice: 0,
        proposedPrice: 0,
        saExpireOn: '',
        address: Immutable.Map({
            dwelling: '',
            nameOrNumber: '',
            street: '',
            locality: '',
            town: '',
            county: '',
            postcode: '',
            country: 'United Kingdom',
            fullAddress: ''
        }),
        owners: Immutable.List([]),
        photos: Immutable.List([]),
        floorPlans: Immutable.List([]),
        virtualTours: Immutable.List([]),
        brochures: Immutable.List([]),
        externalBrochures: Immutable.List([]),
        otherFiles: Immutable.List([]),
        webLinks: Immutable.List([]),
        features: Immutable.Map({
            selectedIndex: 0,
            list: Immutable.List([])
        }),
        rooms: Immutable.List([]),
        matching: Immutable.List([
            Immutable.Map({
                name: 'List 1',
                matchPropertyType: 'No',
                matchBedrooms: 'No',
                matchBathrooms: 'No',
                matchReceptions: 'No',
                matchParking: 'No',
                matchFloorAreas: 'No',
                matchLandAreas: 'No',
                matchFeatures: 'Yes',
                minPrice: 0,
                maxPrice: 0,
                isSelected: true,
                filters: Immutable.List([
                    {name: 'Active Applicants', selected: false},
                    {name: 'All Vendors', selected: false},
                    {name: 'Archived Applicants', selected: false},
                    {name: 'Hot Buyers', selected: false},
                    {name: 'Landlord with email', selected: false},
                    {name: 'Move July', selected: false},
                    {name: 'Potential Vendors', selected: false},
                    {name: 'Rental Apps', selected: false},
                    {name: 'Rental Apps Last 7 days', selected: false},
                    {name: 'Sales Apps', selected: false}
                ]),
                essentials: Immutable.fromJS(dropDownValues.matchingEssentials),
            })
        ]),
        epc: Immutable.Map({
            EnergyEfficiencyRating: Immutable.Map({
                current: 0,
                potential: 0
            }),
            EnvironmentalImpactRating: Immutable.Map({
                current: 0,
                potential: 0
            })
        }),
        contract: Immutable.Map({
            contractType: 'For Sale',
            price: 0,
            saleTerms: '',
            rentalFrequency: '',
            priceQualifier: '',
            contractLength: '',
            contractEndsOn: null,
            instructId: '',
            commissionAndFees: Immutable.Map({
                commissionMin: 0,
                commissionMax: 0,
                commissionAdditional: 0,
                fee: 0,
                minFee: 0,
                withdrawalFee: 0,
                isVatInclusive: false
            }),
            discount: Immutable.Map({
                percent: 0,
                fee: 0,
                appliesFrom: null
            }),
            management: Immutable.Map({
                deposit: 0,
                type: '',
                percentage: Immutable.Map({
                    upfrontFee: 0,
                    recurringFee: 0,
                    recurringPeriod: ''
                }),
                fixed: Immutable.Map({
                    upfrontFee: 0,
                    recurringFee: 0,
                    recurringPeriod: ''
                }),
                maintenanceType: '',
                overseasLandlordType: '',
                availableOn: null,
                notes: ''
            }),
        }),
        termsSent: Immutable.Map({
            notes: '',
            isTermsSent: false,
            termsSentId: ''
        }),
        termsSigned: Immutable.Map({
            notes: '',
            isTermsSigned: false,
            termsSentId: ''
        }),
        agency: '',
        // negotiator: '',
        status: 'Pre Appraisal',
        reviewDate: null,
        reviewNotes: '',
        lastContactedDate: null,
        recordStatus : 'Active'
    }
};
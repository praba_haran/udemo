import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        notes : '',
        isTermsSigned : false,
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
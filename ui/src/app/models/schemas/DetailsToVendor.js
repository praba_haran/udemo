import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        recipients : Immutable.List([]),
        selectedIndex : 0,
        notes : '',
        isApproved : false,
        approvedDate : null,
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
import Immutable from 'immutable';

export default ()=>{
    return {
        type :'Client',
        companyName :'',
        companyWebsite:'',
        intension :'To Buy',
        leadSource : '',
        notes:'',
        position:'',
        disposal:'',
        movingReason:'',
        rating:'',
        timeScale:null,
        action:'',
        referredBy:null,
        referredToInt:null,
        referredToExt:null,
        referralNotes:'',
        moneyLaunderingCheck:{
            isIdCheckCompleted:false,
            notes:''
        },
        status:'Active',
        address:Immutable.Map({
            dwelling:'',
            nameOrNumber:'',
            street:'',
            locality:'',
            town:'',
            county:'',
            postcode:'',
            country:'United Kingdom'
        }),
        firstPerson:Immutable.Map({
            title:'Mr',
            forename:'',
            surname:'',
            salutation:'',
            telephones:Immutable.List([
                Immutable.Map({
                    type:'Home',
                    label:'',
                    number:''
                })
            ]),
            emails:Immutable.List([
                Immutable.Map({
                    email:''
                })
            ])
        }),
        secondPerson:Immutable.Map({
            title:'Mr',
            forename:'',
            surname:'',
            salutation:'',
            telephones:Immutable.List([
                Immutable.Map({
                    type:'Home',
                    label:'',
                    number:''
                })
            ]),
            emails:Immutable.List([
                Immutable.Map({
                    email:''
                })
            ])
        }),
        solicitor:{
            solicitor:null,
            notes:'',
            referralInt:null,
            referralExt:null
        },
        interestedProperties:Immutable.List([]),
        docs:Immutable.List([]),
        agency: '',
        // negotiator: '',
        properties:Immutable.List([]),// owner properties
        reviewDate:null,
        reviewNotes:'',
        lastContactedDate:null,
        extra:{
            selected : false
        }
    }
};
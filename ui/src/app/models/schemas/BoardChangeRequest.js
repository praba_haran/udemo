import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        currentBoard : 'No Board',
        newBoard :'',
        boardContractor : '',
        message : '',
        includeOwnerInMail : false,
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        oldStatus: '',
        status: '',
        notes: '',
        actions : Immutable.Map({
            updateLastContactedDate : false,
            requireBoardChange : false
        }),
        boardChangeRequest : Immutable.Map({
            currentBoard : 'No Board',
            newBoard :'',
            boardContractor : '',
            message : '',
            includeOwnerInMail : false
        }),
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
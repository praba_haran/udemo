import Immutable from 'immutable';

export default ()=>{
    return {
        agency: null,
        property : null,
        notes : '',
        isTermsSent : false,
        createdAt : null,
        createdBy : Immutable.Map({
        })
    };
};
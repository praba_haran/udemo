import _ from 'lodash';
import Alert from '../lib/Alert';
import Immutable from 'immutable';
import uuid from 'uuid';
import store from '../store';
import { getInstance,ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Commands from './Commands';
import dropDownValues from '../components/shared/dropdown-values';

let _clientId = uuid.v1();

let filters = [
    { name : 'Active Applicants',selected:false },
    { name : 'All Vendors',selected:false },
    { name : 'Archived Applicants',selected:false },
    { name : 'Hot Buyers',selected:false },
    { name : 'Landlord with email',selected:false },
    { name : 'Move July',selected:false },
    { name : 'Potential Vendors',selected:false },
    { name : 'Rental Apps',selected:false },
    { name : 'Rental Apps Last 7 days',selected:false },
    { name : 'Sales Apps',selected:false }
];

function getMatching(index){
    let clientId = uuid.v1();
    return {
        _clientId : clientId,
        index : index,
        name: 'List '+(index+1),
        matchPropertyType: 'No',
        matchBedrooms: 'No',
        matchBathrooms: 'No',
        matchReceptions: 'No',
        matchParking: 'No',
        matchFloorAreas: 'No',
        matchLandAreas: 'No',
        matchFeatures: 'Yes',
        minPrice: 0,
        maxPrice: 0,
        filters : Immutable.fromJS(filters),
        essentials:Immutable.fromJS(dropDownValues.matchingEssentials),
        status:'Active'
    };
}

let Collection = Factory.getCollection(constants.MODELS.MATCHING, ()=>{
    return {
        matching: Immutable.List([
            Immutable.Map({
                _clientId : _clientId,
                index : 0,
                name: 'List 1',
                matchPropertyType: 'No',
                matchBedrooms: 'No',
                matchBathrooms: 'No',
                matchReceptions: 'No',
                matchParking: 'No',
                matchFloorAreas: 'No',
                matchLandAreas: 'No',
                matchFeatures: 'Yes',
                minPrice: 0,
                maxPrice: 0,
                filters : Immutable.fromJS(filters),
                essentials:Immutable.fromJS(dropDownValues.matchingEssentials),
                status:'Active'
            })
        ]),
        matches:Immutable.List([]),
        selectedId:_clientId,
        isProcessing : true
    };
});

Collection.api.getList = function(propertyId,cb){
    Collection.actions.change('isProcessing',true);

    ajax({
        method : 'get',
        url:'property/'+propertyId+'/matching'
    }).done(function( result ) {
        if(result.success){
            if(result.data.length>0) {
                Collection.actions.change('selectedId', result.data[0]._clientId);
                Collection.actions.setObjectInPath(['matching'], result.data);
                Collection.api.getMatches(propertyId,result.data[0]);
            }
            else{
                // AFTER DELETION, IF THERE IS NO MATCHING FOR A PROPERTY, LOAD DEFAULT MATCHING
                let item = getMatching(0);
                Collection.actions.change('selectedId', item._clientId);
                Collection.actions.setObjectInPath(['matching'],[item]);
                Collection.api.getMatches(propertyId,item);
            }
            Collection.actions.change('isProcessing', false);
        }
        if (cb) cb(res.data.data);
    });
};

Collection.api.save = function(propertyId,model,cb){
    ajax({
        method : 'post',
        url:'property/'+propertyId+'/matching/save',
        data : model
    }).done(function( result ) {
        if(result.success){
            Collection.actions.setObjectInPath(['matching', model.index],result.data);
            Alert.clearLogs().success(result.message);
            if(cb) cb(result.data);
        }
    });
};

Collection.api.remove = function(propertyId,model,cb){

    return getInstance().delete('property/'+propertyId+'/matching/remove/'+model._id).then((res)=>{
        if(res.data.success){
            //CALLING GET LIST TO LOAD A UPDATED LIST FROM SERVER
            Collection.api.getList(propertyId);
            if(cb) cb(res.data.data);
        }
        else{
            console.log(res.data);
        }
        return res;
    });
};

Collection.api.addList = function(){
    let MATCHING = store.getState().MATCHING.toJS();
    let { matching } = MATCHING;
    let item = getMatching(matching.length);
    Collection.actions.setObjectInPath(['matching', matching.length],item);
    Collection.actions.change('selectedId',item._clientId);
};

Collection.api.getMatches = function(propertyId,requirement){
    ajax({
        method : 'post',
        url:'property/'+propertyId+'/matching/matches',
        data : requirement
    }).done(function( result ) {
        if(result.success){
            Collection.actions.setObjectInPath(['matches'], result.data);
        }
    });
};

export default Collection;
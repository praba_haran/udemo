import Immutable from 'immutable';
import _ from 'lodash';
import store from '../store';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import GoogleClient from '../lib/GoogleClient';
import App from './App';
import Login from './Login';

let Collection = Factory.getCollection(constants.MODELS.GOOGLE_CALENDARS,()=>{
    return {
        items : Immutable.List([]),
        error : null,
        isProcessing : false,
        isReloadRequired : false
    };
});

Collection.api.getCalendars = function(cb){

    let callbackHandler = (result)=>{
        if (result.items && result.items.length > 0) {
            let items = _.filter(result.items,(item)=>{
                return item.id.indexOf('group.calendar.google.com')>-1;
            });
            Collection.actions.setObjectInPath(['items'], items);
            Collection.actions.change('isReloadRequired',false);
            if(cb) cb(items);
        }
        else {
            Collection.actions.change('error', result.message);
        }
        Collection.actions.change('isProcessing',false);
    };

    let GOOGLE_CALENDARS = store.getState().GOOGLE_CALENDARS.toJS();
    if(GOOGLE_CALENDARS.items.length===0 || GOOGLE_CALENDARS.isReloadRequired) {
        Collection.actions.change('isProcessing',true);
        let Calendar = GoogleClient.getCalendar();
        Calendar.checkIsUserLoggedIn((isUserLoggedIn) => {
            if (isUserLoggedIn) {
                Calendar.getCalendars(callbackHandler);
                App.actions.change('meta.isGoogleAuthDone',true);
            }
            else {
                App.actions.change('meta.isGoogleAuthDone',false);
                Collection.actions.change('isProcessing',false);
            }
        });
    }
    else{
        if(cb) cb(GOOGLE_CALENDARS.items);
    }
};

Collection.api.connectToGoogle = function(cb){
    let Calendar = GoogleClient.getCalendar();
    Calendar.login((profile)=>{
        Login.api.logUserLogin({
            login_via:'google',
            profile : profile
        },function(){
            App.actions.change('meta.isGoogleAuthDone',true);
            cb(profile);
        });
    });
};

export default Collection;
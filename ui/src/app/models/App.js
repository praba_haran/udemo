import _ from 'lodash';
import Immutable from 'immutable';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Requirement from './Requirement';

let Collection = Factory.getCollection(constants.MODELS.APP, ()=>{
    return {
        userInfo: null,
        userProfile: null,
        agencyDetails: null,
        meta : Immutable.Map({
            isGoogleApiLoaded : false,
            isGoogleAuthDone : false
        })
    };
});

Collection.api.getUserInfo = function () {
    ajax({
        method: 'get',
        url: 'account/user/me'
    }).done(function (result) {
        if (result.success) {
            let userInfo = result.data.userInfo;
            let userProfile = result.data.profile;
            Collection.actions.setObjectInPath(['userInfo'], userInfo);
            if (userProfile) {
                Collection.actions.setObjectInPath(['userProfile'], userProfile);
            }
            // SETTING DEFAULT AREAS FOR REQUIREMENT PAGE
            if (userInfo.agency.settings.areas.length > 0) {
                let areas = [];
                _.each(userInfo.agency.settings.areas, (p) => {
                    areas.push({
                        name: p,
                        selected: false
                    });
                });
                Requirement.actions.setObjectInPath(['requirements', 0, 'areas'], areas);
            }
        }
    });
};

Collection.api.getAgencyDetails = function () {
    ajax({
        method: 'get',
        url: 'agency/details'
    }).done(function (result) {
        if (result.success) {
            Collection.actions.setObjectInPath(['agencyDetails'], result.data);
        }
    });
};

// NEED TO DEPRECATE ABOVE METHOD
Collection.api.getAgency = function (cb) {
    ajax({
        method: 'get',
        url: 'agency/details'
    }).done(cb);
};

export default Collection;
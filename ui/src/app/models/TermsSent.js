import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/TermsSent';

let Collection = Factory.getCollection(constants.MODELS.TERMS_SENT,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/terms/save',
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'property/terms/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'property/terms/list',
        data : model
    }).done(cb);
};

export default Collection;
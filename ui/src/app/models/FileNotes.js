import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.FILE_NOTES,()=>{
    return {
        property:null,
        notes:'',
        applicants : Immutable.List([])
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'file-notes/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'file-notes/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.cancel = function(id,cb){
    ajax({
        method : 'delete',
        url:'file-notes/cancel/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'file-notes/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'file-notes/search',
        data : model
    }).done(cb);
};

export default Collection;
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/DetailsToVendor';

let Collection = Factory.getCollection(constants.MODELS.DETAILS_TO_VENDOR,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/details-to-vendor/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'property/details-to-vendor/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'property/details-to-vendor/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'property/details-to-vendor/list',
        data : model
    }).done(cb);
};

export default Collection;
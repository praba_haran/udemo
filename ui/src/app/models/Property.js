import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/Property';

let Collection = Factory.getCollection(constants.MODELS.PROPERTY,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/save',
        data: model
    }).done(cb);
};

Collection.api.searchAddress = function(postcode,cb){
    ajax({
        method : 'get',
        url:'address/search/'+postcode
    }).done(cb);
};

export default Collection;
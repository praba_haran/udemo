import _ from 'lodash';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

const model = {};
_.each(constants.ERROR_MESSAGES,function(p){
   model[p] = false;
});
let Collection = Factory.getCollection(constants.MODELS.ERROR_MESSAGES,()=>{
   return model;
});

export default Collection;
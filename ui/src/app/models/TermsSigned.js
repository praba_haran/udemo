import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/TermsSigned';

let Collection = Factory.getCollection(constants.MODELS.TERMS_SIGNED,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/terms-signed/save',
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'property/terms-signed/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'property/terms-signed/list',
        data : model
    }).done(cb);
};

export default Collection;
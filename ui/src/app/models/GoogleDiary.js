import Immutable from 'immutable';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import GoogleApiClient from '../lib/GoogleApiClient';
import Login from './Login';

let Collection = Factory.getCollection(constants.MODELS.GOOGLE_DIARY,()=>{
    return {
        authResult : null,
        calendars : Immutable.List([]),

        error : null,
        isProcessing : false
    };
});

// Collection.api.login = function(cb){
//     GoogleApiClient.getInstance().login((profile)=>{
//         Login.api.logUserLogin({
//             login_via:'google',
//             profile : profile
//         },function(){
//             cb(profile);
//         });
//     });
// };



export default Collection;
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.PROPERTY_LISTING,()=>{
    return {
        properties:Immutable.List([]),
        isProcessing :false
    };
});

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'property/search',
        data : model
    }).done(cb);
};

Collection.api.deleteItems = function(model,cb){
    ajax({
        method : 'post',
        url:'property/delete/items',
        data : model
    }).done(cb);
};

Collection.api.archiveItems = function(model,cb){
    ajax({
        method : 'post',
        url:'property/archive/items',
        data : model
    }).done(cb);
};

Collection.api.activateItems = function(model,cb){
    ajax({
        method : 'post',
        url:'property/activate/items',
        data : model
    }).done(cb);
};

export default Collection;
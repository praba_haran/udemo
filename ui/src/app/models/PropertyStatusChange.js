import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/PropertyStatusChange';

let Collection = Factory.getCollection(constants.MODELS.PROPERTY_STATUS_CHANGE,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/status/save',
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'property/status/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'property/status/list',
        data : model
    }).done(cb);
};

export default Collection;
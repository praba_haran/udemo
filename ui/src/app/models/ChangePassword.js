import Immutable from 'immutable';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

let Collection = Factory.getCollection(constants.MODELS.CHANGE_PASSWORD, ()=>{
    return {
        model :Immutable.Map({
            password : '',
            confirmPassword : '',
            token : null
        }),
        error : null,
        success : null,
        isProcessing : false
    };
});

Collection.api.changePassword = function(model,cb){
    ajax({
        method : 'post',
        url:'forgot-password/change-password',
        data: model
    }).done(cb);
};

Collection.api.validateToken = function(token,cb){
    ajax({
        method : 'get',
        url:'forgot-password/validate-token/'+token
    }).done(cb);
};

export default Collection;
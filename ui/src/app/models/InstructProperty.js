import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/InstructProperty';

let Collection = Factory.getCollection(constants.MODELS.INSTRUCT_PROPERTY,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'property/instruct/save',
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'property/instruct/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'property/instruct/list',
        data : model
    }).done(cb);
};

export default Collection;
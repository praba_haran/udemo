import Immutable from 'immutable';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

let Collection = Factory.getCollection(constants.MODELS.INVITE_USERS, ()=>{
    return {
        users: Immutable.List([
            Immutable.Map({
                email : '',
                forename : '',
                surname : ''
            })
        ]),
        responses : Immutable.List([]),
        error : null,
        isProcessing : false
    };
});

Collection.api.inviteUsers = function(model,cb){
    ajax({
        method : 'post',
        url:'setting/branch/invite-users',
        data : model
    }).done(cb);
};

export default Collection;
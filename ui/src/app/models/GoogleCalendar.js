import _ from 'lodash';
import Immutable from 'immutable';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import GoogleClient from '../lib/GoogleClient';
import GoogleCalendars from './GoogleCalendars';
import store from '../store';

let Collection = Factory.getCollection(constants.MODELS.GOOGLE_CALENDAR,()=>{

    return {
        calendar : Immutable.Map({
            accessRole :'',
            backgroundColor :'',
            colorId : '',
            etag:'',
            foregroundColor:'',
            id:'',
            kind :'',
            primary : false,
            selected :false,
            summary : '',
            description:'',
            timeZone :'',
            defaultReminders:Immutable.Map({
                method:'',
                minutes:10
            }),
            notificationSettings: Immutable.Map({
                notifications:Immutable.List([
                    Immutable.Map({
                        method:'email',
                        type : 'eventCreation'
                    })
                ])
            })
        }),
        permissions : Immutable.List([
            Immutable.Map({
                etag :'',
                id:'',
                role :'reader',
                scope:Immutable.Map({
                    type:'user',
                    value :''
                }),
                status:'active'
            })
        ]),
        message : null,
        error : null,
        isProcessing : false
    };
});

Collection.api.createCalendar = function(model){
    Collection.actions.change('isProcessing',true);
    let Calendar = GoogleClient.getCalendar();
    Calendar.saveCalendar(model.calendar,(result)=>{
        if(result.error){
            Collection.actions.change('error',result.error.message);
        }
        else{
            Collection.actions.change('message','Calendar has been created successfully.');
            // AFTER SAVING, SET THIS FLAG TO TRUE FOR RE LOADING
            GoogleCalendars.actions.change('isReloadRequired',true);
        }
        Collection.actions.change('isProcessing',false);
    });
};

Collection.api.updateCalendar = function(model){
    Collection.actions.change('isProcessing',true);
    let Calendar = GoogleClient.getCalendar();
    Calendar.updateCalendarList(model.calendar,(result)=>{
        if(result.error){
            Collection.actions.change('error',result.error.message);
            Collection.actions.change('isProcessing',false);
        }
        else{
            Calendar.updateCalendar(model.calendar,(result)=>{
                if(result.error){
                    Collection.actions.change('error',result.error.message);
                }
                else{
                    if(model.permissions.length>0) {
                        Calendar.addPermissions(model.calendar.id, model.permissions, (results) => {
                            console.log('addPermissions',results);
                            // get result with at least one error
                            let result = _.find(results,(p)=>{
                                return p.error;
                            });
                            if(result!==undefined){
                                Collection.actions.change('error', result.error.message);
                            }
                            else{
                                Collection.actions.change('message', 'Calendar has been updated successfully.');
                                // AFTER SAVING, SET THIS FLAG TO TRUE FOR RE LOADING
                                GoogleCalendars.actions.change('isReloadRequired', true);
                                Collection.actions.change('isProcessing',false);
                                Collection.api.getPermissions(model.calendar);
                            }
                        });
                    }
                    else{
                        Collection.actions.change('message', 'Calendar has been updated successfully.');
                        // AFTER SAVING, SET THIS FLAG TO TRUE FOR RE LOADING
                        GoogleCalendars.actions.change('isReloadRequired', true);
                        Collection.actions.change('isProcessing',false);
                    }
                }
            });
        }
    });
};

Collection.api.getPermissions = function(model){
    let Calendar = GoogleClient.getCalendar();
    Calendar.getPermissions(model.id, (result) => {
        if (result.error) {
            Collection.actions.change('error', result.error.message);
        }
        else {
            let APP = store.getState().APP.toJS();
            let { userProfile, agencyDetails } = APP;
            if(userProfile && agencyDetails){
                // show only permissions belongs to agency users
                // we don't need to show permissions of all users who are not belongs to agency user list
                let agencyUserEmails = _.map(agencyDetails.agencyUsers,(p)=>{
                    return p.email;
                });
                let items = _.filter(result.items,(item)=>{
                    return item.scope.value !== userProfile.email && agencyUserEmails.indexOf(item.scope.value)>-1;
                });
                // display only active permissions in screen
                _.each(items,(p)=>{
                    p.status = 'active';
                });
                if(items.length>0) {
                    Collection.actions.setObjectInPath(['permissions'], items);
                }
                else{
                    Collection.actions.setObjectInPath(['permissions',0],{
                        etag :'',
                        id:'',
                        role :'reader',
                        scope:{
                            type:'user',
                            value :''
                        },
                        status:'active'
                    });
                }
            }
        }
    });
};

export default Collection;
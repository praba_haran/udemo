import _ from 'lodash';
import Immutable from 'immutable';
import uuid from 'uuid';
import store from '../store';
import Alert from '../lib/Alert';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import App from './App';
import dropDownValues from '../components/shared/dropdown-values';

function getDefaultRequirement(index){
    return {
        _clientId :  uuid.v1(),
        name :'Untitled Profile',
        intension :'Buy',
        newBuild:'No',
        minPrice : 0,
        maxPrice : 0,
        rentalFrequency :'Pcm',
        furnishing :'Furnished',
        bedrooms:2,
        bathrooms:2,
        receptions:0,
        parking:0,
        floorArea:0,
        floorAreaUnit :'Sq.Ft',
        landArea:0,
        landAreaUnit:'Sq.Ft',
        features :Immutable.fromJS(dropDownValues.propertyFeatures),
        propertyTypes:Immutable.fromJS(dropDownValues.propertyTypesByCategory),
        areas:Immutable.List([]),
        allAreasSelected : false
    };
}

let defaultRequirement = getDefaultRequirement(0);

let Collection = Factory.getCollection(constants.MODELS.CONTACT_REQUIREMENT, ()=>{
    return {
        requirements:Immutable.List([
            Immutable.Map(defaultRequirement)
        ]),
        matches:Immutable.List([]),
        shortlisted : Immutable.List([]),
        selectedId:defaultRequirement._clientId,
        isProcessing : true
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'requirement/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'requirement/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.delete = function(id,cb){
    ajax({
        method : 'delete',
        url:'requirement/delete/'+id
    }).done(cb);
};

Collection.api.list = function(contactId,cb){
    ajax({
        method : 'get',
        url:'requirement/list/'+contactId
    }).done(cb);
};

Collection.api.matches = function(model,cb){
    ajax({
        method : 'post',
        url:'requirement/matches',
        data : model
    }).done(cb);
};

Collection.api.getDefaultRequirement = function(index){
    return getDefaultRequirement(index);
};

// Collection.api.removeRequirement = function(contactId,model,cb){
//     let CONTACT_REQUIREMENT = store.getState().CONTACT_REQUIREMENT.toJS();
//     let { requirements } = CONTACT_REQUIREMENT;
//
//     return getInstance().delete('contact/'+contactId+'/requirement/remove/'+model._id).then((res)=>{
//         if(res.data.success){
//             //CALLING GET LIST TO LOAD A UPDATED LIST FROM SERVER
//             Collection.api.getRequirements(contactId);
//             if(cb) cb(res.data.data);
//         }
//         return res;
//     });
// };

// Collection.api.addRequirement = function(){
//     let CONTACT_REQUIREMENT = store.getState().CONTACT_REQUIREMENT.toJS();
//     let { requirements } = CONTACT_REQUIREMENT;
//     let requirement = getDefaultRequirement(requirements.length);
//     Collection.actions.setObjectInPath(['requirements', requirements.length], requirement);
//     Collection.actions.change('selectedId',requirement._clientId);
// };

export default Collection;
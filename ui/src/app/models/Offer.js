import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.OFFER,()=>{
    return {
        property:null,
        price :0,
        priceInPercent : 50,
        aboutAnOffer:'',
        chainNotes: '',
        fixtureAndFittingsNotes:'',
        mortgageAndFinancialNotes:'',
        estimatedDates:'',
        accepted : false,
        rejected : false,
        updateApplicantLastContactedDate : false,
        updateVendorLastContactedDate : false,
        applicants : Immutable.List([]),
        status:'Received'
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'offer/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'offer/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.delete = function(id,cb){
    ajax({
        method : 'delete',
        url:'offer/delete/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'offer/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'offer/search',
        data : model
    }).done(cb);
};

export default Collection;
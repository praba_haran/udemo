import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.CABINET_MASTER_LISTING,()=>{
    return {
        items : Immutable.List([]),
        itemsBackup : Immutable.List([]),
        query:{
            searchText : '',
            clientType : 'All'
        },
        allSelected : false,
        isProcessing :false
    };
});

Collection.api.list = function(cb){
    ajax({
        method : 'get',
        url:'cabinet-master/list'
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'cabinet-master/search',
        data : model
    }).done(cb);
};

Collection.api.deleteItems = function(model,cb){
    ajax({
        method : 'post',
        url:'cabinet-master/delete/items',
        data : model
    }).done(cb);
};

export default Collection;
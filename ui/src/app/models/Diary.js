import moment from 'moment';
import Immutable from 'immutable';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import GoogleClient from '../lib/GoogleClient';

let Collection = Factory.getCollection(constants.MODELS.DIARY, ()=>{
    return {
        events : Immutable.List([]),
        selectedCalendarId : null,

        options: Immutable.Map({
            view:'month',
            currentDate : moment(new Date()),
            startDate : null,
            endDate : null,
            changedIn:'FC'
        }),

        error : null,
        isProcessing : false
    };
});


Collection.api.getEvents = function(calendar){
    Collection.actions.change('selectedCalendarId',calendar.id);
    let Calendar = GoogleClient.getCalendar();
    Calendar.getEvents(calendar,(result)=>{
        if(result.items){
            Collection.actions.setObjectInPath(['events'],result.items);
        }
        else{
            Collection.actions.change('error',result.message);
        }
    });
};

export default Collection;
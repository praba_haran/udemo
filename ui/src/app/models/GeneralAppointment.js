import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';
import Helper from '../components/shared/helper';

let Collection = Factory.getCollection(constants.MODELS.GENERAL_APPOINTMENT,()=>{
    return {
        type : 'Miscellaneous',
        appraiser:'',
        accompaniedBy : '',
        date: Helper.getCurrentDate().toISOString(),
        startTime : Helper.getStartTime(),
        endTime : Helper.getEndTime(),
        notes:'',
        additionalNotes : '',
        reminder:'None',
        reminderChannel:'',
        applicants : Immutable.List([]),
        properties : Immutable.List([]),
        status:'Booked'
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'general-appointment/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'general-appointment/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.delete = function(id,cb){
    ajax({
        method : 'delete',
        url:'general-appointment/delete/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'general-appointment/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'general-appointment/search',
        data : model
    }).done(cb);
};

export default Collection;
import _ from 'lodash';
import moment from 'moment';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Commands from './Commands';
import Factory from '../CollectionFactory';
import Helper from '../components/shared/helper';

let Collection = Factory.getCollection(constants.MODELS.MARKET_APPRAISAL,()=>{
    return {
        property:null,
        type: 'Free market appraisal',
        leadSource: '',
        appraiser:'',
        accompaniedBy : '',
        date: Helper.getCurrentDate().toISOString(),
        startTime : Helper.getStartTime(),
        endTime : Helper.getEndTime(),
        notes:'',
        reminder:'None',
        reminderChannel:'',
        status:'Active',
        updateLastContactedDate : false
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'market-appraisal/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'market-appraisal/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.delete = function(id,cb){
    ajax({
        method : 'delete',
        url:'market-appraisal/delete/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'market-appraisal/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'market-appraisal/search/',
        data : model
    }).done(cb);
};

export default Collection;
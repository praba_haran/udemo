import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';
import Helper from '../components/shared/helper';

let Collection = Factory.getCollection(constants.MODELS.VIEWING,()=>{
    return {
        property:null,
        appraiser:'',
        accompaniedBy : '',
        date: Helper.getCurrentDate().toISOString(),
        startTime : Helper.getStartTime(),
        endTime : Helper.getEndTime(),
        isConfirmed : false,
        vendorConfirmed : false,
        applicantsConfirmed : false,
        history : Immutable.List([]),
        notes:'',
        additionalNotes : '',
        reminder:'None',
        reminderChannel:'',
        status:'Booked',
        applicants : Immutable.List([]),
        owners : Immutable.List([]),
        isReScheduled : false,
        sendConfirmationMailToAppraiser : false,
        sendRescheduledMailToAppraiser : false,
        sendCancellationMailToAppraiser : false,
        confirmationEmailSent : false,
        confirmationMailSentAt : null,
        reScheduledEmailSent : false,
        reScheduledEmailSentAt : null,
        cancellationEmailSent : false,
        cancellationEmailSentAt : null
    };
});

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'viewing/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'viewing/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.cancel = function(id,cb){
    ajax({
        method : 'delete',
        url:'viewing/cancel/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'viewing/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'viewing/search',
        data : model
    }).done(cb);
};

Collection.api.sendConfirmationEmail = function(model,cb){
    ajax({
        method : 'post',
        url:'viewing/send-confirmation-email',
        data : model
    }).done(cb);
};

Collection.api.getBusyApplicants = function(model,cb){
    ajax({
        method : 'post',
        url:'viewing/get/busy-applicants',
        data : model
    }).done(cb);
};

export default Collection;
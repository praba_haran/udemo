import _ from 'lodash';
import store from '../store';
import Immutable from 'immutable';
import Alert from '../lib/Alert';
import { getInstance,ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Commands from './Commands';

let Collection = Factory.getCollection(constants.MODELS.USERS,()=>{
    return {
        items:Immutable.List([]),
        isProcessing :false
    };
});

// Collection.api.getUsers = function(cb){
//     Collection.actions.change('isProcessing',true);
//
//     ajax({
//         method : 'get',
//         url:'users',
//     }).done(function( result ) {
//         if(result.success){
//            let data = result.data;
//             _.each(data,function(p){
//                 p.selected = false;
//             });
//             Collection.actions.setObjectInPath(['items'], data);
//             if (cb) cb(data);
//         }
//         else{
//             Alert.clearLogs().error(result.error);
//         }
//         Collection.actions.change('isProcessing', false);
//     });
// };


Collection.api.deleteItems = function(model,cb){
    ajax({
        method : 'post',
        url:'user/delete/items',
        data : model
    }).done(cb);
};

Collection.api.disableItems = function(model,cb){
    ajax({
        method : 'post',
        url:'user/disable/items',
        data : model
    }).done(cb);
};

Collection.api.activateItems = function(model,cb){
    ajax({
        method : 'post',
        url:'user/activate/items',
        data : model
    }).done(cb);
};

export default Collection;
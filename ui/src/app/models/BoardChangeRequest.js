import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/BoardChangeRequest';

let Collection = Factory.getCollection(constants.MODELS.BOARD_CHANGE_REQUEST,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'board-change-request/save',
        data: model
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'board-change-request/get/'+id
    }).done(cb);
};

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'board-change-request/list',
        data : model
    }).done(cb);
};

export default Collection;
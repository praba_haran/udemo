import _ from 'lodash';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

const model = {};
_.each(constants.COMMANDS,function(p){
   model[p] = false;
});
let Collection = Factory.getCollection(constants.MODELS.COMMANDS,()=>{
   return model;
});

export default Collection;
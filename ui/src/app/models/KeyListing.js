import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.KEY_LISTING,()=>{
    return {
        items:Immutable.List([]),
        query:{
            searchText : ''
        },
        allSelected : false,
        isProcessing :false
    };
});

Collection.api.list = function(cb){
    ajax({
        method : 'get',
        url:'key/list'
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'key//search',
        data : model
    }).done(cb);
};

export default Collection;
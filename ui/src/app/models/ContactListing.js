import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.CONTACT_LISTING,()=>{
    return {
        contacts:Immutable.List([]),
        isProcessing :false
    };
});

Collection.api.list = function(cb){
    ajax({
        method : 'get',
        url:'contact/list'
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/search',
        data : model
    }).done(cb);
};

Collection.api.deleteItems = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/delete/items',
        data : model
    }).done(cb);
};

Collection.api.archiveItems = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/archive/items',
        data : model
    }).done(cb);
};

Collection.api.activateItems = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/activate/items',
        data : model
    }).done(cb);
};

export default Collection;
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/Contact';

let Collection = Factory.getCollection(constants.MODELS.CONTACT_DETAIL,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/save',
        data: model
    }).done(cb);
};

Collection.api.update = function(id,model,cb){
    ajax({
        method : 'put',
        url:'contact/update/'+id,
        data: model
    }).done(cb);
};

Collection.api.delete = function(id,cb){
    ajax({
        method : 'delete',
        url:'contact/delete/'+id
    }).done(cb);
};

Collection.api.get = function(id,cb){
    ajax({
        method : 'get',
        url:'contact/get/'+id
    }).done(cb);
};

Collection.api.search = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/search',
        data : model
    }).done(cb);
};


export default Collection;
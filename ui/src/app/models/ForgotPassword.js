import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

let Collection = Factory.getCollection(constants.MODELS.FORGOT_PASSWORD, ()=>{
    return {
        email : '',
        error : null,
        success : null,
        isProcessing : false
    };
});

Collection.api.sendInstructions = function(model,cb){
    ajax({
        method : 'post',
        url:'forgot-password/send-instructions',
        data: model
    }).done(cb);
};

export default Collection;
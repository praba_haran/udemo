import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.DASHBOARD,()=>{
    return {
        summary:Immutable.Map({
            items : Immutable.List([]),
            type : 'property'
        }),
        propertySummary : Immutable.List([]),

        contactSummary : Immutable.List([]),
        branchPerformance : Immutable.Map({
            properties : Immutable.List([]),
            contacts : Immutable.List([])
        }),
        branchActivity : Immutable.List([]),
        userActivity : Immutable.List([])
    };
});

Collection.api.getPropertySummary = function(cb){
    ajax({
        method : 'get',
        url:'dashboard/property-summary'
    }).done(cb);
};

Collection.api.getContactSummary = function(cb){
    ajax({
        method : 'get',
        url:'dashboard/contact-summary'
    }).done(cb);
};

Collection.api.getBranchPerformance = function(cb){
    ajax({
        method : 'get',
        url:'dashboard/branch-performance'
    }).done(cb);
};

Collection.api.getBranchActivity = function(type,cb){
    ajax({
        method : 'get',
        url:'dashboard/branch-activity/'+type
    }).done(cb);
};

Collection.api.getUserActivity = function(type,cb){
    ajax({
        method : 'get',
        url:'dashboard/user-activity/'+type
    }).done(cb);
};

Collection.api.getViewings = function(cb){
    ajax({
        method : 'get',
        url:'dashboard/viewing/search'
    }).done(cb);
};

Collection.api.getAppointments = function(cb){
    ajax({
        method : 'get',
        url:'dashboard/appointments/search'
    }).done(cb);
};

export default Collection;
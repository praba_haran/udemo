import localStorage from 'localStorage';
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';

let Collection = Factory.getCollection(constants.MODELS.LOGIN, ()=>{
    return {
        email : '',
        password : '',
        error : null,
        isProcessing : false,
        isLoggingInWithGoogle : false
    };
});

Collection.api.login = function(model,cb){
    Collection.actions.change('error',null);
    Collection.actions.change('isProcessing',true);
    ajax({
        method : 'post',
        url:'authenticate',
        data: model
    }).done(function( result ) {
        if(result.success){
            Collection.actions.change('isProcessing',false);
            localStorage.setItem('x-access-token',result.data.token);
            cb(result.data);
        }
        else{
            Collection.actions.change('isProcessing',false);
            Collection.actions.change('error',result.error);
        }
    }).fail(function(jqXHR){
        Collection.actions.change('isProcessing',false);
    });
};

Collection.api.isLoggedIn = function(){
    return localStorage.getItem('x-access-token')!==null;
};

Collection.api.logUserLogin = function(model,cb){
    Collection.actions.change('error',null);
    Collection.actions.change('isLoggingInWithGoogle',true);

    ajax({
        method : 'post',
        url:'account/google/authenticate',
        data: model
    }).done(function( result ) {
        if(result.success){
            localStorage.setItem('x-access-token',result.data.token);
            cb();
        }
        else{
            console.log('result',result);
            //Collection.actions.change('error',result.data.error);
        }
        Collection.actions.change('isLoggingInWithGoogle',false);
    }).fail(function(jqXHR){
        console.log('jqXHR',jqXHR);
        Collection.actions.change('isLoggingInWithGoogle',false);
    });
};

export default Collection;
import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Schema from './schemas/Contact';

let Collection = Factory.getCollection(constants.MODELS.CONTACT,Schema);

Collection.api.save = function(model,cb){
    ajax({
        method : 'post',
        url:'contact/save',
        data: model
    }).done(cb);
};

Collection.api.searchAddress = function(postcode,cb){
    ajax({
        method : 'get',
        url:'address/search/'+postcode
    }).done(cb);
};

export default Collection;
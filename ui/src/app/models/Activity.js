import { ajax } from '../api/axios';
import * as constants from '../constants';
import Factory from '../CollectionFactory';
import Immutable from 'immutable';

let Collection = Factory.getCollection(constants.MODELS.ACTIVITY,()=>{
    return {
        items:Immutable.List([]),
        isProcessing :false
    };
});

Collection.api.list = function(model,cb){
    ajax({
        method : 'post',
        url:'activity/list',
        data: model
    }).done(cb);
};

export default Collection;
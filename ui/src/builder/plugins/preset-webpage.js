grapesjs.plugins.add('gjs-preset-webpage', (editor, opts) => {
    var opt = opts || {};
    var config = editor.getConfig();

    config.showDevices = 0;

    var updateTooltip = function(coll, pos) {
        coll.each(function(item) {
            var attrs = item.get('attributes');
            attrs['data-tooltip-pos'] = pos || 'bottom';
            item.set('attributes', attrs);
        });
    }


    /****************** COMMANDS *************************/

    var cmdm = editor.Commands;
    cmdm.add('undo', {
        run: function(editor, sender) {
            sender.set('active', 0);
            editor.UndoManager.undo(1);
        }
    });
    cmdm.add('redo', {
        run: function(editor, sender) {
            sender.set('active', 0);
            editor.UndoManager.redo(1);
        }
    });
    cmdm.add('set-device-desktop', {
        run: function(editor) {
            editor.setDevice('Desktop');
        }
    });
    cmdm.add('set-device-tablet', {
        run: function(editor) {
            editor.setDevice('Tablet');
        }
    });
    cmdm.add('set-device-mobile', {
        run: function(editor) {
            editor.setDevice('Mobile portrait');
        }
    });
    cmdm.add('clean-all', {
        run: function(editor, sender) {
            sender && sender.set('active',false);
            if(confirm('Are you sure to clean the canvas?')){
                var comps = editor.DomComponents.clear();
                localStorage.setItem('gjs-css', '');
                localStorage.setItem('gjs-html', '');
            }
        }
    });
    cmdm.add('export-pdf',{
       run : function(editor,sender){
           //console.log('create pdf');
           var doc = new jsPDF()

           //doc.text('Hello world!', 10, 10)
           var specialElementHandlers = {
               '#editor': function(element, renderer){
                   return true;
               }
           };
           console.log(editor.getHtml());
           doc.fromHTML('<div class="blk-row" style="box-sizing: border-box; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; background-color: rgb(123, 239, 201);"><div class="blk1" style="box-sizing: border-box; width: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; min-height: 75px; float: right;"> <div class="c875" style="box-sizing: border-box; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; display: block; float: none; color: rgb(219, 35, 35); font-weight: 700;">$ 45,000,00</div> <div class="blk-row" style="box-sizing: border-box; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; background-color: rgb(123, 239, 201);"> <div class="blk2" style="box-sizing: border-box; float: left; width: 50%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; min-height: 75px;"><iframe class="c1484" frameborder="0" src="https://maps.google.com/maps?&z=1&t=q&output=embed" style="box-sizing: border-box; height: 350px;"></iframe></div><div class="blk2" style="box-sizing: border-box; float: left; width: 50%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; min-height: 75px;"></div></div></div></div><div class="c927" style="box-sizing: border-box; width: 50px; min-height: 50px; margin: 0 auto;"></div>', 15, 15, {
               'width': 170,
           });
           doc.save('a4.pdf');
       }
    });

    /****************** BLOCKS *************************/

    var bm = editor.BlockManager;
    bm.add('link-block', {
        label: 'Link Block',
        attributes: {class:'fa fa-link'},
        content: {
            type:'link',
            editable: false,
            droppable: true,
            style:{
                display: 'inline-block',
                padding: '5px',
                'min-height': '50px',
                'min-width': '50px'
            }
        },
    });

    /****************** BUTTONS *************************/

    var pnm = editor.Panels;
    pnm.addButton('options', [{
        id: 'undo',
        className: 'fa fa-undo icon-undo',
        command: 'undo',
        attributes: { title: 'Undo (CTRL/CMD + Z)'}
    },{
        id: 'redo',
        className: 'fa fa-repeat icon-redo',
        command: 'redo',
        attributes: { title: 'Redo (CTRL/CMD + SHIFT + Z)' }
    },{
        id: 'clean-all',
        className: 'fa fa-trash icon-blank',
        command: 'clean-all',
        attributes: { title: 'Empty canvas' }
    },{
        id: 'export-pdf',
        className: 'fa fa-file-pdf-o',
        command: 'export-pdf',
        attributes: { title: 'Export to PDF' }
    }]);

    // Add devices buttons
    var panelDevices = pnm.addPanel({id: 'devices-c'});
    var deviceBtns = panelDevices.get('buttons');
    deviceBtns.add([{
        id: 'deviceDesktop',
        command: 'set-device-desktop',
        className: 'fa fa-desktop',
        attributes: {'title': 'Desktop'},
        active: 1,
    },{
        id: 'deviceTablet',
        command: 'set-device-tablet',
        className: 'fa fa-tablet',
        attributes: {'title': 'Tablet'},
    },{
        id: 'deviceMobile',
        command: 'set-device-mobile',
        className: 'fa fa-mobile',
        attributes: {'title': 'Mobile'},
    }]);
    updateTooltip(deviceBtns);
    updateTooltip(pnm.getPanel('options').get('buttons'));
    updateTooltip(pnm.getPanel('options').get('buttons'));
    updateTooltip(pnm.getPanel('views').get('buttons'));



    /****************** EVENTS *************************/

    // On component change show the Style Manager
    editor.on('change:selectedComponent', function() {
        var openLayersBtn = editor.Panels.getButton('views', 'open-layers');

        // Don't switch when the Layer Manager is on or
        // there is no selected component
        if((!openLayersBtn || !openLayersBtn.get('active')) &&
            editor.editor.get('selectedComponent')) {
            var openSmBtn = editor.Panels.getButton('views', 'open-sm');
            openSmBtn && openSmBtn.set('active', 1);
        }
    });

    // Do stuff on load
    editor.on('load', function() {
        // Open block manager
        var openBlocksBtn = editor.Panels.getButton('views', 'open-blocks');
        openBlocksBtn && openBlocksBtn.set('active', 1);
    });

});
var fs = require('fs');
var del = require('del');
var gulp = require('gulp');
var sass = require("gulp-sass");
var less = require('gulp-less');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var stylelint = require('stylelint');
var minify = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var autoPrefixer = require('gulp-autoprefixer');
var reporter    = require('postcss-reporter');
var postcss     = require('gulp-postcss');
var syntax_scss = require('postcss-scss');
var sourcemaps = require('gulp-sourcemaps');
var webpack = require('webpack-stream');
var webpackConfig = require('./webpack.production.config');

var autoPrefixArray = [
    "Android 2.3",
    "Android >= 4",
    "Chrome >= 20",
    "Firefox >= 24",
    "Explorer >= 8",
    "iOS >= 6",
    "Opera >= 12",
    "Safari >= 6"
];

var styleLintConfig = JSON.parse(fs.readFileSync('scss-lint-config.json'));
var styleLintProcessors = [
    stylelint(styleLintConfig),
    reporter({
        clearMessages: true,
        throwError: true
    })
];

// Build Theme Start
// Run "gulp build:skin" command only once. No need to generate this files frequently
gulp.task('build:skin',function () {
    return gulp.src('./base-theme/base/skins/*.less')
        .pipe(plumber())
        .pipe(less({
            strictMath: false,
            paths: [
                './base-theme/base/less',
                './base-theme/global/less',
                './base-theme/global/less/bootstrap',
                './base-theme/global/less/mixins'
            ]
        }))
        .pipe(autoPrefixer(autoPrefixArray))
        .pipe(minify({
            compatibility: 'ie8',
            keepSpecialComments: '*',
            advanced: false
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./dist/css'));
});
gulp.task('clean:bootstrap', function () {
    return del([
        './dist/css/bootstrap.min.css'
    ]);
});
gulp.task('build:bootstrap',['clean:bootstrap'], function () {
    return gulp.src('./base-theme/global/less/bootstrap.less')
        .pipe(plumber())
        .pipe(less({
            strictMath: false,
            paths: [
                './base-theme/global/less',
                './base-theme/global/less/bootstrap',
                './base-theme/global/less/mixins'
            ]
        }))
        .pipe(autoPrefixer(autoPrefixArray))
        .pipe(minify({
            compatibility: 'ie8',
            keepSpecialComments: '*',
            advanced: false
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./dist/css'));
});
gulp.task('clean:bootstrap-extend', function () {
    return del([
        './dist/css/bootstrap-extend.min.css'
    ]);
});
gulp.task('build:bootstrap-extend',['clean:bootstrap-extend'], function () {
    return gulp.src('./base-theme/global/less/bootstrap-extend.less')
        .pipe(plumber())
        .pipe(less({
            strictMath: false,
            paths: [
                './base-theme/global/less',
                './base-theme/global/less/bootstrap',
                './base-theme/global/less/mixins'
            ]
        }))
        .pipe(autoPrefixer(autoPrefixArray))
        .pipe(minify({
            compatibility: 'ie8',
            keepSpecialComments: '*',
            advanced: false
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./dist/css'));
});
// Build Theme End

// Vendor Scripts and Css Start
gulp.task('clean:vendor-css',function () {
    return del([
        './dist/css/vendor.min.css'
    ]);
});
gulp.task('build:vendor-css',['clean:vendor-css'],function(){
    return gulp.src([
        './base-theme/vendor/fullcalendar/fullcalendar.css',
        './base-theme/vendor/animsition/animsition.css',
        //'./base-theme/vendor/asscrollable/asScrollable.css', //commented on 21-Dec-2016
        './base-theme/vendor/switchery/switchery.css',
        './base-theme/vendor/intro-js/introjs.css',
        './base-theme/vendor/slidepanel/slidePanel.css',
        './base-theme/vendor/flag-icon-css/flag-icon.css',
        './base-theme/vendor/waves/waves.css',
        './base-theme/vendor/bootstrap-touchspin/bootstrap-touchspin.css',
        './base-theme/vendor/alertify-js/alertify.css',

        './node_modules/rc-calendar/assets/index.css',
        './node_modules/rc-slider/dist/rc-slider.css',
        './node_modules/react-image-gallery/styles/css/image-gallery.css'

        //'./base-theme/site/css/page/login-v3.css'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.min.css'))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('./dist/css'));
});
gulp.task('clean:vendor-js',function () {
    return del([
        './dist/css/vendor.min.js'
    ]);
});
gulp.task('build:vendor-js',['clean:vendor-js'],function(){
    return gulp.src([
        './base-theme/vendor/modernizr/modernizr.js',
        './base-theme/vendor/breakpoints/breakpoints.js',
        './base-theme/vendor/jquery/jquery.js',
        './base-theme/vendor/bootstrap/bootstrap.js',
        './base-theme/vendor/animsition/animsition.js',
        './base-theme/vendor/asscroll/jquery-asScroll.js',
        './base-theme/vendor/mousewheel/jquery.mousewheel.js',
        //'./base-theme/vendor/asscrollable/jquery.asScrollable.all.js',
        //'./base-theme/vendor/ashoverscroll/jquery-asHoverScroll.js', //commented
        './base-theme/vendor/waves/waves.js',
        './base-theme/vendor/moment/moment.min.js',
        './base-theme/vendor/fullcalendar/fullcalendar.js',

        './base-theme/vendor/switchery/switchery.min.js',
        './base-theme/vendor/intro-js/intro.js',
        './base-theme/vendor/screenfull/screenfull.js',
        './base-theme/vendor/slidepanel/jquery-slidePanel.js',
        './base-theme/vendor/bootbox/bootbox.js',
        './base-theme/vendor/alertify-js/alertify.js',

        './base-theme/js/core.js',
        './base-theme/js/site.js',
        './base-theme/js/sections/menu.js',
        './base-theme/js/sections/menubar.js',
        './base-theme/js/sections/sidebar.js',

        './base-theme/js/components/animsition.js',
        './base-theme/js/components/material.js',
        './src/app/lib/cascade-slider.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('watch:vendor-js',function () {
    var watcher = gulp.watch([
        './base-theme/vendor/modernizr/modernizr.js',
        //'./base-theme/vendor/breakpoints/breakpoints.js',
        './base-theme/vendor/jquery/jquery.js',
        './base-theme/vendor/bootstrap/bootstrap.js',
        './base-theme/vendor/animsition/animsition.js',
        './base-theme/vendor/asscroll/jquery-asScroll.js',
        './base-theme/vendor/mousewheel/jquery.mousewheel.js',
        './base-theme/vendor/waves/waves.js',
        './base-theme/vendor/moment/moment.min.js',
        './base-theme/vendor/fullcalendar/fullcalendar.js',

        './base-theme/vendor/switchery/switchery.min.js',
        './base-theme/vendor/intro-js/intro.js',
        './base-theme/vendor/screenfull/screenfull.js',
        './base-theme/vendor/slidepanel/jquery-slidePanel.js',
        './base-theme/vendor/bootbox/bootbox.js',
        './base-theme/vendor/alertify-js/alertify.js',

        './base-theme/js/core.js',
        './base-theme/js/site.js',
        './base-theme/js/sections/menu.js',
        './base-theme/js/sections/menubar.js',
        './base-theme/js/sections/sidebar.js',

        './base-theme/js/components/animsition.js',
        './base-theme/js/components/material.js',

        './src/app/lib/cascade-slider.js'
    ], ['build:vendor-js']);

    watcher.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('build:vendor',['build:vendor-css','build:vendor-js']);
// Vendor Scripts and Css End

// Custom Script Start
gulp.task('clean:scss', function () {
    return del([
        './dist/css/ultima.css'
    ]);
});

gulp.task('watch:scss',function () {
    var watcher = gulp.watch([
        './src/scss/*.scss',
        './src/scss/**/*.scss',
        './src/scss/**/**/*.scss'
    ], ['build:scss']);

    watcher.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('build:scss',['clean:scss'], function () {
    return gulp.src('./src/scss/ultima.scss')
        .pipe(postcss(styleLintProcessors, {syntax: syntax_scss}))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('clean:merge-css', function () {
    return del([
        './dist/css/integrated.min.css'
    ]);
});

gulp.task('build:merge-css',['clean:merge-css'], function () {

    return gulp.src([
        './dist/css/bootstrap.min.css',
        './dist/css/bootstrap-extend.min.css',
        './base-theme/site/site.css',
        './dist/css/vendor.min.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('integrated.min.css'))
        .pipe(minify())
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('./dist/css'));
});
// Custom Script End

gulp.task('clean:jsx', function() {
    del('./dist/css/bundle.js');
});


gulp.task('build:jsx', ['clean:jsx'], function() {
    return gulp.src('./src/app/index.js')
        .pipe(webpack(webpackConfig))
        .on('error', function handleError() {
            this.emit('end'); // Recover from errors
        })
        .pipe(gulp.dest('./'));
});
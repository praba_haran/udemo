var webpack = require('webpack');

var config = {
    devTool:'cheap-module-source-map',
    entry:[
        'babel-polyfill',
        './src/app/index.js'
    ],

    output: {
        filename: "./dist/js/bundle.js",
        sourceMapFilename: "./dist/js/bundle.map"
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                //exclude: /node_modules/,
                include : /src/,
                loader: 'babel',

                query: {
                    presets: ['es2015','stage-0', 'react']
                }
            }
        ]
    },
    plugins:[
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        })
    ]
};

module.exports = config;
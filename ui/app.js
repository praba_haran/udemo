process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var cacheOneDay = 86400000 * 30;

var express = require('express');
var compress = require('compression');

var app = express();
app.use(compress());
//app.use('/dist', express.static(__dirname+'/dist',{ maxAge: cacheOneDay }));
//app.use('/base-theme', express.static(__dirname+'/base-theme',{ maxAge: cacheOneDay }));
app.use('/static', express.static(__dirname+'/node_modules'));
app.use('/static/builder', express.static(__dirname+'/src/builder'));
app.use('/dist', express.static(__dirname+'/dist'));
app.use('/base-theme', express.static(__dirname+'/base-theme'));
app.use('/',function(req,res){

    //res.setHeader("Cache-Control", "public, max-age=345600");// 4 days
    res.sendfile('index.html');
});

app.use('/builder',function(req,res){
    //res.setHeader("Cache-Control", "public, max-age=345600");// 4 days
    res.sendfile('views/builder.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: err
    });
});


module.exports = app;
